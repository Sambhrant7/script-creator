import operator, random
import time
import datetime


"""* -----------@@ DEPRICATED AFTER CTIT IMPLEMENTATIONS-------
	>>>>FUNCTION TO CALCULATE TIME SLEEPS BETWEEN VARIABLES<<<<
*"""
def calculate_time_differences(trackings, ts):
	tz = ""
	event_diff = ""
	session_diff = ""
	ref_diff = ""
	ins_diff = ""
	info_diff = ""
	attr_diff = ""
	click_diff = ""
	iad_diff = ""
	deep_diff = ""
	clicktime = False
	click_time_deeplink = False

	def def_sec(timez):
		sec = (abs(int(timez)) / 100) * 3600 + (abs(int(timez)) % 100) * 60
		if int(timez) < 0:
			sec = (-1) * sec
		return sec

	calling_dict = {}
	time_stamps = []
	time_function_list = []
	time_function_list.append("\n	################generating_realtime_differences##################\n")

	#####################################################################################################
	if "Appsflyer" in trackings:

		timedict = ts.get('Appsflyer')
		if '+' in str(timedict.get('firstLaunchDate')):
			fld = str(timedict.get('firstLaunchDate')).split('+')[0]
			tz = str(timedict.get('firstLaunchDate')).split('+')[1]
		else:
			fld = str(timedict.get('firstLaunchDate')).split('-')[0]
			tz = str(timedict.get('firstLaunchDate')).split('-')[1]

		if len(fld) == 15:
			firstlaunchDatets = int(
				time.mktime(datetime.datetime.strptime(fld, "%Y-%m-%d_%H%M").timetuple())) + def_sec('+0530')
		else:
			firstlaunchDatets = int(
				time.mktime(datetime.datetime.strptime(fld, "%Y-%m-%d_%H%M%S").timetuple())) + def_sec('+0530')

		calling_dict[str(
			int(firstlaunchDatets) * 1000) + ":def_firstLaunchDate"] = "	def_firstLaunchDate(app_data,device_data)\n"
		time_stamps.append(str(int(firstlaunchDatets) * 1000) + ":def_firstLaunchDate")


	#####################################################################################################
	if "Adjust" in trackings:
		timedict_adJ = ts.get('Adjust')
		if '+' in str(timedict_adJ.get('session_created_at')):
			saperator = '+'
		else:
			saperator = '-'
		if not tz == "0000":
			tz = str(timedict_adJ.get('session_created_at')).split(saperator)[1]

		def get_ts(date):
			return int(time.mktime(datetime.datetime.strptime(date.strip('Z'), "%Y-%m-%dT%H:%M:%S.%f").timetuple()))

		sca = str(timedict_adJ.get('session_created_at')).split(saperator)[0]
		ssa = str(timedict_adJ.get('session_sent_at')).split(saperator)[0]
		delta = (int(get_ts(ssa))) - (int(get_ts(sca)))
		if delta == 0:
			session_diff = "	s1=0\n	s2=0\n"
		else:
			if delta < 5:
				a, b = str(delta), str(delta + random.randint(1, 2))
			else:
				a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

			session_diff = "	s1=" + a + "\n	s2=" + b + "\n"
		time_function_list.append(session_diff)
		session_created_at = get_ts(sca)
		session_sent_at = get_ts(ssa)
		calling_dict[str(int(
			session_created_at) * 1000) + ":created_at_session"] = "	created_at_session=get_date(app_data,device_data)\n"
		calling_dict[
			str(int(session_sent_at) * 1000) + ":sent_at_session"] = "	sent_at_session=get_date(app_data,device_data)\n"
		# time_stamps.append(str(int(session_created_at)*1000)+":created_at_session")
		# time_stamps.append(str(int(session_sent_at)*1000)+":sent_at_session")
		if timedict_adJ.get('attribution_created_at'):
			aca = str(timedict_adJ.get('attribution_created_at')).split(saperator)[0]
			asa = str(timedict_adJ.get('attribution_sent_at')).split(saperator)[0]
			delta = (int(get_ts(asa))) - (int(get_ts(aca)))
			if delta == 0:
				attr_diff = "	a1=0\n	a2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				attr_diff = "	a1=" + a + "\n	a2=" + b + "\n"
			time_function_list.append(attr_diff)
			calling_dict[str(int(get_ts(
				aca)) * 1000) + ":created_at_attribution"] = "	created_at_attribution=get_date(app_data,device_data)\n"
			calling_dict[str(int(
				get_ts(asa)) * 1000) + ":sent_at_attribution"] = "	sent_at_attribution=get_date(app_data,device_data)\n"
		# time_stamps.append(str(int(get_ts(aca))*1000)+":created_at_attribution")
		# time_stamps.append(str(int(get_ts(asa))*1000)+":sent_at_attribution")

		if timedict_adJ.get('reftag_sdk_click_created_at'):
			rsca = str(timedict_adJ.get('reftag_sdk_click_created_at')).split(saperator)[0]
			rscsa = str(timedict_adJ.get('reftag_sdk_click_sent_at')).split(saperator)[0]
			delta = (int(get_ts(rscsa))) - (int(get_ts(rsca)))
			if delta == 0:
				ref_diff = "	r1=0\n	r2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				ref_diff = "	r1=" + a + "\n	r2=" + b + "\n"
			time_function_list.append(ref_diff)
			calling_dict[str(
				int(get_ts(rsca)) * 1000) + ":created_at_sdk1"] = "	created_at_sdk1=get_date(app_data,device_data)\n"
			calling_dict[
				str(int(get_ts(rscsa)) * 1000) + ":sent_at_sdk1"] = "	sent_at_sdk1=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(rsca))*1000)+":created_at_sdk1")
			# time_stamps.append(str(int(get_ts(rscsa))*1000)+":sent_at_sdk1")
			if timedict_adJ.get('reftag_click_time'):
				clicktime = True
				val = "	click_time1=" + str(clicktime) + "\n"
				time_function_list.append(val)
				rscct = str(timedict_adJ.get('reftag_click_time')).split(saperator)[0]
				delta = (int(get_ts(rsca))) - (int(get_ts(rscct)))
				if delta == 0:
					click_diff = "	c1=0\n	c2=0\n"
				else:
					if delta < 5:
						a, b = str(delta), str(delta + random.randint(1, 2))
					else:
						a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

					click_diff = "	c1=" + a + "\n	c2=" + b + "\n"
				time_function_list.append(click_diff)
				calling_dict[
					str(int(get_ts(rscct)) * 1000) + ":click_time1"] = "	click_time1=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(rscct))*1000)+":click_time1")
			else:
				time_function_list.append('	click_time1=None\n')

		if timedict_adJ.get('install_referrer_sdk_click_created_at'):
			irscca = str(timedict_adJ.get('install_referrer_sdk_click_created_at')).split(saperator)[0]
			irscsa = str(timedict_adJ.get('install_referrer_sdk_click_sent_at')).split(saperator)[0]
			delta = (int(get_ts(irscsa))) - (int(get_ts(irscca)))
			if delta == 0:
				ins_diff = "	ir1=0\n	ir2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				ins_diff = "	ir1=" + a + "\n	ir2=" + b + "\n"
			time_function_list.append(ins_diff)
			calling_dict[str(
				int(get_ts(irscca)) * 1000) + ":created_at_sdk2"] = "	created_at_sdk2=get_date(app_data,device_data)\n"
			calling_dict[
				str(int(get_ts(irscsa)) * 1000) + ":sent_at_sdk2"] = "	sent_at_sdk2=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(irscca))*1000)+":created_at_sdk2")
			# time_stamps.append(str(int(get_ts(irscsa))*1000)+":sent_at_sdk2")
			if timedict_adJ.get('install_referrer_click_time'):
				clicktime = True
				val = "	click_time2=" + str(clicktime) + "\n"
				time_function_list.append(val)

				irscct = str(timedict_adJ.get('install_referrer_click_time')).split(saperator)[0]
				calling_dict[
					str(int(get_ts(irscct)) * 1000) + ":click_time2"] = "	click_time2=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(irscct))*1000)+":click_time2")
			else:
				time_function_list.append('	click_time2=None\n')

		if timedict_adJ.get('iad3_sdk_click_created_at'):
			iadscca = str(timedict_adJ.get('iad3_sdk_click_created_at')).split(saperator)[0]
			iadscsa = str(timedict_adJ.get('iad3_sdk_click_sent_at')).split(saperator)[0]
			delta = (int(get_ts(iadscsa))) - (int(get_ts(iadscca)))
			if delta == 0:
				iad_diff = "	id1=0\n	id2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				iad_diff = "	id1=" + a + "\n	id2=" + b + "\n"
			time_function_list.append(iad_diff)
			calling_dict[str(int(get_ts(
				iadscca)) * 1000) + ":created_at_sdk_iad3"] = "	created_at_sdk_iad3=get_date(app_data,device_data)\n"
			calling_dict[str(int(
				get_ts(iadscsa)) * 1000) + ":sent_at_sdk_iad3"] = "	sent_at_sdk_iad3=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(iadscca))*1000)+":created_at_sdk_iad3")
			# time_stamps.append(str(int(get_ts(iadscsa))*1000)+":sent_at_sdk_iad3")
			if timedict_adJ.get('iad3_click_time'):
				irscct = str(timedict_adJ.get('iad3_click_time')).split(saperator)[0]

				calling_dict[str(int(
					get_ts(iadscct)) * 1000) + ":click_time_iad3"] = "	click_time_iad3=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(iadscct))*1000)+":click_time_iad3")
			else:
				time_function_list.append('	click_time_iad3=False\n')

		if timedict_adJ.get('deeplink_sdk_click_created_at'):
			dscca = str(timedict_adJ.get('deeplink_sdk_click_created_at')).split(saperator)[0]
			dscsa = str(timedict_adJ.get('deeplink_sdk_click_sent_at')).split(saperator)[0]
			delta = (int(get_ts(dscsa))) - (int(get_ts(dscca)))
			if delta == 0:
				deep_diff = "	dp1=0\n	dp2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				deep_diff = "	dp1=" + a + "\n	dp2=" + b + "\n"
			time_function_list.append(deep_diff)
			calling_dict[str(int(get_ts(
				dscca)) * 1000) + ":created_at_sdk_deeplink"] = "	created_at_sdk_deeplink=get_date(app_data,device_data)\n"
			calling_dict[str(int(get_ts(
				dscsa)) * 1000) + ":sent_at_sdk_deeplink"] = "	sent_at_sdk_deeplink=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(dscca))*1000)+":created_at_sdk_deeplink")
			# time_stamps.append(str(int(get_ts(dscsa))*1000)+":sent_at_sdk_deeplink")
			if timedict_adJ.get('deeplink_click_time'):
				click_time_deeplink = True
				val = "	click_time_deeplink=" + str(click_time_deeplink) + "\n"
				time_function_list.append(val)
				dscct = str(timedict_adJ.get('deeplink_click_time')).split(saperator)[0]

				calling_dict[str(int(get_ts(
					dscct)) * 1000) + ":click_time_deeplink"] = "	click_time_deeplink=get_date(app_data,device_data)\n"
			# time_stamps.append(str(int(get_ts(dscct))*1000)+":click_time_deeplink")
			else:
				time_function_list.append('	click_time_deeplink=False\n')

		if timedict_adJ.get('sdk_info_created_at'):
			rsca = str(timedict_adJ.get('sdk_info_created_at')).split(saperator)[0]
			rscsa = str(timedict_adJ.get('sdk_info_sent_at')).split(saperator)[0]
			delta = (int(get_ts(rscsa))) - (int(get_ts(rsca)))
			if delta == 0:
				info_diff = "	in1=0\n	in2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(1, 3)), str(delta + random.randint(1, 3))

				info_diff = "	in1=" + a + "\n	in2=" + b + "\n"
			time_function_list.append(info_diff)
			calling_dict[str(
				int(get_ts(rsca)) * 1000) + ":created_at_info"] = "	created_at_info=get_date(app_data,device_data)\n"
			calling_dict[
				str(int(get_ts(rscsa)) * 1000) + ":sent_at_info"] = "	sent_at_info=get_date(app_data,device_data)\n"
		# time_stamps.append(str(int(get_ts(rsca))*1000)+":created_at_info")
		# time_stamps.append(str(int(get_ts(rscsa))*1000)+":sent_at_info")

		if timedict_adJ.get('event_created_at'):
			eca = str(timedict_adJ.get('event_created_at')).split(saperator)[0]
			esa = str(timedict_adJ.get('event_sent_at')).split(saperator)[0]
			delta = (int(get_ts(esa))) - (int(get_ts(eca)))
			if delta == 0:
				event_diff = "	e1=0\n	e2=0\n"
			else:
				if delta < 5:
					a, b = str(delta), str(delta + random.randint(1, 2))
				else:
					a, b = str(delta - random.randint(2, 4)), str(delta + random.randint(2, 4))

				event_diff = "	e1=" + a + "\n	e2=" + b + "\n"


	#####################################################################################################
	if "Adbrix" in trackings:
		adbrx_timedict = ts.get('Adbrix')
		if adbrx_timedict.get('adbrix_app_first_open_time'):
			app_first_open = str(adbrx_timedict.get('adbrix_app_first_open_time'))
			calling_dict[str(int(
				app_first_open) * 1000) + ":adbrix_app_first_open_time"] = "	app_data['app_first_open_time'] = int(time.time())\n"
			time_stamps.append(str(int(app_first_open) * 1000) + ":adbrix_app_first_open_time")


	#####################################################################################################
	"""* 
		>>>>SORT ALL TIMESTAMPS FROM LOGS IN ASCENDING ORDER<<<<
	*"""
	times = sorted(time_stamps)
	"""* 
		>>>>EXTRACT ONLY TIMESTAMP FROM COMBINATION OF TS AND STRING<<<<
	*"""
	t = [int(str(x).split(':')[0]) for x in times]
	"""* 
		>>>>PROCESS AND CREATE A LIST OF DIFFERENCES BETWEEN LIST ITEMS IN "t" <<<<
	*"""
	sleeps = map(operator.sub, t[1:], t[:-1])
	"""* 
		>>>>ITERATING THROUGH RANGE OF ALL THE TIME SLEEPS REQUIRED<<<<
	*"""
	for x in range(len(times)):
		time_function_list.append(calling_dict.get(times[x]))
		if x < len(times) - 1:
			################### IF DIFFERENCE IS 0 ####################
			if str(sleeps[x]) == "0":
				time_function_list.append("")

			elif str(times[x]).split(':')[0][-3:] == "000":
				if sleeps[x] / 1000 > 100:
					a, b = '60', '100'
					time_function_list.append("	time.sleep(random.randint(" + a + "," + b + "))\n")
				else:
					if sleeps[x] / 1000 < 10:
						a, b = str(sleeps[x] / 1000), str((sleeps[x] / 1000) + random.randint(1, 2))
						time_function_list.append("	time.sleep(random.randint(" + a + "," + b + "))\n")
					else:
						a, b = str((sleeps[x] / 1000) - random.randint(5, 8)), str(
							(sleeps[x] / 1000) + random.randint(5, 8))
						time_function_list.append("	time.sleep(random.randint(" + a + "," + b + "))\n")
			################# IF DIFFERENCE IS NOT 0 ##################
			else:
				a, b = str(sleeps[x] - 100), str(sleeps[x] + 100)
				time_function_list.append("	time.sleep(random.randint(" + a + "," + b + "))\n")
	
	if ts.get('Adjust'):
		if ts.get('Adjust').get('event_created_at'):
			time_function_list.append(event_diff)

	return time_function_list
