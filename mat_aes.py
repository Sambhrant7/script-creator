from Crypto.Cipher import AES
import urlparse

global mat_decrypted_data, counter
mat_decrypted_data = []
counter = 0

"""* 
	>>>>AES CBC CLASS TO DECRYPT MAT ENCRYPTED DATA<<<<
*"""
class AESCipher:
 def __init__(self, key):
		self.key = key

 def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw


 def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
		enc = enc.decode('hex')#base64.b64decode(enc)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)
		

"""* 
	>>>>FUNCTION THAT DECRYPTS MAT ENCRYPTED DATA AND RETURNS A DICTIONARY<<<<
*"""
def decrypt_mat_data(key, data):
	globals()['counter']  += 1 
	iv = 'heF9BATUfWuISyO8'
	aes = AESCipher(key)
	b = aes.decrypt(data,iv)

	a_1= urlparse.parse_qs(b)

	b_1=sorted(a_1.keys())
	returnValue={}
	for key in b_1:
		returnValue[key]=a_1[key][0].strip()
	mat_decrypted_data.append("Data for call Number:- "+str(globals()['counter'])+"\n\n")
	mat_decrypted_data.append(str(returnValue))
	return returnValue