# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals, absolute_import
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os
import platform
import random
import re
import subprocess
import sys
import time
import traceback
import webbrowser

"""* 
	>>>>FILE IMPORTS<<<<
*"""
from match_data import report
from read_logs import beautifulSoup_import_error
from writer import writting_robo


"""* 
	>>>>PYSIDE IMPORT<<<<
*"""
pySide_import_error = None
try:
	from PySide import QtGui, QtCore
except ImportError:
	try:
		pySide_import_error = "PySide was Not Found."
		print "PySide not installed "
		print "Please wait installing PySide..."
		subprocess.call([sys.executable, "-m", "pip", "install", "PySide"])
	except:
		try:
			pySide_import_error = "PySide was Not Found."
			print "PySide installation failed!!"
			print "Please wait retrying PySide installation..."
			os.system("apt-get install python-pyside")
			os.system("apt-get install python-qt4")
		except:
			print "ERROR:- PySide not installed "
			print "Please try with:- sudo apt-get install python-pyside AND sudo apt-get install python-qt4"

	from PySide import QtGui, QtCore

op_sys = platform.system()

logs_path = ""

"""* 
	>>>>MAIN WINDOW CLASS<<<<
*"""
class MainWindowWidget(QtGui.QWidget):

	def __init__(self):
		super(MainWindowWidget, self).__init__()
		self.logs_name = False

		# TOP LABELS
		self.selectLog = QtGui.QLabel('Select your logs file:-')
		self.status_l = QtGui.QLabel('Status: ')
		self.status = QtGui.QLabel('Ready')

		# Button that allows loading of logs
		self.load_button = QtGui.QPushButton("Load Logs")
		self.load_button.clicked.connect(self.file_explorer)
		self.load_button.clicked.connect(self.load_logs_but)

		self.selectedLogFile = QtGui.QLabel('Your selected logs file location:-')
		self.File_name = QtGui.QLabel('No logs file selected')

		# Switch for OS
		self.os = QtGui.QLabel('Choose app platform:- ')
		self.Android = QtGui.QRadioButton("Android")
		self.Android.setChecked(True)
		self.iOS = QtGui.QRadioButton("iOS")

		# Text Box that takes text input for file name
		self.enter_name = QtGui.QLabel("\n\nENTER SCRIPT NAME BELOW:-")
		self.textBox = QtGui.QLineEdit()
		self.textBox.setObjectName("script_name")
		self.textBox.setStyleSheet("background-color: white")

		# Button that creates script
		self.pushbutton = QtGui.QPushButton()
		self.pushbutton.setText("CREATE SCRIPT")
		self.pushbutton.clicked.connect(self.create_file)

		# Network status LABEL
		self.net_statusBAR = QtGui.QHBoxLayout()
		self.net_status = QtGui.QLabel("Internet Status:- " + self.check_ping())
		if "Online" in self.net_status.text():
			self.indic = QtGui.QLabel("✔")
			self.indic.setStyleSheet("background-color: green")
		else:
			self.indic = QtGui.QLabel("✖")
			self.indic.setStyleSheet("background-color: red")
		self.net_statusBAR.addWidget(self.net_status)
		self.net_statusBAR.addWidget(self.indic)
		self.net_statusBAR.addStretch(2)

		# INSTRUCTIONs LABEL
		self.instructions = QtGui.QLabel(
			'Instructions:-\n 1) Either drag and drop logs file, or choose from first selection button--->(Load Logs).\n 2) Enter your desired file name for the script.\n 3) Click on --->(CREATE SCRIPT).\n THATS ALL!!\n\n\n\n****************PLEASE CHECK THE LOG FILE PRESENT IN REPORT FOLDER SO AS TO CHECK SOME OF THE MISSING PARTS IN THE SCRIPT IF EXISTING.****************\n\n 	       #######PLEASE HAVE SOME PATIENCE, ONCE THE SCRIPT IS WRITTEN THE PROGRAM WILL TERMINATE AUTOMATICALLY.#########')

		######		 PROGRESS BAR	######
		self.progress = QtGui.QProgressBar(self)
		self.progress.setGeometry(30, 40, 500, 25)
		self.progress.setValue(0)

		# Platform Radio
		self.platform_radio = QtGui.QHBoxLayout()
		self.platform_radio.addWidget(self.os)
		self.platform_radio.addWidget(self.Android)
		self.platform_radio.addWidget(self.iOS)
		self.platform_radio.addStretch(2)
		# A horizontal vertical_items to include the button on the left
		self.horizontal_items = QtGui.QHBoxLayout()
		self.horizontal_items.addWidget(self.selectLog)
		self.horizontal_items.addWidget(self.load_button)
		self.horizontal_items.addWidget(self.progress)
		self.horizontal_items.addStretch()
		self.horizontal_items.addWidget(self.status_l)
		self.horizontal_items.addWidget(self.status)

		# Another horizontal vertical_items to include the button on the second row
		self.horizontal_items2 = QtGui.QHBoxLayout()
		self.horizontal_items2.addWidget(self.selectedLogFile)
		self.horizontal_items2.addWidget(self.File_name)
		self.horizontal_items2.addStretch(2)

		# A Vertical vertical_items to include the button vertical_items and then the image
		vertical_items = QtGui.QVBoxLayout()
		vertical_items.addLayout(self.platform_radio)
		vertical_items.addLayout(self.horizontal_items)
		vertical_items.addLayout(self.horizontal_items2)
		vertical_items.addWidget(self.enter_name)
		vertical_items.addWidget(self.textBox)
		vertical_items.addWidget(self.pushbutton)
		vertical_items.addLayout(self.net_statusBAR)		
		vertical_items.addWidget(self.instructions)
		vertical_items.addStretch()
		self.setStyleSheet("background-color: #bcd1d0")
		self.setLayout(vertical_items)

		self.setAcceptDrops(True)

		self.setWindowTitle("Script Creator Tool!!!								 AppAnalytics")

		self.setWindowIcon(QtGui.QIcon("reference/logo.ico"))

		self.show()

	def create_file(self):
		"""* 
			>>>>CHECK PLATFORM<<<<
		*"""
		if self.Android.isChecked():
			platform = "Android"
		elif self.iOS.isChecked():
			platform = "ios"
		print "Platform:- " + platform
		###########################################################################################################
		"""* 
			>>>>VALIDATIONS FOR FIELDS<<<<
		*"""
		getPath = os.getcwd()
		fileName = self.textBox.text()
		if fileName == "":
			QtGui.QMessageBox.information(self, "ERROR", 'NO FILE NAME ENTERED.')
			self.hide()
			self.show()
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
		l = open(getPath + "/report/" + fileName + "_logged.txt", 'a')

		l.write("LOG for your script:-" + str(fileName) + "\n")
		print "Your desired file Name:-" + str(fileName)

		if not self.logs_name:
			QtGui.QMessageBox.information(self, "ERROR", 'PLEASE SELECT LOGS FIRST.')
			self.hide()
			self.show()
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			return False

		if platform == "Android" and "ios" in self.logs_name:
			QtGui.QMessageBox.information(self, "ERROR",
										  'Selected logs seems to be from ios device, please select ios as platform.')
			self.hide()
			self.show()
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			return False

		if not re.match("^[a-z0-9]*$", fileName):
			QtGui.QMessageBox.information(self, "ERROR", 'INVALID FILE NAME.')
			self.hide()
			self.show()
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			return False

		if platform == "ios" and not "ios" in fileName:
			QtGui.QMessageBox.information(self, "ERROR", 'ios aap must end with "ios".')
			self.hide()
			self.show()
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			return False
		if platform == "ios":
			apple_link, entered = QtGui.QInputDialog.getText(self, "Apple Store link required",
															 "Please enter apple store link for the app:-")
			print "Apple Store Link :-" + apple_link
			if not "?" in apple_link:
				link = apple_link + "?l=en&mt=8"
			else:
				link = apple_link
			l.write("Apple store link for app:- " + link)

		else:
			link = None
		###########################################################################################################
		"""* 
			>>>>PROCEED TO FURTHER PROCESSING<<<<
		*"""

		try:
			processed = writting_robo(platform, self.logs_name, fileName, ping=self.check_ping(), url=link, ui=self)
			######################################################################
			"""* 
				>>>>DECRYPTION KEY CHECK<<<<
			*"""
			if processed == "REQUIRES KEY":
				self.status.deleteLater()
				self.status = QtGui.QLabel('------>NEED DECRYPTION KEY!!')
				self.horizontal_items.addWidget(self.status)
				key, ok = QtGui.QInputDialog.getText(self, "Referral decryption key required !",
													 "Enter Adbrix getReferral iv:-")
				if ok and len(key) == 16:
					processed = writting_robo(platform, self.logs_name, fileName, key.strip(), ping=self.check_ping(), url=link, ui=self)
				else:
					self.status.deleteLater()
					self.status = QtGui.QLabel('------>Failed!!')
					self.horizontal_items.addWidget(self.status)
					print "Please enter a valid iv for decrypting GetReferral data in Adbrix."
					QtGui.QMessageBox.information(self, "ERROR",
												  'Please enter a valid iv for decrypting GetReferral data in Adbrix.')
					os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			elif processed == "REQUIRES MULTIPART KEY":
				key = subprocess.check_output(['python' ,'reference/get_adbrix_multi_key.py'])
				processed = writting_robo(platform, self.logs_name, fileName, key, ping=self.check_ping(), url=link, ui=self)
			#########################################################################
			"""* 
				>>>>PROGRESS BAR<<<<
			*"""
			for x in range(101):
				time.sleep(0.002)
				self.progress.setValue(x)
			self.status.deleteLater()
			self.status = QtGui.QLabel('------>SUCCESS!!')
			self.horizontal_items.addWidget(self.status)
			#########################################################################
			"""* 
				>>>>ERROR REPORTING<<<<
			*"""
		except Exception:
			for x in range(random.randint(5, 50)):
				time.sleep(0.002)
				self.progress.setValue(x)
			error = traceback.format_exc()
			processed = False
			print error
			l.write("Some error occurred while writting script.\n")
			print "Some error occurred while writting script."
			self.status.deleteLater()
			self.status = QtGui.QLabel('------>Failed!!')
			self.horizontal_items.addWidget(self.status)
			QtGui.QMessageBox.information(self, "ERROR",
										  'SOME ERROR OCCURRED WHILE CREATING SCRIPT, Press OK to check traceback.')
			QtGui.QMessageBox.information(self, "TraceBack", error)
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
		#################################################################################################################
		"""* 
			>>>>LOGGING<<<<
		*"""
		if pySide_import_error:
			l.write(pySide_import_error)
			l.write('\n')

		if beautifulSoup_import_error:
			l.write(beautifulSoup_import_error)
			l.write('\n')

		if len(report) > 1:
			l.write("##### WARNINGS ######\n")
			for i in report:
				l.write(i)
		else:
			l.write("********************** NO SEVERE WARNINGS *********************\n")

		if processed:
			print "	 ********************** DONE *********************"
			print "Script written Successfully to the Output directory of this folder.\n\n\n\n"
			l.write(
				"-->> please maintain time sleeps between the calls on your own, however all realtime sleeps have been taken care.\n")
			l.write(
				"-->> DO NOT RELY ON THIS TOOL COMPLETELY. EVEN AFTER NO SEVERE WARNINGS IN LOG FILE, MATCH DATA AND ASSERT THE FILE BEFORE FINALISING.\n")
			l.write("********************** DONE *********************\n")
			l.write("Script written Successfully to the Output directory of this folder.\n")
			QtGui.QMessageBox.information(self, "SUCCESS!!",
										  "	********************** DONE *********************\nScript written Successfully to the Output directory of this folder.")
			self.progress.setValue(0)
			webbrowser.open(getPath + "/Output/" + fileName+".py")
			sys.exit(app.exec_())
		else:
			l.write("Some error occurred while writting script.\n")
			print "Some error occurred while writting script."
			QtGui.QMessageBox.information(self, "console says:- ", str(error) + "\n\n PLEASE REPORT THIS ERROR.")
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
		l.close()
		#################################################################################################################

	def file_explorer(self):
		# Get the file location
		self.logs_name, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file')

	def load_logs_but(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		if str(self.logs_name)[-3:] != "saz":
			self.load_button.setStyleSheet("background-color: red")
			QtGui.QMessageBox.information(self, "ERROR",
										  'Invalid logs file. Please use a valid .saz extension log file.')
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])

		self.File_name.deleteLater()
		self.File_name = None
		self.File_name = QtGui.QLabel(self.logs_name)
		self.status.deleteLater()
		self.status = None
		self.load_button.setStyleSheet("background-color: green")
		self.status = QtGui.QLabel('Logs Loaded')
		self.horizontal_items.addWidget(self.status)
		self.horizontal_items2.addWidget(self.File_name)
		self.horizontal_items2.addStretch(100)
		print "Location for your logs-->" + str(self.logs_name)
		# Load the logs from the location

	#######################################################################################################################
	# The following three methods set up dragging and dropping for the app  [ UNUSED ]  
	def dragEnterEvent(self, e):
		if e.mimeData().hasUrls:
			e.accept()
		else:
			e.ignore()

	def dragMoveEvent(self, e):
		if e.mimeData().hasUrls:
			e.accept()
		else:
			e.ignore()

	def dropEvent(self, e):
		"""
		Drop files directly onto the widget
		File locations are stored in fname
		:param e:
		:return:
		"""
		if e.mimeData().hasUrls:
			e.setDropAction(QtCore.Qt.CopyAction)
			e.accept()
			# Workaround for OSx dragging and dropping
			for url in e.mimeData().urls():
				fname = str(url.toLocalFile())
			self.logs_name = fname
			self.load_logs_but()
		else:
			e.ignore()

	def check_ping(self):
		sHost = "google.com"
		try:
			if platform.system().lower() == "windows":
				output = subprocess.check_output(
					"ping -{} 1 {}".format('n', sHost), shell=True)
			else:
				hostname = "google.com" #example
				response = os.system("ping -c 1 " + hostname)
				if response == 0:
					return "Online"
		except Exception, e:
			return "Offline"
		return "Online"

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:
			if not self.textBox.text() == "" and not self.logs_name == "":
				if str(self.logs_name)[-3:] != "saz":
					QtGui.QMessageBox.information(self, "ERROR",
												  'Invalid logs file. Please use a valid .saz extension log file.')
					os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
				if not re.match("^[a-z0-9]*$", self.textBox.text()):
					QtGui.QMessageBox.information(self, "ERROR", 'INVALID FILE NAME.')
					self.hide()
					self.show()
					os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
				self.create_file()
				event.accept()
			else:
				QtGui.QMessageBox.information(self, "ERROR", 'Input All fields before proceeding.')
				self.hide()
				self.show()
				os.execl(sys.executable, 'python', __file__, *sys.argv[1:])


############################################################################################################################
# Run if called directly
if __name__ == '__main__':
	# Initialise the application
	app = QtGui.QApplication(sys.argv)
	# Call the widget
	ex = MainWindowWidget()
	sys.exit(app.exec_())
