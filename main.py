# -*- coding: utf-8 -*-
import os

"""* 
	>>>>PREREQUISITE IMPORTS AND INSTALLS<<<<
*"""
try:
	from PyQt4 import QtCore, QtGui
except:
	os.system("prerequisites\\pyqt64.exe")
	try:
		from PyQt4 import QtCore, QtGui
	except:
		os.system("prerequisites\\pyqt32.exe")
		from PyQt4 import QtCore, QtGui

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

try:
	_encoding = QtGui.QApplication.UnicodeUTF8
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig)

"""* 
	>>>>USER INTERFACE CLASS<<<<
*"""
class Ui_Dialog(object):
	def setupUi(self, Dialog):
		self.ui=Dialog
		Dialog.setObjectName(_fromUtf8("Dialog"))
		Dialog.resize(530, 400)
		Dialog.setSizeGripEnabled(False)
		self.pushButton = QtGui.QPushButton(Dialog)
		self.pushButton.clicked.connect(self.new_app)
		self.pushButton.setGeometry(QtCore.QRect(30, 30, 461, 85))
		font = QtGui.QFont()
		font.setPointSize(16)
		font.setBold(True)
		font.setWeight(75)
		self.pushButton.setFont(font)
		self.pushButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
		self.pushButton.setMouseTracking(False)
		# self.pushButton.setTabletTracking(False)
		self.pushButton.setObjectName(_fromUtf8("pushButton"))
		self.pushButton_2 = QtGui.QPushButton(Dialog)
		self.pushButton_2.clicked.connect(self.open)
		self.pushButton_2.setGeometry(QtCore.QRect(30, 156, 461, 85))
		font = QtGui.QFont()
		font.setPointSize(16)
		font.setBold(True)
		font.setWeight(75)
		self.pushButton_2.setFont(font)
		self.pushButton_2.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
		self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))

		self.pushButton_3 = QtGui.QPushButton(Dialog)
		self.pushButton_3.clicked.connect(self.update)
		self.pushButton_3.setGeometry(QtCore.QRect(30, 280, 461, 85))
		font = QtGui.QFont()
		font.setPointSize(16)
		font.setBold(True)
		font.setWeight(75)
		self.pushButton_3.setFont(font)
		self.pushButton_3.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
		self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))

		self.retranslateUi(Dialog)
		QtCore.QMetaObject.connectSlotsByName(Dialog)

		"""* 
			>>>>NAMING CONVENTIONS<<<<
		*"""
	def retranslateUi(self, Dialog):
		Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
		self.pushButton.setText(_translate("Dialog", "NEW APP", None))
		self.pushButton_2.setText(_translate("Dialog", "OPEN", None))
		self.pushButton_3.setText(_translate("Dialog", "UPDATE", None))
		"""* 
			>>>>REDIRECTION TO NEW APP MODULE<<<<
		*"""
	def new_app(self):
		self.ui.hide()
		os.system("python newapp.py")

		"""* 
			>>>>REDIRECTION TO NEW OPEN COMPLETE MODULE<<<<
		*"""
	def open(self):
		self.ui.hide()
		os.system("python open_complete/open.py")

		"""* 
			>>>>REDIRECTION TO NEW OPEN COMPLETE MODULE<<<<
		*"""
	def update(self):
		self.ui.hide()
		os.system("python updater.py")

"""* 
	>>>>EXECUTION LOOP FOR PYQT4<<<<
*"""
if __name__ == "__main__":
	import sys
	app = QtGui.QApplication(sys.argv)
	Dialog = QtGui.QDialog()
	ui = Ui_Dialog()
	ui.setupUi(Dialog)
	Dialog.setWindowIcon(QtGui.QIcon("reference/logo.png"))
	Dialog.setWindowTitle("Script Creator Tool  -  AppAnalytics")
	Dialog.show()
	sys.exit(app.exec_())


