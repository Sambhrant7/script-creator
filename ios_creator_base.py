# -*- coding: utf-8 -*-
import requests
import re
import subprocess
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


"""* 
	>>>>FILE IMPORTS<<<<
*"""
from reference import Adjustios, mat_ios, Apsalarios, Kochavaios, Adforceios
from static_data_lists import imports
from static_data_lists import ios_required_functions
from static_data_lists import campaign_ending, event_definition_start
from match_data import access_required_data


"""* 
	>>>>PYSIDE IMPORT<<<<
*"""
ui_adapter=[]
try:
	from PySide import QtGui, QtCore
except ImportError:
	pySide_import_error = "PySide was Not Found."
	print "PySide not installed "
	print "Please wait installing PySide..."
	subprocess.call([sys.executable, "-m", "pip", "install", "PySide"])
	from PySide import QtGui, QtCore

tracking_def_list = []
global comp, app_id
comp = "8.0"
app_id = "ENTER APP ID"


"""* 
	>>>>POP UP UI TO GET CUSTOM INFORMATION FROM USER<<<<
*"""
def get_popUp(ui,campaign_data,required):
	ui.status.deleteLater()
	ui.status = QtGui.QLabel('------>ENTER '+required+'!!')
	ui.horizontal_items.addWidget(ui.status)
	response, ok = QtGui.QInputDialog.getText(ui, "Could not fetch "+required+"!!",
	"Please enter "+required+" manually, either you are not connected to Internet or the app_size varies with device:-")
	print required+" you entered:- " + str(response)
	if required=="app_size":
		campaign_data.append("	'"+required+"'			 : " + response + ",\n")
	else:
		campaign_data.append("	'"+required+"'			 : '" + response + "',\n")


"""* 
	>>>>APPLE APP STORE CALL TO FETCH APP DETAILS<<<<
*"""
def app_store(url):
	method = 'get'
	headers = {
		'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Acer Build/2323)',
		'Accept-Encoding': 'gzip'
	}
	params = None
	data = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None}


"""* 
	>>>>FETCH DETAILS FROM APPLE STORE RESPONSE<<<<
*"""
def get_appstore_info(url):
	apple_store = app_store(url)
	result = requests.get(apple_store.get('url'), headers=apple_store.get('headers'), params=apple_store.get('params'),
							data=None, timeout=30, verify=False, auth=None, files=None)
	res = str(result.text)

	info = res.split("information-list information-list")[1].split("<!", 1)[0]
	try:
		name = res.split('<meta property="og:title" content="')[1].split('"')[0].strip('"')
	except:
		name = "ENTER NAME"
	try:
		c = info.split("iOS")[1]
		compatible = re.findall(r"[-+]?\d*\.\d+|\d+", c)[0]
	except:
		compatible = "8.0"
	try:
		a = info.split("</dd>", 2)[1].rsplit(">", 1)[1].strip()
		if "MB" in a:
			app_size = re.findall(r"[-+]?\d*\.\d+|\d+", a)[0]
		elif "GB" in a:
			app_size = str(float(re.findall(r"[-+]?\d*\.\d+|\d+", a)[0]) * 1024)
	except:
		app_size = "0.0"

	return name, app_size, compatible


"""* 
	>>>>CLASS TO FORMAT REQUIRED DATA IN SCRIPT<<<<
*"""
class output_file_formatting:
	def __init__(self):
		pass
	"""* 
		>>>>FUNCTION TO HANDLE TRACKING WISE DATA IN SCRIPT<<<<
	*"""
	################################################################# CAMPAIGN DATA ###############################################################################
	def tracking_data_handling(self, campaign_data, install_function, trackings, calls_data_dict, key=None,
								 ping="Offline", url="", ui=""):
		ui_adapter.append(ui)
		online = False
		if ping == "Online":
			online = True
		print "Internet Status:-"
		print ping
		times_and_stamps = {}

		"""* 
			>>>>USE FETCHED DETAILS FROM APP STORE CALL<<<<
		*"""
		if url:
			try:
				globals()['app_id'] = url.split("?")[0].split("id")[1]
			except:
				pass
		if online:
			try:
				inf = get_appstore_info(url)
				app_name = inf[0]
				app_size = inf[1]
				globals()['comp'] = inf[2]
			except:
				app_name = "ENTER APP NAME"
				app_size = "0.0"
		else:
			app_name = "ENTER APP NAME"
			app_size = "0.0"

		##################################################################################################
		if 'Adjust' in trackings:
			#ADD IMPORTS#
			for library in Adjustios.import_list:
				if not library in imports:
					imports.append(library)

			#GET ADJUST RELATED DATA#
			adjust_data = access_required_data(calls_data_dict).adjust_definition_data()
			main_campaign_data = adjust_data[2]
			tracking_def_list.extend(adjust_data[0])
			global adjust_campaign_data
			adjust_campaign_data = adjust_data[1]
			times_and_stamps['Adjust'] = adjust_data[3]
			valid = adjust_data[4]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('bundle_id') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_version_short'):
					campaign_data.append(
						"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_short') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + main_campaign_data.get('app_version') + "',\n")

			if valid and type(valid)!=bool:
				rec = valid[1]
				ios_required_functions.append(
					"""\ndef install_receipt():\n	return '""" + rec + """'\n""")
			else:
				ios_required_functions.append(
					"""\ndef install_receipt():\n	return 'MIIStAYJKoZIhvcNAQcCoIISpTCCEqECAQExCzAJBgUrDgMCGgUAMIICVQYJKoZIhvcNAQcBoIICRgSCAkIxggI'+util.get_random_string('google_token',6303)+'=='\n""")
				print "Could not validate receipt."

			############# INSTALL DATA ###############	if not app_data.get('install_receipt'):\n
			install_function.append(
				"""	install_receipt()\n	pushtoken(app_data)\n	if not app_data.get('adjust_call_time'):\n		app_data['adjust_call_time']=int(time.time())\n	if not app_data.get('ios_uuid'):\n		app_data['ios_uuid'] = str(uuid.uuid4())\n	if not device_data.get('idfa_id'):\n		app_data['idfa_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfa_id'] = device_data.get('idfa_id')\n	if not device_data.get('idfv_id'):\n		app_data['idfv_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfv_id'] = device_data.get('idfv_id')\n""")
			ios_required_functions.insert(0, Adjustios.adjust_required_functions)

		##################################################################################################
		if 'MAT' in trackings:
			#ADD IMPORTS#
			for library in mat_ios.import_list:
				if not library in imports:
					imports.append(library)

			#GET MAT RELATED DATA#
			mat_data = access_required_data(calls_data_dict).mat_definition_data(key=key)
			main_campaign_data = mat_data[2]
			tracking_def_list.extend(mat_data[0])
			global mat_campaign_data
			mat_campaign_data = mat_data[1]
			times_and_stamps['MAT'] = mat_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_name') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + str(main_campaign_data.get('app_version')) + "',\n")

			install_function.append(
				"""	set_MATID(app_data)\n	if not app_data.get('ios_device_data'):\n		ios_device_data(app_data)\n\n	if not app_data.get('device_cpu_type'):\n		for key in app_data.get('ios_device_data'):\n			if key == device_data.get('device'):\n				app_data['device_cpu_type'] = app_data.get('ios_device_data').get(key)\n				print key\n				break\n			else:\n				app_data['device_cpu_type']=16777228\n\n	if not device_data.get('idfa_id'):\n		app_data['idfa_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfa_id'] = device_data.get('idfa_id')\n\n	if not device_data.get('idfv_id'):\n		app_data['idfv_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfv_id'] = device_data.get('idfv_id')""")

			ios_required_functions.insert(0, mat_ios.mat_ios_required_functions)

		##################################################################################################
		if 'Adbrix' in trackings:
			#ADD IMPORTS#
			for library in Adbrix.import_list:
				if not library in imports:
					imports.append(library)

			#GET AD-BRIX RELATED DATA#
			adbrix_data = access_required_data(calls_data_dict).adbrix_definition_data(key=key)
			main_campaign_data = adbrix_data[2]
			tracking_def_list.extend(adbrix_data[0])
			global adbrix_campaign_data
			adbrix_campaign_data = adbrix_data[1]
			times_and_stamps['Adbrix'] = adbrix_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_name') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version_code'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + str(main_campaign_data.get('app_version_code')) + "',\n")

			install_function.append(
				"""	update_activity(app_data,device_data)\n	get_demo_value(app_data,device_data)\n	make_sec(app_data,device_data)\n\n	email_sha1 = ""\n	for i in range(0,random.randint(2,3)):\n		return_userinfo(app_data)\n	email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'\n	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]\n	""")

			ios_required_functions.insert(0, Adbrix.adbrix_android_required_functions)

		##################################################################################################
		if 'Apsalar' in trackings:
			#ADD IMPORTS#
			for library in Apsalarios.import_list:
				if not library in imports:
					imports.append(library)

			#GET APSALAR RELATED DATA#
			apsalar_data = access_required_data(calls_data_dict).apsalar_definition_data()
			main_campaign_data = apsalar_data[2]
			tracking_def_list.extend(apsalar_data[0])
			global apsalar_campaign_data
			apsalar_campaign_data = apsalar_data[1]
			times_and_stamps['Apsalar'] = apsalar_data[3]
			valid = apsalar_data[4]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('i') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_name'" in c for c in campaign_data):
				if main_campaign_data.get('av'):
					campaign_data.append(
						"	'app_version_name' 	 :'" + main_campaign_data.get('av') + "',\n")

			if valid and type(valid)!=bool:
				rec = valid[1]
				ios_required_functions.append(
					"""\ndef install_receipt():\n	return '""" + rec + """'\n""")
			else:
				ios_required_functions.append(
					"""\ndef install_receipt():\n	return 'MIIStAYJKoZIhvcNAQcCoIISpTCCEqECAQExCzAJBgUrDgMCGgUAMIICVQYJKoZIhvcNAQcBoIICRgSCAkIxggI'+util.get_random_string('google_token',6303)+'=='\n""")
				print "Could not validate receipt."

			############# INSTALL DATA ###############	if not app_data.get('install_receipt'):\n
			install_function.append(
				"""	install_receipt()\n	if not device_data.get('idfa_id'):\n		app_data['idfa_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfa_id'] = device_data.get('idfa_id')\n	if not device_data.get('idfv_id'):\n		app_data['idfv_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfv_id'] = device_data.get('idfv_id')\n""")
			ios_required_functions.insert(0, Apsalarios.apsalar_ios_required_functions)

		##################################################################################################
		if 'Kochava' in trackings:
			#ADD IMPORTS#
			for library in Kochavaios.import_list:
				if not library in imports:
					imports.append(library)

			#GET KOCHAVA RELATED DATA#
			kochava_data = access_required_data(calls_data_dict).kochava_definition_data()
			main_campaign_data = kochava_data[2]
			tracking_def_list.extend(kochava_data[0])
			global kochava_campaign_data
			kochava_campaign_data = kochava_data[1]
			times_and_stamps['Kochava'] = kochava_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('v'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + main_campaign_data.get('v') + "',\n")

			############# INSTALL DATA ###############	if not app_data.get('install_receipt'):\n
			install_function.append(
				"""	kochava_device_ID(app_data)\n	screen_Brightness(app_data)\n\n	app_data['battery_level']=random.randint(10,100)	\n\n	app_data['volume']=random.randint(0,1)\n\n	ios_device_data = {\n		'iPad mini 2':'16777228',\n		'iPad mini Wi-Fi':'12',\n		'iPad mini Wi-Fi + Cellular':'12',\n		'iPad 4 Wi-Fi':'16777228',\n		'iPad 4 Wi-Fi + Cellular':'16777228',\n		'iPhone 7 Plus':'16777228',\n		'iPhone 7':'16777228',\n		'iPhone SE':'16777228',\n		'iPhone 6s Plus':'16777228',\n		'iPhone 6s':'16777228',\n		'iPhone 6 Plus':'16777228',\n		'iPhone 6':'16777228', \n		'iPhone 5s':'16777228', \n		'iPhone 5c':'12',\n		'iPhone 5 CDMA':'12',\n		'iPhone 5':'12',\n		'iPhone 4s':'12',\n		'iPhone 4 CDMA':'12',\n		'iPhone 4':'12',\n		'iPad Air':'16777228',\n		'iPad mini 3':'16777228',\n		'iPad Air 2':'16777228',\n		'iPad mini 4':'16777228'\n		}\n\n	if not app_data.get('device_cpu_type'):\n		for key in ios_device_data:\n			if key == device_data.get('device'):\n				app_data['device_cpu_type'] = ios_device_data.get(key)\n				break\n			else:\n				app_data['device_cpu_type'] = '16777228'\n	\n	if not device_data.get('idfa_id'):\n		app_data['idfa_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfa_id'] = device_data.get('idfa_id')\n		\n	if not app_data.get('idfv_id'):\n		app_data['idfv_id'] = str(uuid.uuid4()).upper()	\n\n	app_data['call_run_time']=time.time()\n\n	app_data['registration_flag']=False\n\n	if not app_data.get('pcpno'):\n		app_data['pcpno']="949"+util.get_random_string('decimal',6)\n\n	if not app_data.get('session_id'):\n		app_data['session_id']=str(uuid.uuid4())\n\n	if not app_data.get('sender_country'):\n		app_data['sender_country']=random.choice(['FI','DK','SE','NO'])\n""")
			ios_required_functions.insert(0, Kochavaios.kochava_ios_required_functions)

		##################################################################################################
		if 'Adforce' in trackings:
			#ADD IMPORTS#
			for library in Adforceios.import_list:
				if not library in imports:
					imports.append(library)

			#GET ADFORCE RELATED DATA#
			adforce_data = access_required_data(calls_data_dict).adforce_definition_data()
			main_campaign_data = adforce_data[2]
			tracking_def_list.extend(adforce_data[0])
			global adforce_campaign_data
			adforce_campaign_data = adforce_data[1]
			times_and_stamps['Kochava'] = adforce_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				try:
					campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('_bundle_id') + "',\n")
				except:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER PACKAGE NAME!!')
					ui.horizontal_items.addWidget(ui.status)
					package_name, ok = QtGui.QInputDialog.getText(ui, "Could not find package_name!!",
															  "Please enter package_name manually, not found in logs")
					print "Package name you entered:- " + str(app_size)
					campaign_data.append("	'package_name'		 :'"+package_name+"',\n")

			if not any("installtimenew.main" in c for c in install_function):
				if not app_size == "0.0":
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				else:
					ui.status.deleteLater()
					ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
					ui.horizontal_items.addWidget(ui.status)
					app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!",
															  "Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
					print "app_size you entered:- " + str(app_size)
					campaign_data.append("	'app_size'			 : " + app_size + ",\n")
				install_function.append(
					"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),\n")

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					get_popUp(ui,campaign_data,required="app_name")

			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('v'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + main_campaign_data.get('v') + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				if main_campaign_data.get('_bv'):
					campaign_data.append(
						"	'app_version_name' 	 :'" + main_campaign_data.get('_bv') + "',\n")

			############# INSTALL DATA ###############	if not app_data.get('install_receipt'):\n
			install_function.append(
				"""	if not app_data.get('install_id'):\n		app_data['install_id'] = str(uuid.uuid4())	\n	if not device_data.get('idfa_id'):\n		app_data['idfa_id'] = str(uuid.uuid4()).upper()\n	else:\n		app_data['idfa_id'] = device_data.get('idfa_id')\n		\n	if not app_data.get('idfv_id'):\n		app_data['idfv_id'] = str(uuid.uuid4()).upper()	\n""")
			ios_required_functions.insert(0, Adforceios.adforce_ios_required_functions)

		"""* 
			>>>>RETURN TIME RELATED DATA FOR FURTHER USE<<<<
		*"""
		return times_and_stamps

	def common_campaign_data(self, campaign_data, trackings):
		############## COMMON CAMPAIGN DATA FOR ALL TRACKINGS ##############
		if app_id=="ENTER APP ID":
			get_popUp(ui_adapter[len(ui_adapter)-1],campaign_data,required="app_id")
		else:
			campaign_data.append("	'app_id' 			 : '" + app_id + "',\n")
		campaign_data.append("	'ctr' 				 : 6,\n")
		campaign_data.append("	'sdk' 		 		 : 'ios',\n")
		campaign_data.append("	'device_targeting'	 : True,\n")
		campaign_data.append("	'supported_countries': 'WW',\n")
		campaign_data.append("	'supported_os'		 : '" + globals().get('comp') + "',\n")
		if len(trackings) == 1:
			campaign_data.append("	'tracker'		 	 : '" + trackings[0] + "',\n")

	def campaign_data_dictionary(self, campaign_data, trackings):

		if 'Adjust' in trackings:
			###################### Adjust DATA DICTIONARY ###################
			campaign_data.append("	'adjust'		 : {\n")
			campaign_data.append("		'app_token'	 	: '" + adjust_campaign_data.get('app_token') + "',\n")
			campaign_data.append("		'sdk'		 	: '" + adjust_campaign_data.get('Client-Sdk') + "',\n")
			if adjust_campaign_data.get('app_updated_at'):
				campaign_data.append(
					"		'app_updated_at': '" + adjust_campaign_data.get('app_updated_at')[:-5] + "',\n")
			if adjust_campaign_data.get('secret_key'):
				campaign_data.append("		'secret_key' : '" + adjust_campaign_data.get('secret_key') + "',\n")
				campaign_data.append("		'secret_id'	 : '" + adjust_campaign_data.get('secret_id') + "',\n")
			campaign_data.append('		},\n')

		if 'Adbrix' in trackings:
			###################### AD-BRIX DATA DICTIONARY ###################
			campaign_data.append("	'ad-brix'		 : {\n")
			campaign_data.append("		'app_key'	 	 : '" + adbrix_campaign_data.get('app_key') + "',\n")
			campaign_data.append("		'ver'	 	 	 : '" + adbrix_campaign_data.get('ver') + "',\n")
			campaign_data.append("		'key_getreferral': '" + adbrix_campaign_data.get('key_getreferral') + "',\n")
			campaign_data.append("		'iv_getreferral' : '" + adbrix_campaign_data.get('iv_getreferral') + "',\n")
			campaign_data.append("		'key_tracking' 	 : 'srkterowgawrsozerruly82nfij625w9',\n")
			campaign_data.append("		'iv_tracking' 	 : 'srkterowgawrsoze',\n")
			campaign_data.append("	},\n")

		if 'MAT' in trackings:
			###################### MAT DATA DICTIONARY ###################
			campaign_data.append("	'mat'		 : {\n")
			campaign_data.append("		'ver'	 		: '" + mat_campaign_data.get('ver') + "',\n")
			campaign_data.append("		'advertiser_id' : '" + mat_campaign_data.get('advertiser_id') + "',\n")
			campaign_data.append("		'sdk' 			: '" + mat_campaign_data.get('sdk') + "',\n")
			campaign_data.append("		'key' 			: '" + mat_campaign_data.get('key') + "',\n")
			campaign_data.append('		},\n')

		if 'Apsalar' in trackings:
			###################### Apsalar DATA DICTIONARY ###################
			campaign_data.append("	'apsalar'		 	    : {\n")
			campaign_data.append("		'version'		    : '" + apsalar_campaign_data.get('sdk') + "',\n")
			campaign_data.append("		'key'				: '" + apsalar_campaign_data.get('a') + "',\n")
			campaign_data.append("		'secret'		    : '" + apsalar_campaign_data.get('key') + "',\n")
			campaign_data.append("	},\n")

		if 'Kochava' in trackings:
			###################### Kochava DATA DICTIONARY ###################
			campaign_data.append("	'apsalar'		 	    : {\n")
			campaign_data.append("		'kochava_app_id'	    : '" + kochava_campaign_data.get('kochava_app_id') + "',\n")
			campaign_data.append("		'sdk_version'			: '" + kochava_campaign_data.get('sdk_version') + "',\n")
			campaign_data.append("		'sdk_protocol'		    : '" + kochava_campaign_data.get('sdk_protocol') + "',\n")
			if(kochava_campaign_data.get('sdk_build_date')):
				campaign_data.append("		'sdk_build_date'	    : '" + kochava_campaign_data.get('sdk_build_date') + "',\n")
			campaign_data.append("	},\n")

		if 'Adforce' in trackings:
			###################### Adforce Data Dictionary ####################
			campaign_data.append("	'adforce'		 	    : {\n")
			campaign_data.append("		'app_id'		    : '" + adforce_campaign_data.get('_app') + "',\n")
			campaign_data.append("		'sdk_version'				: '" + adforce_campaign_data.get('_sdk_ver') + "',\n")
			if adforce_campaign_data.get('_gms_version'):
				campaign_data.append("		'_gms_version'		    : '" + adforce_campaign_data.get('_gms_version') + "',\n")
			campaign_data.append("	},\n")

	def campaign_data_retention(self, campaign_data):
		################### RETENTION DATA COMMON FOR ALL ###################
		campaign_data.append(campaign_ending)

	################################################################# INSTALL FUNCTION ###############################################################################
	def add_calls(self, install_function, calls_data_dict, genuine_hosts_data):
		install_function.append("\n ###########	 CALLS		 ############\n")
		call_counter=0
		for call_exec in calls_data_dict.get('host'):
			call_counter+=1
			if call_counter==1:
				if "response=util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("response=util.execute_request","app_data['api_hit_time']=time.time()\n	response=util.execute_request")
				elif "result=util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("result=util.execute_request","app_data['api_hit_time']=time.time()\n	result=util.execute_request")

				elif "resp = util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("resp = util.execute_request","app_data['api_hit_time']=time.time()\n	resp = util.execute_request")
				else:
					call_exec=genuine_hosts_data.get(call_exec).replace("util.execute_request","app_data['api_hit_time']=time.time()\n	util.execute_request")
				install_function.append(call_exec)
			else:
				install_function.append(genuine_hosts_data.get(call_exec))

		install_function.append("\n\n	try:\n		if campaign_data.get('link'):\n			cd = {\n					'agentId': Config.AGENTID,\n					'packageName': campaign_data['app_id'],\n					'link': campaign_data.get('link'),\n					'data': json.dumps({'source': 'New','app_version': campaign_data.get('app_version_name')})\n				}\n			saveClicks(cd)\n	except:\n		pass")
		install_function.append("\n\n	return {'status':'true'}\n\n")

	################################################################# events definition ##############################################################################
	def event_definitions(self, event_list, event_data_list, trackings):
		events_definitions_list = []
		e = list(set(event_list))
		events_definitions_list.append(event_definition_start)
		for event in range(len(event_list)):
			if event_list[event] in e:
				if 'adjust' in event_list[event]:
					cp = pp = 'None'
					if event_data_list[event]:
						if event_data_list[event][0]:
							cp = 'str(json.dumps(' + event_data_list[event][0] + '))'
						if event_data_list[event][1]:
							pp = 'str(json.dumps(' + event_data_list[event][1] + '))'

					token = event_list[event].split('adjust-->')[1]
					events_definitions_list.append(
						"def adjust_token_" + token + "(campaign_data, app_data, device_data,t1=0,t2=0):\n" + "	print 'Adjust : EVENT______________________________" + token + "'\n	request=adjust_event(campaign_data, app_data, device_data,callback_params=" + cp + ",partner_params=" + pp + ",event_token='" + token + "',t1=t1,t2=t2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp,event_token='" + token + "')\n	except:\n		print 'Could not save attributionCallStatus data'\n\n")
					e.remove(event_list[event])

				if 'apsalar' in event_list[event]:
					if not event_data_list[event]:
						event_data_list[event]=json.dumps({})
					ev = 'urllib.quote_plus(json.dumps(' + event_data_list[event] + '))'

					eventName = event_list[event].split('apsalar-->')[1]
					events_definitions_list.append(
						"def apsalar_event_" + eventName.replace(" ","_") + "(campaign_data, app_data, device_data, custom_user_id=False):\n" + "	print 'Apsalr : EVENT______________________________" + eventName.replace(" ","_") + "'\n	request=apsalar_event(campaign_data, app_data, device_data,event_name='" + eventName + "',value=" + ev + ",custom_user_id=custom_user_id)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp,event_token='" + token + "')\n	except:\n		print 'Could not save attributionCallStatus data'\n\n")	
					e.remove(event_list[event])

		return events_definitions_list
