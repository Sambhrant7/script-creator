"""* 
	>>>>FILE IMPORTS<<<<
*"""
from static_data_lists import campaign_data, open_function
from static_data_lists import install_function
from static_data_lists import imports
from match_data import access_required_data
from adbrix_aes256 import decrypted_data
from mat_aes import mat_decrypted_data
from filter import filter_logs_data
from time_sleep_generator import calculate_time_differences
from updates.fileRunner import run

"""* 
	>>>>MODULE IMPORTS<<<<
*"""
import os, shutil, json, urlparse
getPath = os.getcwd()

"""* 
	>>>>APP VERSION DECLARATIONS<<<<
*"""
globals()['exceptions']=['eventName','installed_at','prev_event','operator','af_v','eventValue','af_gcm_token','af_v2','firstLaunchDate','counter','cksm_v1','carrier','af_timestamp','iaecounter','uid','launch_counter','timepassedsincelastlaunch','af_gcm_token','date1','date2','installDate','advertiserId','gps_adid','session_count','sent_at','created_at','screen_size','android_uuid','screen_format','country','time_spent','queue_size','session_length','subsession_count','event_count','referrer','raw_referrer']

globals()['ExtrakeysToignore']=['mcc','mnc','referrer','raw_referrer','reftag','Content-Type', 'Accept-Encoding', 'User-Agent','Client-Sdk','Client-SDK', 'Accept-Language', 'Authorization', 'Accept','Connection']


globals()['supportedTrackers']=['adjust','appsflyer','kochava','branch','apsalar']

globals()['script_appVersion']=0
globals()['logs_appVersion']=0

"""* 
	>>>>GARBAGE REMOVAL<<<<
*"""
try:
	shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
	os.remove(getPath + "/Input/res/source.zip")
except:
	pass

"""* 
	>>>>WRITTING STRUCTURE<<<<
*"""
def writting_robo(platform, logs, file_name="test", key=None, ping="Offline", url="", ui="", device="LG"):
	"""* 
		>>>>PLATFORM DEPENDENT IMPORTS<<<<
	*"""
	if platform == "Android":
		from static_data_lists import android_required_functions
		from android_creator_base import output_file_formatting, tracking_def_list
		required_functions = android_required_functions
		devices = {
				"LG":{"model":"LG-VS985","manufacturer":"LGE","dpi":"640","device":"vs985","build":"LRX22G","fingerprint":"lge/g3_vzw/g3:4.4.2/KVT49L.VS98512B/VS98512B.1414669625:user/release-keys","brand":"lge","product":"vs985","mainboard":"MSM8974","resolution":"2560x1440","screen_size":"4.6","os_codename":"REL","os_version":"5.0.2","cpu_frequency":{"max":"2457.6","min":"300.0"},"sdk":"21","cpu_core":"4","gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"ram":{"shown":"2904","available":"2904"},"internal_memory":{"shown":"32 GB","available":"29.121 GB"},"kernel":"3.4.0-LS-g063100b-dirty","bluetooth":"3.0","processor":"ARMv7 Processor rev 1 (v7l)","hardware":"g3","os_incremental":"417ae65dbf","device_type":"mobile","name":"g3_vzw_us","display":"liquid_vs985-userdebug 5.0.2 LRX22G 417ae65dbf test-keys","cpu_arch":"3","cpu_abi":"armeabi-v7a","cpu_abi2":"armeabi","User-Agent":"Mozilla/5.0 (Linux; Android 5.0.2; ko-kr; LG-VS985 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.49 Mobile Safari/<WebKit Rev>","mac":"bc:l4:09:cp:04:28","private_ip":"192.168.240.57","imei":"356833057305462","adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0","android_id":"cpb2bb54241u9l6u","serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"}},
				"Google Pixel 2": {"android_id":"f3fcd734302fb1ff","brand":"google","manufacturer":"Google","model":"Pixel 2","name":"walleye","device":"walleye","product":"walleye","os_name":"android","display_height":"1794","display_width":"1080","dpi":"683","resolution":"1794x1080","fingerprint":"google\/walleye\/walleye:9\/PQ2A.190305.002\/5240760:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"9","sdk":"28","os_incremental":"5240760","build":"PQ2A.190305.002","display":"PQ2A.190305.002","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 9; Pixel 2 Build\/PQ2A.190305.002)","screen_size":"5.0","mainboard":"walleye","mac":"02:00:00:00:00:00","private_ip":"192.168.1.148","imei":"357537083118935","bluetooth":"Yes","kernel":"","hardware":"PQ2A.190305.002","cpu_core":"7","cpu_frequency":{"Max":"1900.0","Min":"300.0"},"processor":"7","flash":"128GB","flash_actual":"119 GB","ram":{"available":"3656","shown":"0"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"One plus 5T": {"android_id":"b2748330cad229ff","brand":"OnePlus","manufacturer":"OnePlus","model":"ONEPLUS A5010","name":"OnePlus5T","device":"OnePlus5T","product":"OnePlus5T","os_name":"android","display_height":"2046","display_width":"1080","dpi":"861","resolution":"2046x1080","fingerprint":"OnePlus\/OnePlus5T\/OnePlus5T:9\/PKQ1.180716.001\/1907311828:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"9","sdk":"28","os_incremental":"1907311828","build":"PKQ1.180716.001","display":"ONEPLUS A5010_43_190731","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 9; ONEPLUS A5010 Build\/PKQ1.180716.001)","screen_size":"6.1","mainboard":"msm8998","mac":"02:00:00:00:00:00","private_ip":"192.168.1.163","imei":"866817033854233","bluetooth":"Yes","kernel":"","hardware":"ONEPLUS A5010_43_190731","cpu_core":"7","cpu_frequency":{"Max":"1900.0","Min":"300.0"},"processor":"7","flash":"64GB","flash_actual":"56.62 GB","ram":{"available":"5713","shown":"0"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"Vivo 1606": {"android_id":"8fd7dbd1ef3edd1b","brand":"vivo","manufacturer":"vivo","model":"vivo 1606","name":"1606","device":"1606","product":"1606","os_name":"android","display_height":"960","display_width":"540","dpi":"640","resolution":"960x540","fingerprint":"vivo\/1606\/1606:6.0.1\/MMB29M\/compiler01052117:user\/release-keys","device_type":"False","cpu_abi":"armeabi-v7a","cpu_abi2":"armeabi","cpu_arch":"armv7l","os_version":"6.0.1","sdk":"23","os_incremental":"eng.compiler.20190105.211334","build":"MMB29M","display":"MMB29M release-keys","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 6.0.1; vivo 1606 Build\/MMB29M)","screen_size":"4.6","mainboard":"QC_Reference_Phone","mac":"02:00:00:00:00:00","private_ip":"192.168.0.10","imei":"865116032579599","bluetooth":"Yes","kernel":"Linux version 3.18.24-perf-ga8e3b8c0a (compiler@compiler026) (gcc version 4.8 (GCC) )","hardware":"MMB29M release-keys","cpu_core":"3","cpu_frequency":{"Max":"1401.0","Min":"960.0"},"processor":"3","flash":"16GB","flash_actual":"10.11 GB","ram":{"available":"1877","shown":"0"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"Oppo A37 fw": {"android_id":"c78d18f8f8962c07","brand":"OPPO","manufacturer":"OPPO","model":"A37fw","name":"A37f","device":"A37f","product":"A37fw","os_name":"android","display_height":"1280","display_width":"720","dpi":"640","resolution":"1280x720","fingerprint":"OPPO\/A37fw\/A37f:5.1.1\/LMY47V\/1510797605:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"5.1.1","sdk":"22","os_incremental":"1456818039","build":"LMY47V","display":"A37fwEX_11_171209","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 5.1.1; A37fw Build\/LMY47V)","screen_size":"4.6","mainboard":"msm8916","mac":"4c:18:9a:cc:9b:43","private_ip":"192.168.1.131","bluetooth":"Yes","kernel":"Linux version 3.10.49-perf-g88f85af-01176-g6e3e11e (root@ubuntu-122-192) (gcc version 4.9.x-google 20140827 (prerelease) (GCC) )","hardware":"A37fwEX_11_171209","cpu_core":"3","cpu_frequency":{"Max":"1209.0","Min":"200.0"},"processor":"3","flash":"16GB","flash_actual":"10.72GB","ram":{"available":"1888","shown":"1944"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"Lenovo PB2 650M": {"android_id":"40c754372364ef4a","brand":"Lenovo","manufacturer":"LENOVO","model":"Lenovo PB2-650M","name":"PB2","device":"PB2","product":"PB2-650M","os_name":"android","display_height":"1280","display_width":"720","dpi":"853","resolution":"1280x720","fingerprint":"Lenovo\/PB2-650M\/PB2:6.0\/MRA58K\/PB2-650M_S050_181101_ROW:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"6.0","sdk":"23","os_incremental":"PB2-650M_S050_181101_ROW","build":"MRA58K","display":"PB2-650M_S050_181101_ROW","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 6.0; Lenovo PB2-650M Build\/MRA58K)","screen_size":"6.1","mainboard":"Lenovo_PB2-650M","mac":"02:00:00:00:00:00","private_ip":"192.168.0.12","imei":"869986021393913","bluetooth":"Yes","kernel":"Linux version 3.18.19+ (fengjiyong@wt) (gcc version 4.9.x-google 20140827 (prerelease) (GCC) )","hardware":"PB2-650M_S050_181101_ROW","cpu_core":"3","cpu_frequency":{"Max":"1300.0","Min":"299.0"},"processor":"3","flash":"32GB","flash_actual":"25.56 GB","ram":{"available":"2933","shown":"0"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"LAVA X81": {"android_id":"15e70813076b57b2","brand":"Lava","manufacturer":"LAVA","model":"X81","name":"X81","device":"X81","product":"X81","os_name":"android","display_height":"1280","display_width":"720","dpi":"640","resolution":"1280x720","fingerprint":"Lava\/X81\/X81:6.0\/MRA58K\/1470576607:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"6.0","sdk":"23","os_incremental":"1470576607","build":"MRA58K","display":"LAVA_X81_S117_20160807","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 6.0; X81 Build\/MRA58K)","screen_size":"4.6","mainboard":"unknown","mac":"02:00:00:00:00:00","private_ip":"192.168.0.23","imei":"356833057305462","bluetooth":"Yes","kernel":"Linux version 3.18.19 (admin@lava-Precision-Tower-3620) (gcc version 4.9.x-google 20140827 (prerelease) (GCC) )","hardware":"LAVA_X81_S117_20160807","cpu_core":"3","cpu_frequency":{"Max":"1300.0","Min":"299.0"},"processor":"3","flash":"16GB","flash_actual":"10.19 GB","ram":{"available":"2934","shown":"3002"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"},
				"LGE-LM-G710": {"android_id":"527c135932933016","brand":"lge","manufacturer":"LGE","model":"LM-G710","name":"judyln","device":"judyln","product":"judyln_lao_com","os_name":"android","display_height":"2952","display_width":"1440","dpi":"843","resolution":"2952x1440","fingerprint":"lge\/judyln_lao_com\/judyln:8.0.0\/OPR1.170623.032\/190492326deb8.FGN:user\/release-keys","device_type":"False","cpu_abi":"arm64-v8a","cpu_abi2":"","cpu_arch":"aarch64","os_version":"8.0.0","sdk":"26","os_incremental":"190492326deb8.FGN","build":"OPR1.170623.032","display":"OPR1.170623.032","os_codename":"REL","User-Agent":"Dalvik\/2.1.0 (Linux; U; Android 8.0.0; LM-G710 Build\/OPR1.170623.032)","screen_size":"5.9","mainboard":"sdm845","mac":"02:00:00:00:00:00","private_ip":"192.168.1.102","imei":"356900091494733","bluetooth":"Yes","kernel":"Linux version 4.9.65-perf (lgmobile@si-sbs-bld103) (gcc version 4.9.x 20150123 (prerelease) (GCC) )","hardware":"OPR1.170623.032","cpu_core":"7","cpu_frequency":{"Max":"1766.0","Min":"300.0"},"processor":"7","flash":"128GB","flash_actual":"112 GB","ram":{"available":"5644","shown":"0"},"serial":"cbpp9u65acl6143l","country":"SouthKorea","network":"WiFi","carrier":"KT","mnc":"08","mcc":"450","timezone":"+0900","local_tz":"KST","local_timezone":"Korea Standard Time","local_tz_name":"Asia/Seoul","locale":{"language":"ko","country":"KR"},"gpu":{"name":"Adreno (TM) 330","vendor":"Qualcomm","version":"OpenGL ES 3.0 V@66.0 AU@04.04.02.087.006 (CL@)","opengl_version":"196610"},"internal_memory":{"shown":"3002","available":"2912"},"adid":"d4bc2677-06b6-46e1-8b38-7a26cf1dd8b0"}
			}

		device_data = devices.get(device.strip())

	elif platform == "ios":
		from static_data_lists import ios_required_functions
		from ios_creator_base import output_file_formatting, tracking_def_list
		required_functions = ios_required_functions
		device_data = { "User-Agent" : "Mozilla/5.0 (iPhone; CPU iPhone OS 9_2_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13D15 Safari/601.1","build" : "13D15","carrier" : "Verizon","cfnetwork" : "758.2.8_15.0.0","chipset" : "Apple A6","country" : "Usa","cpu" : {   "core" : "2",   "name" : "Swift, ARM v7",   "speed" : "1.3"   },"cpu_type" : "CPU_SUBTYPE_ARM_V7","device" : "iPhone 5 CDMA","device_platform" : "iPhone5,2","device_type" : "iPhone","dpi" : "326","gpu" : { "memory" : "48",   "name" : "PowerVR SGX 543MP3"   },"hardware" : " N49AP","local_timezone" : "Mountain Time Zone","local_tz" : "MST","local_tz_name" : "America/Phoenix","locale" : { "country" : "US",   "language" : "en"   },"mac" : "80:49:71:fd:a0:1b","mcc" : "310","mnc" : "005","model" : "A1429","network" : "WiFi","os_version" : "9.2.1","private_ip" : "10.0.247.119","ram" : "1024","resolution" : "640x1136","screen_size" : "4","timezone" : "-0700","total_disk" : "64"  }
	########################################################################################

	"""* 
		>>>>LOGS FILTER INITIALIZE<<<<
	*"""
	a = filter_logs_data(logs, platform, key)
	calls_data_dict = a[0]
	genuine_hosts_data = a[1]
	event_list = a[2]
	event_data_list = a[3]

	########################################################################################

	"""* 
		>>>>EXECUTE SCRIPT<<<<
	*"""
	# print '################################################'
	r = run(file_name,device_data)
	script_data = r[0]
	if(',' in r[1]):
		st = r[1].split(',').strip()
	else:
		st = [r[1]]
	sript_trackers = [t.lower() for t in st]

	# print '################################################'

	########################################################################################

	"""* 
		>>>>GET TRACKINGS<<<<
	*"""
	trackers = access_required_data(calls_data_dict).get_hosts()
	if 'Adbrix' in trackers and not key:
		shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)  #
		os.remove(getPath + "/Input/res/source.zip")
		if "event.adbrix.io/api/v1/event/single/" in calls_data_dict.get('host') or "event.adbrix.io/api/v1/event/bulk/" in calls_data_dict.get('host'):
			return "REQUIRES MULTIPART KEY"
		else:
			return "REQUIRES KEY"

	trackers = [t.lower() for t in trackers]
	print "All trackers present in logs:- "
	print trackers
	########################################################################################
	"""*
		>>>>READING LOGS DATA<<<<
	*"""
	logs_data = {}
	for x in range(len(calls_data_dict.get('url'))):
		logs_data[calls_data_dict.get('url')[x].replace('https://','http://')] = {'method':calls_data_dict.get('method')[x],'headers':calls_data_dict.get('headers')[x],'params':calls_data_dict.get('params')[x],'data':calls_data_dict.get('data')[x]}
	if not script_data:
		raise Exception("ERROR IN SCRIPT")
	missingData = findMissing(script_data,logs_data)
	extraData = findExtras(script_data,logs_data)
	diff = dataDifferences(script_data,logs_data)
	differentData = diff[0]
	differentNestedData = diff[1]
	########################################################################################
	"""*
		>>>>COMPUTING DIFFERENCES DATA<<<<
	*"""
	differences=''

	if globals().get('script_appVersion') != globals().get('logs_appVersion'):
		differences+='APP VERSION UPDATED FROM '+globals().get('script_appVersion')+' TO '+globals().get('logs_appVersion')
		differences+='\n\n'

	newTracker = list(set(trackers)-set(sript_trackers))
	if newTracker:
		differences+= 'NEW TRACKING FOUND IN LOGS:-\n'
		differences+= '\t\t'+str(newTracker)+'\n\n'

	if missingData[0]:
		differences+= 'NEW CALLS FOUND IN LOGS:-\n'
		for call in missingData[0]: differences+='\t'+str(call)+'\n\n'

	if extraData[0]:
		differences+= 'EXTRA CALLS FOUND IN SCRIPT:-\n'
		for call in extraData[0]: differences+='\t'+str(call)+'\n\n'
	
	if differences:
		differences+="#"*40+'**MISSING AND EXTRA KEYS**'+"#"*40+'\n\n'

	missingHeaderKeys=missingData[1]
	missingParamsKeys=missingData[2]
	missingDataKeys=missingData[3]
	missingNestedDataKeys=missingData[4]

	extraHeaderKeys=extraData[1]
	extraParamsKeys=extraData[2]
	extraDataKeys=extraData[3]
	
	for url in logs_data:
		if missingHeaderKeys.get(url) or missingParamsKeys.get(url) or missingDataKeys.get(url):
			differences+= "\n"+str(url)+"\n"
		if missingHeaderKeys.get(url):
			differences+= "\tMISSING HEADERS KEYs:-\n"
			differences+= "\t\t"+str(list(missingHeaderKeys.get(url)))+"\n\n"
		if missingParamsKeys.get(url):
			differences+= "\tMISSING PARAMS KEYs:-\n"
			differences+= "\t\t"+str(list(missingParamsKeys.get(url)))+"\n\n"
		if missingDataKeys.get(url):
			differences+= "\tMISSING DATA KEYs:-\n"
			differences+= "\t\t"+str(list(missingDataKeys.get(url)))+"\n\n"
		if missingNestedDataKeys.get(url):
			flag=True
			for s in missingNestedDataKeys.get(url).keys():
				if missingNestedDataKeys.get(url).get(s):
					if flag:
						differences+= str(url)+"\n"
						differences+= "\tMISSING NESTED DICTIONARY DATA KEYs:-\n"
						differences+= "\t\t"
						flag=False
					differences+= s+':-\n'
					differences+= "\t\t\t"+str(list(missingNestedDataKeys.get(url).get(s)))
			differences+= "\n\n"
	
	for url in script_data:
		if extraHeaderKeys.get(url) or extraParamsKeys.get(url) or extraDataKeys.get(url):
			differences+= str(url)+"\n"
		if extraHeaderKeys.get(url):
			differences+= "\tEXTRA HEADERS KEYs:-\n"
			differences+= "\t\t"+str(list(extraHeaderKeys.get(url)))+"\n\n"
		if extraParamsKeys.get(url):
			differences+= "\tEXTRA PARAMS KEYs:-\n"
			differences+= "\t\t"+str(list(extraParamsKeys.get(url)))+"\n\n"
		if extraDataKeys.get(url):
			differences+= "\tEXTRA DATA KEYs:-\n"
			differences+= "\t\t"+str(list(extraDataKeys.get(url)))+"\n\n"
	
	if differences:
		differences+="#"*40+'**CHANGES IN DATA VALUES**'+"#"*40+'\n\n'

	for url in differentData:
		if differentData.get(url):
			differences+= str(url)+"\n"
			differences+= "\tCHANGES IN DATA(OLD--->NEW):-\n"
			for k in differentData.get(url):
				differences+= "\t\t"+k+":- "+str(differentData.get(url).get(k))+"\n\n"
				if differentNestedData.get(url).get(k):
					differences+= "\t\t\tCHANGES IN NESTED DATA(OLD--->NEW):-\n"
					for l in differentNestedData.get(url).get(k):
						differences+= "\t\t\t\t"+l+":- "+str(differentNestedData.get(url).get(k).get(l))+"\n\n"


	if not differences:
		differences = "NO CHANGES FOUND IN LOGS AND SCRIPT KEYS, HOWEVER DATA VALUES MIGHT BE DIFFERENT. \nPLEASE KINDLY MATCH DATA MANUALLY ONCE!!\n\n \t !!!!**DO NOT RELY ON THIS MODULE**!!!"

	################### WRITTING SCRIPT ######################
															 #
	f = open(getPath + "/Output/" + file_name + "_update_differences.txt", 'a')#  
	f.write(differences)											     #
	f.close()												 #
	shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
	os.remove(getPath + "/Input/res/source.zip")			 #
	##########################################################

	return True

def findMissing(script,logs):
	missingCalls=[]
	missingDataKeys={}
	missingNestedDataKeys={}
	missingParamsKeys={}
	missingHeaderKeys={}
	missingDataDict={}

	for url in logs:
		logsNestedData={}
		scriptNestedData={}
		if url not in script.keys():
			missingCalls.append(url)
		else:
			if logs.get(url).get('data'):
				logsData = sorted(logs.get(url).get('data').keys())
				for k in logsData:
					if type(logs.get(url).get('data').get(k)) == dict:
						logsNestedData[k]=logs.get(url).get('data').get(k).keys()
					if k=='app_version_name' or k=='app_version':
						globals()['logs_appVersion'] = str(logs.get(url).get('data').get(k))
					if k=='app_version_short':
						globals()['logs_appVersion'] = str(logs.get(url).get('data').get(k))
					
			else:
				logsData = []
			if logs.get(url).get('params'):
				logsParams = logs.get(url).get('params').keys()
			else:
				logsParams = []
			if logs.get(url).get('headers'):
				logsHeaders = logs.get(url).get('headers').keys()
			else:
				logsHeaders = []
			if script.get(url).get('data'):
				scriptData=rawToDict(str(script.get(url).get('data')))

				if type(scriptData) == dict:
					for k in sorted(scriptData.keys()):
						if type(scriptData.get(k)) == dict:
							scriptNestedData[k]=scriptData.get(k).keys()
						if k=='app_version_name' or k=='app_version':
							globals()['script_appVersion'] = str(scriptData.get(k))
						if k=='app_version_short':
							globals()['script_appVersion'] = str(scriptData.get(k))	
					scriptData = scriptData.keys()
			else:
				scriptData = []
			
			if script.get(url).get('params'):
				scriptParams = script.get(url).get('params').keys()
			else:
				scriptParams = []
			if script.get(url).get('headers'):
				scriptHeaders = script.get(url).get('headers').keys()
			else:
				scriptHeaders = []
			
			missingDataKeys[url] = set(logsData)-set(scriptData)
			missingParamsKeys[url] = set(logsParams)-set(scriptParams)
			missingHeaderKeys[url] = set(logsHeaders)-set(scriptHeaders)
			
			if logsNestedData or scriptNestedData:
				missingNestedDataKeys[url]={}

			for key in logsNestedData.keys():
				if missingNestedDataKeys[url].get(key):
					missingNestedDataKeys[url][key]=set(logsNestedData[key])-set(scriptNestedData[key])

	return missingCalls, missingHeaderKeys, missingParamsKeys, missingDataKeys, missingNestedDataKeys

def findExtras(script,logs):
	extraCalls=[]
	extraDataKeys={}
	extraParamsKeys={}
	extraHeaderKeys={}
	for url in script:
		if url not in logs.keys():
			for tracker in globals().get('supportedTrackers'):
				if tracker in url:
					extraCalls.append(url)
		else:
			if logs.get(url).get('data'):
				logsData = logs.get(url).get('data').keys()
			else:
				logsData = []
			if logs.get(url).get('params'):
				logsParams = logs.get(url).get('params').keys()
			else:
				logsParams = []
			if logs.get(url).get('headers'):
				logsHeaders = logs.get(url).get('headers').keys()
			else:
				logsHeaders = []
			if script.get(url).get('data'):
				scriptData = rawToDict(str(script.get(url).get('data')))
			else:
				scriptData = []
			if script.get(url).get('params'):
				scriptParams = script.get(url).get('params').keys()
			else:
				scriptParams = []
			if script.get(url).get('headers'):
				scriptHeaders = script.get(url).get('headers').keys()
			else:
				scriptHeaders = []
			
			extraDataKeys[url] = set(scriptData)-set(logsData)-set(globals()['ExtrakeysToignore'])
			extraParamsKeys[url] = set(scriptParams)-set(logsParams)-set(globals()['ExtrakeysToignore'])
			extraHeaderKeys[url] = set(scriptHeaders)-set(logsHeaders)-set(globals()['ExtrakeysToignore'])

	return extraCalls, extraHeaderKeys, extraParamsKeys, extraDataKeys


def dataDifferences(script,logs):
	differentData = {}
	differentNestedData = {}
	for url in script:
		if url in logs.keys():
			if script.get(url).get('data'):
				scriptData = rawToDict(str(script.get(url).get('data')))
			else:
				scriptData = []

			for key in scriptData:
				if scriptData.get(key)!=logs.get(url).get('data').get(key) and key not in globals().get('exceptions'):
				# if scriptData.get(key)!=logs.get(url).get('data').get(key):
					newUrl = url
					if key=='eventName' and 'androidevent' in url and 'appsflyer' in url:
						newUrl = url+'-->('+scriptData.get(key)+')'
					if not differentData.get(newUrl):
						differentData[newUrl]={}
					if not differentNestedData.get(newUrl):
						differentNestedData[newUrl]={}
					if type(scriptData.get(key))==dict:
						for k in scriptData.get(key):
							print logs.get(url).get('data')
							print key
							if logs.get(url).get('data').get(key):
								if scriptData.get(key).get(k)!=logs.get(url).get('data').get(key).get(k) and k not in globals().get('exceptions'):
									if not differentNestedData.get(newUrl).get(key):
										differentNestedData[newUrl][key]={}	
									differentNestedData[newUrl][key][k] = str(scriptData.get(key).get(k))+'--->'+str(logs.get(url).get('data').get(key).get(k))
					else:		
						differentData[newUrl][key] = str(scriptData.get(key))+'--->'+str(logs.get(url).get('data').get(key))

	return differentData, differentNestedData

def rawToDict(DATA):
	if "{" in DATA:
		try:
			jsonDict = json.loads(DATA.strip('\n'))
		except:
			try:
				jsonDict = eval(DATA.strip('\n'))
			except:
				try:
					jsonDict = DATA.strip('\n')
				except:
					jsonDict = ["UNREADABLE DICTIONARY"]
	else:
		try:
			jsonDict = dict(urlparse.parse_qsl(DATA, keep_blank_values=True))
		except:
			jsonDict = ["Unrecognized Data"]

	return jsonDict