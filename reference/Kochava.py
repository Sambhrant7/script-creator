import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n", "import uuid\n", "import random\n", "import time\n", "import urlparse\n", "import json\n", "import urllib\n", "import datetime\n", "import clicker\n", "import Config\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	
	# Uncomment and use this as per your own event requirement.
	
	###########		FINALIZE	################
	# set_appCloseTime(app_data)
	
	return {'status':'true'}
\n"""

definitions={"kvinit-prod.api.kochava.com/track/kvinit":{"start":"""
def kochavaKvinit(campaign_data, app_data, device_data):
	set_kochava_device_id(app_data)
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),5)\n""",
"url":"""'http://kvinit-prod.api.kochava.com/track/kvinit'""",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{       "action": 'CONSTANT',
        "data": {       "locale": "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
                        "package": "campaign_data.get('package_name')",
                        "platform": 'CONSTANT',
                        "timezone": "device_data.get('local_tz_name')",
                        "uptime": "float('0.'+str(random.randint(100,999)))",
                        "usertime": "int(time.time())",
                        "os_version":""""Android "+device_data.get('os_version')""",},
        "kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
        "kochava_device_id": "app_data.get('kochava_device_id')",
        "nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
        "sdk_build_date": "campaign_data.get('kochava').get('sdk_build_date')",
        "sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
        'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
        "sdk_version": "campaign_data.get('kochava').get('sdk_version')",
        "send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",

        },
'return':"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""

},
"control.kochava.com/track/kvquery":{
	"start":"""
def kv_Query(campaign_data,app_data,device_data):
	set_kochava_device_id(app_data)\n""",
"url":"""'http://control.kochava.com/track/kvquery'""",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
		"action":'CONSTANT',
		"data":{		    
		    'uptime':"app_data.get('uptime')",
		    'usertime':"int(app_data.get('previous_Call_time'))",
		},
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id":"app_data.get('kochava_device_id')",
		"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
		"sdk_protocol":"campaign_data.get('kochava').get('sdk_protocol')",
		'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
		"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		
		},
'return':"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->identityLink":{
	"start":"""
def kochava_identityLink(campaign_data, app_data, device_data):
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),5)\n""",
"url":"""'http://control.kochava.com/track/json'""",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
	"action": 'CONSTANT',
	"data": {	"uptime": "app_data.get('uptime')",
				"userid": 'CONSTANT',
				"usertime": "int(app_data.get('previous_Call_time'))",
				"account_id":'CONSTANT',
				"control_number":'CONSTANT',
				"language" : "device_data.get('locale').get('language')",
				"mobile_number":'CONSTANT',
				"user_app_version": "campaign_data.get('app_version_name')",
				"device_details":"'Android '+device_data.get('device')+' '+device_data.get('model')",
			},
	"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
	"kochava_device_id": "app_data.get('kochava_device_id')",
	"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
	"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
	'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
	"sdk_version": "campaign_data.get('kochava').get('sdk_version')",
	"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
	},
'return':"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->initial":{
	"start":"""
def kochava_initial(campaign_data, app_data, device_data):	
	set_kochava_device_id(app_data)
	get_screen_inches(app_data)
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),5)
	# email_ids=''

	# for i in range(random.randint(3,6)):
	# 	return_userinfo(app_data)
	# 	email_ids+=app_data.get('user_info').get('email')+','
	# email_ids = email_ids[:-1]\n""",

"url":"'http://control.kochava.com/track/json'",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
		"action": 'CONSTANT',
		"data": {
				"adid": "device_data.get('adid')",
				"android_id": "device_data.get('android_id')",
				"app_limit_tracking": 'BOOLEAN',
				"app_short_string": "campaign_data.get('app_version_name')",
				"app_version": """campaign_data.get('app_name')+' '+campaign_data.get('app_version_code')""",
				"architecture": "device_data.get('cpu_abi')",
				"battery_level": "app_data.get('battery_level')",
				"battery_status": 'CONSTANT',
				"bssid": "device_data.get('mac')",
				"carrier_name": "device_data.get('carrier')",
				"connected_devices": 'BOOLEAN',
				"conversion_data": "get_conversion_data(app_data)",
				"conversion_type": 'CONSTANT',
				"device": """device_data.get('model')+"-"+device_data.get('brand')""",
				"device_cores": "int(device_data.get('cpu_core'))",
				"device_limit_tracking": 'BOOLEAN',
				"device_orientation": 'CONSTANT',
				"disp_h": "int(device_data.get('resolution').split('x')[0])",
				"disp_w": "int(device_data.get('resolution').split('x')[1])",
				'ids':"[str(email_ids)]",
				"instant_app":'BOOLEAN',	
				"install_referrer": 
					{
						"attempt_count": 'BOOLEAN',
						"duration": "float('0.0'+str(random.randint(11,99)))",
						"install_begin_time": "int(app_data.get('times').get('download_begin_time'))",
						"referrer": "get_conversion_data(app_data)",
						"referrer_click_time": "int(app_data.get('times').get('click_time'))",
						"status": 'CONSTANT'
					},
				"installed_date": "int(app_data.get('times').get('install_complete_time'))",
				"installer_package": 'CONSTANT',
				"is_genuine": 'BOOLEAN',
				"language": "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
				"locale": "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
				"manufacturer": "device_data.get('manufacturer')",
				"network_conn_type": 'CONSTANT',
				"network_metered": 'BOOLEAN',
				"notifications_enabled": 'BOOLEAN',
				"os_version": '"Android "+device_data.get("os_version")',
				"package": "campaign_data.get('package_name')",
				"product_name": "device_data.get('product')",
				"screen_brightness": "app_data.get('screen_brightness')",
				"screen_dpi": "int(device_data.get('dpi'))",
				"screen_inches": "float(app_data.get('screen_inches')) if app_data.get('screen_inches') else get_screen_inches()",
				"ssid":"device_data.get('carrier')",
				"state_active": 'BOOLEAN',
				"timezone": "device_data.get('local_tz_name')",
				"ui_mode": 'CONSTANT',
				"uptime": "app_data.get('uptime')",
				"usertime": "int(app_data.get('previous_Call_time'))",
				"volume": "app_data.get('volume')"
			},
		"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id": "app_data.get('kochava_device_id')",
		"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
		"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_procotol":"campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_version": "campaign_data.get('kochava').get('sdk_version')",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		},
'return':"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->session":{"start":"""
def kochava_session(campaign_data, app_data, device_data, state='resume'):
	set_kochava_device_id(app_data)
	get_screen_inches(app_data)\n""",
"url":"'http://control.kochava.com/track/json'",
"method":'post',
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
		"action": 'CONSTANT',
		"data": {						
				"app_short_string": "campaign_data.get('app_version_name')",
				"app_version": """campaign_data.get('app_name')+' '+campaign_data.get('app_version_code')""",
				"architecture": "device_data.get('cpu_abi')",
				"battery_level": "app_data.get('battery_level')",
				"battery_status": 'CONSTANT',
				"bssid": "device_data.get('mac')",
				"carrier_name": "device_data.get('carrier')",
				"connected_devices": 'BOOLEAN',				
				"device": """device_data.get('model')+"-"+device_data.get('brand')""",				
				"device_orientation": 'CONSTANT',
				"disp_h": "int(device_data.get('resolution').split('x')[0])",
				"disp_w": "int(device_data.get('resolution').split('x')[1])",		
				"instant_app":'BOOLEAN',		
				"locale": "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
				"manufacturer": "device_data.get('manufacturer')",
				"network_conn_type": 'CONSTANT',
				"network_metered": 'BOOLEAN',
				"notifications_enabled": 'BOOLEAN',
				"os_version": '''"Android "+device_data.get('os_version')''',				
				"product_name": "device_data.get('product')",
				"screen_brightness": "app_data.get('screen_brightness')",
				"screen_dpi": "int(device_data.get('dpi'))",				
				"ssid":"device_data.get('carrier')",
				'state':"state",
				"state_active": 'BOOLEAN',
				"timezone": "device_data.get('local_tz_name')",
				"ui_mode": 'CONSTANT',
				"uptime": "app_data.get('uptime')",
				"updelta":"app_data.get('uptime')",
				"usertime": "int(app_data.get('previous_Call_time'))",
				"volume": "app_data.get('volume')"
			},
		"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id": "app_data.get('kochava_device_id')",
		"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
		"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
		'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_version": "campaign_data.get('kochava').get('sdk_version')",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		},
'return':"""
	if state=='pause':
		data['data']['state_active_count']=1
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},
"control.kochava.com/track/json->event":{
	"start":"""
def kochava_event(campaign_data, app_data, device_data, event_name='', event_data=''):
	set_kochava_device_id(app_data)
	get_screen_inches(app_data)
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),5)\n""",
"url":"'http://control.kochava.com/track/json'",
"method":'post',
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
		"action": 'CONSTANT',
		"currency": "CONSTANT",
		"data": {						
				"app_short_string": "campaign_data.get('app_version_name')",
				"app_version": """campaign_data.get('app_name')+' '+campaign_data.get('app_version_code')""",
				"architecture": "device_data.get('cpu_abi')",
				"battery_level": "app_data.get('battery_level')",
				"battery_status": "CONSTANT",
				"bssid": "device_data.get('mac')",
				"carrier_name": "device_data.get('carrier')",
				"currency":'CONSTANT',
				"connected_devices": 'CONSTANT',				
				"device": """device_data.get('model')+"-"+device_data.get('brand')""",				
				"device_orientation": 'CONSTANT',
				"disp_h": "int(device_data.get('resolution').split('x')[0])",
				"disp_w": "int(device_data.get('resolution').split('x')[1])",	
				'event_name':"event_name",
				'event_data':"event_data",	
				"instant_app":'BOOLEAN',	
				'last_post_time':'CONSTANT',		
				"locale": """device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')""",
				"manufacturer": "device_data.get('manufacturer')",
				"network_conn_type": "CONSTANT",
				"network_metered": 'BOOLEAN',
				"notifications_enabled": 'BOOLEAN',
				"os_version": """"Android "+device_data.get('os_version')""",				
				"product_name": "device_data.get('product')",
				'signal_bars':'BOOLEAN',
				"screen_brightness": "app_data.get('screen_brightness')",
				"screen_dpi": "int(device_data.get('dpi'))",				
				"ssid":"device_data.get('carrier')",
				'state_active':'BOOLEAN',				
				"timezone": "device_data.get('local_tz_name')",
				"ui_mode": "CONSTANT",
				"updelta":"app_data.get('uptime')",
				"uptime": "app_data.get('uptime')",
				"usertime": "int(app_data.get('previous_Call_time'))",
				"volume": "app_data.get('volume')"
			},
		"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id": "app_data.get('kochava_device_id')",
		"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
		"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
		'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_version": "campaign_data.get('kochava').get('sdk_version')",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		},
'return':"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},
"control.kochava.com/track/kvinit":{
	"start":"""
def Kvinit(campaign_data, app_data, device_data):
	set_kochava_device_id(app_data)\n""",
"url":"'http://control.kochava.com/track/kvinit'",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data" : {
		"action":"CONSTANT",
		"data":{
			"currency" :"CONSTANT",
			"session_tracking":"CONSTANT",
			"platform":"CONSTANT",
			"package":"campaign_data.get('package_name')",	
			"uptime": "float('0.'+str(random.randint(100,999)))",
            "usertime": "int(time.time())"   ,
            "locale":"device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')", 
            'timezone' :"device_data.get('local_tz_name')",
            "os_version":""""Android "+device_data.get('os_version')""",
            "last_post_time":'BOOLEAN',
            "partner_name":'CONSTANT',
			},
	    "data_orig":{
	    	'currency' :'CONSTANT',
	    	"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
	    	'session_tracking':'CONSTANT'
	    },
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id":"app_data.get('kochava_device_id')",
		"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
		"sdk_protocol":"campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_procotol":"campaign_data.get('kochava').get('sdk_protocol')",
		"sdk_build_date":"campaign_data.get('sdk_build_date')",
		"nt_id": "app_data.get('nt_id_first')+str(uuid.uuid4())",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",

	},
"return":"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/kvTracker.php->initial":{
	"start":"""
def android_KvTracker_initial(campaign_data, app_data, device_data):	
	set_kochava_device_id(app_data)
	app_data['uptime']=int(time.time())-app_data.get('previous_Call_time')\n""",
"url":"'http://control.kochava.com/track/kvTracker.php'",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},
"params":{},
"data":{
		"action":"CONSTANT",
		"currency" :'CONSTANT',
		
		"data":{
			"adid":"device_data.get('adid')",
			"android_id":"device_data.get('android_id')",
			"app_limit_tracking":'BOOLEAN',
			"app_short_string":"campaign_data.get('app_version_name')",
			"app_version":"""campaign_data.get('app_name')+' '+campaign_data.get('app_version_code')""",
			"carrier_name":"device_data.get('carrier')",
			"conversion_data":"get_conversion_data(app_data)",
			"conversion_type":"CONSTANT",
			"device":"""device_data.get('device')+'-'+device_data.get('manufacturer')""",
			"device_limit_tracking":'BOOLEAN',
			"device_orientation":"CONSTANT",
			"disp_h":"int(device_data.get('resolution').split('x')[0])",
			"disp_w":"int(device_data.get('resolution').split('x')[1])",
			"fb_attribution_id":"CONSTANT",
			"network_conn_type":"device_data.get('network')",
			"os_version":""""Android "+device_data.get('os_version')""",
			"package_name":"campaign_data.get('package_name')",
			"screen_brightness":"app_data.get('screen_brightness')",
			"uptime":"str(int(app_data.get('uptime')))",
			"updelta":"str(int(app_data.get('uptime')))",
			"usertime":"str(int(time.time()))",
			"volume":"str(app_data.get('volume'))",
		},
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id":"app_data.get('kochava_device_id')",
		"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
		"last_post_time":'BOOLEAN'
	},
"return":"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},

"control.kochava.com/track/kvTracker.php->event":{
	"start":"""
def kochava_tracker_event( campaign_data, device_data, app_data, event_name = "", event_value = "" ):
	set_kochava_device_id(app_data)
	app_data['uptime']=int(time.time())-app_data.get('previous_Call_time')\n""",
"url":"'http://control.kochava.com/track/kvTracker.php'",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},

"params":{},
"data":   {
		"action": "CONSTANT",
		"currency": "CONSTANT",
		"data": {
			"event_data": "event_value",
			"event_name": "event_name",
			"updelta": "str(int(app_data.get('uptime')))",
			"uptime": "str(int(app_data.get('uptime')))",
			"usertime": "str(int(time.time()))"
			},
		"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id": "app_data.get('kochava_device_id')",
		"last_post_time": 'BOOLEAN'},
"return":"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""

},
"control.kochava.com/track/kvTracker->session":{
	"start":"""
def kochava_tracker_session( campaign_data, device_data, app_data, state = "" ):
	set_kochava_device_id(app_data)
	app_data['uptime']=int(time.time())-app_data.get('previous_Call_time')\n""",
"url":"'http://control.kochava.com/track/kvTracker.php'",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},

"params":{},
"data":{
		"action": "CONSTANT",
		"data": {
			"state": "state",
			"updelta": "str(int(app_data.get('uptime')))",
			"uptime": "str(int(app_data.get('uptime')))",
			"usertime": "str(int(time.time()))"
				},
		"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id": "app_data.get('kochava_device_id')"
		},
"return":"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},
"control.kochava.com/track/kvTracker.php->update":{
	"start":"""
def kochava_tracker_update( campaign_data, device_data, app_data ):
	set_kochava_device_id(app_data)\n""",
"url":"http://control.kochava.com/track/kvTracker.php",
"method":"post",
"headers":{
		'User-Agent':"get_ua(device_data)",
		'Content-Type' : 'CONSTANT',
		'X-Unity-Version':'CONSTANT',
		'Accept-Encoding':'CONSTANT',
		},

"params":{},
"data": {
	"action": "CONSTANT",
	"data": {
		"app_short_string": "campaign_data.get('app_version_name')",
		"app_version": """campaign_data.get('app_name')+' '+campaign_data.get('app_version_code')""",
			},
	"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
	"kochava_device_id": "app_data.get('kochava_device_id')",
	"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
	'sdk_procotol':"campaign_data.get('kochava').get('sdk_protocol')",
	"sdk_version": "campaign_data.get('kochava').get('sdk_version')",
	},
"return":"""
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
}



}

kochava_android_required_functions="""

###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid = device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_ua(device_data):
	if int(device_data.get("sdk")) >19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def set_kochava_device_id(app_data):
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA350'+str(int(time.time()))+'t'+ str(uuid.uuid4()).replace('-','') 

def get_conversion_data(app_data):	
	conversion_data="utm_source=ko_c4cc5b7147cedb111&utm_medium=1&utm_campaign=kotopface-android-s073792fc1e7d6f3&utm_term=&utm_content=&"
	if app_data.get('referrer'):
		referrer = urllib.unquote(app_data.get('referrer'))
		referrerpart = urlparse.parse_qs(referrer)
		utm_medium = referrerpart.get('utm_medium')[0] if referrerpart.get('utm_medium') else '(not%20set)'
		utm_content = referrerpart.get('utm_content')[0] if referrerpart.get('utm_content') else '(not%20set)'
		utm_term = referrerpart.get('utm_term')[0] if referrerpart.get('utm_term') else '(not%20set)'
		utm_source = referrerpart.get('utm_source')[0] if referrerpart.get('utm_source') else '(not%20set)'
		utm_campaign = referrerpart.get('utm_campaign')[0] if referrerpart.get('utm_campaign') else '(not%20set)'
		conversion_data = 'utm_source='+utm_source+'&utm_medium='+utm_medium+'&utm_campaign='+utm_campaign+'&utm_term='+utm_term+'&utm_content='+utm_content+'&'
	return conversion_data

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_screen_inches(app_data):
	if not app_data.get('screen_inches'):
		app_data['screen_inches']=random.choice(['5.1','4.7','6','4.7','6.5'])

"""

print len(definitions)