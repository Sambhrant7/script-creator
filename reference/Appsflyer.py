# -*- coding: utf-8 -*-

import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","from sdk.AFSesnorManager import AFSensorManger\n","from sdk import batterypercentage\n","import time\n","import random\n","import json\n","import string\n","import datetime\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n\n#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	# def_appsflyerUID(app_data)
	# def_eventsRecords(app_data)
	# generate_realtime_differences(device_data,app_data)

	# print 'Appsflyer : TRACK____________________________________'
	# request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	# util.execute_request(**request)


	# req = appsflyer_api(campaign_data, app_data, device_datadev)
	# util.execute_request(**req)

	# af_login(campaign_data, app_data,batteryLevel device_data)

	# req = appsflyer_register(campaign_data,app_data,device_data)
	# util.execute_request(**req)
	
	# req = appsflyer_attr(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	# print 'Appsflyer : Stats____________________________________'
	# request  = appsflyer_stats(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	return {'status':'true'}\n\n"""	

definitions={"stats.appsflyer.com":{"start":"""
###################################################################
# appsflyer_stats()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Appsflyer's behaviour whenever user leaves app's context
# example : User drags notification bar,
#			Facebook SignUp page opens in app, etc
###################################################################
def appsflyer_stats(campaign_data, app_data, device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	inc_(app_data,'launchCounter')
	def_(app_data,'counter')
""",
	'method' : 'post',
	'url' : '"https://stats.appsflyer.com/stats"', 
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'Content-Type'	  : 'CONSTANT',
		'User-Agent'	  : 'get_ua(device_data)',
		},
	'params' : None,
	'data' : {
		"advertiserId"			: "device_data.get('adid')",
		"app_id"				: "campaign_data.get('package_name')",
		"channel"				: 'BOOLEAN',
		"deviceFingerPrintId"	: "app_data.get('deviceFingerPrintId')",
		"devkey"				: "campaign_data.get('appsflyer').get('key')",
		"gcd_conversion_data_timing": "0",
		"launch_counter"		: "str(app_data.get('counter'))",
		"originalAppsflyerId"	: "app_data.get('uid')",
		"platform"				: "CONSTANT",
		"statType"				: "CONSTANT",
		"time_in_app"			: "str(int(random.randint(0,2)))",
		"uid"					: "app_data.get('uid')",
		},
	'return':"""
	if app_data.get('time_in_app'):
		data['time_in_app']=str(int(time.time())-app_data['time_in_app'])
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}"""},

"register.appsflyer.com":{
"start":"""
###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
 	app_data['registeredUninstall']=True
""",
	'method' : 'post',
	'url'	: "'http://register.appsflyer.com/api/'+"+"campaign_data.get('appsflyer').get('version')"+"+'/androidevent'" ,
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'Content-Type'	  : 'CONSTANT',
		'User-Agent'	  : 'get_ua(device_data)',
		},	
	'params' : {
		'app_id'		  : "campaign_data.get('package_name')",
		'buildnumber'	  : "campaign_data.get('appsflyer').get('buildnumber')",
		},
	'data' : {
			'advertiserId' 			: "device_data.get('adid')",
			'af_gcm_token'			: "app_data.get('af_gcm_token')",
			'app_name'				: "campaign_data.get('app_name')",
			'app_version_code'		: "campaign_data.get('app_version_code')",
			'app_version_name'		: "campaign_data.get('app_version_name')",
			'brand'					: "device_data.get('brand').upper()",
			'carrier'				: "device_data.get('carrier')",
			'devkey'				: "campaign_data.get('appsflyer').get('key')",
			'channel'				: 'BOOLEAN',
			'deviceFingerPrintId'	: "app_data.get('deviceFingerPrintId')",
			'installDate'			: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
			'install_date'			: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
			'launch_counter'		: "str(app_data.get('counter'))",
			'model'					: "device_data.get('model')",
			'network'				: "device_data.get('network').upper()",
			'operator'				: "device_data.get('carrier')",
			'sdk' 					: "device_data.get('sdk')",	
			'uid' 					: "app_data.get('uid')",
		},

	'return':"return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}"},

"attr.appsflyer.com":{"start":"""
def appsflyer_attr(campaign_data, app_data, device_data,day=0):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_google_gcmToken(app_data)
 	get_deviceData(app_data, device_data)
 	batterypercentage.isBatteryPercent(app_data)
 	datadict={
	'package_name': campaign_data.get('package_name', ''),
    'android_id':device_data.get('android_id'), # device_data
    'day': day,  # 1,2,3,4
    'bt_status': app_data.get('battery_choice'), # app_data
    'bt_level': app_data.get('battery_percentage') # app_data
	}

	try:
		print 'save battery data'
		saveBatterydata(datadict)
	except:
		pass
 """,
	'method'  : 'post',
	'url' 	: "'http://attr.appsflyer.com/api/'+"+"campaign_data.get('appsflyer').get('version')"+"+'/androidevent'", 
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'Content-Type'	  : 'CONSTANT',
		'User-Agent'	  : 'get_ua(device_data)',
		},		
	'params' : {
		'app_id'		  : "campaign_data.get('package_name')",
		'buildnumber'	  : "campaign_data.get('appsflyer').get('buildnumber')",
		},	
	'data'  : {
		"advertiserId"			: "device_data.get('adid')",
		"advertiserIdEnabled"	: "CONSTANT",
		"af_events_api"			: "CONSTANT",
		"af_preinstalled"		: "CONSTANT",
		"af_timestamp"			: "timestamp()",
		'af_gcm_token'			: "app_data.get('af_gcm_token')",
		"android_id"			: "device_data.get('android_id')",
		"app_version_code"		: "campaign_data.get('app_version_code')",
		"app_version_name"		: "campaign_data.get('app_version_name')",
		"appUserId"				: "CONSTANT",
		"appid"		        	: "campaign_data.get('package_name')",
		"appsflyerKey"			: "campaign_data.get('appsflyer').get('key')",
		"brand"					: "device_data.get('brand')",
		"carrier"				: "device_data.get('carrier')",
		"cksm_v1"				: "util.get_random_string('hex',34)",
		"counter"				: "str(app_data.get('counter'))",
		"country"				: "device_data.get('locale').get('country')",
		"date1"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"date2"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"device"				: "device_data.get('device')",
		"deviceData"			: {
									"arch": "",
									"btch": "app_data.get('battery_choice')",
									"btl": "str(app_data.get('battery_percentage'))",
									"build_display_id": "device_data.get('display')",
									"cpu_abi": 'device_data.get("cpu_abi") if device_data.get("cpu_abi") else ""',
									"cpu_abi2": 'device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else ""',
									"dim"	: """{"d_dpi": device_data.get('dpi'),								
								"size": str(device_data.get('display_metrics').get('screen_layout_size')),
								"x_px": device_data.get('display_metrics').get('xpx'),
								"xdp": str(device_data.get('display_metrics').get('xdp')),
								"y_px": device_data.get('display_metrics').get('ypx'),
								"ydp": str(device_data.get('display_metrics').get('ydp')),
								}"""
									},
		"deviceFingerPrintId"	: "app_data.get('deviceFingerPrintId')",
		"deviceRm"				: "device_data.get('display')",
		"deviceType"			: "CONSTANT",
		"firstLaunchDate"		: "app_data.get('firstLaunchDate')",
		"gaidError"				: "CONSTANT",
		"iaecounter"			: "str(app_data.get('iaecounter'))",
		"installDate"			: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"installer_package"		: "CONSTANT",
		"installAttribution"	: "str(json.dumps(app_data.get('installAttribution')))",
		"istu"					: "CONSTANT",
		"imei"					: "device_data.get('imei')",
		"isFirstCall"			: "CONSTANT",
		"isGaidWithGps"			: "CONSTANT",
		"ivc"					: "BOOLEAN",
		"lang"					: "util.get_language_name('en')",
		"lang_code"				: "device_data.get('locale').get('language')",
		"model"					: "device_data.get('model')",
		"network"				: "CONSTANT",
		"operator"				: "device_data.get('carrier')",
		"platformextension"		: "CONSTANT",#"android_native",
		"product"				: "device_data.get('product')",
		"registeredUninstall"   : "app_data.get('registeredUninstall')",
		"sdk"					: "device_data.get('sdk')",	
		"timepassedsincelastlaunch": '0',
		"tokenRefreshConfigured": "BOOLEAN",
		"uid"					: "app_data.get('uid')",
		"referrer"				: "'utm_source=google-play&utm_medium=organic'",
		"open_referrer"			: "'android-app://com.android.vending'",
		"extraReferrers"		: """json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})  if app_data.get('referrer') else None """,
		},			
	'extraReferrers':"""\n	if app_data.get('referrer'):\n		data['extraReferrers']=json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})""",
	"conditional_data":"""
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	else:
		data['referrer'] = 'utm_source=(not%20set)&utm_medium=(not%20set)'

	if app_data.get('timepassedsincelastlaunch'):
		data['timepassedsincelastlaunch'] = str(int(time.time()) - int(app_data.get('timepassedsincelastlaunch')))
		app_data['timepassedsincelastlaunch'] = int(time.time())
	else:
		data['timepassedsincelastlaunch'] = '0'
		app_data['timepassedsincelastlaunch'] = int(time.time())

	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))""",

	'cksm_v1':"""
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])""",

	'kef':"""
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(AF_SENSOR.sensorCount)
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal""",
		
	'return':"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}"""},

"t.appsflyer.com":{"start":"""
###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True,day=0):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-int(app_data.get('timepassedsincelastlaunch'))
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-int(app_data.get('timepassedsincelastlaunch'))

 	batterypercentage.isBatteryPercent(app_data)
 	datadict={
	'package_name': campaign_data.get('package_name', ''),
    'android_id':device_data.get('android_id'), # device_data
    'day': day,  # 1,2,3,4
    'bt_status': app_data.get('battery_choice'), # app_data
    'bt_level': app_data.get('battery_percentage') # app_data
	}

	try:
		print 'save battery data'
		saveBatterydata(datadict)
	except:
		pass


""",
	'method'  : 'post',
	'url' : "'http://t.appsflyer.com/api/'+"+"campaign_data.get('appsflyer').get('version')"+"+'/androidevent'", 
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'Content-Type'	  : 'CONSTANT',
		'User-Agent'	  : 'get_ua(device_data)',
		},		
	'params'  : {
		'app_id'		  : "campaign_data.get('package_name')",
		'buildnumber'	  : "campaign_data.get('appsflyer').get('buildnumber')",
		},	
	'data'  : {
		"advertiserId"			: "device_data.get('adid')",
		"advertiserIdEnabled"	: "CONSTANT",
		'af_gcm_token'			: "app_data.get('af_gcm_token')",
		"af_events_api"			: "CONSTANT",
		'af_sdks'				:'CONSTANT',
		'af_v'					: '',
		'af_v2'					: '',
		"af_preinstalled"		: "CONSTANT",
		"af_timestamp"			: "timestamp()",
		"android_id"			: "device_data.get('android_id')",
		"app_version_code"		: "campaign_data.get('app_version_code')",
		"app_version_name"		: "campaign_data.get('app_version_name')",
		"appsflyerKey"			: "campaign_data.get('appsflyer').get('key')",
		"appUserId"				: "CONSTANT",
		"appid"					: "campaign_data.get('package_name')",
		'batteryLevel'			: "str(app_data.get('battery_percentage'))",
		"brand"					: "device_data.get('brand')",
		"carrier"				: "device_data.get('carrier')",
		"cksm_v1"				: "util.get_random_string('hex',34)",
		"counter"				: "str(app_data.get('counter'))",
		"country"				: "device_data.get('locale').get('country')",
		"currency"				: "'USD'",
		"customData"			: 'BOOLEAN',
		"date1"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"date2"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"device"				: "device_data.get('device')",
		"deviceData"			: {
									"arch": "",
									"btch": "app_data.get('battery_choice')",
									"btl": "str(app_data.get('battery_percentage'))",
									"build_display_id": "device_data.get('display')",
									"cpu_abi": 'device_data.get("cpu_abi") if device_data.get("cpu_abi") else ""',
									"cpu_abi2": 'device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else ""',
									"dim"	: """{"d_dpi": device_data.get('dpi'),								
								"size": str(device_data.get('display_metrics').get('screen_layout_size')),
								"x_px": device_data.get('display_metrics').get('xpx'),
								"xdp": str(device_data.get('display_metrics').get('xdp')),
								"y_px": device_data.get('display_metrics').get('ypx'),
								"ydp": str(device_data.get('display_metrics').get('ydp')),
							}"""
									},
		'deviceRm'				: "device_data.get('display')",
		"deviceFingerPrintId"	: "app_data.get('deviceFingerPrintId')",
		'dkh'					: "campaign_data.get('appsflyer').get('dkh')",
		"deviceType"			: "CONSTANT",
		"firstLaunchDate"		: "app_data.get('firstLaunchDate')",
		"gaidError"				: "CONSTANT",
		"iaecounter"			: "str(app_data.get('iaecounter'))",
		"imei"					: "device_data.get('imei')",
		"installDate"			: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"installer_package"		: "CONSTANT",
		"installAttribution"	: "str(json.dumps(app_data.get('installAttribution')))",
		"isFirstCall"			: "isFirstCall",
		"isGaidWithGps"			: "CONSTANT",
		"istu"					: "CONSTANT",
		"ivc"					: "BOOLEAN",
		"lang"					: "util.get_language_name(device_data.get('locale').get('language'))",
		"lang_code"				: "device_data.get('locale').get('language')",
		"model"					: "device_data.get('model')",
		"network"				: "device_data.get('network').upper()",
		"open_referrer"			: "'android-app://com.android.vending'",
		"operator"				: "device_data.get('carrier')",
		"platformextension"		: "CONSTANT",
		"product"				: "device_data.get('product')",
		"p_receipt"				: "util.get_random_string('google_token',637).replace('-','+').replace('_','/')+'='",
		"referrer"				: "'utm_source=google-play&utm_medium=organic'",
		"registeredUninstall"	: "app_data.get('registeredUninstall')",
		"sdk"					: "device_data.get('sdk')",
		"sc_o" 					: "CONSTANT",
		"tokenRefreshConfigured": "BOOLEAN",	
		"timepassedsincelastlaunch": "str(timeSinceLastCall)",
		"uid"					: "app_data.get('uid')",
		"extraReferrers"		: """json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})  if app_data.get('referrer') else None """
		},
	'extraReferrers':"""\n	if app_data.get('referrer'):\n		data['extraReferrers']=json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})""",
	'conditional_data':"""			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if data.get('p_receipt'):
		raise Exception("Please check time-zone in date1 and date2.")

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=str(app_data.get('battery_percentage'))""",

	'cksm_v1':"""
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])""",

	'kef':"""
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(AF_SENSOR.sensorCount)
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal""",

	'return':"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}"""},

"events.appsflyer.com":{"start":"""
###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
""",
	'method' : 'post',
	'url' : "'http://events.appsflyer.com/api/'+"+"campaign_data.get('appsflyer').get('version')+"+"'/androidevent'", 
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'Content-Type'	  : 'CONSTANT',
		'User-Agent'	  : 'get_ua(device_data)',
		},
	'params' : {
		'app_id'		  : "campaign_data.get('package_name')",
		'buildnumber'	  : "campaign_data.get('appsflyer').get('buildnumber')",
		},
	'data' : {
		"advertiserId"			: "device_data.get('adid')",
		"advertiserIdEnabled"	: "CONSTANT",
		"af_events_api"			: "CONSTANT",
		"af_preinstalled"		: "CONSTANT",
		"af_gcm_token"			: "app_data.get('af_gcm_token')",
		"af_timestamp"			: "timestamp()",
		"android_id"			: "device_data.get('android_id')",
		"app_version_code"		: "campaign_data.get('app_version_code')",
		"app_version_name"		: "campaign_data.get('app_version_name')",
		"appsflyerKey"			: "campaign_data.get('appsflyer').get('key')",
		"appUserId"				: "CONSTANT",
		"appid"					: "campaign_data.get('package_name')",
		"brand"					: "device_data.get('brand')",
		"carrier"				: "device_data.get('carrier')",
		"counter"				: "str(app_data.get('counter'))",
		"cksm_v1"				: "util.get_random_string('hex',32)",
		"country"				: "device_data.get('locale').get('country')",
		"currency"				: "'USD'",
		"customData"			: 'BOOLEAN',
		"date1"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"date2"					: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"device"				: "device_data.get('device')",
		"deviceData"			: {
									"arch": "",
									"build_display_id": "device_data.get('display')",
									"cpu_abi": 'device_data.get("cpu_abi") if device_data.get("cpu_abi") else ""',
									"cpu_abi2": 'device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else ""',
									"dim"	: """{"d_dpi": device_data.get('dpi'),								
								"size": str(device_data.get('display_metrics').get('screen_layout_size')),
								"x_px": device_data.get('display_metrics').get('xpx'),
								"xdp": str(device_data.get('display_metrics').get('xdp')),
								"y_px": device_data.get('display_metrics').get('ypx'),
								"ydp": str(device_data.get('display_metrics').get('ydp')),
							}"""
							},
		'deviceRm'				: "device_data.get('display')",
		"deviceFingerPrintId"	: "app_data.get('deviceFingerPrintId')",
		'dkh'					: "campaign_data.get('appsflyer').get('dkh')",
		"deviceType"			: "CONSTANT",
		"firstLaunchDate"		: "app_data.get('firstLaunchDate')",
		"gaidError"				: "CONSTANT",
		"iaecounter"			: "str(app_data.get('iaecounter'))",
		"imei"					: "device_data.get('imei')",
		"installDate"			: "datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000'",
		"installer_package"		: "CONSTANT",
		"installAttribution"	: "str(json.dumps(app_data.get('installAttribution')))",
		"isFirstCall"			: "CONSTANT",
		"isGaidWithGps"			: "CONSTANT",
		"ivc"					: "BOOLEAN",
		"istu"					: "CONSTANT",
		"lang"					: "util.get_language_name(device_data.get('locale').get('language'))",
		"lang_code"				: "device_data.get('locale').get('language')",
		"model"					: "device_data.get('model')",
		"network"				: "device_data.get('network').upper()",
		"operator"				: "device_data.get('carrier')",
		"platformextension"		: "CONSTANT",
		"product"				: "device_data.get('product')",
		"referrer"				: "'utm_source=google-play&utm_medium=organic'",
		"registeredUninstall"	:"app_data.get('registeredUninstall')",
		"sdk"					: "device_data.get('sdk')",
		"tokenRefreshConfigured": "BOOLEAN",	
		"timepassedsincelastlaunch": "str(timeSinceLastCall)",
		"uid"					: "app_data.get('uid')",
		"extraReferrers"		: """json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"}) if app_data.get('referrer') else None"""
	},
	'extraReferrers':"""\n	if app_data.get('referrer'):\n		data['extraReferrers']=json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})""",		
	'conditional_data':"""	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)""",

	'cksm_v1':"""
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])""",

	'kef':"""
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(AF_SENSOR.sensorCount)
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal""",

	'return':"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}"""},

"api.appsflyer.com":{"start":"""
###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
""",
	'url' 	: "'http://api.appsflyer.com/install_data/v3/'"+"+campaign_data.get('package_name')",
	'method' : 'get',
	'headers' : {
		'Accept-Encoding' : 'CONSTANT',
		'User-Agent'	  : "get_ua(device_data)",
		},
	'params' 	: {
		'devkey'		  : "campaign_data.get('appsflyer').get('key')",
		'device_id' 	  : "app_data.get('uid')"
		},
	'data' 	: None,
	'return':"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}"""},

"kef_gen":'''

###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out\n\n''',
"cksm_34":'''
###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb\n\n''',
"cksm_32":'''

###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################

def change_char(s, p, r):
	return s[:p]+r+s[p+1:]

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+ins_date+datetime.datetime.utcfromtimestamp(int(ins_time)/1000).strftime('%Y-%m-%d_%H%M%S')+'+0000'
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	sb = md5_result
	
	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')
	
	return sb\n\n'''
}

appsflyer_required_functions="""

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	if not app_data.get('prev_event_name') or not app_data.get('prev_event_value') or not app_data.get('prev_event_time'):
		app_data['prev_event_name']  = ""
		app_data['prev_event_value'] = ""
		app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	value=random.randint(0,1)
	return str(int((time.time())*1000)+value)

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
"""
