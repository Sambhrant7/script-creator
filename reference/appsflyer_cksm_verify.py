import datetime


def change_char(s, p, r):
    return s[:p] + r + s[p + 1:]


def insert_char(s, p, r):
    return s[:p] + r + s[p:]


def sha256(data):
    import hashlib
    sha_obj = hashlib.sha256()
    sha_obj.update(data)
    return sha_obj.hexdigest()


def md5(data, radix=16):
    import hashlib
    md5_obj = hashlib.md5()
    md5_obj.update(data)
    if radix == 16:
        return md5_obj.hexdigest()
    elif radix == 64:
        return base64(md5_obj.digest())


def cksm_v1_32(ins_date, ins_time, package_name):
    pk = package_name.split('.')
    pk[0], pk[-1] = pk[-1], pk[0]
    pkn = '.'.join(pk)
    string = pkn + (package_name) * 2 + ins_date + datetime.datetime.utcfromtimestamp(int(ins_time) / 1000).strftime(
        "%Y-%m-%d_%H%M%S") + "+0000"
    sha256_result = sha256(string)
    md5_result = md5(sha256_result)

    sb = md5_result

    sb = change_char(sb, 17, 'f')
    sb = change_char(sb, 27, 'f')
    return sb


def cksm_v1_34(ins_date, ins_time, package_name):
    pk = package_name.split('.')
    pk[0], pk[-1] = pk[-1], pk[0]
    pkn = '.'.join(pk)
    string = pkn + (package_name) * 2 + "true" + ins_date + ins_time
    sha256_result = sha256(string)
    md5_result = md5(sha256_result)

    n = ins_time
    sb = md5_result
    n4 = 0

    sb = change_char(sb, 17, 'f')
    sb = change_char(sb, 27, 'f')

    for i in range(0, len(str(n))):
        n4 += int(str(n)[i])

    insert1 = list('{:02x}'.format(n4))
    sb = change_char(sb, 7, insert1[0])
    sb = change_char(sb, 8, insert1[1])

    j = 0
    n6 = 77
    n3 = 0
    for i in range(0, len(str(sb))):
        n3 += int(str(sb)[i], 36)

    if n3 > 100:
        n8 = 90
        n3 %= 100

    if n3<10:
        n3 = '0'+str(n3)
        
    sb = insert_char(sb, 23, str(n3))
    return sb
