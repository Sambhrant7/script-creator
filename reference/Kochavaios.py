import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","import uuid,json,random,urllib,time\n","from sdk import getsleep\n","from sdk import util,installtimenew\n","import clicker, Config\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	
	# Uncomment and use this as per your own event requirement.
	
	###########		FINALIZE	################
	# set_appCloseTime(app_data)
	
	return {'status':'true'}
\n"""

definitions={
	"control.kochava.com/track/kvinit":{
"start":"""
def kochava_kvinit(campaign_data, app_data, device_data):
""",
"url":'"http://control.kochava.com/track/kvinit"',
"method":"post",
"headers":{
	'Accept' : 'CONSTANT',
	'Accept-Encoding': 'CONSTANT',
	'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	'User-Agent' : "device_data.get('User-Agent')",
	'Content-Type' : 'CONSTANT'
	},
"params":{},
"data":{		
			"action": "CONSTANT",
			"sdk_build_date": "campaign_data.get('kochava').get('sdk_build_date')",
			"sdk_protocol": "campaign_data.get('kochava').get('sdk_protocol')",
			"data": {
				"uptime": """float("0."+str(random.randint(20,90)))""",
				"package": "campaign_data.get('package_name')",
				"usertime": "int(time.time())",
				"os_version": "'iOS '+device_data.get('os_version')",
				"platform": "CONSTANT",

				"currency":'CONSTANT',
				"idfa":"app_data.get('idfa_id')",
				"idfv": "app_data.get('idfv_id')",
				"partner_id":"CONSTANT",
				"partner_name":"CONSTANT",
				"platform":"CONSTANT",
				"session_tracking":"CONSTANT"
			},
			"data_orig":{
				"currency":"CONSTANT",
				"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
				"kochava_device_id": "app_data.get('kochava_deviceId')",
				"session_tracking":"CONSTANT"
			},
			"nt_id": "str(uuid.uuid4()).upper()",
			"kochava_app_id": "campaign_data.get('kochava').get('kochava_app_id')",
			"sdk_build_options":"BOOLEAN",
			"kochava_device_id": "app_data.get('kochava_deviceId')",
			"sdk_version": "campaign_data.get('kochava').get('sdk_version')",	
			"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",	
		},

"return":"""
	app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
	"""
	},
"control.kochava.com/track/json->initial":{
"start":"""
def kochava_initial(campaign_data, app_data, device_data):
	screen_Brightness(app_data)
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)	
""",
"url":'"http://control.kochava.com/track/json"',
"method":"post",
"headers":{
	'Accept' : 'CONSTANT',
	'Accept-Encoding': 'CONSTANT',
	'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	'User-Agent' : "device_data.get('User-Agent')",
	'Content-Type' : 'CONSTANT'
	},
"params":{},
"data":{
			'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')", 
			'kochava_device_id': "app_data.get('kochava_deviceId')",
			'action':'CONSTANT',
			'sdk_version': "campaign_data.get('kochava').get('sdk_version')",
			'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
			'nt_id':"str(uuid.uuid4()).upper()",
			"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			'data':{
				'disp_w': "int(device_data.get('resolution').split('x')[0])",
				'app_short_string': "campaign_data.get('app_version_name')",
				'app_version' : "campaign_data.get('app_version_code')",
				'architecture':"app_data.get('device_cpu_type')+'.1'",
				'battery_level':"app_data.get('battery_level')",
				'device_limit_tracking': 'BOOLEAN',
				'device_orientation' : 'CONSTANT',
				'usertime': "int(app_data.get('previous_Call_time'))",
				'uptime': "uptime",
				'package': "campaign_data.get('package_name')",
				'os_version': "'iOS '+ device_data.get('os_version')",
				'disp_h': "int(device_data.get('resolution').split('x')[1])",
				'device': "device_data.get('device_platform')",
				'app_limit_tracking':'BOOLEAN',
				"iad_attribution_details" : 'BOOLEAN',
				'idfa' : "app_data.get('idfa_id')",
				'idfv' : "app_data.get('idfv_id')",
				'is_genuine' : 'BOOLEAN',
				'language' : "device_data.get('locale').get('language')",
				'locale':"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
				'network_conn_type' : "device_data.get('network').lower()",
				'receipt' : "apple_receipt()",
				'screen_brightness': "app_data.get('screen_brightness')",
				'timezone':"device_data.get('local_tz_name')",
				'volume':"app_data.get('volume')",
					}
		},

"return":"""
	app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->event":{
"start":"""
def kochava_event(campaign_data, app_data, device_data,event_name='',event_data=''):
	screen_Brightness(app_data)
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)	
""",
"url":'"http://control.kochava.com/track/json"',
"method":"post",
"headers":{
	'Accept' : 'CONSTANT',
	'Accept-Encoding': 'CONSTANT',
	'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	'User-Agent' : "device_data.get('User-Agent')",
	'Content-Type' : 'CONSTANT'
	},
"params":{},
"data":{
			'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')", 
			'kochava_device_id': "app_data.get('kochava_deviceId')",
			'action':'CONSTANT',
			'sdk_version': "campaign_data.get('kochava').get('sdk_version')",
			'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
			'nt_id':"str(uuid.uuid4()).upper()",
			"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			'data':{
					'disp_w': "int(device_data.get('resolution').split('x')[0])",
					'app_short_string': "campaign_data.get('app_version_name')",
					'app_version' : "campaign_data.get('app_version_code')",
					'architecture':"app_data.get('device_cpu_type')+'.1'",
					'battery_level':"app_data.get('battery_level')",
					'device_orientation' : 'CONSTANT',
					'usertime': "int(app_data.get('previous_Call_time'))",
					'uptime': "uptime",
					'os_version': "'iOS '+ device_data.get('os_version')",
					'disp_h': "int(device_data.get('resolution').split('x')[1])",
					'device': "device_data.get('device_platform')",
					'locale':"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
					'network_conn_type' : "device_data.get('network').lower()",
					'screen_brightness': "app_data.get('screen_brightness')",
					'timezone':"device_data.get('local_tz_name')",
					'volume':"app_data.get('volume')",
					'event_data':"event_data",
					'event_name':"event_name"
				}
		},
"return":"""
app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->session":{
"start":"""
def kochava_session(campaign_data, app_data, device_data,state=''):
	screen_Brightness(app_data)
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)
""",
"url":'"http://control.kochava.com/track/json"',
"method":"post",
"headers":{
	'Accept' : 'CONSTANT',
	'Accept-Encoding': 'CONSTANT',
	'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	'User-Agent' : "device_data.get('User-Agent')",
	'Content-Type' : 'CONSTANT'
	},
"params":{},
"data":{
		'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')", 
		'kochava_device_id': "app_data.get('kochava_deviceId')",
		'action':'CONSTANT',
		'sdk_version': "campaign_data.get('kochava').get('sdk_version')",
		'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
		'nt_id':"str(uuid.uuid4()).upper()",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		'data':{
				'disp_w': "int(device_data.get('resolution').split('x')[0])",
				'app_short_string': "campaign_data.get('app_version_name')",
				'app_version' : "campaign_data.get('app_version_code')",
				'architecture':"app_data.get('device_cpu_type')+'.1'",
				'battery_level':"app_data.get('battery_level')",
				'device_orientation' : 'CONSTANT',
				'usertime': "int(app_data.get('previous_Call_time'))",
				'uptime': "uptime",
				'os_version': "'iOS '+ device_data.get('os_version')",
				'disp_h': "int(device_data.get('resolution').split('x')[1])",
				'device': "device_data.get('device_platform')",
				'locale':"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
				'network_conn_type' : "device_data.get('network').lower()",
				'screen_brightness': "app_data.get('screen_brightness') or screen_Brightness(app_data)",
				'timezone':"device_data.get('local_tz_name')",
				'volume':"app_data.get('volume')",
				'state':"state"
			}
},
"return":"""
app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/kvquery":{
"start":"""
def kv_Query(campaign_data,app_data,device_data):
""",
"url":'"http://control.kochava.com/track/kvquery"',
"method":"post",
"headers":{
	'Accept-Encoding':'CONSTANT',
	'Content-Type':'CONSTANT',
	'Accept-Language':"device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	'Accept': 'CONSTANT',
	'User-Agent':"device_data.get('User-Agent')",	
		},
"params":{},
"data":{
		"action":"CONSTANT",
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id":"app_data.get('kochava_deviceId')",
		"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
		"sdk_protocol":"campaign_data.get('kochava').get('sdk_protocol')",
		"send_date": """datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		"data":{"uptime":"float('0.0'+util.get_random_string('decimal',3))","state_active":"BOOLEAN","usertime":"str(int(app_data.get('previous_Call_time')))"},
		},
"return":"""
app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},	
"kvinit-prod.api.kochava.com/track/kvinit":{
"start":"""
def kochava_kvinit_1(campaign_data, app_data, device_data):
""",
"url":'"http://kvinit-prod.api.kochava.com/track/kvinit"',
"method":"post",
"headers":{
	'Accept' : 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'User-Agent' : "urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
		'Content-Type' : 'CONSTANT'
},
"params":{},
"data":{
		'action' : 'CONSTANT',
		
		'data':{
			'package': "campaign_data.get('package_name')",
			'platform': 'CONSTANT',
			'os_version' : "'iOS '+device_data.get('os_version')",
			'uptime':"float('0.0'+util.get_random_string('decimal',3))",
			'usertime':"int(time.time())",
		},
		'sdk_version' : "campaign_data.get('kochava').get('sdk_version')",
		'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')",
		'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
		'kochava_device_id': "app_data.get('kochava_deviceId')",
		'nt_id':"str(uuid.uuid4()).upper()",
		'sdk_build_date':"campaign_data.get('kochava').get('sdk_build_date')",
		'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		"sdk_build_options":'BOOLEAN'
		},
"return":"""
app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/json->update":{
	"start":"""
def kochava_update(campaign_data,app_data,device_data):
	""",
"url":'"http://control.kochava.com/track/json"',
"method":"post",
"headers": {
				'Accept-Encoding':'CONSTANT',
				'Content-Type':'CONSTANT',
				'Accept-Language':"device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
				'Accept': 'CONSTANT',
				'User-Agent':"device_data.get('User-Agent')",	
			},
"params":{},
"data":{
			"action":"CONSTANT",
			
			"data":{
				"app_version":"campaign_data.get('app_version_code')",
				"is_genuine":"CONSTANT",
				"app_short_string":"campaign_data.get('app_version_name')",
				"language":"device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
				"os_version":""""iOS "+device_data.get('os_version')"""
				},
			"sdk_protocol":"campaign_data.get('kochava').get('sdk_protocol')",
			"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
			"kochava_device_id":"app_data.get('kochava_deviceId')",
			"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
			'nt_id':"str(uuid.uuid4()).upper()",
			'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			
			},
"return":"""
app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/kvTracker.php->initial":{
"start":"""
def kochava_tracker_initial(campaign_data, app_data, device_data):
	screen_Brightness(app_data)
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)
	""",
"url":'"http://control.kochava.com/track/kvTracker.php"',
"headers":{
		'Accept' : 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'User-Agent' : "device_data.get('User-Agent')",
		'Content-Type' : 'CONSTANT'
	},
"params":{},
"method" : 'post',
"data":{
			'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')", 
			'kochava_device_id': "app_data.get('kochava_deviceId')",
			'action':'CONSTANT',
			'sdk_version': "campaign_data.get('kochava').get('sdk_version')",
			'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
			'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			'data':{
				'disp_w': "device_data.get('resolution').split('x')[0]",
				'updelta': "str(int(uptime))", 
				'app_short_string': "campaign_data.get('app_version_name')",
				'app_version' : "campaign_data.get('app_version_code')",
				'currency' : 'CONSTANT',
				'dev_id_strategy': 'CONSTANT',
				'device_limit_tracking': 'CONSTANT',
				'device_orientation' : 'CONSTANT',
				'usertime': "str(int(app_data.get('previous_Call_time')))",
				'uptime': "str(int(uptime))",
				'package_name': "campaign_data.get('package_name')",
				'os_version': "'iOS '+ device_data.get('os_version')",
				'disp_h': "device_data.get('resolution').split('x')[1]",
				'device': "device_data.get('device_platform')",
				'app_limit_tracking': 'CONSTANT',
				'fb_attribution_id': 'CONSTANT',				
				'idfa' : "app_data.get('idfa_id')",
				'idfv' : "app_data.get('idfv_id')",
				'is_genuine' : 'CONSTANT',
				'language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
				'network_conn_type' : "device_data.get('network').lower()",
				'new_user' : 'CONSTANT',
				'receipt' : "apple_receipt()",
				'screen_brightness': "app_data.get('screen_brightness')",
			}

		},
"return":"""
	app_data['previous_Call_time']=time.time()	

	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
"""
},
"control.kochava.com/track/kvTracker.php->event":{
"start":"""
def kochava_tracker_event(campaign_data, app_data, device_data, event_name, event_data):
	screen_Brightness(app_data)
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)
""",
"url":'"http://control.kochava.com/track/kvTracker.php"',
"headers":{
		'Accept' : 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'User-Agent' : "device_data.get('User-Agent')",
		'Content-Type' : 'CONSTANT'
	},
"params":{},
"method" : 'post',
"data":{
			'kochava_app_id': "campaign_data.get('kochava').get('kochava_app_id')", 
			'kochava_device_id': "app_data.get('kochava_deviceId')",
			'action':'CONSTANT',
			'sdk_version': "campaign_data.get('kochava').get('sdk_version')",
			'sdk_protocol': "campaign_data.get('kochava').get('sdk_protocol')",
			'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",

			'data':{
				
				'currency' : 'CONSTANT',
				'event_data':"event_data",
				'event_name' : "event_name",
				'usertime': "str(int(app_data.get('previous_Call_time')))",
				'uptime': "str(int(uptime))",
				}
			},
"return":"""
	app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},
"control.kochava.com/track/kvTracker->session":{
	"start":"""
def kochava_tracker_session(campaign_data, app_data, device_data, state="pause"):
	uptime=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),4)
	""",
"url":'"http://control.kochava.com/track/kvTracker.php"',
"headers":{
		'Accept' : 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'User-Agent' : "device_data.get('User-Agent')",
		'Content-Type' : 'CONSTANT'
	},
"params":{},
"method" : 'post',
"data":{
		"sdk_version":"campaign_data.get('kochava').get('sdk_version')",
		"data":{
			'last_post_time':'CONSTANT',
			"uptime":"str(int(uptime))",
			"state":"state",
			"usertime":"str(int(app_data.get('previous_Call_time')))",
			"updelta":"str(int(uptime))",
		},
		"action":"CONSTANT",
		"sdk_protocol":"campaign_data.get('kochava').get('sdk_protocol')",
		"kochava_device_id":"app_data.get('kochava_deviceId')",
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
	},
"return":"""
	app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""
},
"control.kochava.com/track/kvTracker.php->identityLink":{
	"start":"""
def kochava_identityLink(campaign_data, app_data, device_data):
	""",
"url":'"http://control.kochava.com/track/kvTracker.php"',
"headers":{
		'Accept' : 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'User-Agent' : "device_data.get('User-Agent')",
		'Content-Type' : 'CONSTANT'
	},
"params":{},
"method" : 'post',
"data":{
		"data":
			{
			"ko_gamecenter_alias":"CONSTANT",
			"ko_gamecenter_id":"CONSTANT"
			},
		"kochava_app_id":"campaign_data.get('kochava').get('kochava_app_id')",
		"kochava_device_id":"app_data.get('kochava_deviceId')",
		'send_date':"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
		"action":"CONSTANT"
		},
"return":"""
	app_data['previous_Call_time']=time.time()	
	return {'url': url, 'httpmethod':method, 'headers': headers, 'params': params, 'data': json.dumps([data])}
"""

}

}

kochava_ios_required_functions="""
def screen_Brightness(app_data):
	if not app_data.get('screen_brightness'):
		app_data['screen_brightness']=float('0.'+str(util.get_random_string('decimal',17)))
	return app_data.get('screen_brightness')

def kochava_device_ID(app_data):
	if not app_data.get('kochava_deviceId'):
		app_data['kochava_deviceId'] ='KI'+str(uuid.uuid4()).replace('-','').upper()	


def apple_receipt():
	return 'MIISoQYJKoZIhvcNAQcCoIISkjCCEo4CAQExCzAJBgUrDgMCGgUAMIICQgYJKoZIhvcNAQcBoIICMwSCAi8xggIrMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEDAgEBBAQMAjE5MAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAjTAMAgETAgEBBAQMAjE0MA0CAQsCAQEEBQIDB91tMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIERLdA9zAOAgEJAgEBBAYCBFAyNTIwDgIBEAIBAQQGAgQxiJrZMBACAQ8CAQEECAIGRSlo2DaJMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBD7J4Hgwc0ccV2FNysev2wBMBwCAQUCAQEEFOdtB+3yCOAPc2s3UVp5cVvI3yuRMB4CAQgCAQEEFhYUMjAxOS0wNS0yMFQwNDo1NDoxNFowHgIBDAIBAQQWFhQyMDE5LTA1LTIwVDA0OjU0OjE0WjAeAgESAgEBBBYWFDIwMTktMDMtMTFUMTI6MTA6MDBaMCYCAQICAQEEHgwcY29tLndlc3Rlcm51bmlvbi5tdHIzYXBwLmV1MzBKAgEHAgEBBEJc48eSi/7AoY3snxgFJWs7lnYtG3H45WJJRkuAJBhukw0nEKwzzEO/5ZRG/9tr3RQftwBz1A5mrWy+xq5Roqh+8fkwVgIBBgIBAQRO5Iig5zrpeJQu1Pc0OGWyUlpj5c807GMj9UztjZ8s3BqXPueAadAIMZFquz7ctw3/mivNNwv6hhLwSCQhH/pA2KIKsNnDITWKjqueGMAXoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBd299NHBhozhEY58q323cgiPZZActEu7dKpmXn42HPhCAn87LVaAOOVemTytm6kpwSF3OmpufJiGmZaXTX4EZBshuYxcfpIVbviREFXphest0qKZLFaRMAwLFncP+AUkrmeS1FDJvcVD0bYSe3muT9XcwknZbLI7yP6WORGdE893yoGseAU4LOayLGOToY54o/Iub/AsLMIeR8i36XV0SASln3kxr41+KLgxNs1cUJB0B5Gg5zRT/lxtFZRimy4YMeDPBA/RcIzUExzwLUuSLEEK3jkcy5aBdK70sKaVW5W1nwe43PNF1u2wYfeiY8L6wnHCtf4TbpmsqIiBDDbi6K'	

###########################################################################
#
#               Neccessary Methods
#
############################################################################	
			
def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
	
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
"""
