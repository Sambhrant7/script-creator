import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","import time\n","import random\n","import json\n","import datetime\n","import uuid\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	# set_sessionLength(app_data,forced=True,length=4000)

	###########		CALLS		################	
	# print '\nAdjust : SESSION____________________________________'
	# request=adjust_session(campaign_data, app_data, device_data,isOpen=True)
	# util.execute_request(**request)

	# Uncomment and use this as per your own event requirement.
	# demoEvent(campaign_data, app_data, device_data)

	###########		FINALIZE	################
	# set_appCloseTime(app_data)
	
	return {'status':'true'}
\n"""


definitions={"app.adjust.com/session":{"start":"""
###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	make_session_count(app_data,device_data)
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 0
""",
"url":"""'https://app.adjust.com/session'""",
"method":"post",
"headers" : {
		'Content-Type': 'CONSTANT',
		'Client-Sdk': "campaign_data.get('adjust').get('sdk')",
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'Accept-Encoding': 'CONSTANT',
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
	},
"params":{},
"data" : {
			'app_token': "campaign_data.get('adjust').get('app_token')",
			'app_updated_at':"campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone')",
			'app_version':	"campaign_data.get('app_version_code')",
			'app_version_short': "campaign_data.get('app_version_name')",
			'attribution_deeplink':	"CONSTANT",
			'bundle_id': "campaign_data.get('package_name')",
			'country':"device_data.get('locale').get('country').upper()",
			'cpu_type':	"device_data.get('cpu_type')",
			'created_at': 'created_at',
			"callback_params":"callback_params",
			"partner_params":"partner_params",
			'device_name': "device_data.get('device_platform')",
			'device_type': "device_data.get('device_type')",
			'environment': 'CONSTANT',
			'event_buffering_enabled': "BOOLEAN",
			'hardware_name': "device_data.get('hardware')",
			'idfa': "app_data.get('idfa_id')",
			'idfv':	"app_data.get('idfv_id')",
			'ios_uuid':	"app_data.get('ios_uuid')",
			'os_build':"device_data.get('build')",
			'queue_size': "BOOLEAN",
			'language':	"device_data.get('locale').get('language')",
			'needs_response_details': "BOOLEAN",
			'os_name': 'CONSTANT',
			'os_version': "device_data.get('os_version')",
			'persistent_ios_uuid': "app_data.get('ios_uuid')",
			'sent_at':	"sent_at",
			'session_count': "app_data.get('session_count')",
			'tracking_enabled':	"BOOLEAN",
			'connectivity_type':'CONSTANT',
			'install_receipt':"install_receipt()",
			'installed_at':'get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time"))',
			'mcc' : "device_data.get('mcc')",
			'mnc' : "device_data.get('mnc')",
			'tce':	"CONSTANT",
			'network_type' : 'CONSTANT',
			'last_interval': "get_lastInterval(app_data)",
			'session_length': "app_data.get('session_Length')",
			'subsession_count': "app_data.get('subsession_count')",
			'time_spent': "app_data.get('session_Length')",
			'push_token':"app_data.get('pushtoken')",
			},
'conditional_statements':
"""
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		app_data['adjust_call_time']=int(time.time())

	if data.get('queue_size'):
		del data['queue_size']
""",
"auth":"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')
""",
		
	"return":"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
"""},
"app.adjust.com/sdk_click":{"start":"""
###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,callback_params=None,partner_params=None):
	sdk_clk_time=get_date(app_data,device_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	def_(app_data,'subsession_count')
	time_spent=int(time.time())-app_data.get('adjust_call_time')
""",
	'url' : "'https://app.adjust.com/sdk_click'",
	'method' :"post",
	"headers" : {
		'Content-Type': 'CONSTANT',
		'Client-Sdk': "campaign_data.get('adjust').get('sdk')",
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'Accept-Encoding': 'CONSTANT',
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
	},
	"params":{},
	'data' : {
			
			'app_token': "campaign_data.get('adjust').get('app_token')",
			'details': 'CONSTANT',
			'event_buffering_enabled': "BOOLEAN",
			'idfa': "app_data.get('idfa_id')",
			'idfv':	"app_data.get('idfv_id')",
			'source': 'source',
			'app_version':	"campaign_data.get('app_version_code')",
			'app_version_short': "campaign_data.get('app_version_name')",
			'attribution_deeplink':	"CONSTANT",
			'bundle_id': "campaign_data.get('package_name')",
			'connectivity_type':'CONSTANT',
			'country':"device_data.get('locale').get('country').upper()",
			'cpu_type':	"device_data.get('cpu_type')",
			'created_at': "created_at",
			'device_name': "device_data.get('device_platform')",
			'device_type': "device_data.get('device_type')",
			'environment': 'CONSTANT',
			'event_buffering_enabled': "BOOLEAN",
			'hardware_name': "device_data.get('hardware')",
			'ios_uuid':	"app_data.get('ios_uuid')",
			'language':	"device_data.get('locale').get('language')",
			'needs_response_details': "BOOLEAN",
			'os_name': 'CONSTANT',
			'os_version': "device_data.get('os_version')",
			'persistent_ios_uuid': "app_data.get('ios_uuid')",
			'sent_at': "sent_at",
			'session_count': "app_data.get('session_count',1)",
			'tracking_enabled':	"BOOLEAN",
			'app_updated_at':"campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone')",
			'installed_at':'get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time"))',
			'os_build':	"device_data.get('build')",
			'queue_size': "BOOLEAN",
			'tce':	"CONSTANT",
			'session_length': "time_spent",
			'time_spent' : "time_spent",
			"callback_params":"callback_params",
			"partner_params":"partner_params",
			'install_receipt':"install_receipt()",
			'push_token':"app_data.get('pushtoken')",
			'mcc' : "device_data.get('mcc')",
			'mnc' : "device_data.get('mnc')",
			'network_type' : 'CONSTANT',
			'deeplink':"CONSTANT",
			'last_interval': "get_lastInterval(app_data)",
			'subsession_count': "app_data.get('subsession_count')",
			'click_time':"sdk_clk_time"
			},

'conditional_statements':
"""
	if source=='iad3':
		if data.get('deeplink'):
			del data['deeplink']
		if data.get('click_time'):
			del data['click_time']
		data['details']=json.dumps({"Version3.1":{"iad-attribution":"false"}})

	if source=='deeplink' and click_time:
		if not data.get('deeplink'):
			data['deeplink']="CONSTANT"
			data['click_time']='sdk_clk_time'

	if data.get('queue_size'):
		del data['queue_size']



""",
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app.adjust.com/attribution":{"start":"""
###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
""",
	'url' : "'https://app.adjust.com/attribution'",
	'method' :'head',
	'headers' : {
		'Content-Type': 'CONSTANT',
		'Client-Sdk': "campaign_data.get('adjust').get('sdk')",
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'Accept-Encoding': 'CONSTANT',
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
	},
	'params' : {
			
			'app_token': "campaign_data.get('adjust').get('app_token')",
			'attribution_deeplink' : "CONSTANT",
			'created_at': "created_at",
			'environment' : 'CONSTANT',
			'event_buffering_enabled': "BOOLEAN",
			'idfa': "app_data.get('idfa_id')",
			'idfv':	"app_data.get('idfv_id')",
			'initiated_by':'CONSTANT',
			'needs_response_details': "BOOLEAN",
			'sent_at': "sent_at",
			'persistent_ios_uuid': "app_data.get('ios_uuid')",
			'needs_attribution_data':'CONSTANT',
			'tracking_enabled'		:'CONSTANT',
			'install_receipt'		:"install_receipt()",
			'os_name'               :'CONSTANT',
			'app_version' : "campaign_data.get('app_version_code')",
			'app_version_short' : "campaign_data.get('app_version_name')",
			'bundle_id' : "campaign_data.get('package_name')",
			'device_name' : "device_data.get('device_platform')",
			'device_type' : "device_data.get('device_type')",
			'os_build' : "device_data.get('build')",
			'os_version' : "device_data.get('os_version')",
			},
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')
""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app.adjust.com/sdk_info":{"start":"""
###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_sdkinfo(campaign_data, app_data,device_data,t1=0,t2=0):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
""",
	'url': "'https://app.adjust.com/sdk_info'",
	'method' :"post",
	'headers' : {
		'Content-Type': 'CONSTANT',
		'Client-Sdk': "campaign_data.get('adjust').get('sdk')",
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'Accept-Encoding': 'CONSTANT',
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
	},
	'params':{},
	'data' : {
		'app_token': "campaign_data.get('adjust').get('app_token')",
		'attribution_deeplink' : "BOOLEAN",
		'created_at': "created_at",
		'environment' : 'CONSTANT',
		'event_buffering_enabled': "BOOLEAN",
		'idfa': "app_data.get('idfa_id')",
		'idfv':	"app_data.get('idfv_id')",
		'initiated_by':'CONSTANT',
		'needs_response_details': "BOOLEAN",
		'sent_at': "sent_at",
		'persistent_ios_uuid': "app_data.get('ios_uuid')",
		'push_token':"app_data.get('pushtoken')",
		'install_receipt':"install_receipt()",
		'tce':	"CONSTANT",
		'source' :'CONSTANT'
		},
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'info')""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app.adjust.com/event":{"start":"""
###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	time_spent=int(time.time())-app_data.get('adjust_call_time')
""",
	'url' : "'https://app.adjust.com/event'",
	'method' : "post",
	'headers' : {
		'Content-Type': 'CONSTANT',
		'Client-Sdk': "campaign_data.get('adjust').get('sdk')",
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
		'Accept-Encoding': 'CONSTANT',
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
	},
	'params':{},
	'data' : {
			'persistent_ios_uuid':"app_data.get('ios_uuid')",
			'hardware_name':"device_data.get('hardware')",	
			'event_buffering_enabled':	"BOOLEAN",
			'cpu_type':"device_data.get('cpu_type')",
			'attribution_deeplink': 'BOOLEAN',
			'ios_uuid':"app_data.get('ios_uuid')",
			'connectivity_type':'BOOLEAN',
			'os_name':'CONSTANT',
			"callback_params":"callback_params",
			"partner_params":"partner_params",
			'environment':'CONSTANT',
			'needs_response_details':"BOOLEAN",
			'event_count':"app_data.get('event_count')",
			'session_count':"str(app_data.get('session_count'))",
			'app_version_short':"campaign_data.get('app_version_name')",
			'device_type':"device_data.get('device_type')",
			'created_at': "created_at",
			'event_token':"event_token",
			'bundle_id':"campaign_data.get('package_name')",
			'subsession_count':"app_data.get('subsession_count')",
			'os_version':"device_data.get('os_version')",
			'app_version': "campaign_data.get('app_version_code')",
			'country':"device_data.get('locale').get('country')",
			'language':	"device_data.get('locale').get('language')",
			'idfa':"app_data.get('idfa_id')",
			'idfv':"app_data.get('idfv_id')",
			'app_token':"campaign_data.get('adjust').get('app_token')",
			'tracking_enabled': 'CONSTANT',
			'device_name':"device_data.get('device_platform')",
			'sent_at': "sent_at",
			'os_build':	"device_data.get('build')",
			'tce': 'CONSTANT',
			'queue_size': "BOOLEAN",
			'install_receipt':"install_receipt()",
			'push_token':"app_data.get('pushtoken')",
			'last_interval': "get_lastInterval(app_data)",
			'session_length': "time_spent",
			'time_spent': "time_spent",
			'mcc' : "device_data.get('mcc')",
			'mnc' : "device_data.get('mnc')",
			'network_type' : 'CONSTANT'
			},

'conditional_statements':
"""
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if data.get('queue_size'):
		del data['queue_size']
""",


'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')
""",
'return':"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}"""}}

adjust_required_functions="""

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['secret_key']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
"""