import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import hashlib\n","import base64\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	
	###########		FINALIZE	################
	
	return {'status':'true'}
\n"""

definitions={"apsalar.com/api/v1/resolve":{"start":"""
#################################################
#												#
# 			AppSalar Resolve funcation 			#
#												#
#################################################

def apsalarResolve(campaign_data, app_data, device_data):
	custom_user_id_create(app_data)
""",	
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers":{
		'User-Agent': 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},

	"params": {
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "CONSTANT",
					"lag"	: "str(generateLag())",
					"p"	 	: "CONSTANT",
					"pi" 	: "CONSTANT",
					"rt"	: "CONSTANT",
					"u"		: "device_data.get('adid')",
					"v"		: "device_data.get('os_version')",
					"ab"	: "device_data.get('cpu_abi','armeabi')",
					"aifa"	: "device_data.get('adid')",
					"av"	: "campaign_data.get('app_version_name')",
					"br"	: "device_data.get('brand')",
					"current_device_time"	:"str(int(time.time()*1000))",
					"ddl_enabled"	:"CONSTANT",
					"de"	: "device_data.get('device')",
					"device_type"	:"device_data.get('device_type')",
					"device_user_agent"	:"get_ua(device_data)",
					"dnt"	:"CONSTANT",
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"is"	:"CONSTANT",
					"lc"	: "device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
					"ma"	: "device_data.get('manufacturer')",
					"mo"	: "device_data.get('model')",
					"n"		: "campaign_data.get('app_name')",
					"pr"	: "device_data.get('product')",
					"s"		: "app_data.get('s')",
					"sdk"	: "campaign_data.get('apsalar').get('version')",
					"src"	: "CONSTANT",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"e"		:"CONSTANT",
					"seq"	:"CONSTANT",
					"t"		:"CONSTANT",
					"ddl_to" : "CONSTANT",
					"fi" 	 : "app_data.get('af_gcm_token')",
				},

	"data":{},

	'return':"""

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }"""},	
"apsalar.com/api/v1/start":{"start":"""
#################################################
#												#
# 			AppSalar Start funcation 			#
#												#
#################################################

def apsalarStart(campaign_data, app_data, device_data):
	custom_user_id_create(app_data)

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

	name = urllib.quote(campaign_data.get('app_name'))	

""",	
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers" : {
		'User-Agent': "CONSTANT",
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},


	"params" : {
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "CONSTANT",
					"lag"	: "str(generateLag())",
					"p"	 	: "CONSTANT",
					"pi" 	:"CONSTANT",
					"rt"	:"CONSTANT",
					"u"		:"device_data.get('adid')",
					"v"		:"device_data.get('os_version')",
					"ab"	:"device_data.get('cpu_abi','armeabi')",
					"aifa"	:"device_data.get('adid')",
					"av"	:"campaign_data.get('app_version_name')",
					"br"	:"device_data.get('brand')",
					"current_device_time"	:"str(int(time.time()*1000))",
					"ddl_enabled"	:"CONSTANT",
					"de"	:"device_data.get('device')",
					"device_type"	:"device_data.get('device_type')",
					"device_user_agent"	:"get_ua(device_data)",
					"dnt"	:"CONSTANT",
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"is"	:"CONSTANT",
					"lc"	:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
					"ma"	:"device_data.get('manufacturer')",
					"mo"	:"device_data.get('model')",
					"n"		:"name",
					"pr"	:"device_data.get('product')",
					"s"		:"app_data.get('s')",
					"sdk"	:"campaign_data.get('apsalar').get('version')",
					"src"	:"CONSTANT",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"e"		: "CONSTANT",
					"seq"	:"CONSTANT",
					"t"		:"CONSTANT",
					"ddl_to" : "CONSTANT",
					"fi" 	 : "app_data.get('af_gcm_token')",

				},
			
	"data":{},

	'return':"""

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }"""},

"apsalar.com/api/v1/event":{"start":"""
#################################################
#												#
# 			AppSalar Event funcation 			#
#												#
#################################################

def apsalar_event(campaign_data, app_data, device_data,event_name='',value='',custom_user_id=False):
	custom_user_id_create(app_data)

	if not app_data.get('seq'):
		app_data['seq'] = 1

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))	

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]
""",
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers":{
		'User-Agent': "CONSTANT",
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},

	"params": {
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "CONSTANT",
					"lag"	: "str(generateLag())",
					"p"	 	: "CONSTANT",
					"pi" 	:"CONSTANT",
					"rt"	:"CONSTANT",
					"u"		:"device_data.get('adid')",
					"v"		:"device_data.get('os_version')",
					"ab"	:"device_data.get('cpu_abi','armeabi')",
					"aifa"	:"device_data.get('adid')",
					"av"	:"campaign_data.get('app_version_name')",
					"br"	:"device_data.get('brand')",
					"current_device_time"	:"str(int(time.time()*1000))",
					"ddl_enabled"	:"CONSTANT",
					"de"	:"device_data.get('device')",
					"device_type"	:"device_data.get('device_type')",
					"device_user_agent"	:"get_ua(device_data)",
					"dnt"	:"CONSTANT",
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"is"	:"CONSTANT",
					"lc"	:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
					"ma"	:"device_data.get('manufacturer')",
					"mo"	:"device_data.get('model')",
					"n"		:"event_name",
					"pr"	:"device_data.get('product')",
					"s"		:"app_data.get('s')",
					"sdk"	:"campaign_data.get('apsalar').get('version')",
					"src"	:"CONSTANT",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"e"		:"value",
					"seq"	:"app_data.get('seq')",
					"t"		:"t",
					"ddl_to" : "CONSTANT",
					"fi" 	 : "app_data.get('af_gcm_token')",

				},
	"data":{},

	'conditional_statements':
"""				
	if urllib.unquote(value)=='{}':
		if params.get('e'):
			del params['e']

	if custom_user_id:
		params['custom_user_id'] = 	app_data.get('custom_user_id')	

""",
	'return':"""	
	params = get_params(params)

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	if app_data.get('seq'):
		app_data['seq']=app_data.get('seq')+1

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }""",
}}


apsalar_android_required_functions="""	
#####################################################
#													#
# 			Apsalar Utility function 				#
#													#
#####################################################

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(value)+'='+str(data_value.get(value))+'&'
	a = a.rsplit("&",1)[0]	

	return a
			
def generateLag():
	return (random.randint(40,300)*0.001)

def custom_user_id_create(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id']= '12035'+ str(random.randint(1000,9999))
		raise Exception('please enter custom_user_id')

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())


"""
	