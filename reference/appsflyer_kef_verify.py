# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import random

app_data = {}  ######FOR TESTING
part1 = ""

global temp, counter

temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""


def get_key_half(brand, sdk, lang, af_ts, buildnumber):
    if buildnumber == "4.8.18":
        return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
    else:
        c = '\u0000'
        obj2 = af_ts[::-1]
        out = calculate(sdk, obj2, brand)
        i = len(out)
        if i > 4:
            i2 = globals()['counter'] + 65
            globals()['temp'] = i2 % 128
            if (1 if i2 % 2 != 0 else None) != 1:
                out.delete(4, i)
            else:
                out.delete(5, i)
        else:
            while i < 4:
                i3 = globals()['counter'] + 119
                globals()['temp'] = i3 % 128
                if (16 if i3 % 2 != 0 else 93) != 16:
                    i += 1
                    out += '1'
                else:
                    i += 66
                    out += 'H'
                i3 = globals()['counter'] + 109
                globals()['temp'] = i3 % 128
                i3 %= 2
        return out.__str__()


def getKeyHalf_4_8_18(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk, lang, ts]
    lengths = [len(sdk), len(lang), len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt

        out += str(hex(numb))[2:]

    if len(out) > 4:
        out = out[:4]
    elif len(out) < 4:
        while len(out) < 4:
            out += "1"

    return out


"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""


def generateValue(b, x, s, p, ts, fl, buildnumber):
    part1 = generateValue1get(ts, fl, buildnumber)
    str_ = bytearray("b" + b + "&x" + x + "&s" + s + "&p" + p)
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString)))[2:]
        if 1 == len(toHexString2):
            toHexString2 = "0" + str(toHexString2)
        stringBuilder += toHexString2
    part2 = stringBuilder.__str__()
    return part1 + part2


def generateValue1get(ts, firstLaunch, buildnumber):
    gethash = sha256(ts + firstLaunch + buildnumber)
    return gethash[:16]


"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""


def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter'] + 99
        globals()['temp'] = i4 % 128
        i5 = globals()['temp'] + 67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out += str(hex(int(number)))[2:]
        i3 += 1
    return out


def sha256(data):
    import hashlib
    sha_obj = hashlib.sha256()
    sha_obj.update(data)
    return sha_obj.hexdigest()