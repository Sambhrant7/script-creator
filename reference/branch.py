import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	

	###########		CALLS		################	
	

	# Uncomment and use this as per your own event requirement.
	# demoEvent(campaign_data, app_data, device_data)

	###########		FINALIZE	################
	
	
	return {'status':'true'}
\n"""


definitions={"api2.branch.io/v1/install":{"start":"""
###################################################################
# branch_install()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates branch's behaviour whenever the App gets open.
###################################################################
def api_branch_install(campaign_data,app_data,device_data):
	

""",
"url":"""'http://api2.branch.io/v1/install'""",
"method":"post",
"headers" : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
"params":{},

"data" : {
			"advertising_ids"					: 'CONSTANT',       
			"app_version"						: "campaign_data.get('app_version_name')",
			"branch_key"						: "campaign_data.get('api_branch').get('branchkey')",
			"brand"								: "device_data.get('brand')",
			"build"								:"device_data.get('build')",
			"cd"								: 'CONSTANT',
			"clicked_referrer_ts"				:"int(app_data.get('times').get('click_time'))",
			"connection_type"					:"device_data.get('network')",
			"cpu_type"							:"device_data.get('cpu_abi','aarch64')",
			"country"							: "device_data.get('locale').get('country')",
			"debug"								: 'BOOLEAN',
			"environment"						: 'CONSTANT',
			"facebook_app_link_checked"			: 'BOOLEAN',
			"first_install_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"google_advertising_id"				: "device_data.get('adid')",
			"hardware_id"						: "device_data.get('android_id')",
			"install_begin_ts"					:"int(app_data.get('times').get('download_end_time'))",
			"instrumentation"					: 'CONSTANT',
			"is_hardware_id_real"				: 'BOOLEAN',
			"is_referrable"						: "1",
			"language"							: "device_data.get('locale').get('language')",
			"lat_val"							: "0",
			"latest_install_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"latest_update_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"local_ip"							: "device_data.get('private_ip')",
			"locale"							:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
			"metadata"							: 'CONSTANT',
			"model"								: "device_data.get('model')",
			"os"								: 'CONSTANT',
			"os_version"						: "int(device_data.get('sdk'))",
			"os_version_android"				:"device_data.get('os_version')",
			"previous_update_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"retryNumber"						: "0",
			"screen_dpi"						: "int(device_data.get('dpi'))",
			"screen_height"						: "int(device_data.get('resolution').split('x')[0])",
			"screen_width"						: "int(device_data.get('resolution').split('x')[1])",
			"sdk"								: "campaign_data.get('api_branch').get('sdk')",
			"ui_mode"							: "CONSTANT",
			"update"							: "0",
			"wifi"								: 'BOOLEAN',
			}, 
'conditional_statements':
"""
	get_link_identifier_for_branch(app_data)
	if globals().get('link_identifier_for_branch'):
		data['link_identifier'] = globals().get('link_identifier_for_branch')
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v1/close":{"start":"""
###################################################################
# api_branch_close()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates branch's behaviour incase of In-organic install.
###################################################################
def api_branch_close(campaign_data, app_data, device_data):
	
""",
	'url' : "'api2.branch.io/v1/close'",
	'method' :"post",
	'headers' : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {
			"advertising_ids"					: 'CONSTANT',       
			"app_version"						: "campaign_data.get('app_version_name')",
			"branch_key"						: "campaign_data.get('api_branch').get('branchkey')",
			"brand"								: "device_data.get('brand')",
			"country"							: "device_data.get('locale').get('country')",
			"device_fingerprint_id"				:"app_data.get('device_fingerprint_id')",
			"google_advertising_id"				: "device_data.get('adid')",
			"hardware_id"						: "device_data.get('android_id')",
			"identity_id"						:"app_data.get('identity_id')",
			"instrumentation"					: 'CONSTANT',
			"is_hardware_id_real"				: 'BOOLEAN',
			"language"							: "device_data.get('locale').get('language')",
			"lat_val"							: "0",
			"local_ip"							: "device_data.get('private_ip')",
			"metadata"							: 'CONSTANT',
			"model"								: "device_data.get('model')",
			"os"								: 'CONSTANT',
			"os_version"						: "int(device_data.get('sdk'))",
			"retryNumber"						: "0",
			"screen_dpi"						: "int(device_data.get('dpi'))",
			"screen_height"						: "int(device_data.get('resolution').split('x')[0])",
			"screen_width"						: "int(device_data.get('resolution').split('x')[1])",
			"sdk"								: "campaign_data.get('api_branch').get('sdk')",
			"session_id"						:"app_data.get('session_id')",
			"ui_mode"							: "CONSTANT",
			"wifi"								: 'BOOLEAN',
			},
'conditional_statements':
"""
	""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v1/open":{"start":"""
###################################################################
# api_branch_open()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def api_branch_open(campaign_data, app_data,device_data):
	
""",

	'url' : "'http://api2.branch.io/v1/open'",
	'method' :"post",
	'headers' : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {
			"advertising_ids"					: 'CONSTANT',       
			"app_version"						: "campaign_data.get('app_version_name')",
			"branch_key"						: "campaign_data.get('api_branch').get('branchkey')",
			"brand"								: "device_data.get('brand')",
			"build"								:"device_data.get('build')",
			"cd"								: 'CONSTANT',
			"connection_type"					:"device_data.get('network')",
			"cpu_type"							:"device_data.get('cpu_abi','aarch64')",
			"country"							: "device_data.get('locale').get('country')",
			"debug"								: 'BOOLEAN',
			"device_fingerprint_id"				:"app_data.get('device_fingerprint_id')",
			"environment"						: 'CONSTANT',
			"facebook_app_link_checked"			: 'BOOLEAN',
			"first_install_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"google_advertising_id"				: "device_data.get('adid')",
			"hardware_id"						: "device_data.get('android_id')",
			"identity_id"						:"app_data.get('identity_id')",
			"instrumentation"					: 'CONSTANT',
			"is_hardware_id_real"				: 'BOOLEAN',
			"is_referrable"						: "1",
			"language"							: "device_data.get('locale').get('language')",
			"lat_val"							: "0",
			"latest_install_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"latest_update_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"local_ip"							: "device_data.get('private_ip')",
			"locale"							:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
			"metadata"							: 'CONSTANT',
			"model"								: "device_data.get('model')",
			"os"								: 'CONSTANT',
			"os_version"						: "int(device_data.get('sdk'))",
			"os_version_android"				:"device_data.get('os_version')",
			"previous_update_time"				: "int(app_data.get('times').get('install_complete_time')*1000)",
			"retryNumber"						: "0",
			"screen_dpi"						: "int(device_data.get('dpi'))",
			"screen_height"						: "int(device_data.get('resolution').split('x')[0])",
			"screen_width"						: "int(device_data.get('resolution').split('x')[1])",
			"sdk"								: "campaign_data.get('api_branch').get('sdk')",
			"ui_mode"							: "CONSTANT",
			"update"							: "0",
			"wifi"								: 'BOOLEAN',
			}, 

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v1/profile":{"start":"""
###################################################################
# api_branch_profile()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def api_branch_profile(campaign_data, app_data,device_data):
	
""",
	'url': "'http://api2.branch.io/v1/profile'",
	'method' :"post",
	'headers' : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {
			"advertising_ids"					: 'CONSTANT',       
			"branch_key"						: "campaign_data.get('api_branch').get('branchkey')",
			"brand"								: "device_data.get('brand')",
			"country"							: "device_data.get('locale').get('country')",
			"device_fingerprint_id"				:"app_data.get('device_fingerprint_id')",
			"google_advertising_id"				: "device_data.get('adid')",
			"hardware_id"						: "device_data.get('android_id')",
			"identity"							: 'CONSTANT',
			"identity_id"						:"app_data.get('identity_id')",
			"instrumentation"					: 'CONSTANT',
			"is_hardware_id_real"				: 'BOOLEAN',
			"language"							: "device_data.get('locale').get('language')",
			"lat_val"							: "0",
			"local_ip"							: "device_data.get('private_ip')",
			"metadata"							: 'CONSTANT',
			"model"								: "device_data.get('model')",
			"os"								: 'CONSTANT',
			"os_version"						: "int(device_data.get('sdk'))",
			"retryNumber"						: "0",
			"screen_dpi"						: "int(device_data.get('dpi'))",
			"screen_height"						: "int(device_data.get('resolution').split('x')[0])",
			"screen_width"						: "int(device_data.get('resolution').split('x')[1])",
			"sdk"								: "campaign_data.get('api_branch').get('sdk')",
			"session_id"						:"app_data.get('session_id')",
			"ui_mode"							: "CONSTANT",
			"wifi"								: 'BOOLEAN',
			},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},


"api2.branch.io/v1/cpid/latd":{"start":"""
###################################################################
# api2_branch_latd()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def api2_branch_latd(campaign_data, app_data,device_data):
	
""",
	'url': "'http://api2.branch.io/v1/cpid/latd'",
	'method' :"post",
	'headers' : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {       
        "advertising_ids"		: 'CONSTANT',
        "branch_key"			: "campaign_data.get('api_branch').get('branchkey')",  
        "environment"			: 'CONSTANT',
        "instrumentation"		: 'CONSTANT',
        "metadata"				: 'CONSTANT',
        "retryNumber"			: "0",
        "user_data"				: {       
									"aaid"					: "device_data.get('adid')",
									"android_id"			: "device_data.get('android_id')",
									"app_version"			: "campaign_data.get('app_version_name')",
									"attribution_window"	: 'CONSTANT',
									"brand"					: "device_data.get('brand')",
									"country"				: "device_data.get('locale').get('country')",
									"device_fingerprint_id"	: "app_data.get('device_fingerprint_id')",
									"language"				: "device_data.get('locale').get('language')",
									"limit_ad_tracking"		: '0',
									"local_ip"				: "device_data.get('private_ip')",
									"model"					: "device_data.get('model')",
									"os"					: 'CONSTANT',
									"os_version"			: "int(device_data.get('sdk'))",
									"screen_dpi"			:"int(device_data.get('dpi'))",
									"screen_height"			: "int(device_data.get('resolution').split('x')[0])",
									"screen_width"			: "int(device_data.get('resolution').split('x')[1])",
									"sdk"					: 'CONSTANT',
									"sdk_version"			: 'CONSTANT',
									"user_agent"			:"device_data.get('User-Agent')"
                }
            },

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v2/event/standard":{"start":"""
###################################################################
# api_branch_standard(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def api_branch_standard(campaign_data, app_data, device_data, event_name='',event_data=''):
	
""",
	'url' : "'http://api2.branch.io/v2/event/standard'",
	'method' : "post",
	'headers' : {
		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {       
		"advertising_ids": 'CONSTANT',
        "branch_key": "campaign_data.get('api_branch').get('branchkey')",
        "content_items": 'CONSTANT',
        "customer_event_alias": 'CONSTANT',
        "event_data": "event_data",
        "custom_data": 'CONSTANT',
        "instrumentation": 'CONSTANT',
        "metadata": 'CONSTANT',
        "name": "event_name",
        "retryNumber": "0",
        "user_data": {       
							"aaid"						: "device_data.get('adid')",
							"android_id"				: "device_data.get('android_id')",
							"app_version"				: "campaign_data.get('app_version_name')",
							"brand"						:"device_data.get('brand')",
							"build"						: "device_data.get('build')",
							"country"					: "device_data.get('locale').get('country')",
							"connection_type"			:"device_data.get('network')",
							"cpu_type"					:"device_data.get('cpu_abi','aarch64')",
							"developer_identity"		: 'CONSTANT',
							"device_fingerprint_id"		:"app_data.get('device_fingerprint_id')",
							"environment"				: 'CONSTANT',
							"language"					: "device_data.get('locale').get('language')",
							"limit_ad_tracking"			: '0',
							"local_ip"					: "device_data.get('private_ip')",
							"locale"					:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
							"model"						: "device_data.get('model')",
							"os"						: 'CONSTANT',
							"os_version"				: "int(device_data.get('sdk'))",
							"os_version_android"		:"device_data.get('os_version')",
							"screen_dpi"				:"int(device_data.get('dpi'))",
							"screen_height"				: "int(device_data.get('resolution').split('x')[0])",
							"screen_width"				: "int(device_data.get('resolution').split('x')[1])",
							"sdk"						: 'CONSTANT',
							"sdk_version"				: 'CONSTANT',
							"user_agent"				:" device_data.get('User-Agent')"}},


'conditional_statements':
"""
	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},


"api2.branch.io/v2/event/custom":{"start":"""
###################################################################
# api_branch_custom(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def api_branch_custom(campaign_data, app_data, device_data, event_name='',event_data=''):
	
""",
	'url' : "'http://api2.branch.io/v2/event/custom'",
	'method' : "post",
	'headers' : {		
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Accept'			: 'CONSTANT',
		 },
	'params':{},
	'data' : {       
		"advertising_ids": 'CONSTANT',
        "branch_key": "campaign_data.get('api_branch').get('branchkey')",
        "custom_data": "event_data",
        "instrumentation": 'CONSTANT',
        "metadata": 'CONSTANT',
        "name": "event_name",
        "retryNumber": "0",
        "user_data": {       
							"aaid"						: "device_data.get('adid')",
							"android_id"				: "device_data.get('android_id')",
							"app_version"				: "campaign_data.get('app_version_name')",
							"brand"						:"device_data.get('brand')",
							"build"						: "device_data.get('build')",
							"country"					: "device_data.get('locale').get('country')",
							"connection_type"			:"device_data.get('network')",
							"cpu_type"					:"device_data.get('cpu_abi','aarch64')",
							"developer_identity"		: 'CONSTANT',
							"device_fingerprint_id"		:"app_data.get('device_fingerprint_id')",
							"environment"				: 'CONSTANT',
							"language"					: "device_data.get('locale').get('language')",
							"limit_ad_tracking"			: '0',
							"local_ip"					: "device_data.get('private_ip')",
							"locale"					:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
							"model"						: "device_data.get('model')",
							"os"						: 'CONSTANT',
							"os_version"				: "int(device_data.get('sdk'))",
							"os_version_android"		:"device_data.get('os_version')",
							"screen_dpi"				:"int(device_data.get('dpi'))",
							"screen_height"				: "int(device_data.get('resolution').split('x')[0])",
							"screen_width"				: "int(device_data.get('resolution').split('x')[1])",
							"sdk"						: 'CONSTANT',
							"sdk_version"				: 'CONSTANT',
							"user_agent"				:" device_data.get('User-Agent')"}},




'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},


"api2.branch.io/v1/pageview":{"start":"""
###################################################################
# api2_pageview(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def api2_pageview(campaign_data, app_data, device_data):
	
""",
	'url' : "'http://api2.branch.io/v1/pageview'",
	'method' : "post",
	'headers' : {       

		"Accept"			: 'CONSTANT',
        "Accept-Encoding"	: 'CONSTANT',
        "Accept-Language"	: 'CONSTANT',
        "Content-Type"		: 'CONSTANT',
        "Origin"			: 'CONSTANT',
        "Referer"			: 'CONSTANT',
        "Sec-Fetch-Dest"	: 'CONSTANT',
        "Sec-Fetch-Mode"	: 'CONSTANT',
        "Sec-Fetch-Site"	: 'CONSTANT',
        "User-Agent"		: 'CONSTANT',
        "X-Requested-With": "campaign_data.get('package_name')"},
	'params':{},
	'data' : {       

		"branch_key"				: "campaign_data.get('api_branch').get('branchkey')",
        "browser_fingerprint_id"	: "app_data.get('device_fingerprint_id')",
        "callback_string"			: 'CONSTANT',
        "data"						: 'CONSTANT',
        "event"						: 'CONSTANT',
        "feature"					: 'CONSTANT',
        "has_app_websdk"			: 'BOOLEAN',
        "identity_id"				: "app_data.get('identity_id')",
        "metadata"					: 'CONSTANT',
        "open_app"					: 'BOOLEAN',
        "sdk"						: 'CONSTANT',
        "session_id"				: "app_data.get('session_id')",
        "source"					: 'CONSTANT',
        "user_language"				: "device_data.get('locale').get('language')"},

'conditional_statements':
"""
	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v1/url":{"start":"""
###################################################################
# api2_branch_url(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def api2_branch_url(campaign_data, app_data, device_data):
	
""",
	'url' : "'http://api2.branch.io/v1/url'",
	'method' : "post",
	'headers' : {       

		"Accept"			: 'CONSTANT',
        "Accept-Encoding"	: 'CONSTANT',
        "Accept-Language"	: 'CONSTANT',
        "Content-Type"		: 'CONSTANT',
        },

       
	'params':{},
	'data' : {
		"advertising_ids"		: 'CONSTANT',
        "alias"					: 'CONSTANT',
        "branch_key"			: "campaign_data.get('api_branch').get('branchkey')",
        "brand"					: "device_data.get('brand')",
        "campaign"				: 'CONSTANT',
        "channel"				: 'CONSTANT',
        "country"				: "GB",
        "data"					: 'CONSTANT',
        "device_fingerprint_id"	: "744507014680023766",
        "feature"				: 'CONSTANT',
        "google_advertising_id"	: "7b4b5f89-174a-4bae-991c-440133cadf6d",
        "hardware_id"			: "1b3217b704f90fc4",
        "identity_id"			: "745144489005534593",
        "instrumentation"		: {       "v1\\/url-qwt": "324",
                                   "v2\\/event\\/standard-brtt": "325"},
        "is_hardware_id_real"	: True,
        "language"				: "en",
        "lat_val"				: 0,
        "local_ip"				: "192.168.0.11",
        "metadata"				: {       },
        "model"					: "A37fw",
        "os"					: "Android",
        "os_version"			: 22,
        "retryNumber"			: 0,
        "screen_dpi"			: 320,
        "screen_height"			: 1280,
        "screen_width"			: 720,
        "sdk"					: "android4.2.1",
        "session_id"			: "745151427294446937",
        "stage"					: "",
        "tags"					: [],
        "ui_mode"				: "UI_MODE_TYPE_NORMAL",
        "wifi"					: True	
	},

'conditional_statements':
"""
	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},

"api2.branch.io/v1/has-app":{"start":"""
###################################################################
# api2_branch_has_app(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def api2_branch_has_app(campaign_data, app_data, device_data):
	
""",
	'url' : "'http://api2.branch.io/v1/has-app/'+campaign_data.get('api_branch').get('branchkey')",
	'method' : "get",
	'headers' : {       

		"Accept"			: 'CONSTANT',
        "Accept-Encoding"	: 'CONSTANT',
        "Accept-Language"	: 'CONSTANT',
        "Content-Type"		: 'CONSTANT',
        "Origin"			: 'CONSTANT',
        "Referer"			: 'CONSTANT',
        "Sec-Fetch-Mode"	: 'CONSTANT',
        "Sec-Fetch-Site"	: 'CONSTANT',
        "User-Agent"		: 'CONSTANT',
        "X-Requested-With": "campaign_data.get('package_name')"},

       
	'params':{

			"browser_fingerprint_id": "app_data.get('browser_fingerprint_id')"
	},
	'data' : {},

'conditional_statements':
"""
	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""},




"cdn.branch.io/sdk/uriskiplist_v1.json":{"start":"""
###################################################################
# branch_call(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def branch_call(campaign_data, app_data, device_data):
	
""",
	'url' : "'http://cdn.branch.io/sdk/uriskiplist_v1.json'",
	'method' : "get",
	'headers' : {		
		'Accept-Encoding'	: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		 },
	'params':{},
	'data' : {},




'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"cdn.branch.io/sdk/uriskiplist_v2.json":{"start":"""
###################################################################
# branch_call(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_name,event_data
#
# Simulates branch's behaviour incase of an in-app event.
###################################################################
def branch_call2(campaign_data, app_data, device_data):
	
""",
	'url' : "'http://cdn.branch.io/sdk/uriskiplist_v2.json'",
	'method' : "get",
	'headers' : {		
		'Accept-Encoding'	: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		 },
	'params':{},
	'data' : {},




'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""}}

branch_android_required_functions="""

###########################################################
# Utility methods : branch
#
# Define/declare/Set/Unset/Increment various parameters 
# required by branch 
###########################################################	

def get_link_identifier_for_branch(app_data):
    import urllib
    try:
        print "######################"
        print "Finding link_identifier for branch."
        print "Inside try block."
        link_identifier = False
        if app_data.get('referrer'):
            print 'Referrer is present.'
            print "Trying to unquote referrer."
            unquoted_referrer = urllib.unquote(urllib.unquote(app_data.get('referrer')))
            referrer_dict = {}
            print "Parsing referrer."
            for each in unquoted_referrer.split('&'):
                if '=' in each:
                    referrer_dict[each.split('=')[0]] = each.split('=')[1]
                elif '-' in each:
                    referrer_dict[each.split('-')[0]] = each.split('-')[1]
            print "Referrer parsed successfully."

            if referrer_dict.get('link_click_id'):
                print "link_click_id is present in referrer."
                link_identifier = referrer_dict.get('link_click_id')
            else:
                print "link_click_id is not found in referrer."
        else:
            print "Referrer is not present."

        print "Exiting try block."
        print "############################"
    except:
        print "Inside except block."
        link_identifier = False
        print "############################"

    globals()['link_identifier_for_branch'] = link_identifier 


"""