import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	# set_sessionLength(app_data,forced=True,length=4000)

	###########		CALLS		################	
	# print '\nAdjust : SESSION____________________________________'
	# request=adjust_session(campaign_data, app_data, device_data,isOpen=True)
	# util.execute_request(**request)

	# Uncomment and use this as per your own event requirement.
	# demoEvent(campaign_data, app_data, device_data)

	###########		FINALIZE	################
	# set_appCloseTime(app_data)
	
	return {'status':'true'}
\n"""


definitions={"app.adjust.com/session":{"start":"""
###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

""",
"url":"""'https://app.adjust.com/session'""",
"method":"post",
"headers" : {
		'Client-SDK'		: "campaign_data.get('adjust').get('sdk')",
		'Accept-Encoding'	: 'CONSTANT',
		'Content-Type'		: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'X-NewRelic-ID'		: 'CONSTANT',
		 },
"params":{},
"data" : {
		'android_uuid'			:"app_data.get('android_uuid')",
		'api_level'				:"device_data.get('sdk')",
		'app_token'				:"campaign_data.get('adjust').get('app_token')",
		'app_version'			:"campaign_data.get('app_version_name')",
		'attribution_deeplink'	:'CONSTANT',
		'connectivity_type'		:'CONSTANT',
		'country'				:"device_data.get('locale').get('country')",
		'cpu_type'				:"device_data.get('cpu_abi')",
		'created_at'			:"created_at",
		'device_manufacturer'	:"device_data.get('manufacturer')",
		'device_name'			:"device_data.get('model')",
		'device_type'			:"device_data.get('device_type')",
		'display_height'		:"device_data.get('resolution').split('x')[0]",
		'display_width'			:"device_data.get('resolution').split('x')[1]",
		'environment'			:'CONSTANT',
		'event_buffering_enabled':'CONSTANT',
		'gps_adid'				:"device_data.get('adid')",
		'hardware_name'			:"device_data.get('hardware')",
		'installed_at'			:"get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time'))",
		'language'				:"device_data.get('locale').get('language')",
		'needs_response_details':'CONSTANT',
		'network_type'			:'CONSTANT',
		'queue_size'			:'BOOLEAN',
		'needs_attribution_data':'BOOLEAN',
		'os_build'				:"device_data.get('build')",
		'os_name'				:'CONSTANT',
		'os_version'			:"device_data.get('os_version')",
		'package_name'			:"campaign_data.get('package_name')",
		'push_token'			:"app_data.get('pushtoken')",
		'screen_density'		:"get_screen_density(device_data)",
		'screen_format'			:"get_screen_format(device_data)",
		'screen_size'			:"get_screen_size(device_data)",
		'sent_at'				:'sent_at',
		'session_count'			:"app_data.get('session_count')",
		'tracking_enabled'		:'CONSTANT',
		'updated_at'			:"get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time'))",
		'vm_isa'				:'CONSTANT',
		"tce"					:'CONSTANT',
		'carrier'               :"device_data.get('carrier')",
		'mcc'                   :"device_data.get('mcc')",
		'mnc'                   :"device_data.get('mnc')",
		'raw_referrer'          :'CONSTANT',
		'referrer'              :'CONSTANT',
		'reftag'                :'CONSTANT',
		'callback_params'       :'callback_params',
		'partner_params'        : 'partner_params',
		'gps_adid_src'          :'CONSTANT'
		},
'conditional_statements':
"""
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if data.get('queue_size'):
		del data['queue_size']

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())
""",
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},
"app.adjust.com/sdk_click":{"start":"""
###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
""",
	'url' : "'https://app.adjust.com/sdk_click'",
	'method' :"post",
	'headers' : {
		'Client-SDK'		:"campaign_data.get('adjust').get('sdk')",
		'Accept-Encoding'	:'CONSTANT',
		'Content-Type'		:'CONSTANT',
		'User-Agent'		:"get_ua(device_data)",
		'X-NewRelic-ID'		: 'CONSTANT',
		 },
	'params':{},
	'data' : {
		'source'				:'source',
		'android_uuid'			:"app_data.get('android_uuid')",
		'api_level'				:"device_data.get('sdk')",
		'app_token'				:"campaign_data.get('adjust').get('app_token')",
		'app_version'			:"campaign_data.get('app_version_name')",
		'attribution_deeplink'	:'CONSTANT',
		'country'				:"device_data.get('locale').get('country')",
		'cpu_type'				:"device_data.get('cpu_abi')",
		'created_at'			:'created_at',
		'device_manufacturer'	:"device_data.get('manufacturer')",
		'device_name'			:"device_data.get('model')",
		'device_type'			:"device_data.get('device_type')",
		'display_height'		:"device_data.get('resolution').split('x')[0]",
		'display_width'			:"device_data.get('resolution').split('x')[1]",
		'environment'			:'CONSTANT',
		'event_buffering_enabled':'CONSTANT',
		'needs_attribution_data':'CONSTANT',
		'gps_adid'				:"device_data.get('adid')",
		'hardware_name'			:"device_data.get('hardware')",
		'installed_at'			:'get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time"))',
		'language'				:"device_data.get('locale').get('language')",
		'last_interval'         :'CONSTANT',
		'needs_response_details':'CONSTANT',
		'os_build'				:"device_data.get('build')",
		'os_name'				:'CONSTANT',
		'os_version'			:"device_data.get('os_version')",
		'package_name'			:"campaign_data.get('package_name')",
		'push_token'				:"app_data.get('pushtoken')",
		'screen_density'		:"get_screen_density(device_data)",
		'screen_format'			:"get_screen_format(device_data)",
		'screen_size'			:"get_screen_size(device_data)",
		'sent_at'				:'sent_at',
		'session_count'			:'BOOLEAN',
		'tracking_enabled'		:'CONSTANT',
		'queue_size'			:'BOOLEAN',
		'updated_at'			:'get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time"))',
		'vm_isa'				:'CONSTANT',
		'connectivity_type'		:'CONSTANT',
		'network_type'			:'CONSTANT',
		'session_length'	    : "session_length",
		'subsession_count'      : "app_data.get('subsession_count')",
		'time_spent'            : "time_spent",
		'deeplink'		        :"CONSTANT",
		'carrier'               :"device_data.get('carrier')",
		'mcc'                   :"device_data.get('mcc')",
		'mnc'                   :"device_data.get('mnc')",
		'install_begin_time'    :'get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))',
		'raw_referrer'          :'CONSTANT',
		'referrer'              :'CONSTANT',
		'reftag'                :'CONSTANT',
		'callback_params'       :'callback_params',
		'partner_params'        : 'partner_params',
		"tce"					:'CONSTANT',
		'gps_adid_src'          :'CONSTANT'
		},
'conditional_statements':
"""
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if data.get('queue_size'):
		del data['queue_size']

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']

""",
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app.adjust.com/attribution":{"start":"""
###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
""",

	'url' : "'https://app.adjust.com/attribution'",
	'method' :'head',
	'headers' : {
		'Client-SDK'		:"campaign_data.get('adjust').get('sdk')",
		'Accept-Encoding'	:'CONSTANT',
		'User-Agent'		:"get_ua(device_data)",
		'X-NewRelic-ID'		: 'CONSTANT',
		 },
	'params' : {
		'app_token'				:"campaign_data.get('adjust').get('app_token')",
		'attribution_deeplink'	:'CONSTANT',
		'created_at'			:"created_at",
		'environment'			:'CONSTANT',
		'initiated_by'			:'CONSTANT',
		'event_buffering_enabled':'CONSTANT',
		'needs_attribution_data':'CONSTANT',
		'gps_adid'				:"device_data.get('adid')",
		'needs_response_details':'CONSTANT',
		'sent_at'				:"sent_at",
		'tracking_enabled'		:'CONSTANT',
		'gps_adid_src'          :'CONSTANT',
		'package_name'          :"campaign_data.get('package_name')",
		'device_name'           :"device_data.get('model')",
		'os_version'            :"device_data.get('os_version')",
		'os_name'               :'CONSTANT',
		'android_uuid'          :"app_data.get('android_uuid')",
		'device_type'           :"device_data.get('device_type')",
		'push_token'            :"app_data.get('pushtoken')",
		'app_version'           :"campaign_data.get('app_version_name')",
		'api_level'             :"device_data.get('sdk')"


		},

'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')
""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}"""},

"app.adjust.com/sdk_info":{"start":"""
###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_sdkinfo(campaign_data, app_data,device_data,t1=0,t2=0):
	pushtoken(app_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
""",
	'url': "'https://app.adjust.com/sdk_info'",
	'method' :"post",
	'headers' : {
		'Client-SDK'		:"campaign_data.get('adjust').get('sdk')",
		'Accept-Encoding'	:'CONSTANT',
		'User-Agent'		:"get_ua(device_data)",
		'Content-Type'		:'CONSTANT',
		'X-NewRelic-ID'		: 'CONSTANT',
		 },
	'params':{},
	'data' : {
		'app_token'				:"campaign_data.get('adjust').get('app_token')",
		'attribution_deeplink'	:'CONSTANT',
		'created_at'			:'created_at',
		'environment'			:'CONSTANT',
		'event_buffering_enabled':'CONSTANT',
		'gps_adid'				:"device_data.get('adid')",
		'needs_response_details':'CONSTANT',
		'needs_attribution_data':'CONSTANT',
		'push_token'			:"app_data.get('pushtoken')",
		'sent_at'				:'sent_at',
		'source'				:'CONSTANT',
		'tracking_enabled'		:'CONSTANT',
		'raw_referrer'          :'CONSTANT',
		'referrer'              :'CONSTANT',
		'reftag'                :'CONSTANT',
		"tce"					:'CONSTANT',
		'gps_adid_src'          :'CONSTANT',
		'android_uuid'          :"app_data.get('android_uuid')",
		},
'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'info')""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app.adjust.com/event":{"start":"""
###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
""",
	'url' : "'https://app.adjust.com/event'",
	'method' : "post",
	'headers' : {
		'Client-SDK'		:"campaign_data.get('adjust').get('sdk')",
		'Accept-Encoding'	:'CONSTANT',
		'Content-Type'		:'CONSTANT',
		'User-Agent'		:"get_ua(device_data)",
		'X-NewRelic-ID'		: 'CONSTANT',
		 },
	'params':{},
	'data' : {
		'android_uuid'			:"app_data.get('android_uuid')",
		'api_level'				:"device_data.get('sdk')",
		'app_token'				:"campaign_data.get('adjust').get('app_token')",
		'app_version'			:"campaign_data.get('app_version_name')",
		'attribution_deeplink'	:'CONSTANT',
		'connectivity_type'		:'CONSTANT',
		'country'				:"device_data.get('locale').get('country')",
		'cpu_type'				:"device_data.get('cpu_abi')",
		'created_at'			: 'created_at',
		'device_manufacturer'	:"device_data.get('manufacturer')",
		'device_name'			:"device_data.get('model')",
		'device_type'			:"device_data.get('device_type')",
		'display_height'		:"device_data.get('resolution').split('x')[0]",
		'display_width'			:"device_data.get('resolution').split('x')[1]",
		'event_count'			:"app_data.get('event_count')",
		'event_token'			:'event_token',
		'environment'			:'CONSTANT',
		'needs_attribution_data':'CONSTANT',
		'event_buffering_enabled':'CONSTANT',
		'gps_adid'				:"device_data.get('adid')",
		'hardware_name'			:"device_data.get('hardware')",
		'language'				:"device_data.get('locale').get('language')",
		'needs_response_details':'CONSTANT',
		'network_type'			:'CONSTANT',
		'os_build'				:"device_data.get('build')",
		'os_name'				:'CONSTANT',
		'os_version'			:"device_data.get('os_version')",
		'package_name'			:"campaign_data.get('package_name')",
		'push_token'			:"app_data.get('pushtoken')",
		'screen_density'		:'get_screen_density(device_data)',
		'screen_format'			:'get_screen_format(device_data)',
		'screen_size'			:'get_screen_size(device_data)',
		'sent_at'				:'sent_at',
		'queue_size'			:"BOOLEAN",
		'session_count'			:"app_data.get('session_count')",
		'session_length'		:'session_length',
		'subsession_count'		:"app_data.get('subsession_count')",
		'tracking_enabled'		:'CONSTANT',
		'time_spent'			:'time_spent',
		'vm_isa'				:'CONSTANT',
		'carrier'               :"device_data.get('carrier')",
		'mcc'                   :"device_data.get('mcc')",
		'mnc'                   :"device_data.get('mnc')",
		'raw_referrer'          :'CONSTANT',
		'referrer'              :'CONSTANT',
		'reftag'                :'CONSTANT',
		'callback_params'       :'callback_params',
		'partner_params'        : 'partner_params',
		"tce"					:'CONSTANT',
		'gps_adid_src'          :'CONSTANT'
		},

'conditional_statements':
"""
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if data.get('queue_size'):
		del data['queue_size']
""",

'auth':"""
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')
""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""}}

adjust_android_required_functions="""


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	raise Exception("please confirm auth sequence- created_at+secret_key+gps_adid+activity_type")
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


"""