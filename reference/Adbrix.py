import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from Crypto.Cipher import AES\n","from sdk import util\n","import uuid\n","import time\n","import datetime\n","import string\n","import base64\n","import urlparse\n","import random\n","import json\n","import urllib\n","import clicker\n","import Config\n","import os\n"]

definitions={"cvr.ad-brix.com/v1/conversion/GetReferral":
{"start":"""
def ad_brix_get_referral(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):

	url = 'http://cvr.ad-brix.com/v1/conversion/GetReferral'\n	""",
	"headers" : {	
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
			},
				
	"method" :'CONSTANT',
	
	"referrer_data":"""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		
	""",
	"data" : {
					'k' : "campaign_data.get('ad-brix').get('app_key')",
	},
	
	"data_value" : {

"appkey":"campaign_data.get('ad-brix').get('app_key')",
"package_name":"campaign_data.get('package_name')",
"version":"campaign_data.get('ad-brix').get('ver')",
"app_version_name":"campaign_data.get('app_version_name')",
"app_version_code":"eval(campaign_data.get('app_version_code'))",
"installer":"CONSTANT",
"referral_info":{"conversion_key":"conversion_key_value",
"session_no":"session_number_value",
"referrer_param":"app_data.get('referrer')"},
"conversion_cache":["conversion_key_value"],
"adbrix_user_info":{"adbrix_user_no":"BOOLEAN",
"shard_no":"BOOLEAN",
"install_datetime":'""',
"install_mdatetime":'""',
"life_hour":"BOOLEAN",
"app_launch_count":"BOOLEAN",
"referral_key":"conversion_key_value",
"reengagement_conversion_key":"BOOLEAN",
"last_referral_key":"BOOLEAN",
"last_referral_data":"CONSTANT",
"last_referral_datetime":"CONSTANT",
"set_referral_key":"BOOLEAN",
"sig_type":"BOOLEAN"},
"user_info":{"puid":'""',
"mudid":'""',
"openudid":'""',
"openudid_md5":'""',
"openudid_sha1":'""',
"device_id_md5":'""',
"android_id_md5":"util.md5(device_data.get('android_id'))",
"android_id_sha1":"util.sha1(device_data.get('android_id'))",
"device_id_sha1":'""',
"google_ad_id":"device_data.get('adid')",
"google_ad_id_opt_out":"BOOLEAN",
"initial_ad_id":"device_data.get('adid')",
"ag":"email_ag_1",
"odin":"util.sha1(device_data.get('android_id'))",
"carrier":"device_data.get('carrier')",
"country":"device_data.get('locale').get('country')",
"language":"device_data.get('locale').get('language')",
"first_install_time_offset":"app_data.get('sec')",
"first_install_time":"datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S')",
"android_id":"base64.b64encode(device_data.get('android_id'))"},
"cohort_info":"{}",
"device_info":{
"vendor":"CONSTANT",
"build_id":"device_data.get('build')",
"model":"device_data.get('model')",
"kn":"device_data.get('kernel')",
"is_wifi_only":"BOOLEAN",
"network":"CONSTANT",
"noncustomnetwork":"int(device_data.get('mnc'))",
"os":'"a_"'+"+device_data.get('os_version')",
"ptype":"CONSTANT",
"width":"int(device_data.get('resolution').split('x')[1])",
"height":"int(device_data.get('resolution').split('x')[0])",
"is_portrait":"BOOLEAN",
"utc_offset":"float(app_data.get('sec'))/float(60)/float(60)"},
"demographics":[{"demo_key":"CONSTANT","demo_value":"app_data.get('demo_value')"}],
"complete_conversions":"BOOLEAN",
"activity_info":[{"prev_group":"CONSTANT",
"prev_activity":"CONSTANT",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":"CONSTANT",
"event_id":"CONSTANT",
"created_at":"event_date"},{"prev_group":"CONSTANT",
"prev_activity":"start_application",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":'""',
"event_id":"CONSTANT",
"created_at":"event_date"}],
"impression_info":[{'created_at': 'event_date', 'resource_key': "BOOLEAN", 'campaign_key': "BOOLEAN", 'conversion_key': 'CONSTANT', 'isFirstTime': "BOOLEAN", 'space_key': 'CONSTANT'}],
"installation_info":{"market_referrer":"urlparse.unquote(app_data.get('referrer'))",
"install_actions_timestamp":{
	"market_install_btn_clicked":"int(app_data.get('times').get('click_time'))",
	"app_install_start":"int(app_data.get('times').get('download_end_time'))",
	"app_install_completed":"int(app_data.get('times').get('install_complete_time'))",
	"app_first_open":"int(app_first_open_time_1)"}},
"request_info":{"client_timestamp":"int(time.time())"}
},

	"return":
	"""
	key = campaign_data.get('ad-brix').get('key_getreferral')
	iv = campaign_data.get('ad-brix').get('iv_getreferral')
	aes = AESCipher(key)
	if data.get("isEncode")=="false":
		data['j'] = json.dumps(data_value)
	else:
		data['j'] =aes.encrypt(iv,json.dumps(data_value))
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

"""	
},
"first->tracking.ad-brix.com":
{"start":"""		
def ad_brix_tracking_start(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'\n	""",
	"headers" : {	
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
			},
				
	"method" : 'CONSTANT',
	"referrer_data":"""
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)

	""",
	"data" : {
					'k' : "campaign_data.get('ad-brix').get('app_key')",
	},
	
	"data_value" : {

"appkey":"campaign_data.get('ad-brix').get('app_key')",
"package_name":"campaign_data.get('package_name')",
"version":"campaign_data.get('ad-brix').get('ver')",
"app_version_name":"campaign_data.get('app_version_name')",
"app_version_code":"eval(campaign_data.get('app_version_code'))",
"installer":"CONSTANT",
"referral_info":{"conversion_key":"conversion_key_value",
"session_no":"session_number_value",
"referrer_param":"app_data.get('referrer')"},
"conversion_cache":["conversion_key_value"],
"adbrix_user_info":{"adbrix_user_no":"int(app_data.get('user_number'))",
"first_install_time_offset":"app_data.get('sec')",
"shard_no":"int(app_data.get('shard_number'))",
"install_datetime":"app_data.get('install_datetime')",
"install_mdatetime":"app_data.get('install_mdatetime')",
"life_hour":"BOOLEAN",
"app_launch_count":"BOOLEAN",
"referral_key":"conversion_key_value",
"reengagement_conversion_key":"BOOLEAN",
"last_referral_key":"BOOLEAN",
"last_referral_data":'""',
"last_referral_datetime":'""',
"referral_data":"referrer_additional_info",
"set_referral_key":"BOOLEAN",
"sig_type":"BOOLEAN"},
"user_info":{"puid":'""',
"mudid":'""',
"openudid":'""',
"openudid_md5":'""',
"openudid_sha1":'""',
"device_id_md5":'""',
"android_id_md5":"util.md5(device_data.get('android_id'))",
"android_id_sha1":"util.sha1(device_data.get('android_id'))",
"first_install_time":"datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S')",
"first_install_time_offset":"app_data.get('sec')",
"device_id_sha1":'""',
"google_ad_id":"device_data.get('adid')",
"google_ad_id_opt_out":"BOOLEAN",
"initial_ad_id":"device_data.get('adid')",
"ag":"email_ag_1",
"odin":"util.sha1(device_data.get('android_id'))",
"carrier":"device_data.get('carrier')",
"country":"device_data.get('locale').get('country')",
"language":"device_data.get('locale').get('language')",
"android_id":"base64.b64encode(device_data.get('android_id'))"},
"cohort_info":"{}",
"device_info":{"vendor":"CONSTANT",
"is_portrait":"BOOLEAN",
"build_id":"device_data.get('build')",
"model":"device_data.get('model')",
"kn":"device_data.get('kernel')",
"is_wifi_only":"BOOLEAN",
"network":"CONSTANT",
"noncustomnetwork":"int(device_data.get('mnc'))",
"os":'"a_"'+"+device_data.get('os_version')",
"ptype":"CONSTANT",
"width":"int(device_data.get('resolution').split('x')[1])",
"height":"int(device_data.get('resolution').split('x')[0])",
"utc_offset":"float(app_data.get('sec'))/float(60)/float(60)"},
"demographics":[{"demo_key":"CONSTANT","demo_value":"app_data.get('demo_value')"}],
"complete_conversions":"BOOLEAN",
"activity_info":[{"prev_group":"CONSTANT",
"prev_activity":"CONSTANT",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":"CONSTANT",
"event_id":"CONSTANT",
"created_at":"event_date"},{"prev_group":"CONSTANT",
"prev_activity":"CONSTANT",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":"CONSTANT",
"event_id":"CONSTANT",
"created_at":"event_date"},{"prev_group":"CONSTANT",
"prev_activity":"CONSTANT",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":"CONSTANT",
"event_id":"CONSTANT",
"created_at":"event_date"},{"prev_group":"CONSTANT",
"prev_activity":"CONSTANT",
"group":"CONSTANT",
"activity":"CONSTANT",
"param":"CONSTANT",
"event_id":"CONSTANT",
"created_at":"event_date"}],
"impression_info":[{'created_at': 'event_date', 'resource_key': "BOOLEAN", 'campaign_key': "BOOLEAN", 'conversion_key': 'CONSTANT', 'isFirstTime': "BOOLEAN", 'space_key': 'CONSTANT'}],
"request_info":{"client_timestamp":"int(time.time())"},
"installation_info":{"market_referrer":"urlparse.unquote(app_data.get('referrer'))",
"install_actions_timestamp":{
	"market_install_btn_clicked":"int(app_data.get('times').get('click_time'))",
	"app_install_start":"int(app_data.get('times').get('download_end_time'))",
	"app_install_completed":"int(app_data.get('times').get('install_complete_time'))",
	"app_first_open":"int(app_first_open_time_1)"}},
	},
	"return":
	"""
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	if data.get("isEncode")=="false":
		data['j'] = json.dumps(data_value)
	else:
		data['j'] =aes.encrypt(iv,json.dumps(data_value))
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


"""},
"tracking.ad-brix.com/v1/tracking":
{"start":"""
def ad_brix_tracking_event(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1,group='',param='',activity=''):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	""",
	'headers' : {	
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
			},
				
	'method': 'CONSTANT',
	"referrer_data":"""
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	""",

	'data' : {
					'k' : "campaign_data.get('ad-brix').get('app_key')",
	},
	
	"data_value" :
	{"appkey":"campaign_data.get('ad-brix').get('app_key')",
"package_name":"campaign_data.get('package_name')",
"version":"campaign_data.get('ad-brix').get('ver')",
"app_version_name":"campaign_data.get('app_version_name')",
"app_version_code":"eval(campaign_data.get('app_version_code'))",
"installer":"CONSTANT",
"referral_info":{"conversion_key":"conversion_key_value",
"session_no":"session_number_value",
"referrer_param":"app_data.get('referrer')"},
"conversion_cache":["conversion_key_value"],
"adbrix_user_info":{"adbrix_user_no":"int(app_data.get('user_number'))",
"first_install_time_offset":"app_data.get('sec')",
"shard_no":"int(app_data.get('shard_number'))",
"install_datetime":"app_data.get('install_datetime')",
"install_mdatetime":"app_data.get('install_mdatetime')",
"life_hour":"BOOLEAN",
"app_launch_count":"BOOLEAN",
"referral_key":"conversion_key_value",
"referral_data":"referrer_additional_info",
"reengagement_conversion_key":"BOOLEAN",
"last_referral_key":"BOOLEAN",
"last_referral_data":'""',
"last_referral_datetime":'""',
"set_referral_key":"BOOLEAN",
"sig_type":"BOOLEAN"},
"user_info":{"puid":'""',
"mudid":'""',
"openudid":'""',
"openudid_md5":'""',
"openudid_sha1":'""',
"device_id_md5":'""',
"android_id_md5":"util.md5(device_data.get('android_id'))",
"android_id_sha1":"util.sha1(device_data.get('android_id'))",
"first_install_time":"datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S')",
"first_install_time_offset":"app_data.get('sec')",
"device_id_sha1":'""',
"google_ad_id":"device_data.get('adid')",
"google_ad_id_opt_out":"BOOLEAN",
"initial_ad_id":"device_data.get('adid')",
"ag":"email_ag_1",
"odin":"util.sha1(device_data.get('android_id'))",
"carrier":"device_data.get('carrier')",
"country":"device_data.get('locale').get('country')",
"language":"device_data.get('locale').get('language')",
"android_id":"base64.b64encode(device_data.get('android_id'))"},
"cohort_info":"{}",
"device_info":{"vendor":"CONSTANT",
"is_portrait":"BOOLEAN",
"model":"device_data.get('model')",
"kn":"device_data.get('kernel')",
"build_id":"device_data.get('build')",
"is_wifi_only":"BOOLEAN",
"network":"CONSTANT",
"noncustomnetwork":"int(device_data.get('mnc'))",
"os":'"a_"'+"+device_data.get('os_version')",
"ptype":"CONSTANT",
"width":"int(device_data.get('resolution').split('x')[1])",
"height":"int(device_data.get('resolution').split('x')[0])",
"utc_offset":"float(app_data.get('sec'))/float(60)/float(60)"},
"demographics":[{"demo_key":"CONSTANT","demo_value":"app_data.get('demo_value')"}],
"complete_conversions":"BOOLEAN",
"activity_info":[{"prev_group":"app_data.get('group')",
"prev_activity":"app_data.get('activity')",
"group":"group",
"activity":"activity",
"param":"param",
"event_id":"str(uuid.uuid4())",
"created_at":"event_date"}],
"impression_info":[{'created_at': 'event_date', 'resource_key': "BOOLEAN", 'campaign_key': "BOOLEAN", 'conversion_key': 'CONSTANT', 'isFirstTime': "BOOLEAN", 'space_key': 'CONSTANT'}],
"installation_info":{"market_referrer":"urlparse.unquote(app_data.get('referrer'))",
"install_actions_timestamp":{
	"market_install_btn_clicked":"int(app_data.get('times').get('click_time'))",
	"app_install_start":"int(app_data.get('times').get('download_end_time'))",
	"app_install_completed":"int(app_data.get('times').get('install_complete_time'))",
	"app_first_open":"int(app_first_open_time_1)"}},
"request_info":{"client_timestamp":"int(time.time())"}},
	
	"return":
	"""
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	if data.get("isEncode")=="false":
		data['j'] = json.dumps(data_value)
	else:
		data['j'] =aes.encrypt(iv,json.dumps(data_value))

	update_activity(app_data,device_data,group,activity)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}
"""},
"apiab4c.ad-brix.com/v1/event":
{"start":"""		
def adbrix_purchase(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):
	url='http://apiab4c.ad-brix.com/v1/event'
	""",
	'headers': {	
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
			},			
	'method' : 'CONSTANT',
	"referrer_data":"""
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
		
	""",

	"data":
	{
"appkey":"campaign_data.get('ad-brix').get('app_key')",
"package_name":"campaign_data.get('package_name')",
"installer":"CONSTANT",
"version":"campaign_data.get('ad-brix').get('ver')",
"app_version_name":"campaign_data.get('app_version_name')",
"app_version_code":"eval(campaign_data.get('app_version_code'))",
"referral_info":{"conversion_key":"conversion_key_value",
"session_no":"session_number_value",
"referrer_param":"app_data.get('referrer')"},
"conversion_cache":["conversion_key_value"],
"adbrix_user_info":{
"adbrix_user_no":"int(app_data.get('user_number'))",
"shard_no":"int(app_data.get('shard_number'))",
"install_datetime":"app_data.get('install_datetime')",
"install_mdatetime":"app_data.get('install_mdatetime')",
"life_hour":"BOOLEAN",
"app_launch_count":"BOOLEAN",
"referral_key":"conversion_key_value",
"referral_data":"referrer_additional_info",
"reengagement_conversion_key":"BOOLEAN",
"last_referral_key":"BOOLEAN",
"last_referral_data":'""',
"last_referral_datetime":'""',
"set_referral_key":"BOOLEAN",
"sig_type":"BOOLEAN"},
"user_info":{"puid":'""',
"mudid":'""',
"openudid":'""',
"openudid_md5":'""',
"openudid_sha1":'""',
"android_id_md5":"util.md5(device_data.get('android_id'))",
"android_id_sha1":"util.sha1(device_data.get('android_id'))",
"device_id_md5":'""',
"device_id_sha1":'""',
"google_ad_id":"device_data.get('adid')",
"google_ad_id_opt_out":"BOOLEAN",
"initial_ad_id":"device_data.get('adid')",
"ag":"email_ag_1",
"odin":"util.sha1(device_data.get('android_id'))",
"carrier":"device_data.get('carrier')",
"country":"device_data.get('locale').get('country')",
"language":"device_data.get('locale').get('language')",
"android_id":"base64.b64encode(device_data.get('android_id'))",
"first_install_time":"datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S')",
"first_install_time_offset":"app_data.get('sec')"},
"cohort_info":"{}",
"device_info":{"vendor":"CONSTANT",
"model":"device_data.get('model')",
"kn":"device_data.get('kernel')",
"is_wifi_only":"BOOLEAN",
"network":"CONSTANT",
"noncustomnetwork":"int(device_data.get('mnc'))",
"os":"'a_'"+"+device_data.get('os_version')",
"ptype":"CONSTANT",
"width":"int(device_data.get('resolution').split('x')[1])",
"height":"int(device_data.get('resolution').split('x')[0])",
"is_portrait":"BOOLEAN",
"utc_offset":"float(app_data.get('sec'))/float(60)/float(60)",
"build_id":"device_data.get('build')"},
"demographics":[{"demo_key":"CONSTANT","demo_value":"app_data.get('demo_value')"}],
"complete_conversions":"BOOLEAN",
"activity_info":"BOOLEAN",
"impression_info":[{'created_at': 'event_date', 'resource_key': "BOOLEAN", 'campaign_key': "BOOLEAN", 'conversion_key': 'CONSTANT', 'isFirstTime': "BOOLEAN", 'space_key': 'CONSTANT'}],
"installation_info":{"market_referrer":"urlparse.unquote(app_data.get('referrer'))",
"install_actions_timestamp":{
	"market_install_btn_clicked":"int(app_data.get('times').get('click_time'))",
	"app_install_start":"int(app_data.get('times').get('download_end_time'))",
	"app_install_completed":"int(app_data.get('times').get('install_complete_time'))",
	"app_first_open":"int(app_first_open_time_1)"}},
"request_info":{"client_timestamp":"int(time.time())"},
"commerce_events":[{"event_type":"CONSTANT",
"event_id":"str(uuid.uuid4())",
"created_at":"event_date",
"attributes":{"order_id":'"GPA."+str(util.get_random_string("decimal",4))+"-"+str(util.get_random_string("decimal",4))+"-"+str(util.get_random_string("decimal",4))+"-"+str(util.get_random_string("decimal",4))',
"discount":"BOOLEAN",
"delivery_charge":"BOOLEAN",
"payment_method":"CONSTANT"},
"products":[{"product_id":"CONSTANT",
"product_name":"CONSTANT",
"price":"CONSTANT",
"discount":"BOOLEAN",
"quantity":"BOOLEAN",
"currency":"CONSTANT",
"extra_attrs":{"category1":"CONSTANT"}}]}]},
	
	"return":"""

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json.dumps(data)}
"""},

"tracking.ad-brix.com/v1/tracking/SetUserDemographic":
{"start":"""
def ad_brix_tracking_SetUserDemographic(app_data, device_data):

	url = 'http://tracking.ad-brix.com/v1/tracking/SetUserDemographic'
	""",
	"headers" : {	
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
			},
	"method" : 'CONSTANT',
	
	"data" : {
					'k' : "campaign_data.get('ad-brix').get('app_key')",
	},
	
	"data_value" : {"puid":'""',
"google_ad_id":"device_data.get('adid')",
"user_demo_info":[{"demo_key":"CONSTANT","demo_value":"app_data.get('demo_value')"}]},	
	
	"return":"""
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}
	"""},	

"campaign.ad-brix.com/v1/CampaignVer2/GetSchedule":
{"start":"""
def ad_brix_get_schedule(campaign_data, app_data, device_data):
	
	url = 'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule'
	""",
	"method" : 'CONSTANT',
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},
		
	"data" : {
				'checksum': "BOOLEAN",
				'co' : "device_data.get('locale').get('country')",
				'google_ad_id': "device_data.get('adid')",
				'k': "campaign_data.get('ad-brix').get('app_key')",
				'la': "device_data.get('locale').get('language')",
				'os': "'a_'"+"+device_data.get('os_version')",
				'puid': "''",	
				'version' : "'a_'+device_data.get('os_version')"
	},
	"return":"""
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': data}		
"""},

"as.ad-brix.com/v1/users/login":
{"start":"""
def ad_brix_user_login(campaign_data, app_data, device_data):
	url = 'https://as.ad-brix.com/v1/users/login'
	""",
	"method" : 'CONSTANT',
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},
		
	"data" : {"appKey":"campaign_data.get('ad-brix').get('app_key')",
"adid":"device_data.get('adid')",
"userId":"app_data.get('demo_value')",
"deviceType":"CONSTANT"},
	"return":"""

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}	
	"""},

"as.ad-brix.com/v1/users/save":
{"start":"""

def ad_brix_users_save(campaign_data, app_data, device_data):
	url = 'https://as.ad-brix.com/v1/users/save'
	""",
	"method" : 'CONSTANT',	
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},
	"data" : {
"appKey":"campaign_data.get('ad-brix').get('app_key')",
"puid":"device_data.get('adid')",
"attrs":{"gp4":"device_data.get('locale').get('language')",
		 "gp7":"CONSTANT",
		 "ap10":"'a_'"+"+device_data.get('os_version')",
		 "gp1":"device_data.get('locale').get('country')",
		 "gp5":"device_data.get('model')"
		 },
"sessionId":"app_data.get('session_id')"
},
	"return":"""

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}	
"""},

"as.ad-brix.com/v1/users/updateConversion":
{"start":"""
def ad_brix_update_conversion(campaign_data, app_data, device_data):
	url = 'https://as.ad-brix.com/v1/users/updateConversion'
	""",
	"method" : "CONSTANT",
	"referrer_data":"""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
	""",
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},
			
	"data" : {
"appKey":"campaign_data.get('ad-brix').get('app_key')",
"adid":"device_data.get('adid')",
"userId":"app_data.get('demo_value')",
"ck":'conversion_key_value',
"sub_ck":'BOOLEAN'
},
	"return":"""	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}	
	"""},

"as.ad-brix.com/v1/users/updateRegistration":
{"start":"""
def ad_brix_update_registration(campaign_data, app_data, device_data):
	def_gcmToken(app_data)
	url = 'https://as.ad-brix.com/v1/users/updateRegistration'
	""",
	"method" : 'CONSTANT',
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},		
	"data" : {
"appKey":"campaign_data.get('ad-brix').get('app_key')",
"puid":'""',
"adid":"device_data.get('adid')",
"userId":"app_data.get('demo_value')",
"registrationId":"app_data.get('gcm')",
"sessionId":"app_data['session_id']",
},
	"return":"""
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}	
"""},

"as.ad-brix.com/v1/users/enablePushService":
{"start":"""
def enablepushservice(app_data,campaign_data,device_data):
	url='https://as.ad-brix.com/v1/users/enablePushService'
	""",
	"headers":{
	'Accept-Charset':'CONSTANT',
	'Accept-Encoding':'CONSTANT',
	'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
	'Content-Type':'CONSTANT',
	'Cookie': 'CONSTANT',

	},
	"method":'CONSTANT',

	"data":{

	"appKey":"campaign_data.get('ad-brix').get('app_key')", 
	"puid":"device_data.get('adid')", 
	'enable':"BOOLEAN",
	"sessionId":"app_data.get('session_id')"
	},

	"return":"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json.dumps(data)}	
	"""},

"as.ad-brix.com/v1/users/getPopups":
{"start":"""
def ad_brix_update_get_popups(campaign_data, app_data, device_data):

	url = 'https://as.ad-brix.com/v1/users/getPopups'
	""",
	"method" : 'CONSTANT',
	"headers":{
				'Accept-Charset': 'CONSTANT',
				'Content-Type': 'CONSTANT',
				'User-Agent':"'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'",
				'Accept-Encoding': 'CONSTANT',
				'Cookie': 'CONSTANT',
		},		
	"data" : {
	"appKey":"campaign_data.get('ad-brix').get('app_key')",
	"revision":"BOOLEAN"
	},	
	"return":"""	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}
	"""},
"event.adbrix.io/api/v1/deferred-deeplink":{"start":"""
def adbrix_deferred_deeplink(campaign_data, app_data, device_data):

	url= "http://event.adbrix.io/api/v1/deferred-deeplink/"+campaign_data.get("ad-brix").get("app_key")\n	""",
	"method": "post",
	"headers": {       
		"Accept-Charset": "CONSTANT",
        "Accept-Encoding": "CONSTANT",
        "Content-Type": "CONSTANT",
        "Cookie": "'abxusr='+app_data.get('abxusr')",
        "User-Agent": "'IGAWorks/'+campaign_data.get('ad-brix').get('ver')+' (Android '+device_data.get('os_version')+'; U; '+device_data.get('model')+' Build/'+device_data.get('build')+')'"
        },

	"params": {},

	"data": {
			"device_id": "device_data.get('adid')",
			"platform": "CONSTANT"
			},
	"return":
	"""
	return {"url": url, "httpmethod": method, "headers": headers, "params": None, "data": json.dumps(data)}	
	"""},
"event.adbrix.io/api/v1/event/single/":{"start":"""
def adbrix_event_single(campaign_data, app_data, device_data, event_name):
	previous_id=str(int(time.time()*1000))+":"+str(uuid.uuid4())

	url= "http://event.adbrix.io/api/v1/event/single/"+campaign_data.get("ad-brix").get("app_key")\n	""",
	"method": "post",
	"headers": {       "Accept-Charset": "CONSTANT",
        "Accept-Encoding": "CONSTANT",
        "Content-Type": "CONSTANT",
        "User-Agent": "'IGAWorks/'+campaign_data.get('ad-brix').get('ver')+' (Android '+device_data.get('os_version')+'; U; '+device_data.get('model')+' Build/'+device_data.get('build')+')'"},

	"params": {},
	"data":{},
	"data_value": {
			"common":{"request_datetime":"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			"identity":{"adid":"device_data.get('adid')",
			"idfv":"app_data.get('adid')",
			"ad_id_opt_out":"BOOLEAN",
			"user_hash":"BOOLEAN",
			"device_id":"BOOLEAN",
			"registration_id":"BOOLEAN",
			"is_push_enable":"BOOLEAN"},
			"device_info":{"os":"device_data.get('os_version')",
			"model":"device_data.get('model')",
			"vendor":"device_data.get('manufacturer')",
			"resolution":"device_data.get('resolution')",
			"is_portrait":"BOOLEAN",
			"platform":"CONSTANT",
			"network":"device_data.get('network').lower()",
			"carrier":"device_data.get('carrier')",
			"language":"device_data.get('locale').get('language')",
			"country":"device_data.get('locale').get('country')"},
			"package_name":"campaign_data.get('package_name')",
			"appkey":"campaign_data.get('ad-brix').get('app_key')",
			"api_version":"CONSTANT",
			"sdk_version":"campaign_data.get('ad-brix').get('ver')",
			"installer":"CONSTANT",
			"app_version":"campaign_data.get('app_version_name')",
			"build_id":"device_data.get('build')",
			"last_firstopen_id":"app_data.get('last_firstopen_id')",
			"last_deeplink_id":"BOOLEAN"},
			"evt":{"prev_id":"previous_id",
			"event_datetime":"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			"group":"CONSTANT",
			"event_name":"event_name",
			"param":{"market_install_btn_clicked":"int(app_data.get('times').get('click_time'))",
			"app_install_start":"int(app_data.get('times').get('download_begin_time'))",
			"app_install_completed":"int(app_data.get('times').get('install_complete_time'))",
			"referrer":"app_data.get('referrer')",
			"app_first_open":"app_data.get('app_first_open_time')",
			"abx:click_adkey":"BOOLEAN"},
			"session_interval":"BOOLEAN",
			"session_length":"BOOLEAN",
			"log_id":"app_data.get('last_firstopen_id')",
			"session_id":"app_data.get('session_id')",
			"location":"BOOLEAN",
			"user_snapshot_id":"BOOLEAN",
			"user_properties":"BOOLEAN"}
			},

	"return":
	"""	
	key = campaign_data.get('ad-brix').get('key_multipart')
	iv = campaign_data.get('ad-brix').get('iv_multipart')
	aes = AESCipher(key)
	data = aes.encrypt(iv,json.dumps(data_value))

	update_id(app_data,device_data,previous_id)
	
	return {"url": url, "httpmethod": method, "headers": headers, "params": None, "data": data}
	"""},

"event.adbrix.io/api/v1/event/bulk/":{"start":"""
def adbrix_event_bulk(campaign_data, app_data, device_data,eventName='',eventValue='',group='abx'):

	url= "http://event.adbrix.io/api/v1/event/bulk/"+campaign_data.get("ad-brix").get("app_key")\n	""",
	"method": "post",
	"headers": {       
		"Accept-Charset": "CONSTANT",
        "Accept-Encoding": "CONSTANT",
        "Content-Type": "CONSTANT",
        "Cookie": "'abxusr='+app_data.get('abxusr')",
        "User-Agent": "'IGAWorks/'+campaign_data.get('ad-brix').get('ver')+' (Android '+device_data.get('os_version')+'; U; '+device_data.get('model')+' Build/'+device_data.get('build')+')'"},

	"params": {},
	"data":{},
	"data_value" : {"common":{"request_datetime":"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			"identity":{"adid":"device_data.get('adid')",
			"idfv":"app_data.get('adid')",
			"ad_id_opt_out":"BOOLEAN",
			"user_hash":"BOOLEAN",
			"device_id":"BOOLEAN",
			"registration_id":"BOOLEAN",
			"is_push_enable":"BOOLEAN"},
			"device_info":{"os":"device_data.get('os_version')",
			"model":"device_data.get('model')",
			"vendor":"device_data.get('manufacturer')",
			"resolution":"device_data.get('resolution')",
			"is_portrait":"BOOLEAN",
			"platform":"CONSTANT",
			"network":"device_data.get('network').lower()",
			"carrier":"device_data.get('carrier')",
			"language":"device_data.get('locale').get('language')",
			"country":"device_data.get('locale').get('country')"},
			"package_name":"campaign_data.get('package_name')",
			"appkey":"campaign_data.get('ad-brix').get('app_key')",
			"api_version":"CONSTANT",
			"sdk_version":"campaign_data.get('ad-brix').get('ver')",
			"installer":"CONSTANT",
			"app_version":"campaign_data.get('app_version_name')",
			"build_id":"device_data.get('build')",
			"last_firstopen_id":"app_data.get('last_firstopen_id')",
			"last_deeplink_id":"BOOLEAN"},
			"evts":[{"prev_id":"BOOLEAN",
			"event_datetime":"""datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'""",
			"group":"group",
			"event_name":"eventName",
			"param":"eventValue",
			"session_interval":"BOOLEAN",
			"session_length":"BOOLEAN",
			"log_id":"CONSTANT",
			"session_id":"app_data.get('session_id')",
			"session_type":"BOOLEAN",
			"location":"BOOLEAN",
			"user_snapshot_id":"BOOLEAN",
			"user_properties":"BOOLEAN"}]
			},

	"return":"""
	if eventName=='start_session' and globals().get("session_flag")==False:		
		data_value['evts'][0]['prev_id']=None
		data_value['evts'][0]['log_id']=app_data.get('log_id')
		globals()["session_flag"] = True
	else:
		data_value['evts'][0]['prev_id']=app_data.get('log_id')
		data_value['evts'][0]['log_id']=str(int(time.time()*1000))+':'+str(uuid.uuid4())
	
	if not eventName=='start_session':
		del data_value['evts'][0]['session_type']


	if app_data.get('user_id'):
		data_value['evts'][0]['user_properties']={"user_id_hash":util.md5(app_data.get('user_id'))[1:],"user_id":app_data.get('user_id')}
		data_value['common']['identity']['user_hash']=util.md5(app_data.get('user_id'))[1:]
	else:
		data_value['evts'][0]['user_properties']={}
		data_value['common']['identity']['user_hash']=None

	if app_data.get('user_snapshot_id'):
		data_value['evts'][0]['user_snapshot_id']=app_data.get('user_snapshot_id')

	key = campaign_data.get('ad-brix').get('key_multipart')
	iv = campaign_data.get('ad-brix').get('iv_multipart')
	aes = AESCipherTracking(key)
	data = aes.encrypt(iv,json.dumps(data_value))
	
	update_id(app_data,device_data,data_value['evts'][0]['log_id'])
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}
	"""
	}
}

adbrix_android_required_functions="""
class AESCipher(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class AESCipherTracking(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]	

def def_gcmToken(app_data):
	if not app_data.get('gcm'):
		app_data['gcm']='APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def update_activity(app_data,device_data,group='',activity=''):
	app_data['group']=group
	app_data['activity']=activity
	
def get_demo_value(app_data,device_data):
	if not app_data.get('demo_value'):
		app_data['demo_value']='66-2-'+device_data.get('android_id')
		raise Exception('please enter demo_value')
	
def return_userinfo(app_data):
	
	# gender_n = app_data.get('gender')
	# app_data['gender'] = random.choice(['male', 'female'])
	# gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	user_data = util.generate_name(gender=random.choice(['male', 'female']))
	user_info = {}
	# user_info['username'] = user_data.get('username').lower()
	user_info['email'] = user_data.get('username').lower() + '@gmail.com'
	# user_info['f_name'] = user_data.get('firstname')
	# user_info['l_name'] = user_data.get('lastname')
	# user_info['sex'] = gender_n
	# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
	# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
	# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
	# user_info['password'] = util.get_random_string(type='all',size=16)
	app_data['user_info'] = user_info	
	
	return app_data.get('user_info')

def get_date(app_data,device_data,para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	make_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y%m%d%H%M%S")
	return date

def update_id(app_data,device_data,log_id=''):
	app_data['log_id']=log_id

def get_timestamp(para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	# time.sleep(random.randint(0,0))
	time1 = int(time.time())
	return time1

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

"""