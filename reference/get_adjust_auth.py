# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'auth.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Authorization(object):
    def setupUi(self, Authorization):
        Authorization.setObjectName(_fromUtf8("Authorization"))
        Authorization.resize(273, 167)
        self.label = QtGui.QLabel(Authorization)
        self.label.setGeometry(QtCore.QRect(20, 90, 91, 24))
        self.label.setMaximumSize(QtCore.QSize(16777166, 16777215))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Authorization)
        self.label_2.setGeometry(QtCore.QRect(20, 30, 91, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.spinBox = QtGui.QSpinBox(Authorization)
        self.spinBox.setGeometry(QtCore.QRect(130, 40, 71, 22))
        self.spinBox.setProperty("value", 1)
        self.spinBox.setObjectName(_fromUtf8("spinBox"))
        self.lineEdit = QtGui.QLineEdit(Authorization)
        self.lineEdit.setGeometry(QtCore.QRect(130, 90, 113, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.buttonBox = QtGui.QDialogButtonBox(Authorization)
        self.buttonBox.setGeometry(QtCore.QRect(90, 130, 156, 23))
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.buttonBox.clicked.connect(self.print_auth)

        self.retranslateUi(Authorization)
        QtCore.QMetaObject.connectSlotsByName(Authorization)

    def retranslateUi(self, Authorization):
        Authorization.setWindowTitle(_translate("Authorization", "Secret Key Required!!", None))
        self.label.setText(_translate("Authorization", "Secret_key", None))
        self.label_2.setText(_translate("Authorization", "Secret_id", None))

    def print_auth(self):
    	print self.spinBox.value()
    	print self.lineEdit.text()
    	sys.exit()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Authorization = QtGui.QDialog()
    ui = Ui_Authorization()
    ui.setupUi(Authorization)
    Authorization.setWindowIcon(QtGui.QIcon("reference/logo.png"))
    Authorization.show()
    sys.exit(app.exec_())

