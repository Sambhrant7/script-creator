import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","import time\n","import random\n","import datetime\n","import uuid\n","import urlparse\n","import urllib\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	

	###########		CALLS		################	
	

	###########		FINALIZE	################
	
	return {'status':'true'}
\n"""


definitions={"track.tenjin.io/v0/event":{"start":"""
###################################################################
# tenjin_event()	: method
# parameter 		: campaign_data, app_data, device_data,event_name
#
###################################################################
def tenjin_event(campaign_data, app_data, device_data,event_name=False):

""",
"url":"""'http://track.tenjin.io/v0/event'""",
"method":"post",
"headers" : {
		'Content-Type'		: 'CONSTANT',
		'Accept-Encoding'	: 'CONSTANT',
		'charset'			: 'CONSTANT',
		'User-Agent'		: "get_ua(device_data)",
		'Authorization'		: "'Basic '+campaign_data.get('tenjin').get('auth')",
		 },
"params":{},
"data" : {
			'advertising_id':"device_data.get('adid')",
			'app_version':"campaign_data.get('app_version_name')+'.'+campaign_data.get('app_version_code')",
			'build_id':	"device_data.get('build')",
			'bundle_id': "campaign_data.get('package_name')",
			'carrier': "device_data.get('carrier')",
			'connection_type':"device_data.get('network').lower()",
			'country':"device_data.get('locale').get('country')",
			'device':"device_data.get('device')",
			'device_model':	"device_data.get('model')",
			'language':"device_data.get('locale').get('language')",
			'limit_ad_tracking': 'CONSTANT',
			'locale':"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
			'os_version':"device_data.get('sdk')",
			'os_version_release':"device_data.get('os_version')",
			'platform':	'CONSTANT',
			'referrer':	"app_data.get('referrer') if app_data.get('referrer') else 'utm_source=(not%20set)&utm_medium=(not%20set)'",
			'referrer_click_ts': "app_data.get('times').get('click_time')",
			'referrer_install_ts': "app_data.get('times').get('download_begin_time')",
			'screen_height':"device_data.get('resolution').split('x')[0]",
			'screen_width':"device_data.get('resolution').split('x')[1]",
			'sdk_version':"campaign_data.get('tenjin').get('sdk')",
			'timezone':	"device_data.get('local_tz_name')",
			'utm_medium':	'CONSTANT',
			'device_brand':"device_data.get('brand')",
			'device_manufacturer':"device_data.get('manufacturer')",
			'device_product':"device_data.get('product')",
			'sent_at':"str(int(time.time()*1000))",
			'session_id':"app_data.get('session_id')",
			'tenjin_reference_id':"app_data.get('tenjin_reference_id')",
			'opt_in' : 'CONSTANT',
			'opt_out': 'CONSTANT'
	},
	'conditional_statements':
	"""
	if event_name:
			data['event'] = event_name
			del data['referrer']
			del data['referrer_click_ts']
			del data['referrer_install_ts']
			del data['utm_medium']
	""",


'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"track.tenjin.io/v0/user":{"start":"""
###################################################################
# tenjin_user()	: method
# parameter 		: campaign_data, app_data, device_data
#
###################################################################
def tenjin_user(campaign_data, app_data, device_data):
""",
	'url' : "'http://track.tenjin.io/v0/user'",
	'method' :"get",
	'headers' : {
		'User-Agent': "get_ua(device_data)",
		'Accept-Encoding': 'CONSTANT',
		},
	'params':{
			'advertising_id':"device_data.get('adid')",
			'api_key':"campaign_data.get('tenjin').get('apikey')",
			'app_version':"campaign_data.get('app_version_name')+'.'+campaign_data.get('app_version_code')",
			'build_id':	"device_data.get('build')",
			'bundle_id': "campaign_data.get('package_name')",
			'carrier': "device_data.get('carrier')",
			'connection_type':"device_data.get('network').lower()",
			'country':"device_data.get('locale').get('country')",
			'device':"device_data.get('device')",
			'device_model':	"device_data.get('model')",
			'language':"device_data.get('locale').get('language')",
			'limit_ad_tracking': 'CONSTANT',
			'locale':"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')",
			'os_version':"device_data.get('sdk')",
			'os_version_release':"device_data.get('os_version')",
			'platform':	'CONSTANT',
			'screen_height':"device_data.get('resolution').split('x')[0]",
			'screen_width':"device_data.get('resolution').split('x')[1]",
			'sdk_version':"campaign_data.get('tenjin').get('sdk')",
			'timezone':	"device_data.get('local_tz_name')",
			'device_brand':"device_data.get('brand')",
			'device_manufacturer':"device_data.get('manufacturer')",
			'device_product':"device_data.get('product')",
			'sent_at':"str(int(time.time()*1000))",
			'session_id':"app_data.get('session_id')",
			'tenjin_reference_id':"app_data.get('tenjin_reference_id')",
	},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""}}


tenjin_android_required_functions="""


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	

###########################################################
# Utility methods : TENJIN
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Tenjin 
###########################################################	

"""