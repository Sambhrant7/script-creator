import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import hashlib\n","import base64\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	
	###########		FINALIZE	################
	
	return {'status':'true'}
\n"""

definitions={"apsalar.com/api/v1/resolve":{"start":"""
#################################################
#												#
# 			AppSalar Resolve funcation 			#
#												#
#################################################

def apsalarResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False):
	custom_user_id_create(app_data)
""",	
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers":{
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},

	"params": {
					"seq"	:"app_data.get('seq')",
					"is"	:"CONSTANT",
					"install_receipt" : "install_receipt()",
					"device_type"	:"device_data.get('device_type')",
					"av"	: "campaign_data.get('app_version_name')",
					"cr"	: 'CONSTANT',
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"scs" 	: "CONSTANT",
					"rt"	:"CONSTANT",
					"tok" 	: "util.get_random_string(type='decimal',size=128)",
					"dnt"	:"CONSTANT",
					"custom_user_id" : "app_data.get('custom_user_id')",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"idfv" 	: "app_data.get('idfv_id')",
					"lag"	: "str(generateLag())",
					"idfa"  : "app_data.get('idfa_id')",
					"ddl_to" : "CONSTANT",
					"sdk"	: "campaign_data.get('apsalar').get('version')",
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"e"		: "value",
					"d"		: "device_data.get('device_platform','iPhone5,2')",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "ifu",
					"ddl_enabled"	:"CONSTANT",
					"n"		:"campaign_data.get('app_name')",
					"p"	 	: "CONSTANT",
					"s"		:"app_data.get('s')",
					"u"		:"CONSTANT",
					"t"		:"t",
					"v"		:"device_data.get('os_version')",
					"sc" 	: "CONSTANT",
				},

	"data":{},

	'conditional_statements':
"""
	if ifu=='APID':
		params['u']=str(uuid.uuid4()).upper()
	elif ifu=='IDFV':
		params['u']=app_data.get('idfv_id')
	elif ifu=='IDFA':
		params['u']=app_data.get('idfa_id')	

	if retries==True:	
		params["tries"] = "."

"""	,

	'return':"""

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }"""},	
"apsalar.com/api/v1/start":{"start":"""
#################################################
#												#
# 			AppSalar Start funcation 			#
#												#
#################################################

def apsalarStart(campaign_data, app_data, device_data):
	custom_user_id_create(app_data)

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

	name = urllib.quote(campaign_data.get('app_name'))	

""",	
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers" : {
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},


	"params" : {
					"seq"	:"app_data.get('seq')",
					"is"	:"CONSTANT",
					"install_receipt" : "install_receipt()",
					"device_type"	:"device_data.get('device_type')",
					"av"	: "campaign_data.get('app_version_name')",
					"cr"	: 'CONSTANT',
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"scs" 	: "CONSTANT",
					"rt"	:"CONSTANT",
					"tok" 	: "util.get_random_string(type='decimal',size=128)",
					"dnt"	:"CONSTANT",
					"custom_user_id" : "app_data.get('custom_user_id')",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"idfv" 	: "app_data.get('idfv_id')",
					"lag"	: "str(generateLag())",
					"idfa"  : "app_data.get('idfa_id')",
					"ddl_to" : "CONSTANT",
					"sdk"	: "campaign_data.get('apsalar').get('version')",
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"e"		: "value",
					"d"		: "device_data.get('device_platform','iPhone5,2')",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "ifu",
					"ddl_enabled"	:"CONSTANT",
					"n"		:"name",
					"p"	 	: "CONSTANT",
					"s"		:"app_data.get('s')",
					"u"		:"CONSTANT",
					"t"		:"t",
					"v"		:"device_data.get('os_version')",
					"sc" 	: "CONSTANT",

				},
			
	"data":{},

	'return':"""

	if params.get('scs'):
		params['scs'] = urllib.quote(str(params.get('scs')))

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }"""},

"apsalar.com/api/v1/event":{"start":"""
#################################################
#												#
# 			AppSalar Event funcation 			#
#												#
#################################################

def apsalar_event(campaign_data, app_data, device_data,event_name='',value='',custom_user_id=False):
	custom_user_id_create(app_data)

	if not app_data.get('seq'):
		app_data['seq'] = 1

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))	

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]
""",
	"url":'CONSTANT',
	"method":'CONSTANT',
	"headers":{
		'User-Agent': "campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language' : "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()",
	},

	"params": {
					"seq"	:"app_data.get('seq')",
					"is"	:"CONSTANT",
					"install_receipt" : "install_receipt()",
					"device_type"	:"device_data.get('device_type')",
					"av"	: "campaign_data.get('app_version_name')",
					"cr"	: 'CONSTANT',
					"install_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"scs" 	: "CONSTANT",
					"rt"	:"CONSTANT",
					"tok" 	: "util.get_random_string(type='decimal',size=128)",
					"dnt"	:"CONSTANT",
					"custom_user_id" : "app_data.get('custom_user_id')",
					"update_time"	:"str(int(app_data.get('times').get('install_complete_time')*1000))",
					"idfv" 	: "app_data.get('idfv_id')",
					"lag"	: "str(generateLag())",
					"idfa"  : "app_data.get('idfa_id')",
					"ddl_to" : "CONSTANT",
					"sdk"	: "campaign_data.get('apsalar').get('version')",
					"a"		: "campaign_data.get('apsalar').get('key')",
					"c"		: "CONSTANT",
					"e"		: "value",
					"d"		: "device_data.get('device_platform','iPhone5,2')",
					"i"		: "campaign_data.get('package_name')",
					"k"		: "ifu",
					"ddl_enabled"	:"CONSTANT",
					"n"		:"event_name",
					"p"	 	: "CONSTANT",
					"s"		:"app_data.get('s')",
					"u"		:"CONSTANT",
					"t"		:"t",
					"v"		:"device_data.get('os_version')",
					"sc" 	: "CONSTANT",

				},
	"data":{},

	'conditional_statements':
"""				
	if urllib.unquote(value)=='{}':
		if params.get('e'):
			del params['e']

	if custom_user_id:
		data['custom_user_id'] = 	app_data.get('custom_user_id')	

""",
	'return':"""	
	params = get_params(params)

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	if app_data.get('seq'):
		app_data['seq']=app_data.get('seq')+1

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }""",
}}


apsalar_ios_required_functions="""	
#####################################################
#													#
# 			Apsalar Utility function 				#
#													#
#####################################################


def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(value).encode('utf-8'))+'='+str(urllib.quote_plus(data_value.get(value)).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]

	return a
			
def generateLag():
	return (random.randint(40,300)*0.001)

def custom_user_id_create(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id']= '12035'+ str(random.randint(1000,9999))
		raise Exception('please enter custom_user_id')

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())


"""
	