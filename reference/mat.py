import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from Crypto.Cipher import AES\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	
	
	return {'status':'true'}
\n"""

definitions={"engine.mobileapptracking.com/serve":{"start":"""
###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def mat_serve(campaign_data, app_data, device_data,action,log=False):
	set_userAgent(app_data,device_data)
	return_userinfo(app_data)
	set_MATID(app_data)

	if ":" in action:
		site_event_name = action.split(":")[1]
		action = action.split(":")[0]

""",
"url":"""'http://'+campaign_data.get('mat').get('advertiser_id')+'.engine.mobileapptracking.com/serve'""",
"method":"post",
"headers" : {
		'User-Agent'		:"app_data.get('ua')",
		'Content-Type'		: 'CONSTANT',
		'Accept'			:'CONSTANT',
		'Accept-Encoding'	:'CONSTANT'
		
		 },
"params":{
		'ver'						:"campaign_data.get('mat').get('ver')",
		'transaction_id'			:"str(uuid.uuid4()).lower()",
		'sdk'						:"campaign_data.get('mat').get('sdk')",
		'sdk_retry_attempt'			:'BOOLEAN',
		'action'					:"action",
		'advertiser_id'				:"campaign_data.get('mat').get('advertiser_id')",
		'package_name'				:"campaign_data.get('package_name')",
		'referral_source'			:"campaign_data.get('package_name')",
		'referral_url'				:'CONSTANT',
		'advertiser_sub_campaign'	:'CONSTANT',
		'publisher_id'				:'CONSTANT',
		'attr_set'					:'CONSTANT'
},
"data" : {"data":'BOOLEAN'},
"data_value" : {
		"app_ad_tracking"				:'CONSTANT',
		'altitude'						:'CONSTANT',
		'app_name'						:"campaign_data.get('app_name')",
		'app_version'					:"campaign_data.get('app_version_code')",
		'app_version_name'				: "campaign_data.get('app_version_name')",
		'build'							: "device_data.get('build')",
		'connection_type' 				: 'CONSTANT',
		'conversion_user_agent'			:"app_data.get('ua')",
		'country_code'					:"device_data.get('locale').get('country').lower()",
		'currency_code'					:'CONSTANT',
		'mobile_country_code'			:"device_data.get('mcc')",
		'mobile_network_code'			:"device_data.get('mnc')",
		'device_brand'					:"device_data.get('brand')",
		'device_carrier'				:"device_data.get('carrier')",
		'device_cpu_type'				:"device_data.get('cpu_abi')",
		'device_model'					:"device_data.get('model')",
		'date1'							:"int(app_data.get('times').get('install_complete_time'))",
		'date2'							:"int(app_data.get('times').get('install_complete_time'))",
		'existing_user'					:'CONSTANT',
		'google_ad_tracking_disabled'	:'CONSTANT',
		'google_aid'					: "device_data.get('adid')",
		'is_coppa' 						:'CONSTANT',
		'is_paying_user'				:'BOOLEAN',
		'insdate'						:"int(app_data.get('times').get('install_complete_time'))",
		'language'						:"device_data.get('locale').get('language')",
		'locale'						:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper()",
		'latitude'						:'CONSTANT',
		'longitude'						:'CONSTANT',
		'mat_id'						:"app_data.get('mat_id')",
		'os_version'					:"device_data.get('os_version')",
		'revenue'						:'CONSTANT',
		'referrer_delay'				:"app_data.get('referrer_delay')",
		'referral_source'				:"campaign_data.get('package_name')",
		'referral_url'					:'CONSTANT',
		'attribute_sub1'				:'CONSTANT',
		'attribute_sub2'				:'CONSTANT',
		'attribute_sub3'				:'CONSTANT',
		'attribute_sub4'				:'CONSTANT',
		'attribute_sub5'				:'CONSTANT',
		'screen_density'				:"util.getdensity(device_data.get('dpi'))",
		'system_date'					:"str(int(time.time()))",
		'install_referrer' 				:'CONSTANT',
		'installer' 					:'CONSTANT',
		'screen_layout_size' 			:"device_data.get('resolution')",
		'sdk_plugin' 					:'CONSTANT',
		'sdk_version'					:"campaign_data.get('mat').get('ver')",
		'userid' 						:"CONSTANT",
		'user_name_md5'					:"util.md5(app_data.get('user_info').get('email'))",
		'user_name_sha1'			 	:"util.sha1(app_data.get('user_info').get('email'))",
		'user_name_sha256' 				:"util.sha256(app_data.get('user_info').get('email'))",
		'advertiser_ref_id'				:"str(uuid.uuid4()).upper()",
		'click_timestamp'				:"int(app_data.get('times').get('click_time'))",
		'download_date' 				:"int(app_data.get('times').get('click_time'))",
		'mac_address' 					:"device_data.get('mac').replace(':','')",
		'content_type'					:'CONSTANT',
		'fire_ad_tracking_disabled'		:'CONSTANT',
		'platform_ad_tracking_disabled'	:'CONSTANT',
		'platform_aid' 					:"device_data.get('adid')",
		'content_id'					:'CONSTANT',
		'search_string'					:'CONSTANT',
		'level'							:'CONSTANT',
		'advertiser_ref_id'				:"'GPA.33'+util.get_random_string('decimal',2)+'-'+util.get_random_string('decimal',4)+'-'+util.get_random_string('decimal',4)+'-'+util.get_random_string('decimal',5)",

		},

'conditional_statements':
"""
	if(log):
		if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
			data_value['open_log_id'] = str(app_data['open_log_id'])
			data_value['last_open_log_id'] = str(app_data['last_open_log_id'])
	if action=="conversion":
		params['site_event_name'] = site_event_name

	if app_data.get('referrer'):
		data_value['install_referrer'] = app_data.get('referrer')

	if not app_data.get('referrer_delay'):
		app_data['referrer_delay']=random.randint(100,200)

	da_str = urllib.urlencode(data_value)

	key = campaign_data.get('mat').get('key')
	iv = campaign_data.get('mat').get('iv')
	aes = AESCipher(key)
	params['data'] = aes.encrypt(iv, da_str)	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""}}


mat_android_required_functions="""


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	




def set_MATID(app_data):
	if not app_data.get('mat_id'):
		app_data['mat_id'] = str(uuid.uuid4()).upper()

def set_matOpenLogID(app_data):
	if app_data.get('last_open_log_id'):
		app_data['open_log_id'] = app_data['last_open_log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="PHPSESSID":
			header = response.get('res').headers
			return header.get('Set-Cookie').split("PHPSESSID=")[1].split(";")[0]
		if paramName=='COOKIE':
			header = response.get('res').headers
			return header.get('Set-Cookie')
		if paramName=="user_id":
			jsonData = json.loads(response.get('data'))
			app_data['user_id'] = jsonData.get('result').get('Achievements_getUserAchievementsAndProgress')[0].get('user_achievement').get('user_id')
		if paramName=="mat":
			jsonData = json.loads(response.get('data'))
			app_data['last_open_log_id'] = jsonData.get('log_id')

	except:
		print "Exception:: Couldn't fetch "+paramName+" from response"

class AESCipher:
 def __init__(self, key):
		self.key = key

 def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

 def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
		enc = enc.decode('hex')#base64.b64decode(enc)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)

def set_userAgent(app_data,device_data):
	if not app_data.get('ua'):
		if int(device_data.get("sdk")) >19:
			app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
		else:
			app_data['ua'] = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'



def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info




"""