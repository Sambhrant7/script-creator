import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import string\n","import datetime\n","import uuid\n","import urlparse\n","import clicker\n","import Config\n","import os\n"]


open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	

	###########		CALLS		################	
	

	###########		FINALIZE	################
	
	return {'status':'true'}
\n"""


definitions={"app-adforce.jp/ad/p/tmck":{"start":"""
###################################################################
# adforce_tmck()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
###################################################################
def adforce_tmck(campaign_data, app_data, device_data):

""",
"url":"""'http://app-adforce.jp/ad/p/tmck'""",
"method":"get",
"headers" : {
		'Accept-Encoding'	: 'CONSTANT',
		'Accept': 'CONSTANT',
		'User-Agent'		: "'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/'+device_data.get('device')",
		 },
"params":{
				'_adid':"'00'+util.md5(device_data.get('adid'))",
				'_adte': 'BOOLEAN',
				'_app':	"campaign_data.get('adforce').get('app_id')",
				'_build': "device_data.get('build')",
				'_bundle_id': "campaign_data.get('package_name')",
				'_bv':	"campaign_data.get('app_version_name')",
				'_country' : "device_data.get('locale').get('country')",
				'_install_id' : "app_data.get('install_id')",
				'_language' : "device_data.get('locale').get('language')",
				'_model': "device_data.get('model')",
				'_os_ver':	"device_data.get('os_version')",
				'_sdk_ver':	"campaign_data.get('adforce').get('sdk_version')",
				'_icc'	: "device_data.get('locale').get('country').lower()",
				'_sim_carrier': "device_data.get('carrier')",
				'_sim_icc' : "device_data.get('locale').get('country').lower()",
				'_carrier'	: "device_data.get('carrier')",
				'_mcc' 	: "device_data.get('mcc')",
				'_mnc'	: "device_data.get('mnc')",
				'_sim_mcc' : "device_data.get('mcc')",
				'_sim_mnc' : "device_data.get('mnc')",
	},
"data" : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/view/collect.html":{"start":"""
###################################################################
# adforce_collect()	: method
# parameter 		: campaign_data, app_data, device_data
#
###################################################################
def adforce_collect(campaign_data, app_data, device_data):
""",
	'url' : "'http://app-adforce.jp/ad/view/collect.html'",
	'method' :"get",
	'headers' : {
		'Upgrade-Insecure-Requests': 'CONSTANT',
		'User-Agent': "device_data.get('User-Agent')",
		'Accept': 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
		'X-Requested-With': "campaign_data.get('package_name')"
		 },
	'params':{},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"analytics.app-adforce.jp/fax/analytics":{"start":"""
###################################################################
# adforce_analytics()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_analytics(campaign_data, app_data, device_data):

""",

	'url' : "'http://analytics.app-adforce.jp/fax/analytics'",
	'method' :'post',
	'headers' : {
		'User-Agent': "'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/'+device_data.get('model')",
		'Content-Type': 'CONSTANT',
		'Accept': 'CONSTANT',
		 },
	'params' : {
				'_adid':"'00'+util.md5(device_data.get('adid'))",
				'_adte': 'BOOLEAN',
				'_app':	"campaign_data.get('adforce').get('app_id')",
				'_build': "device_data.get('build')",
				'_bundle_id': "campaign_data.get('package_name')",
				'_carrier' : "device_data.get('carrier')",
				'_country' : "device_data.get('locale').get('country')",
				'_install_id' : "app_data.get('install_id')",
				'_language' : "device_data.get('locale').get('language')",
				'_model': "device_data.get('model')",
				'_os_ver':	"device_data.get('os_version')",
				'_icc'	: "device_data.get('locale').get('country').lower()",
				'_sim_carrier': "device_data.get('carrier')",
				'_sim_icc' : "device_data.get('locale').get('country').lower()",
				'_sdk_ver':	"campaign_data.get('adforce').get('sdk_version')",
				'_mcc' 	: "device_data.get('mcc')",
				'_mnc'	: "device_data.get('mnc')",
				'_sim_mcc' : "device_data.get('mcc')",
				'_sim_mnc' : "device_data.get('mnc')",
	},

	'data' : {
				'd' :'BOOLEAN',
				'e': 'BOOLEAN',
				'o': 'BOOLEAN',
				'p':"'00'+util.md5(device_data.get('adid'))",
				'v' : "campaign_data.get('adforce').get('sdk_version')"
	},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/p/cv->cv":{"start":"""
###################################################################
# adforce_cv()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_cv(campaign_data, app_data, device_data):

""",
	'url': "'http://app-adforce.jp/ad/p/cv'",
	'method' :"get",
	'headers' : {
		'Accept-Encoding'	:'CONSTANT',
		'Accept': 'CONSTANT',
		'User-Agent'		:"'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/GL'",
		 },

	'params': {
				'_adid':"'00'+util.md5(device_data.get('adid'))",
				'_adte': 'BOOLEAN',
				'_app':	"campaign_data.get('adforce').get('app_id')",
				'_build': "device_data.get('build')",
				'_bundle_id': "campaign_data.get('package_name')",
				'_country' : "device_data.get('locale').get('country')",
				'_cv_target' : 'CONSTANT',
				'_fpid'	: "urlparse.unquote(str(fp_ID))",
				'_fptdl':"fp_TDL",	
				'_gms_version': 'CONSTANT',
				'_hash': "util.get_random_string('hex',40)",
				'_install_id' : "app_data.get('install_id')",
				'_language' : "device_data.get('locale').get('language')",
				'_local_xuniq' : 'BOOLEAN',
				'_model':"device_data.get('model')",
				'_os_ver':"device_data.get('os_version')",
				'_rurl' : 'CONSTANT',
				'_sdk_ver':"campaign_data.get('adforce').get('sdk_version')",
				'_ua': "device_data.get('User-Agent')",
				'_use_bw' : 'CONSTANT',
				'_xevent' : 'BOOLEAN',
				'_xuniq':"'00'+util.md5(device_data.get('adid'))",				
				'_xuniq_type' : 'BOOLEAN',
				'_carrier':"device_data.get('carrier')",
				'_icc':"device_data.get('locale').get('country').lower()",
				'_sim_carrier':"device_data.get('carrier')",
				'_sim_icc':"device_data.get('locale').get('country').lower()",
				'_xroute':'CONSTANT',
				'_xuid':'CONSTANT',
				'_mcc' 	: "device_data.get('mcc')",
				'_mnc'	: "device_data.get('mnc')",
				'_sim_mcc' : "device_data.get('mcc')",
				'_sim_mnc' : "device_data.get('mnc')",
				'_referrer_click_time' : "int(app_data.get('times').get('click_time'))",
				'_referrer_install_begin_time' : "int(app_data.get('times').get('download_end_time'))"
	},
	'data' : {},
		
'conditional_statements':
"""
	if app_data.get('uid'):
		headers['Cookie']='uid='+app_data.get('uid')
		
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('_xroute'):
			params['_xroute'] = temp_a['_xroute'][0]
			
		if temp_a.get('_xuid'):
			params['_xuid'] = temp_a['_xuid'][0]

		
		params['_referrer'] = urlparse.unquote(app_data.get('referrer'))
""",
'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/p/cv->short":{"start":"""
###################################################################
# adforce_cv()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_cv_short(campaign_data, app_data, device_data):

""",
	'url': "'http://app-adforce.jp/ad/p/cv'",
	'method' :"get",
	'headers' : {
		'Accept-Encoding'	:'CONSTANT',
		'Accept': 'CONSTANT',
		'User-Agent'		:"'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/GL'",
		 },
	'params': {
				'_app':	"campaign_data.get('adforce').get('app_id')",
				'_xuniq':"'00'+util.md5(device_data.get('adid'))",	
				'_model':"device_data.get('model')",
				'_adid':"'00'+util.md5(device_data.get('adid'))",
				'_adte': 'BOOLEAN',
				'_buid':'BOOLEAN',
				'_build':"device_data.get('build')",
				'_bundle_id':"campaign_data.get('package_name')",
				'_country':"device_data.get('locale').get('country')",
				'_cvpoint':'BOOLEAN',
				'_gms_version':'CONSTANT',
				'_hash': "util.get_random_string('hex',40)",
				'_install_id' : "app_data.get('install_id')",
				'_language':"device_data.get('locale').get('language')",
				'_local_xuniq':'CONSTANT',
				'_os_ver':"device_data.get('os_version')",
				'_price':'CONSTANT',
				'_sdk_ver':"campaign_data.get('adforce').get('sdk_version')",
				'_xuniq_type' : 'BOOLEAN',
				'_carrier':"device_data.get('carrier')",	
				'_icc':"device_data.get('locale').get('country').lower()",	
				'_sim_carrier':"device_data.get('carrier')",	
				'_sim_icc':"device_data.get('locale').get('country').lower()",	
				'_mcc' 	: "device_data.get('mcc')",
				'_mnc'	: "device_data.get('mnc')",
				'_sim_mcc' : "device_data.get('mcc')",
				'_sim_mnc' : "device_data.get('mnc')",
				'_xroute':'CONSTANT',
				'_xuid':'CONSTANT',
	},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/i/fprc":{"start":"""
###################################################################
# adforce_fprc()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_fprc(campaign_data, app_data, device_data):

""",
	'url': "'http://app-adforce.jp/ad/i/fprc'",
	'method' :"get",
	'headers' : {
		'User-Agent': "device_data.get('User-Agent')",
		'Referer': 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
		'X-Requested-With': "campaign_data.get('package_name')"
		 },
	'params': {},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/p/fp":{"start":"""
###################################################################
# adforce_fp()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_fp(campaign_data, app_data, device_data):

""",
	'url': "'http://app-adforce.jp/ad/p/fp'",
	'method' :"get",
	'headers' : {
		'Accept' : 'CONSTANT',
		'User-Agent': "device_data.get('User-Agent')",
		'Referer': 'CONSTANT',
		'Accept-Encoding': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
		'X-Requested-With': 'CONSTANT'
		 },
	'params': {
		'_fpsd' : 'CONSTANT'
	},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""},

"app-adforce.jp/ad/p/jump":{"start":"""
###################################################################
# adforce_p_jump()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_p_jump(campaign_data, app_data, device_data):

""",
	'url': "'http://app-adforce.jp/ad/p/jump'",
	'method' :"get",
	'headers' : {
		'Upgrade-Insecure-Requests' : 'CONSTANT',
		'Accept' : 'CONSTANT',
		'User-Agent': "device_data.get('User-Agent')",
		'Accept-Encoding': 'CONSTANT',
		'Accept': 'CONSTANT',
		'Accept-Language': "device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')",
		'Cookie': "'uid='+app_data.get('cookie_uid')+'; _history_app='+campaign_data.get('adforce').get('app_id')"
		 },
	'params': {
		'_app':	"campaign_data.get('adforce').get('app_id')",
		'_rurl':'CONSTANT'
	},
	'data' : {},

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}"""}}

adforce_android_required_functions="""


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	

###########################################################
# Utility methods : ADFORCE
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adforce 
###########################################################	

"""