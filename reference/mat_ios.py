import_list=["# -*- coding: utf-8 -*-\n","import sys\n","reload(sys)\n","sys.setdefaultencoding('utf-8')\n","import requests\n","from sdk import util\n","from sdk import NameLists\n","import time\n","import random\n","import json\n","import urllib\n","import datetime\n","import urlparse\n","import uuid\n","import clicker\n","import Config\n","from Crypto.Cipher import AES\n","import base64\n","import string\n","import os\n"]

open_function="""\n
#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	
	
	return {'status':'true'}
\n"""

definitions={"engine.mobileapptracking.com/serve":{"start":"""
###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def mat_serve(campaign_data, app_data, device_data,action,log=False):
	return_userinfo(app_data)

	if ":" in action:
		site_event_name = action.split(":")[1]
		action = action.split(":")[0]

	if action== 'install':
		params['post_conversion']=1

""",
"url":"""'http://%s.engine.mobileapptracking.com/serve' % campaign_data.get('mat').get('advertiser_id')""",
"method":"post",
"headers" : {
		'User-Agent'		:"campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]",
		'Content-Type'		: 'CONSTANT',
		'Accept'			:'CONSTANT',
		'Accept-Encoding'	:'CONSTANT',
		'x-dynatrace'		:'CONSTANT',
		'Accept-Language'	:"device_data.get('locale').get('language').lower()+'-'+device_data.get('locale').get('country').lower()",
		'X-Unity-Version' 	:'CONSTANT',
		
		 },
"params":{
		'ver'						:"campaign_data.get('mat').get('ver')",
		'transaction_id'			:"str(uuid.uuid4()).upper()",
		'sdk'						:"campaign_data.get('mat').get('sdk')",
		'sdk_retry_attempt'			:'BOOLEAN',
		'action'					:"action",
		'advertiser_id'				:"campaign_data.get('mat').get('advertiser_id')",
		'package_name'				:"campaign_data.get('package_name')",
		'response_format'			:'CONSTANT',
},
"data" : {"data":'BOOLEAN'},


"data_value" : {
		'altitude'						:'CONSTANT',
		'app_name'						:"campaign_data.get('app_name')",
		'app_version'					:"campaign_data.get('app_version_code')",
		'app_version_name'				: "campaign_data.get('app_version_name')",
		'build'							: "device_data.get('build')",
		'connection_type' 				: 'CONSTANT',
		'conversion_user_agent'			:"device_data.get('User-Agent')",
		'country_code'					:"device_data.get('locale').get('country').lower()",
		'currency_code'					:'CONSTANT',
		'carrier_country_code'			:"device_data.get('locale').get('country').lower()",
		'mobile_country_code'			:"device_data.get('mcc')",
		'mobile_network_code'			:"device_data.get('mnc')",
		'device_brand'					:"CONSTANT",
		'device_carrier'				:"device_data.get('carrier')",
		'device_cpu_type'				:"app_data.get('device_cpu_type')",
		'device_cpu_subtype'			:'BOOLEAN',
		'device_model'					:"device_data.get('device_platform')",
		'date1'							:"int(app_data.get('times').get('install_complete_time'))",
		'date2'							:"int(app_data.get('times').get('install_complete_time'))",
		'existing_user'					:'CONSTANT',
		'google_ad_tracking_disabled'	:'CONSTANT',
		'is_coppa' 						:'CONSTANT',
		'is_paying_user'				:'BOOLEAN',
		'insdate'						:"int(app_data.get('times').get('install_complete_time'))",
		'language'						:"device_data.get('locale').get('language')",
		'locale'						:"device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper()",
		'latitude'						:'CONSTANT',
		'longitude'						:'CONSTANT',
		'mat_id'						:"app_data.get('mat_id')",
		'os_version'					:"device_data.get('os_version')",
		'revenue'						:'CONSTANT',
		'referrer_delay'				:"app_data.get('referrer_delay')",
		'referral_source'				:"campaign_data.get('package_name')",
		'referral_url'					:'CONSTANT',
		'attribute_sub1'				:'CONSTANT',
		'attribute_sub2'				:'CONSTANT',
		'attribute_sub3'				:'CONSTANT',
		'attribute_sub4'				:'CONSTANT',
		'attribute_sub5'				:'CONSTANT',
		'screen_density'				:"util.getdensity(device_data.get('dpi'))",
		'system_date'					:"str(int(time.time()))",
		'screen_layout_size' 			:"device_data.get('resolution')",
		'sdk_plugin' 					:'CONSTANT',
		'sdk_version'					:"campaign_data.get('mat').get('ver')",
		'userid' 						:'CONSTANT',
		'user_email_md5'				:"util.md5(app_data.get('user_info').get('email'))",
		'user_email_sha1'			 	:"util.sha1(app_data.get('user_info').get('email'))",
		'user_email_sha256' 			:"util.sha256(app_data.get('user_info').get('email'))",
		'user_name_md5'					:"util.md5(app_data.get('user_info').get('username'))",
		'user_name_sha1'			 	:"util.sha1(app_data.get('user_info').get('username'))",
		'user_name_sha256' 				:"util.sha256(app_data.get('user_info').get('username'))",
		'gender'						:'BOOLEAN',
		'advertiser_ref_id'				:"str(uuid.uuid4()).upper()",
		'click_timestamp'				:"int(app_data.get('times').get('click_time'))",
		'download_date' 				:"int(app_data.get('times').get('click_time'))",
		'mac_address' 					:"device_data.get('mac').replace(':','')",
		'content_type'					:'CONSTANT',
		'fire_ad_tracking_disabled'		:'CONSTANT',
		'platform_ad_tracking_disabled'	:'CONSTANT',
		'content_id'					:'CONSTANT',
		'search_string'					:'CONSTANT',
		'level'							:'CONSTANT',
		'ios_ad_tracking'				:'1',
		'ios_ifa'						:"app_data.get('idfa_id')",
		'ios_ifv'						:"app_data.get('idfv_id')",
		'ios_purchase_status'			:'CONSTANT',
		'location_auth_status' 			:'BOOLEAN',
		'location_vertical_accuracy'	:'BOOLEAN',
		'location_horizontal_accuracy'	:'BOOLEAN',
		'location_timestamp'			:'BOOLEAN',
		'is_testflight_build'			:'BOOLEAN',
		'os_jailbroke'					:'BOOLEAN',
		'iad_attribution'				:'BOOLEAN',
		'package_name' 					:"campaign_data.get('package_name')",
		'quantity'						:'CONSTANT',
		'rating'						:'CONSTANT',
		'screen_size'					:"device_data.get('resolution')",
		"device_form" 					:'CONSTANT',
		"user_id" 						:'CONSTANT',
		"bundle_id"						:"campaign_data.get('package_name')"
		},

'conditional_statements':
"""
	if(log):
		if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
			data_value['open_log_id'] = str(app_data['open_log_id'])
			data_value['last_open_log_id'] = str(app_data['last_open_log_id'])
	if action=="conversion":
		params['site_event_name'] = site_event_name

	

	if not app_data.get('referrer_delay'):
		app_data['referrer_delay']=random.randint(100,200)

	da_str = urllib.urlencode(data_value)

	key = campaign_data.get('mat').get('key')
	data_str = encrypt_AES(key,da_str)
	
	params['data'] = data_str	
""",

'return':"""
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}"""}}


mat_ios_required_functions="""


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	




def set_MATID(app_data):
	if not app_data.get('mat_id'):
		app_data['mat_id'] = str(uuid.uuid4()).upper()

def set_matOpenLogID(app_data):
	if app_data.get('last_open_log_id'):
		app_data['open_log_id'] = app_data['last_open_log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="PHPSESSID":
			header = response.get('res').headers
			return header.get('Set-Cookie').split("PHPSESSID=")[1].split(";")[0]
		if paramName=='COOKIE':
			header = response.get('res').headers
			return header.get('Set-Cookie')
		if paramName=="user_id":
			jsonData = json.loads(response.get('data'))
			app_data['user_id'] = jsonData.get('result').get('Achievements_getUserAchievementsAndProgress')[0].get('user_achievement').get('user_id')
		if paramName=="mat":
			jsonData = json.loads(response.get('data'))
			app_data['last_open_log_id'] = jsonData.get('log_id')

	except:
		print "Exception:: Couldn't fetch "+paramName+" from response"

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def encrypt_AES(key, data):
	data=pad(data.encode('hex').upper())
	return AES.new(key, AES.MODE_ECB).encrypt(data).encode('hex').upper()


def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info

def ios_device_data(app_data):
	if not app_data.get('ios_device_data'):
		app_data['ios_device_data'] = {
		'iPad mini 2':'16777228',
		'iPad mini Wi-Fi':'12',
		'iPad mini Wi-Fi + Cellular':'12',
		'iPad 4 Wi-Fi':'16777228',
		'iPad 4 Wi-Fi + Cellular':'16777228',
		'iPhone 7 Plus':'16777228',
		'iPhone 7':'16777228',
		'iPhone SE':'16777228',
		'iPhone 6s Plus':'16777228',
		'iPhone 6s':'16777228',
		'iPhone 6 Plus':'16777228',
		'iPhone 6':'16777228', 
		'iPhone 5s':'16777228', 
		'iPhone 5c':'12',
		'iPhone 5 CDMA':'12',
		'iPhone 5':'12',
		'iPhone 4s':'12',
		'iPhone 4 CDMA':'12',
		'iPhone 4':'12',
		'iPad Air':'16777228',
		'iPad mini 3':'16777228',
		'iPad Air 2':'16777228',
		'iPad mini 4':'16777228'
		}
	return app_data.get('ios_device_data')




"""