"""* 
	>>>>FILE IMPORTS<<<<
*"""
from static_data_lists import campaign_data, open_function
from static_data_lists import install_function
from static_data_lists import imports
from match_data import access_required_data
from adbrix_aes256 import decrypted_data
from mat_aes import mat_decrypted_data
from filter import filter_logs_data
from time_sleep_generator import calculate_time_differences


"""* 
	>>>>MODULE IMPORTS<<<<
*"""
import os, shutil
getPath = os.getcwd()


"""* 
	>>>>GARBAGE REMOVAL<<<<
*"""
try:
	shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
	os.remove(getPath + "/Input/res/source.zip")
except:
	pass

"""* 
	>>>>WRITTING STRUCTURE<<<<
*"""
def writting_robo(platform, logs, file_name="test", key=None, ping="Offline", url="", ui=""):
	"""* 
		>>>>PLATFORM DEPENDENT IMPORTS<<<<
	*"""
	if platform == "Android":
		from static_data_lists import android_required_functions
		from android_creator_base import output_file_formatting, tracking_def_list
		required_functions = android_required_functions
	elif platform == "ios":
		from static_data_lists import ios_required_functions
		from ios_creator_base import output_file_formatting, tracking_def_list
		required_functions = ios_required_functions
	########################################################################################

	"""* 
		>>>>FILTER INITIALIZE<<<<
	*"""
	a = filter_logs_data(logs, platform, key)
	calls_data_dict = a[0]
	genuine_hosts_data = a[1]
	event_list = a[2]
	event_data_list = a[3]
	########################################################################################

	"""* 
		>>>>GET TRACKINGS<<<<
	*"""
	trackings = access_required_data(calls_data_dict).get_hosts()
	if 'Adbrix' in trackings and not key:
		shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)  #
		os.remove(getPath + "/Input/res/source.zip")
		if "event.adbrix.io/api/v1/event/single/" in calls_data_dict.get('host') or "event.adbrix.io/api/v1/event/bulk/" in calls_data_dict.get('host'):
			return "REQUIRES MULTIPART KEY"
		else:
			return "REQUIRES KEY"

	print "All trackings present in app:- "
	print trackings
	########################################################################################
	"""* 
		>>>>PROCESS BREAKDOWN INTO MODULES<<<<
	*"""
	ts_data = output_file_formatting().tracking_data_handling(campaign_data, install_function, trackings,
															  calls_data_dict, key, ping, url, ui)
	try:
		install_function.extend(calculate_time_differences(trackings, ts_data))
	except:
		install_function.extend("	raise Exception('calculate_time_differences')")
	output_file_formatting().common_campaign_data(campaign_data, trackings)
	output_file_formatting().campaign_data_dictionary(campaign_data, trackings)
	output_file_formatting().campaign_data_retention(campaign_data)
	output_file_formatting().add_calls(install_function, calls_data_dict, genuine_hosts_data)
	el = output_file_formatting().event_definitions(event_list, event_data_list, trackings)

	########################################################################################
	################### WRITTING SCRIPT ######################
															 #
	f = open(getPath + "/Output/" + file_name + ".py", 'a')  #
	for i in imports:										 #
		f.write(i)										     #
	for j in campaign_data:								     #
		f.write(j)										     #
	for k in install_function:							     #
		if k:												 #
			f.write(k)									     #
	f.write(open_function)								     #
	for l in el:											 #
		f.write(l)										     #
	for m in tracking_def_list:							     #
		f.write(m)										     #
	for o in required_functions:							 #
		f.write(o)										     #
	f.close()												 #
	shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
	os.remove(getPath + "/Input/res/source.zip")			 #
	##########################################################
	
	######################## WRITTING DECRYPTION DATA #############################
	if 'Adbrix' in trackings:
		fd = open(getPath + "/report/" + file_name + "_adbrix_decrypted.txt", 'a')
		for dd in decrypted_data:
			fd.write(dd)
		fd.close()

	if 'MAT' in trackings:
		matD = open(getPath + "/report/" + file_name + "_mat_decrypted.txt", 'a')
		for matdd in mat_decrypted_data:
			matD.write(matdd)
		matD.close()

	###############################################################################
	return True
