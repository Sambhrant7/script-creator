# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import re, json
import subprocess

"""* 
	>>>>FILE IMPORTS<<<<
*"""

from reference import Appsflyer, Adjust, Adbrix, mat, Apsalar, Kochava,Adforce , Tenjin , branch
from static_data_lists import imports
from static_data_lists import android_required_functions
from static_data_lists import campaign_ending, event_definition_start
from match_data import access_required_data

tracking_def_list = []

"""* 
	>>>>PYSIDE IMPORT<<<<
*"""

try:
	from PySide import QtGui, QtCore
except ImportError:
	pySide_import_error = "PySide was Not Found."
	print "PySide not installed "
	print "Please wait installing PySide..."
	subprocess.call([sys.executable, "-m", "pip", "install", "PySide"])
	from PySide import QtGui, QtCore

global minimum_os
minimum_os = "4.4"


"""* 
	>>>>PLAYSTORE CALL TO FETCH APP DETAILS<<<<
*"""
def playstore_details(pkg_name):
	url = 'https://play.google.com/store/apps/details'
	method = 'get'
	headers = {
		'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Acer Build/2323)',
		'Accept-Encoding': 'gzip',
	}
	params = {'id': pkg_name}
	data = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None}

"""* 
	>>>>FETCH DETAILS FROM PLAYSTORE RESPONSE<<<<
*"""
def fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online):
	if online:
		playstore = playstore_details(main_campaign_data.get('app_id'))
		result = requests.get(playstore.get('url'), headers=playstore.get('headers'),
								params=playstore.get('params'), data=None, timeout=30, verify=False,
								auth=None, files=None)
		try:
			play_res = str((result.text).encode('utf-8'))
			r = play_res.split("Additional Information")[1].split("Requires Android")[1].split("</span>")[
				0].rsplit(">", 1)[1].strip()
			try:
				globals()['minimum_os'] = re.findall(r"[-+]?\d*\.\d+|\d+", r)[0]
			except:
				pass
			app_size = str(float(
				play_res.split("Additional Information")[1].split("Size")[1].split("</span>", 1)[0].rsplit(
					">", 1)[1].strip("M")))
			campaign_data.append("	'app_size'			 : " + app_size + ",\n")
		except:
			play_res = ""
			ui.status.deleteLater()
			ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
			ui.horizontal_items.addWidget(ui.status)
			app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!","Please enter app_size manually, either you are not connected to Internet or the app_size varies with device:-")
			print "app_size you entered:- "+str(app_size)
			campaign_data.append("	'app_size'			 : " + app_size + ",\n")
	else:
		play_res=""
		ui.status.deleteLater()
		ui.status = QtGui.QLabel('------>ENTER APP SIZE!!')
		ui.horizontal_items.addWidget(ui.status)
		app_size, ok = QtGui.QInputDialog.getText(ui, "Could not fetch app_size!!","Please enter app_size manually, as it seems you are not connected to Internet:-")
		print "app_size you entered:- "+str(app_size)
		campaign_data.append("	'app_size'			 : " + app_size + ",\n")
	install_function.append(
				"	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')\n")
	return play_res
			

"""* 
	>>>>CLASS TO FORMAT REQUIRED DATA IN SCRIPT<<<<
*"""
class output_file_formatting:
	def __init__(self):
		pass

	"""* 
		>>>>FUNCTION TO HANDLE TRACKING WISE DATA IN SCRIPT<<<<
	*"""
	def tracking_data_handling(self, campaign_data, install_function, trackings, calls_data_dict, key=None,
								 ping="Offline", url="", ui=""):
		online = False
		if ping == "Online":
			online = True
		print "Internet Status:-"+ping
		times_and_stamps = {}

		##################################################################################################
		if 'Appsflyer' in trackings:
			#ADD IMPORTS#
			for library in Appsflyer.import_list:
				if not library in imports:
					imports.append(library)

			#GET APPSFLYER RELATED DATA#
			appsflyer_data = access_required_data(calls_data_dict).appsflyer_definition_data
			main_campaign_data = appsflyer_data[2]
			tracking_def_list.extend(appsflyer_data[0])
			global appsflyer_campaign_data
			appsflyer_campaign_data = appsflyer_data[1]
			times_and_stamps['Appsflyer'] = appsflyer_data[3]


			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('app_id') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_name') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_code' 	 :'" + main_campaign_data.get('app_version_code') + "',\n")
			campaign_data.append("	'CREATE_DEVICE_MODE' : 3,\n")
			############# INSTALL DATA ###############

			install_function.append("""	batterypercentage.isBatteryPercent(app_data, day='newsession')\n	AF_SENSOR.init(device_data['sensors'])\n	batteryChargingStatus(app_data)\n	def_appsflyerUID(app_data)\n	def_eventsRecords(app_data)\n	app_data['registeredUninstall']=False\n""")
			android_required_functions.insert(0, Appsflyer.appsflyer_required_functions)

		##################################################################################################
		if 'Adjust' in trackings:
			#ADD IMPORTS#
			for library in Adjust.import_list:
				if not library in imports:
					imports.append(library)

			#GET ADJUST RELATED DATA#
			adjust_data = access_required_data(calls_data_dict).adjust_definition_data()
			main_campaign_data = adjust_data[2]
			tracking_def_list.extend(adjust_data[0])
			global adjust_campaign_data
			adjust_campaign_data = adjust_data[1]
			times_and_stamps['Adjust'] = adjust_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append("	'app_version_name' 	 :'" + main_campaign_data.get('app_version') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version_short'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + main_campaign_data.get('app_version_short') + "',\n")

			############# INSTALL DATA ###############
			install_function.append("""	pushtoken(app_data)\n	set_androidUUID(app_data)\n""")
			android_required_functions.insert(0, Adjust.adjust_android_required_functions)


		##################################################################################################
		if 'branch' in trackings:
			#ADD IMPORTS#
			for library in branch.import_list:
				if not library in imports:
					imports.append(library)

			#GET ADJUST RELATED DATA#
			branch_data = access_required_data(calls_data_dict).branch_definition_data()
			# print "branch_data"
			# print branch_data
			# print "branch_data[0]"
			# print branch_data[0]
			# print "branch_data[1]"
			# print branch_data[1]
			# print "branch_data[2]"
			# print branch_data[2]

			main_campaign_data = branch_data[2]
			# print "main_campaign_data"
			# print main_campaign_data
			tracking_def_list.extend(branch_data[0])
			global branch_campaign_data
			branch_campaign_data = branch_data[1]
			# print "branch_campaign_data"
			# print branch_campaign_data
			times_and_stamps['branch'] = branch_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append("	'app_version_name' 	 :'" + main_campaign_data.get('app_version') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version_short'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + main_campaign_data.get('app_version_short') + "',\n")

			############# INSTALL DATA ###############
			install_function.append("""	def_sec(app_data,device_data)\n""")
			android_required_functions.insert(0, branch.branch_android_required_functions)


		##################################################################################################
		if 'Adbrix' in trackings:
			#ADD IMPORTS#
			for library in Adbrix.import_list:
				if not library in imports:
					imports.append(library)

			#GET AD-BRIX RELATED DATA#
			adbrix_data = access_required_data(calls_data_dict).adbrix_definition_data(key=key)
			main_campaign_data = adbrix_data[2]
			tracking_def_list.extend(adbrix_data[0])
			global adbrix_campaign_data
			adbrix_campaign_data = adbrix_data[1]
			times_and_stamps['Adbrix'] = adbrix_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_name') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version_code'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + str(main_campaign_data.get('app_version_code')) + "',\n")

			if adbrix_campaign_data.get('key_multipart') and not adbrix_campaign_data.get('key_getreferral'):
				install_function.append("""	if not app_data.get('adid'):\n		# app_data['adid'] = \n		raise Exception('please enter adid value')\n	app_data['app_first_open_time'] = get_timestamp(para1=10,para2=20)\n	update_id(app_data,device_data)\n	if not app_data.get('last_firstopen_id'):\n		app_data['last_firstopen_id']=str(int(time.time()*1000))+":"+str(uuid.uuid4())\n\n	if not app_data.get('session_id'):\n		app_data['session_id']=str(int(time.time()*1000))+":"+str(uuid.uuid4())\n	""")
			
			elif adbrix_campaign_data.get('key_getreferral') and not adbrix_campaign_data.get('key_multipart'):
				install_function.append(
				"""	update_activity(app_data,device_data)\n	get_demo_value(app_data,device_data)\n\n	email_sha1 = ""\n	for i in range(0,random.randint(2,3)):\n		return_userinfo(app_data)\n	email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'\n	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]\n	""")
			else:
				install_function.append(
				"""	if not app_data.get('adid'):\n		# app_data['adid'] = \n		raise Exception('please enter adid value')\n	app_data['app_first_open_time'] = get_timestamp(para1=10,para2=20)\n	update_id(app_data,device_data)\n	if not app_data.get('last_firstopen_id'):\n		app_data['last_firstopen_id']=str(int(time.time()*1000))+":"+str(uuid.uuid4())\n\n	if not app_data.get('session_id'):\n		app_data['session_id']=str(int(time.time()*1000))+":"+str(uuid.uuid4())\n	update_activity(app_data,device_data)\n	get_demo_value(app_data,device_data)\n	make_sec(app_data,device_data)\n\n	email_sha1 = ""\n	for i in range(0,random.randint(2,3)):\n		return_userinfo(app_data)\n	email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'\n	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]\n	""")

			android_required_functions.insert(0, Adbrix.adbrix_android_required_functions)

		##################################################################################################
		if 'MAT' in trackings:
			#ADD IMPORTS#
			for library in mat.import_list:
				if not library in imports:
					imports.append(library)

			#GET MAT RELATED DATA#
			mat_data = access_required_data(calls_data_dict).mat_definition_data(key=key)
			main_campaign_data = mat_data[2]
			tracking_def_list.extend(mat_data[0])
			global mat_campaign_data
			mat_campaign_data = mat_data[1]
			times_and_stamps['MAT'] = mat_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_version_name') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				if main_campaign_data.get('app_version'):
					campaign_data.append(
						"	'app_version_code' 	 :'" + str(main_campaign_data.get('app_version')) + "',\n")

			android_required_functions.insert(0, mat.mat_android_required_functions)


		##################################################################################################
		if 'Apsalar' in trackings:
			#ADD IMPORTS#
			for library in Apsalar.import_list:
				if not library in imports:
					imports.append(library)

			#GET APSALAR RELATED DATA#
			apsalar_data = access_required_data(calls_data_dict).apsalar_definition_data()
			main_campaign_data = apsalar_data[2]
			tracking_def_list.extend(apsalar_data[0])
			global apsalar_campaign_data
			apsalar_campaign_data = apsalar_data[1]
			times_and_stamps['Apsalar'] = apsalar_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('i') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append("	'app_version_name' 	 :'" + main_campaign_data.get('av') + "',\n")

			############# INSTALL DATA ###############
			android_required_functions.insert(0, Apsalar.apsalar_android_required_functions)

		##################################################################################################
		if 'Adforce' in trackings:
			#ADD IMPORTS#
			for library in Adforce.import_list:
				if not library in imports:
					imports.append(library)

			#GET ADFORCE RELATED DATA#
			adforce_data = access_required_data(calls_data_dict).adforce_definition_data()
			main_campaign_data = adforce_data[2]
			tracking_def_list.extend(adforce_data[0])
			global adforce_campaign_data
			adforce_campaign_data = adforce_data[1]
			times_and_stamps['Adforce'] = adforce_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('_bundle_id') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append("	'app_version_name' 	 :'" + main_campaign_data.get('_bv') + "',\n")

			############# INSTALL DATA ###############
			install_function.append("""	if not app_data.get('install_id'):\n		app_data['install_id'] = str(uuid.uuid4())""")
			android_required_functions.insert(0, Adforce.adforce_android_required_functions)

		##################################################################################################
		if 'Tenjin' in trackings:
			#ADD IMPORTS#
			for library in Tenjin.import_list:
				if not library in imports:
					imports.append(library)

			#GET TENJIN RELATED DATA#
			tenjin_data = access_required_data(calls_data_dict).tenjin_definition_data()
			main_campaign_data = tenjin_data[2]
			tracking_def_list.extend(tenjin_data[0])
			global tenjin_campaign_data
			tenjin_campaign_data = tenjin_data[1]
			times_and_stamps['Tenjin'] = tenjin_data[3]
			if not any("'package_name'" in c for c in campaign_data):
				campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('bundle_id') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = \
							res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append("	'app_version_name' 	 :'" + main_campaign_data.get('app_version') + "',\n")

			############# INSTALL DATA ###############
			install_function.append("""	if not app_data.get('session_id'):\n		app_data['session_id']=str(uuid.uuid4())\n	if not app_data.get('tenjin_reference_id'):\n		app_data['tenjin_reference_id']=str(uuid.uuid4())\n""")
			android_required_functions.insert(0, Tenjin.tenjin_android_required_functions)

		##################################################################################################
		if 'Kochava' in trackings:
			#ADD IMPORTS#
			for library in Kochava.import_list:
				if not library in imports:
					imports.append(library)

			#GET KOCHAVA RELATED DATA#
			kochava_data = access_required_data(calls_data_dict).kochava_definition_data()
			main_campaign_data = kochava_data[2]
			tracking_def_list.extend(kochava_data[0])
			global kochava_campaign_data
			kochava_campaign_data = kochava_data[1]
			times_and_stamps['Kochava'] = kochava_data[3]

			if not any("'package_name'" in c for c in campaign_data):
				if main_campaign_data.get('package_name'):
					campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package_name') + "',\n")
				elif main_campaign_data.get('package'):
					campaign_data.append("	'package_name'		 :'" + main_campaign_data.get('package') + "',\n")

			if not any("installtimenew.main" in c for c in install_function):
				res = fetch_app_details(main_campaign_data, campaign_data, install_function, ui, online)

			if not any("'app_name'" in c for c in campaign_data):
				if main_campaign_data.get('app_name'):
					campaign_data.append(
						"	'app_name' 			 :'" + main_campaign_data.get('app_name').replace('\n',
																										' ') + "',\n")
				else:
					try:
						app_name = res.split('<meta property="og:title" content="')[1].split(' - Apps on Google Play">')[
								0]
					except:
						app_name = 'ENTER APP NAME HERE'

					campaign_data.append("	'app_name' 			 :'" + app_name + "',\n")

			if not any("'app_version_name'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_name' 	 :'" + main_campaign_data.get('app_short_string') + "',\n")
			if not any("'app_version_code'" in c for c in campaign_data):
				campaign_data.append(
					"	'app_version_code' 	 :'" + main_campaign_data.get('app_version') + "',\n")
			campaign_data.append("	'CREATE_DEVICE_MODE' : 3,\n")
			############# INSTALL DATA ###############
			install_function.append("""	def_sec(app_data,device_data)\n	app_data['battery_level']=random.randint(5,85)\n	app_data['screen_brightness']=round(random.uniform(0,1), 4)\n	app_data['volume']=random.randint(0,1)\n\n	app_data['nt_id_first']=util.get_random_string('hex',5)+'-1-'\n\n	app_data['previous_Call_time']=int(time.time())-random.randint(5,10)\n\n	app_data['call_run_time']=time.time()""")
			android_required_functions.insert(0, Kochava.kochava_android_required_functions)

		"""* 
			>>>>RETURNS TIME RELATED DATA FOR FURTHER USE<<<<
		*"""
		return times_and_stamps

	def common_campaign_data(self, campaign_data, trackings):
		if float(globals().get('minimum_os')) < 4.4:
			globals()['minimum_os'] = "4.4"
		############## COMMON CAMPAIGN DATA FOR ALL TRACKINGS ##############
		campaign_data.append("	'ctr' 				 : 6,\n")
		campaign_data.append("	'no_referrer' 		 : False,\n")
		campaign_data.append("	'device_targeting'	 : True,\n")
		campaign_data.append("	'supported_countries': 'WW',\n")
		campaign_data.append("	'supported_os'		 : '" + globals().get('minimum_os') + "',\n")
		if len(trackings) == 1:
			tracker=trackings[0]
		else:
			tracker='ENTER TRACKER NAME HERE'
		campaign_data.append("	'tracker'		 	 : '" + tracker + "',\n")

	def campaign_data_dictionary(self, campaign_data, trackings):

		if 'Appsflyer' in trackings:
			###################### APPSFLYER DATA DICTIONARY ###################
			campaign_data.append("	'appsflyer'		 	 : {\n")
			campaign_data.append("		'key'		 : '" + appsflyer_campaign_data.get('appsflyerKey') + "',\n")
			campaign_data.append("		'dkh'		 : '" + appsflyer_campaign_data.get('appsflyerKey')[:8] + "',\n")
			campaign_data.append("		'buildnumber': '" + appsflyer_campaign_data.get('buildnumber') + "',\n")
			campaign_data.append("		'version'	 : '" + appsflyer_campaign_data.get('version') + "',\n	},\n")


		if 'branch' in trackings:
			###################### APPSFLYER DATA DICTIONARY ###################
			campaign_data.append("	'api_branch'		 	 : {\n")
			campaign_data.append("		'branchkey'		 : '" + branch_campaign_data.get('branch_key') + "',\n")
			campaign_data.append("		'sdk'		 : '" + branch_campaign_data.get('sdk') + "',},\n")
			# campaign_data.append("		'buildnumber': '" + appsflyer_campaign_data.get('buildnumber') + "',\n")
			# campaign_data.append("		'version'	 : '" + appsflyer_campaign_data.get('version') + "',\n	},\n")

		if 'Adjust' in trackings:
			###################### APPSFLYER DATA DICTIONARY ###################
			campaign_data.append("	'adjust'		 : {\n")
			campaign_data.append("		'app_token'	 : '" + adjust_campaign_data.get('app_token') + "',\n")
			if adjust_campaign_data.get('Client-SDK'):
				campaign_data.append("		'sdk'		 : '" + adjust_campaign_data.get('Client-SDK') + "',\n")
			if adjust_campaign_data.get('secret_key'):
				campaign_data.append("		'secret_key' : '" + adjust_campaign_data.get('secret_key') + "',\n")
				campaign_data.append("		'secret_id'	 : '" + adjust_campaign_data.get('secret_id') + "',\n")
			campaign_data.append('		},\n')

		if 'Adbrix' in trackings:
			###################### AD-BRIX DATA DICTIONARY ###################
			campaign_data.append("	'ad-brix'		 : {\n")
			campaign_data.append("		'app_key'	 	 : '" + adbrix_campaign_data.get('app_key') + "',\n")
			campaign_data.append("		'ver'	 	 	 : '" + adbrix_campaign_data.get('ver') + "',\n")
			if adbrix_campaign_data.get('key_multipart') and adbrix_campaign_data.get('iv_multipart'):
				campaign_data.append("		'key_multipart': '" + adbrix_campaign_data.get('key_multipart') + "',\n")
				campaign_data.append("		'iv_multipart' : '" + adbrix_campaign_data.get('iv_multipart') + "',\n")
			else:
				campaign_data.append("		'key_getreferral': '" + adbrix_campaign_data.get('key_getreferral') + "',\n")
				campaign_data.append("		'iv_getreferral' : '" + adbrix_campaign_data.get('iv_getreferral') + "',\n")
				campaign_data.append("		'key_tracking' 	 : 'srkterowgawrsozerruly82nfij625w9',\n")
				campaign_data.append("		'iv_tracking' 	 : 'srkterowgawrsoze',\n")
			campaign_data.append("	},\n")

		if 'MAT' in trackings:
			###################### MAT DATA DICTIONARY ###################
			campaign_data.append("	'mat'		 	   : {\n")
			campaign_data.append("		'ver'		   : '" + mat_campaign_data.get('ver') + "',\n")
			campaign_data.append("		'advertiser_id': '" + mat_campaign_data.get('advertiser_id') + "',\n")
			campaign_data.append("		'sdk'		   : '" + mat_campaign_data.get('sdk') + "',\n")
			campaign_data.append("		'key'		   : '" + mat_campaign_data.get('key') + "',\n")
			campaign_data.append("		'iv'		   : '" + "heF9BATUfWuISyO8" + "',\n	},\n")

		if 'Apsalar' in trackings:
			###################### Apsalar DATA DICTIONARY ###################
			campaign_data.append("	'apsalar'		 	    : {\n")
			campaign_data.append("		'version'		    : '" + apsalar_campaign_data.get('sdk') + "',\n")
			campaign_data.append("		'key'				: '" + apsalar_campaign_data.get('a') + "',\n")
			campaign_data.append("		'secret'		    : '" + apsalar_campaign_data.get('key') + "',\n")
			campaign_data.append("	},\n")

		if 'Kochava' in trackings:
			###################### Kochava DATA DICTIONARY ###################
			campaign_data.append("	'kochava'		 	    : {\n")
			campaign_data.append("		'sdk_version'		: '" + kochava_campaign_data.get('sdk_version') + "',\n")
			campaign_data.append("		'sdk_protocol'		: '" + kochava_campaign_data.get('sdk_protocol') + "',\n")
			campaign_data.append("		'kochava_app_id'	: '" + kochava_campaign_data.get('kochava_app_id') + "',\n")
			if kochava_campaign_data.get('sdk_build_date'):
				campaign_data.append("		'sdk_build_date'	: '" + kochava_campaign_data.get('sdk_build_date') + "',\n")
			campaign_data.append("	},\n")

			
		if 'Adforce' in trackings:
			###################### Adforce Data Dictionary ####################
			campaign_data.append("	'adforce'		 	    : {\n")
			campaign_data.append("		'app_id'		    : '" + adforce_campaign_data.get('_app') + "',\n")
			campaign_data.append("		'sdk_version'				: '" + adforce_campaign_data.get('_sdk_ver') + "',\n")
			if adforce_campaign_data.get('_gms_version'):
				campaign_data.append("		'_gms_version'		    : '" + adforce_campaign_data.get('_gms_version') + "',\n")
			campaign_data.append("	},\n")

		if 'Tenjin' in trackings:
			###################### Tenjin Data Dictionary ####################
			campaign_data.append("	'tenjin'		 	    : {\n")
			campaign_data.append("		'auth'		    : '" + tenjin_campaign_data.get('Authorization').split(" ")[1] + "',\n")
			campaign_data.append("		'sdk'				: '" + tenjin_campaign_data.get('sdk_version') + "',\n")
			try:
				campaign_data.append("		'apikey'		    : '" + tenjin_campaign_data.get('api_key') + "',\n")
			except:
				print "NO API KEY IN TENJIN"
				pass
			campaign_data.append("	},\n")

	def campaign_data_retention(self, campaign_data):
		################### RETENTION DATA COMMON FOR ALL ###################
		campaign_data.append(campaign_ending)

	################################################################# INSTALL FUNCTION ###############################################################################
	def add_calls(self, install_function, calls_data_dict, genuine_hosts_data):
		install_function.append("\n ###########	 CALLS		 ############\n")
		call_counter=0
		# print calls_data_dict.get('host')
		# print genuine_hosts_data.keys()
		
		for call_exec in calls_data_dict.get('host'):

			call_counter+=1
			if call_counter==1:
				
				if "response=util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("response=util.execute_request","app_data['api_hit_time']=time.time()\n	response=util.execute_request")
					
				elif "result=util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("result=util.execute_request","app_data['api_hit_time']=time.time()\n	result=util.execute_request")

				elif "resp = util.execute_request" in genuine_hosts_data.get(call_exec):
					call_exec=genuine_hosts_data.get(call_exec).replace("resp = util.execute_request","app_data['api_hit_time']=time.time()\n	resp = util.execute_request")
					
				else:					
					call_exec=genuine_hosts_data.get(call_exec).replace("util.execute_request","app_data['api_hit_time']=time.time()\n	util.execute_request")
					
				install_function.append(call_exec)
				
			else:
				
				install_function.append(genuine_hosts_data.get(call_exec))

		install_function.append("\n\n	try:\n		if campaign_data.get('link'):\n			cd = {\n					'agentId': Config.AGENTID,\n					'packageName': campaign_data['package_name'],\n					'link': campaign_data.get('link'),\n					'data': json.dumps({'source': 'New','app_version': campaign_data.get('app_version_name')})\n				}\n			saveClicks(cd)\n	except:\n		pass")		
		install_function.append("\n\n	return {'status':'true'}\n\n")

	################################################################# events definition ##############################################################################
	def event_definitions(self, event_list, event_data_list, trackings):
		events_definitions_list = []
		e = list(set(event_list))
		events_definitions_list.append(event_definition_start)
		for event in range(len(event_list)):
			if event_list[event] in e:
				if 'adjust' in event_list[event]:
					cp = pp = 'None'
					if event_data_list[event]:
						if event_data_list[event][0]:
							cp = 'str(json.dumps(' + event_data_list[event][0] + '))'
						if event_data_list[event][1]:
							pp = 'str(json.dumps(' + event_data_list[event][1] + '))'

					token = event_list[event].split('adjust-->')[1]
					events_definitions_list.append(
						"def adjust_token_" + token + "(campaign_data, app_data, device_data,t1=0,t2=0):\n" + "	print 'Adjust : EVENT______________________________" + token + "'\n	request=adjust_event(campaign_data, app_data, device_data,callback_params=" + cp + ",partner_params=" + pp + ",event_token='" + token + "',t1=t1,t2=t2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp,event_token='" + token + "')\n	except:\n		print 'Could not save attributionCallStatus data'\n\n")
					e.remove(event_list[event])

				elif 'apsalar' in event_list[event]:
					if not event_data_list[event]:
						event_data_list[event]=json.dumps({})
					ev = 'urllib.quote_plus(json.dumps(' + event_data_list[event] + '))'

					eventName = event_list[event].split('apsalar-->')[1]
					events_definitions_list.append(
						"def apsalar_event_" + eventName.replace(" ","_") + "(campaign_data, app_data, device_data, custom_user_id=False):\n" + "	print 'Apsalr : EVENT______________________________" + eventName.replace(" ","_") + "'\n	request=apsalar_event(campaign_data, app_data, device_data,event_name='" + eventName + "',value=" + ev + ",custom_user_id=custom_user_id)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n\n")	
					e.remove(event_list[event])


				elif 'branch_standard' in event_list[event]:
					# print event_list[event].split('branch_standard-->')[1]
					# print event_data_list[event]
					if event_data_list[event]:						
						cp =str(eval(event_data_list[event]))
						# print cp
					else:
						cp="None"
					events_definitions_list.append("def " + re.sub(r'\W+', '', event_list[
						event]).split('branch_standard')[1].lower() + "(campaign_data, app_data, device_data):\n" + "	print '________________branch_standard_event____________________" +
													 event_list[event].split('branch_standard-->')[1] + "'\n	eventName			= '" + event_list[
														 event].split('branch_standard-->')[1] + """'\n	eventValue			= """ + cp + """\n	request	= api_branch_standard(campaign_data, app_data, device_data, event_name=eventName,event_data=eventValue)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n\n\n""")
					# print events_definitions_list
					e.remove(event_list[event])

				elif 'branch_custom' in event_list[event]:
					if event_data_list[event]:						
						cp =str(eval(event_data_list[event]))
						# print cp
					else:
						cp="None"
					events_definitions_list.append("def " + re.sub(r'\W+', '', event_list[
						event]).split('branch_custom')[1].lower() + "(campaign_data, app_data, device_data):\n" + "	print '________________branch_custom_event____________________" +
													 event_list[event].split('branch_custom-->')[1] + "'\n	eventName			= '" + event_list[
														 event].split('branch_custom-->')[1] + """'\n	eventValue			= """ + cp + """\n	request	= api_branch_custom(campaign_data, app_data, device_data, event_name=eventName,event_data=eventValue)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n\n\n""")
					e.remove(event_list[event])
					# print events_definitions_list

				else:
					events_definitions_list.append("def " + re.sub(r'\W+', '', event_list[
						event]).lower() + "(campaign_data, app_data, device_data):\n" + "	print 'Appsflyer : EVENT___________________________" +
													 event_list[event] + "'\n	eventName			= '" + event_list[
														 event] + """'\n	eventValue			= json.dumps(""" + str(eval(
						event_data_list[
							event])) + """)\n	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n\n\n""")
					e.remove(event_list[event])
		return events_definitions_list
