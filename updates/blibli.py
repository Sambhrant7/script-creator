# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import NameLists
from sdk import installtimenew
from sdk import util
from sdk import getsleep
import base64
import time
import random
import json
import datetime
import clicker
import Config
import uuid


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'blibli.mobile.commerce',
	'app_size'		     : 29.0,#28.0, 27.0,#26.0,#25.0,#23.0,#22.0,
	'app_name' 			 :'Blibli.com',
	'app_version_name' 	 :'6.5.5',#6.5.1, '6.4.0',#'6.3.0',#'6.2.6',#'6.2.5',#'6.2.1',#6.2.0, 6.1.5, '6.1.0',#'6.0.5',#"6.0.0",#'5.9.5',
	'app_version_code' 	 :'2230',#2221, '2130',#'2068',#'2043',#'2019',#'1993',#1981, 1936, '1876',#'1834',#"1817",#'1784',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'tHu6W8t9EpSvifDatvX7V',
		'dkh'		 : 'tHu6W8t9',
		'buildnumber': '4.8.11',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:25,
				2:25,
				3:24,
				4:24,
				5:23,
				6:22,
				7:20,
				8:20,
				9:20,
				10:19,
				11:19,
				12:19,
				13:18,
				14:18,
				15:17,
				16:16,
				17:15,
				18:14,
				19:13,
				20:13,
				21:13,
				22:13,
				23:11,
				24:10,
				25:10,
				26:9,
				27:8,
				28:8,
				29:6,
				30:6,
				31:5,
				32:5,
				33:4,
				34:4,
				35:3,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################
	def_firstLaunchDate(app_data,device_data)
	time.sleep(random.randint(5,6))
	app_data['gcm_token_timestamp']=int(time.time()*1000)
	app_data['registeredUninstall']=False

	if not app_data.get('register'):
		app_data['register']=False

	if not app_data.get('cart_list'):
		app_data['cart_list']=[]

	global sku_list, selsku_list
	sku_list=[]
	selsku_list=[]

	


	###########		CALLS		############
	# print '\nConnectivity_Check : generate_204____________________________________'
	# request=connectivity_check(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print "Appsflyer :: gcm_token-----------------------------"
	req=gcmToken_call(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		resp_str=resp.get('data')
		app_data['af_gcm_token']=resp_str.split('token=')[1]
	except:
		app_data['af_gcm_token']=util.get_random_string('google_token',11)+':APA91B'+util.get_random_string('all',134)

	time.sleep(random.randint(5,7))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
	app_data['registeredUninstall']=True
		
	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data,opt=True)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)

	# print "config call-----------"
	# req = measurement_config(campaign_data, app_data, device_data)
	# util.execute_request(**req)
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
		
	time.sleep(random.randint(3,4))
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)

	time.sleep(random.randint(2,3))
	print '\n'+'Appsflyer : Attr____________________________________'
	request = appsflyer_attr(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	time.sleep(random.randint(60,100))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
	
	for i in range(random.randint(3,5)):
		time.sleep(random.randint(60,100))
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)


	
	

	if random.randint(1,100)<=70:
		if not app_data.get('auth_token'):
			get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

		print "\nself_call_register\n"
		request=register( campaign_data, device_data, app_data )
		util.execute_request(**request)
		app_data['register']=True

		get_auth_token_func(campaign_data, device_data, app_data,call=1)

		print "\nself_call_user_profile\n"
		request=user_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		print "\nself_call_account_data\n"
		request=account_data( campaign_data, device_data, app_data )
		result=util.execute_request(**request)
		try:
			app_data['customer_user_id']=json.loads(result.get('data')).get('id')
			if app_data.get('customer_user_id')==None or app_data.get('customer_user_id')=='':
				app_data['customer_user_id']='518617241'
			print app_data['customer_user_id']
		except:
			app_data['customer_user_id']='518617241'

		if app_data.get('customer_user_id')==None or app_data.get('customer_user_id')=='':
			app_data['customer_user_id']='518617241'


		

	for _ in range(1):				

		if random.randint(1,100) <= 90:			

			print "------from banner---------------"
			var=random.choice([0,0,1,1,1,1,1,2,2,2])
			if var==0:
				if not app_data.get('auth_token'):
					get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

				get_sku_list_func(campaign_data, device_data, app_data)		# globals()['sku_list']

				app_data['filter']=False
				app_data['selected_sku'] = random.choice(globals()['sku_list'])
				print app_data['selected_sku']			

				af_content_view(campaign_data, app_data, device_data)
				if random.randint(1,100)<=60:
					af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)


			elif var==1:
				if not app_data.get('auth_token'):
					get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

				get_sku_list_func(campaign_data, device_data, app_data)		# globals()['sku_list']

				app_data['filter']=False
				app_data['selected_sku'] = random.choice(globals()['sku_list'])
				print app_data['selected_sku']
				
				print "------from homePage---------------"
				af_product_click(campaign_data, app_data, device_data)

				af_content_view(campaign_data, app_data, device_data)
				if random.randint(1,100)<=60:
					af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)
			else:
				if len(globals().get('selsku_list'))<1:
					if not app_data.get('auth_token'):
						get_auth_token_func(campaign_data, device_data, app_data)

					print "------using filter-(search)--------------"
					app_data['filter']=True
					print "\nself_call_sort_product\n"
					request=sort_product( campaign_data, device_data, app_data )
					result=util.execute_request(**request)
					global root_category,price_list
					try:						
						
						info=json.loads(result.get('data')).get('data').get('products')
						for i in range(len(info)):
							selsku=info[i].get('sku')
							root_category=info[i].get('rootCategory').get('name')
							price_list=(int(info[i].get('price').get('minPrice')))

							selsku_list.append({'sku' : selsku.encode('utf-8'), 'price' : price_list,'category':root_category})

						if len(selsku_list)<1 :

							selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]
							

						print selsku_list
						

					except:
						selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]
						
					if len(selsku_list)<1:

						selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]				
				
				app_data['count']=0
				for _ in range(0,random.randint(1,3)):
					af_product_impression(campaign_data, app_data, device_data)

				if random.randint(1,100)<=90:
					af_product_click(campaign_data, app_data, device_data)

					af_content_view(campaign_data, app_data, device_data)

					if random.randint(1,100)<=60:
						af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)

		if random.randint(1,100)<=20:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)					

	
	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	if not app_data.get('prev_event_name'):
		def_eventsRecords(app_data)

	global sku_list, selsku_list
	sku_list=[]
	selsku_list=[]

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)
	time.sleep(random.randint(5,6))
	if not app_data.get('gcm_token_timestamp'):
		app_data['gcm_token_timestamp']=int(time.time()*1000)
	if not app_data.get('registeredUninstall'):
		app_data['registeredUninstall']=False

	if not app_data.get('register'):
		app_data['register']=False

	if not app_data.get('cart_list'):
		app_data['cart_list']=[]

	

	###########		CALLS		############

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)
	

	if app_data.get('register')==False and random.randint(1,100)<=80:
		if not app_data.get('auth_token'):
			get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

		print "\nself_call_register\n"
		request=register( campaign_data, device_data, app_data )
		util.execute_request(**request)
		app_data['register']=True

		get_auth_token_func(campaign_data, device_data, app_data,call=1)

		print "\nself_call_user_profile\n"
		request=user_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		print "\nself_call_account_data\n"
		request=account_data( campaign_data, device_data, app_data )
		result=util.execute_request(**request)
		try:
			app_data['customer_user_id']=json.loads(result.get('data')).get('id')
			if app_data.get('customer_user_id')==None or app_data.get('customer_user_id')=='':
				app_data['customer_user_id']='518617241'
			print app_data['customer_user_id']
		except:
			app_data['customer_user_id']='518617241'

		if app_data.get('customer_user_id')==None or app_data.get('customer_user_id')=='':
			app_data['customer_user_id']='518617241'	

	if day<3:
		no_of_times=random.randint(2,3)

	elif day<6:
		no_of_times=random.randint(1,2)

	else:
		no_of_times=random.randint(0,1)

	for _ in range(no_of_times):				

		if random.randint(1,100) <= 90:
			

			print "------from banner---------------"
			var=random.choice([0,0,1,1,1,1,1,2,2,2])
			if var==0:
				if not app_data.get('auth_token'):
					get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

				get_sku_list_func(campaign_data, device_data, app_data)		# globals()['sku_list']

				app_data['filter']=False
				app_data['selected_sku'] = random.choice(globals()['sku_list'])
				print app_data['selected_sku']			

				af_content_view(campaign_data, app_data, device_data)
				if random.randint(1,100)<=60:
					af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)


			elif var==1:
				if not app_data.get('auth_token'):
					get_auth_token_func(campaign_data, device_data, app_data)	# app_data['auth_token']

				get_sku_list_func(campaign_data, device_data, app_data)		# globals()['sku_list']

				app_data['filter']=False
				app_data['selected_sku'] = random.choice(globals()['sku_list'])
				print app_data['selected_sku']
				
				print "------from homePage---------------"
				af_product_click(campaign_data, app_data, device_data)

				af_content_view(campaign_data, app_data, device_data)
				if random.randint(1,100)<=60:
					af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)
			else:
				if len(globals().get('selsku_list'))<1:
					if not app_data.get('auth_token'):
						get_auth_token_func(campaign_data, device_data, app_data)

					print "------using filter-(search)--------------"
					app_data['filter']=True
					print "\nself_call_sort_product\n"
					request=sort_product( campaign_data, device_data, app_data )
					result=util.execute_request(**request)
					global root_category,price_list
					try:
						
						
						info=json.loads(result.get('data')).get('data').get('products')
						for i in range(len(info)):
							selsku=info[i].get('sku')
							root_category=info[i].get('rootCategory').get('name')
							price_list=(int(info[i].get('price').get('minPrice')))

							selsku_list.append({'sku' : selsku.encode('utf-8'), 'price' : price_list,'category':root_category})

						if len(selsku_list)<1 :

							selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]
							

						print selsku_list
						

					except:
						selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]
						
					if len(selsku_list)<1:

						selsku_list=[{'sku': 'JUP-57615-00467', 'category': u'Handphone & Tablet', 'price': 4476000}, {'sku': 'APH-60023-00329', 'category': u'Handphone & Tablet', 'price': 12583000}, {'sku': 'ROG-60022-00270', 'category': u'Handphone & Tablet', 'price': 3064000}, {'sku': 'APY-60023-00016', 'category': u'Handphone & Tablet', 'price': 9200000}, {'sku': 'BUC-29100-00396', 'category': u'Handphone & Tablet', 'price': 4099000}, {'sku': 'GAG-60035-00038', 'category': u'Handphone & Tablet', 'price': 5625000}, {'sku': 'BAG-60029-00835', 'category': u'Handphone & Tablet', 'price': 2164000}, {'sku': 'APH-60023-00200', 'category': u'Handphone & Tablet', 'price': 14728000}, {'sku': 'HPO-60023-00270', 'category': u'Handphone & Tablet', 'price': 14168000}, {'sku': 'TOI-60054-00194', 'category': u'Handphone & Tablet', 'price': 16613000}, {'sku': 'HPO-60023-00041', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'ROG-60022-00269', 'category': u'Handphone & Tablet', 'price': 3063000}, {'sku': 'TOI-60054-00031', 'category': u'Handphone & Tablet', 'price': 19017000}, {'sku': 'APH-60023-00502', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'HPO-60023-00049', 'category': u'Handphone & Tablet', 'price': 11350000}, {'sku': 'AUE-33956-00640', 'category': u'Handphone & Tablet', 'price': 15945000}, {'sku': 'HPO-60023-00271', 'category': u'Handphone & Tablet', 'price': 16614000}, {'sku': 'APH-60023-00002', 'category': u'Handphone & Tablet', 'price': 16615000}, {'sku': 'ARP-60046-00641', 'category': u'Handphone & Tablet', 'price': 2150000}, {'sku': 'BAG-60029-00832', 'category': u'Handphone & Tablet', 'price': 2168000}, {'sku': 'MAS-60117-00026', 'category': u'Handphone & Tablet', 'price': 2300000}, {'sku': 'TRM-60030-00096', 'category': u'Handphone & Tablet', 'price': 2450000}, {'sku': 'ROG-60022-00558', 'category': u'Handphone & Tablet', 'price': 2453000}, {'sku': 'BAG-60029-00455', 'category': u'Handphone & Tablet', 'price': 2493000}]		

				app_data['count']=0

				for _ in range(0,random.randint(1,3)):
					af_product_impression(campaign_data, app_data, device_data)

				if random.randint(1,100)<=90:
					af_product_click(campaign_data, app_data, device_data)

					af_content_view(campaign_data, app_data, device_data)

					if random.randint(1,100)<=60:
						af_add_to_wishlist(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 50:
					af_add_to_cart(campaign_data, app_data, device_data)
					if random.randint(1,100)<=10:
						af_remove_cart(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 57 and app_data.get('register')==True:
						af_initiated_checkout(campaign_data, app_data, device_data)
						if random.randint(1,100)<=50:
							af_add_payment_info(campaign_data, app_data, device_data)

		if random.randint(1,100)<=20:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)							




	return {'status':'true'}

def sort_product( campaign_data, device_data, app_data ):
	url= "https://www.blibli.com/backend/search/products"
	method= "get"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "bearer " + app_data.get('auth_token'),
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
        "X-NewRelic-ID": "VQQGWFRSDxACVFNbBQQHUA==",
        "channelId": "android",
        "storeId": app_data.get('store_id'),
        "x-requestId": str(uuid.uuid4()),
        "x-sessionId": app_data.get('session_id'),
        "x-userId": app_data.get('user_id')}

	params= {       "intent": "true", "itemPerPage": str(random.randint(18,25)), "searchTerm": "Iphone", "start": "0"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def user_profile( campaign_data, device_data, app_data ):
	url= "https://www.blibli.com/backend/mobile/member/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "bearer " + app_data.get('auth_token'),
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
        "X-NewRelic-ID": "VQQGWFRSDxACVFNbBQQHUA==",
        "channelId": "android",
        "storeId": app_data.get('store_id'),
        "x-requestId": str(uuid.uuid4()),
        "x-sessionId": app_data.get('session_id'),
        "x-userId": app_data.get('user_id')}

	params= None

	data= {       "emailAddress": app_data.get('user').get('email'),
        "firstName": app_data.get('user').get('firstname'),
        "gender": app_data.get('user').get('sex').upper(),
        "handphone": "89535"+util.get_random_string('decimal',5)}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def register( campaign_data, device_data, app_data ):
	register_user(app_data,country='united states')
	url= "https://www.blibli.com/backend/mobile/reg"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "bearer " + app_data.get('auth_token'),
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
        "X-NewRelic-ID": "VQQGWFRSDxACVFNbBQQHUA==",
        "channelId": "android",
        "storeId":app_data.get('store_id'),
        "x-requestId": str(uuid.uuid4()),
        "x-sessionId": app_data.get('session_id'),
        "x-userId": app_data.get('user_id')}

	params= None

	data= {       "password": app_data.get('user').get('password'), "username": app_data.get('user').get('email')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def account_data( campaign_data, device_data, app_data ):
	url= "https://www.blibli.com/backend/mobile/member/account-data"
	method= "get"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "bearer " + app_data.get('auth_token'),
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
        "X-NewRelic-ID": "VQQGWFRSDxACVFNbBQQHUA==",
        "channelId": "android",
        "storeId":app_data.get('store_id'),
        "x-requestId": str(uuid.uuid4()),
        "x-sessionId": app_data.get('session_id'),
        "x-userId": app_data.get('user_id')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_content_view(campaign_data, app_data, device_data):
	get_product_data_func(campaign_data, device_data, app_data)
	time.sleep(random.randint(10,35))
	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName		  = 'af_content_view'
	

	if app_data.get('register')==False:
		eventValue		  = {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_currency":"IDR","af_screen_name":"retail-product-detail"}

	else:
		eventValue		  = {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_customer_user_id":app_data.get('customer_user_id'),"af_currency":"IDR","af_screen_name":"retail-product-detail"}

	

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	

def af_add_to_cart(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	time.sleep(random.randint(10,30))
	eventName		  = 'af_add_to_cart'

	if not app_data.get('af_category'):
		get_product_data_func(campaign_data, device_data, app_data)
	
	app_data.get('cart_list').append(app_data.get('selected_sku'))
	print "cart_list"+str(app_data.get('cart_list'))

	if app_data.get('register')==False:
		eventValue		  = {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_quantity":"1","af_currency":"IDR","af_screen_name":"retail-product-detail"}	

	else:
		eventValue		  = {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_quantity":"1","af_customer_user_id":app_data.get('customer_user_id'),"af_currency":"IDR","af_screen_name":"retail-product-detail"}	

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	


def af_initiated_checkout(campaign_data, app_data, device_data):
	time.sleep(random.randint(10,35))
	product=[]
	for i in range(len(app_data.get('cart_list'))):
		product.append({"af_price":str(app_data.get('cart_list')[i].get('price')),"af_shipping":"Standard","af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('cart_list')[i].get('sku'),"af_quantity":"1","af_currency":"IDR"})

	print 'Appsflyer : EVENT___________________________af_initiated_checkout'
	eventName		  = 'af_initiated_checkout'
	

	
	if app_data.get('register')==False:
		eventValue		  = {"af_screen_name":"retail-checkout-1","product":product}
	else:
		eventValue		  = {"af_screen_name":"retail-checkout-1","product":product,"af_customer_user_id":app_data.get('customer_user_id')}
	

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	


def af_add_payment_info(campaign_data, app_data, device_data):
	time.sleep(random.randint(10,35))
	product=[]
	for i in range(len(app_data.get('cart_list'))):
		product.append({"af_price":str(app_data.get('cart_list')[i].get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('cart_list')[i].get('sku'),"af_quantity":"1","af_currency":"IDR"})
	print 'Appsflyer : EVENT___________________________af_add_payment_info'
	eventName		  = 'af_add_payment_info'
	

	if app_data.get('register')==False:
		eventValue		  = {"af_screen_name":"retail-checkout-2","product":product,"af_payment_method":"VISACreditCard"}

	else:
		eventValue		  = {"af_screen_name":"retail-checkout-2","product":product,"af_payment_method":"VISACreditCard","af_customer_user_id":app_data.get('customer_user_id')}

	
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	



def af_add_to_wishlist(campaign_data, app_data, device_data):
	time.sleep(random.randint(10,35))
	print 'Appsflyer : EVENT___________________________af_add_to_wishlist'
	eventName			= 'af_add_to_wishlist'
	

	if app_data.get('register')==False:
		eventValue			= {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_currency":"IDR","af_screen_name":"retail-product-detail"}

	else:
		eventValue			= {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('selected_sku').get('sku'),"af_customer_user_id":app_data.get('customer_user_id'),"af_currency":"IDR","af_screen_name":"retail-product-detail"}

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	





def af_remove_cart(campaign_data, app_data, device_data):
	time.sleep(random.randint(10,35))
	index=random.randint(0,len(app_data.get('cart_list'))-1)
	print 'Appsflyer : EVENT___________________________af_remove_cart'
	eventName			= 'af_remove_cart'
	

	if app_data.get('register')==False:
		eventValue			= {"af_price":str(app_data.get('cart_list')[index].get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('cart_list')[index].get('sku'),"af_quantity":"1","af_currency":"IDR","af_screen_name":"retail-cart"}

	else:
		eventValue			= {"af_price":str(app_data.get('cart_list')[index].get('price')),"af_content_type":app_data.get('af_category')[2:],"af_content_id":app_data.get('cart_list')[index].get('sku'),"af_quantity":"1","af_customer_user_id":app_data.get('customer_user_id'),"af_currency":"IDR","af_screen_name":"retail-cart"}

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)
	del app_data.get('cart_list')[index]


def af_product_click(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_product_click'
	eventName			= 'af_product_click'
	time.sleep(random.randint(10,35))
	if app_data.get('filter')==True:
		af_content_type=app_data.get('selected_sku').get('category')
	else:
		af_content_type=""
	

	if app_data.get('register')==False:
		eventValue			= {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":af_content_type,"af_content_id":app_data.get('selected_sku').get('sku'),"af_currency":"IDR","af_screen_name":"retail-home"}

	else:
		eventValue			= {"af_price":str(app_data.get('selected_sku').get('price')),"af_content_type":af_content_type,"af_content_id":app_data.get('selected_sku').get('sku'),"af_currency":"IDR","af_customer_user_id":app_data.get('customer_user_id'),"af_screen_name":"retail-home"}

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)


def af_product_impression(campaign_data, app_data, device_data):
	time.sleep(random.randint(10,35))
	print 'Appsflyer : EVENT___________________________af_product_impression'
	eventName			= 'af_product_impression'
	product_temp=[]
	temp=[]

	print len(selsku_list)

	for i in range(app_data.get('count'),app_data.get('count')+5):
		product_temp.append({"af_price":selsku_list[i].get('price'),"af_content_type":selsku_list[i].get('category'),"af_content_id":selsku_list[i].get('sku'),"af_currency":"IDR"})
		temp.append({'sku' : selsku_list[i].get('sku'), 'price' : selsku_list[i].get('price'),'category':selsku_list[i].get('category')})
		app_data['count']=i+1
		print app_data['count']
		# if app_data.get('count')==len(selsku_list):
		# 	app_data['count']

	app_data['selected_sku'] = random.choice(temp)
	print app_data['selected_sku']
	

	if app_data.get('register')==False:
		eventValue			= {"af_screen_name":"retail-search","product":product_temp}

	else:
		eventValue			= {"af_screen_name":"retail-search","product":product_temp,"af_customer_user_id":app_data.get('customer_user_id')}

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)




def gcmToken_call(campaign_data, app_data, device_data):
	url = 'https://android.clients.google.com/c2dm/register3'
	method = 'post'
	headers = {
	'Accept-Encoding' : 'gzip',
	'User-Agent'	 : 'Android-GCM/1.5 ('+device_data.get('device')+' '+device_data.get('build')+')',
	'Content-Type'	 : 'application/x-www-form-urlencoded',
	'app'	 : campaign_data.get('package_name'),
	'gcm_ver'	 : '19420011',
	'Authorization'	 : 'AidLogin 4382913187332241747:6744718531449003724',
	}
	params = None
	data = {
	'X-subtype':'13709275050',
	'sender':'13709275050',
	'X-app_ver':campaign_data.get('app_version_code'),
	'X-osv':device_data.get('sdk'),
	'X-cliv':'fiid-'+util.get_random_string('decimal',8),
	'X-gmsv':'19420011',
	'X-appid':util.get_random_string('char_all',11),
	'X-scope':'*',
	'X-gmp_app_id':'1:'+'13709275050'+':android:'+device_data.get('android_id'),
	'X-app_ver_name':campaign_data.get('app_version_name'),
	'app':campaign_data.get('package_name'),
	'device':'4382913187332241747',
	'cert':util.get_random_string('hex',40),
	'app_ver':campaign_data.get('app_version_code'),
	'info':'4-'+util.get_random_string('all',18)+'_'+util.get_random_string('all',10),
	'gcm_ver':'19420011',
	'plat':'0',
	'target_ver'	:'28',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def connectivity_check(campaign_data, app_data, device_data):
	url = 'http://connectivitycheck.gstatic.com/generate_204'
	method = 'head'
	headers = {
		'Accept-Encoding': 'gzip',
		'User-Agent': device_data.get('User-Agent'),
	}
	params=None
	data = None
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A13709275050%3Aandroid%3A1780223ebb7e30fc'
	
	header={
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'3fad9c30c7947cdf767beb7c954e86df',
		'platform':	'android',
		'gmp_version':	'17122',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}	



#####################################################
#	self calls functions
#####################################################

def get_auth_token_func(campaign_data, device_data, app_data,call=0):

	print '\nCall to get auth_token'
	req = get_auth_token(campaign_data, device_data, app_data,call=call)
	res = util.execute_request(**req)
	try:
		result = json.loads(res.get('data'))
		app_data['auth_token'] = result.get('access_token')
		if not app_data.get('auth_token'):
			app_data['auth_token'] = "1F01FE09-2FDE-4593-96BD-A505065AA083"
			print '\nDefault value for auth_token'
		print '\nOk response for auth_token'
		print 'auth_token = ',
		print app_data['auth_token']
	except:
		print '\nException for auth_token'
		app_data['auth_token'] = "1F01FE09-2FDE-4593-96BD-A505065AA083"


def get_sku_list_func(campaign_data, device_data, app_data):

	if len(globals().get('sku_list'))<1:

		print '\nCall to get sku_list'
		req = get_sku_list(campaign_data, device_data, app_data)
		res = util.execute_request(**req)
		try:
			sku_list = []
			result = json.loads(res.get('data'))
			if result.get('data'):
				for product in result.get('data'):
					sku = product.get('productSku')
					price = None
					if product.get('otherOfferings'):
						if product.get('otherOfferings').get('startPrice'):
							if int(product.get('otherOfferings').get('startPrice')) <= 143000:
								price = product.get('otherOfferings').get('startPrice')
					if sku and price:
						sku_list.append({'sku' : sku.encode('utf-8'), 'price' : price})
			if not sku_list:
				sku_list = [{'sku': 'BHK-60022-00002', 'price': u'16200'}, {'sku': 'BLC-60027-00005', 'price': u'128000'}, {'sku': 'BLC-60027-00006', 'price': u'124000'}, {'sku': 'GOS-60047-00220', 'price': u'129000'}, {'sku': 'PEM-60038-00001', 'price': u'91200'}, {'sku': 'BEF-60034-00057', 'price': u'21950'}, {'sku': 'ANT-60031-00007', 'price': u'20000'}, {'sku': 'PTM-26971-00173', 'price': u'15900'}, {'sku': 'MOS-60032-00108', 'price': u'72900'}, {'sku': 'UNI-60025-00134', 'price': u'24000'}, {'sku': 'PTM-26971-00172', 'price': u'15950'}, {'sku': 'MOS-60032-00109', 'price': u'39000'}, {'sku': 'BEF-60034-00047', 'price': u'19686'}, {'sku': 'BLC-60027-00033', 'price': u'125000'}, {'sku': 'WIT-35794-00022', 'price': u'58500'}, {'sku': 'BLC-15022-00856', 'price': u'11500'}]
				print '\nDefault value for sku_list'
			print '\nOk response for sku_list'
			globals()['sku_list'] = sku_list
			print globals().get('sku_list')
		except:
			print '\nException for sku_list'
			globals()['sku_list'] = [{'sku': 'BHK-60022-00002', 'price': u'16200'}, {'sku': 'BLC-60027-00005', 'price': u'128000'}, {'sku': 'BLC-60027-00006', 'price': u'124000'}, {'sku': 'GOS-60047-00220', 'price': u'129000'}, {'sku': 'PEM-60038-00001', 'price': u'91200'}, {'sku': 'BEF-60034-00057', 'price': u'21950'}, {'sku': 'ANT-60031-00007', 'price': u'20000'}, {'sku': 'PTM-26971-00173', 'price': u'15900'}, {'sku': 'MOS-60032-00108', 'price': u'72900'}, {'sku': 'UNI-60025-00134', 'price': u'24000'}, {'sku': 'PTM-26971-00172', 'price': u'15950'}, {'sku': 'MOS-60032-00109', 'price': u'39000'}, {'sku': 'BEF-60034-00047', 'price': u'19686'}, {'sku': 'BLC-60027-00033', 'price': u'125000'}, {'sku': 'WIT-35794-00022', 'price': u'58500'}, {'sku': 'BLC-15022-00856', 'price': u'11500'}]


def get_product_data_func(campaign_data, device_data, app_data):

	print '\n Call to get product_data'
	req = get_product_data(campaign_data, device_data, app_data)
	res = util.execute_request(**req)
	try:
		app_data['af_category'] = ''
		brand = ''
		af_content = ''
		result = json.loads(res.get('data'))
		if result.get('data'):

			if result.get('data').get('categories'):
				info=result.get('data').get('categories')
				if len(info)>0:
					for i in range(len(info)):
						app_data['af_category']=app_data.get('af_category')+"\/"+info[i].get('label')

		if app_data.get('af_category')=='' or app_data.get('af_category')==None:
			app_data['af_category']='\/Handphone & Tablet\/Handphone\/iPhone'
		print app_data['af_category']

	except:
		app_data['af_category']='\/Handphone & Tablet\/Handphone\/iPhone'

	if app_data.get('af_category')=='' or app_data.get('af_category')==None:
		app_data['af_category']='\/Handphone & Tablet\/Handphone\/iPhone'


				

#########################################################
#	self calls def.
#########################################################


def get_auth_token(campaign_data, device_data, app_data,call=0):

	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	if not app_data.get('session_id'):
		app_data['session_id'] = str(uuid.uuid4())

	if not app_data.get('request_id'):
		app_data['request_id'] = str(uuid.uuid4())

	if not app_data.get('store_id'):
		app_data['store_id'] = '1000' + str(random.randint(1,9))

	url= "https://account.blibli.com/gdn-oauth/token"
	method= "post"
	headers= {      "accept": "application/json",
					"x-userid": app_data.get('user_id'),
					"x-sessionid": app_data.get('session_id'),
					"x-requestid": str(uuid.uuid4()),
					"user-agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
					'Accept-Language': device_data.get('locale').get('language'),
					'channelId': 'android',
					'storeId': app_data.get('store_id'),
					'Content-Type': 'application/x-www-form-urlencoded',
					'Accept-Encoding': 'gzip',
					'X-NewRelic-ID': 'VQQGWFRSDxACVFNbBQQHUA=='}

	params= None

	data= {'grant_type':'client_credentials',
	'client_id':'6354c4ea-9fdf-4480-8a0a-b792803a7f11',
	'client_secret':'XUQpvvcxxyjfb7KG'}

	if call==1:
		data['grant_type']='password'
		data['password']=app_data.get('user').get('password')
		data['scope']='write'
		data['username']=app_data.get('user').get('email')


	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def get_sku_list( campaign_data, device_data, app_data ):

    if not app_data.get('user_id'):
        app_data['user_id'] = str(uuid.uuid4())

    if not app_data.get('session_id'):
        app_data['session_id'] = str(uuid.uuid4())

    if not app_data.get('request_id'):
        app_data['request_id'] = str(uuid.uuid4())

    if not app_data.get('store_id'):
    	app_data['store_id'] = '1000' + str(random.randint(1,9))

    url= "https://www.blibli.com/backend/search/homepage-recommendation?pageTypeId=homePage&placement=recs_home_page_android_2&itemPerPage=10"
    method= "get"
    headers= {      "accept": "application/json",
					"x-userid": app_data.get('user_id'),
					"x-sessionid": app_data.get('session_id'),
					"x-requestid": str(uuid.uuid4()),
					"user-agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
					'Accept-Language': device_data.get('locale').get('language'),
					"content-type": "application/json; charset=UTF-8",
					"channelid": "android",
					"storeid": app_data.get('store_id'),
					"authorization": "bearer " + app_data.get('auth_token'),
					"accept-encoding": "gzip",
					"x-newrelic-id": "VQQGWFRSDxACVFNbBQQHUA=="}

    params= None

    data= None

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def get_product_data(campaign_data, device_data, app_data):

	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	if not app_data.get('session_id'):
		app_data['session_id'] = str(uuid.uuid4())

	if not app_data.get('request_id'):
		app_data['request_id'] = str(uuid.uuid4())

	if not app_data.get('store_id'):
		app_data['store_id'] = '1000' + str(random.randint(1,9))

	
	url = "https://www.blibli.com/backend/product/products/ps--"+ app_data.get('selected_sku').get('sku') +"/_summary"
	method = "get"
	headers = {      "accept": "application/json",
					"x-userid": app_data.get('user_id'),
					"x-sessionid": app_data.get('session_id'),
					"x-requestid": str(uuid.uuid4()),
					"user-agent": "BlibliAndroid/"+ campaign_data.get('app_version_name') +"("+ campaign_data.get('app_version_code') +") "+ app_data.get('user_id') +" " + get_ua(device_data),
					'Accept-Language': device_data.get('locale').get('language'),
					"content-type": "application/json; charset=UTF-8",
					"channelid": "android",
					"storeid": app_data.get('store_id'),
					"authorization": "bearer " + app_data.get('auth_token'),
					"accept-encoding": "gzip",
					"x-newrelic-id": "VQQGWFRSDxACVFNbBQQHUA=="}

	params = None
	data = None

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_sdks" : "0000000000",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "ac",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [-7.715, 34.131, 2.111], u'sV': u'MTK', u'sVS': [-10.865, 33.626, 5.614], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.049, 0.393, 10.22], u'sV': u'MTK', u'sVS': [-0.031, 0.398, 10.206], u'sT': 1}, {u'sN': u'GYROSCOPE', u'sVE': [0, 0, 0], u'sV': u'MTK', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),
		'tokenRefreshConfigured':False,

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	# if app_data.get('installAttribution'):
	# 	data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
		
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	if app_data.get('counter')>2:
		del data['rfr']		
		del data['deviceData']['sensors']


	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data,opt=False):
	def_appsflyerUID(app_data)
	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQQGWFRSDxACVFNbBQQHUA==",

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}
	if opt:
		data['af_gcm_token']=str(app_data.get('gcm_token_timestamp'))+','+app_data.get('af_gcm_token')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQQGWFRSDxACVFNbBQQHUA==",

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def appsflyer_attr(campaign_data, app_data, device_data):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	method = "post"
	url = 'http://attr.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQQGWFRSDxACVFNbBQQHUA==",

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		# "cksm_v1" : cksm_v1(),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "ac",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
								},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [-7.715, 34.131, 2.111], u'sV': u'MTK', u'sVS': [-10.865, 33.626, 5.614], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.049, 0.393, 10.22], u'sV': u'MTK', u'sVS': [-0.031, 0.398, 10.206], u'sT': 1}, {u'sN': u'GYROSCOPE', u'sVE': [0, 0, 0], u'sV': u'MTK', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name('en'),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : "WIFI",
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : 0,
		"uid" : app_data.get('uid'),

		}
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	else:
		data['referrer'] = 'utm_source=(not%20set)&utm_medium=(not%20set)'

	if app_data.get('timepassedsincelastlaunch'):
		data['timepassedsincelastlaunch'] = str(int(time.time()) - app_data.get('timepassedsincelastlaunch'))
		app_data['timepassedsincelastlaunch'] = int(time.time())
	else:
		data['timepassedsincelastlaunch'] = '0'
		app_data['timepassedsincelastlaunch'] = int(time.time())

	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQQGWFRSDxACVFNbBQQHUA==",

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		#"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		# "cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		'tokenRefreshConfigured':False,

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	# if app_data.get('installAttribution'):
	# 	data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))


###########################################################
# Utility methods : MISC
#
###########################################################

def timestamp():
	return str(int(time.time()*1000))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 	= util.get_random_string(type='char_small',size=10)+'@'+util.get_random_string(type='decimal',size=3)