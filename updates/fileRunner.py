import os,time,urllib,random,sys

def run(filename,device_data):
  direct = os.getcwd()+'/updates'
  with open(direct+'/'+filename+'.py', "r+") as f:
    file = f.read()
    file=file.replace('saveProxyConsumptionRawdata','#saveProxyConsumptionRawdata')
    file=file.replace('#saveProxyConsumptionRawdata(data):','saveProxyConsumptionRawdata(data):')
    file=file.replace('time.sleep','#time.sleep')
    file=file.replace('import clicker','#import clicker')
    file=file.replace('import Config','#import Config')
    file=file.replace('import Config','#import Config')
    file=file.replace('Config.AGENTID','0')
    file=file.replace('from sdk import getsleep','#from sdk import getsleep')
    file=file.replace("'status':'true'","'data':util.allData")
    file=file.replace("'status':True","'data':util.allData")
    with open(direct+'/'+filename+'_copy.py', "a") as copy:
      copy.write(file)
      time.sleep(3)


  sys.path.insert(0, direct)
  i = __import__(filename+'_copy')
  tracker=i.campaign_data.get('tracker')
  # if True:
  # 	os.environ['http_proxy'] = "http://127.0.0.1:8888"
  # 	os.environ['https_proxy'] = "http://127.0.0.1:8888"

  app_data = {						
  	'referrer':"link_clic_id-722621491516512077%26utm_source%3DBranch",
  	}
  				
  globals()['total_sleep'] = 0
  def remove_sleep(func):
    def wrapper(a):
      if isinstance(a, (int, float)) and a >= 0:
        globals()['total_sleep'] += a
      else:
        raise Exception('Must be >= 0')

    return wrapper
  time.sleep = remove_sleep(time.sleep)
  allData={}
  try:
    for _ in range(1):
      for _ in range(1):
        allData=i.install(app_data, device_data).get('data')
  except Exception,e:
    print str(e)

  finally:
    os.remove(direct+'/'+filename+'_copy.py')
    os.remove(direct+'/'+filename+'_copy.pyc')
    os.remove(direct+'/'+filename+'.py')

  return allData,tracker
