import hashlib
import json
from Crypto import Random
from Crypto.Cipher import AES

global decrypted_data
decrypted_data = []

"""* 
	>>>>AES CIPHER CLASS FOR AD-BRIX REFERRAL CALL<<<<
*"""
class AESCipher(object):

	def __init__(self, key):
		self.bs = 32
		self.key = key.encode()

	def encrypt(self, iv, raw):
		raw = self._pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

	def decrypt(self, iv, enc):
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc.decode('hex'))

	def _pad(self, s):
		return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

	@staticmethod
	def _unpad(s):
		return s[:-ord(s[len(s) - 1:])]

"""* 
	>>>>AES CIPHER CLASS FOR AD-BRIX TRACKING CALLS<<<<
*"""
class AESCipherTracking(object):

	def __init__(self, key):
		self.bs = 16
		self.key = key.encode()

	def encrypt(self, iv, raw):
		raw = self._pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

	def decrypt(self, iv, enc):
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc.decode('hex'))

	def _pad(self, s):
		return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

	@staticmethod
	def _unpad(s):
		return s[:-ord(s[len(s) - 1:])]


"""* 
	>>>>FUNCTION TO DECRYPT AD-BRIX DATA<<<<
*"""
def decrypt_data(data, key="srkterowgawrsozerruly82nfij625w9", iv="srkterowgawrsoze", call_type="", call_seq=None,
				 log=True):
	if call_type == "referral":
		#######FOR CONVERSION GET REFERRAL########
		aes = AESCipher(key)
	else:
		#######FOR TRACKING#######################
		aes = AESCipherTracking(key)

	#######SAME FOR BOTH#######################
	dec = aes.decrypt(iv, data)

	if log:
		decrypted_data.append(
			"#################################################################################\nCall Number:-" + str(
				call_seq) + "\n")
		decrypted_data.append("#######################################\nDecoded data:-\n")
		decrypted_data.append(str(eval(
			(dec.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"', "sfalse").replace("true",
																										  "True").replace(
				"false", "False").replace(
				"null", "None").replace("strue", '"true"').replace("sfalse", '"false"'))))
		decrypted_data.append("\n#################################################################################\n\n")

	return dec

# print dec
# print aes.encrypt(iv,json.dumps({"appkey":"44976100","package_name":"com.pang.gifight.google","installer":"com.android.vending","version":"4.5.4a","app_version_name":"1.0.11","app_version_code":11,"referral_info":{"conversion_key":-1,"session_no":-1,"referrer_param":""},"conversion_cache":[],"adbrix_user_info":{"adbrix_user_no":878135156368655382,"shard_no":6,"install_datetime":"20180630132236","install_mdatetime":1530345156000,"life_hour":0,"app_launch_count":1,"referral_key":0,"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":True,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"f79c1a4996b4e46559b53953fa03160e","android_id_sha1":"283dc92e5bfa563482df4a286f838cb25f2885cd","device_id_md5":"","device_id_sha1":"","google_ad_id":"3487698f-5e5b-447a-b79f-77bebd0be996","google_ad_id_opt_out":False,"initial_ad_id":"3487698f-5e5b-447a-b79f-77bebd0be996","ag":"c805e2b374053e69581b167d198ed163d45d6532|66cec5f487c72fae0ee5e0bc305d83d892afe531|4c87a944842cdd717d763784188c432f1b4c2aa2","odin":"283dc92e5bfa563482df4a286f838cb25f2885cd","carrier":"airtel","country":"GB","language":"en","android_id":"ZjdjNGFiNjEwZDI3NWM0Nw==","first_install_time":"20180630041901","first_install_time_offset":210582},"cohort_info":{},"device_info":{"vendor":"google","model":"A37fw","kn":"3.10.49-perf-g88f85af-01176-g6e3e11e","is_wifi_only":False,"network":"wifi","noncustomnetwork":13,"os":"a_5.1.1","ptype":"android","width":720,"height":1280,"is_portrait":False,"utc_offset":5.5,"build_id":"LMY47V"},"demographics":[{"demo_key":"userId","demo_value":"66-2-f7c4ab610d275c47"}],"complete_conversions":[],"activity_info":[{"prev_group":"","prev_activity":"","group":"session","activity":"retention","param":"","event_id":"a5421b3a-f7d2-49c3-96ae-ba0f952aba51","created_at":"20180630132237"},{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":"e2f09a43-2a9a-40bd-9d42-0292db1ea97f","created_at":"20180630132237"},{"prev_group":"session","prev_activity":"start","group":"fte","activity":"App_Start","param":"","event_id":"56773b7c-7f7b-4a99-bb86-b2708f03a22b","created_at":"20180630132237"}],"impression_info":[],"installation_info":{"market_referrer":"utm_source=(not%20set)&utm_medium=(not%20set)","install_actions_timestamp":{"market_install_btn_clicked":1530332213,"app_install_start":1530332225,"app_install_completed":1530332341,"app_first_open":1530332551}},"request_info":{"client_timestamp":1530332557}}))
