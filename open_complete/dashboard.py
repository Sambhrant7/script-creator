# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import subprocess
from PySide import QtCore, QtGui
import argparse, os

try:
	from Shiboken import shiboken
except:
	os.system("pip install Shiboken")
	from Shiboken import shiboken

global ui_Data, ui_Keys, totalCalls, urls, calls_from_logs, urls_from_logs, script
globals()['ui_Data'] = []
globals()['ui_Keys'] = {}

globals()['totalCalls'] = []
globals()['urls'] = []
globals()['calls_from_logs'] = []
globals()['urls_from_logs'] = []
globals()['script'] = ""

def main(call,url,calls_from_log,urls_from_log,fileName):
	globals()['totalCalls'] = call
	globals()['urls'] = url
	globals()['calls_from_logs'] = calls_from_log
	globals()['urls_from_logs'] = urls_from_log
	globals()['script'] = str(fileName)
	initialize()

#UI for the Dashboard
class Ui_fiberModesMainWindow(object):
	def setupUi(self, fiberModesMainWindow, call, url):
		fiberModesMainWindow.setObjectName("fiberModesMainWindow")
		# self.setFixedHeight(200)
		self.centralwidget = QtGui.QWidget(fiberModesMainWindow)
		self.centralwidget.setObjectName("centralwidget")
		self.centralwidget.setStyleSheet("background-color: #bcd1d0")
		self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
		self.horizontalLayout_2.setObjectName("horizontalLayout_2")
		self.MainLayout = QtGui.QGridLayout()
		self.MainLayout.setObjectName("MainLayout")
		self.PropertyLayout = QtGui.QGridLayout()
		self.PropertyLayout.setObjectName("PropertyLayout")

		##############	TITLES ##############
		self.lbl_Name = QtGui.QLabel(self.centralwidget)
		self.lbl_Name.setObjectName("lbl_Name")
		self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Occurance", None, QtGui.QApplication.UnicodeUTF8))
		self.lbl_Name.setStyleSheet("font-weight: bold")
		self.PropertyLayout.addWidget(self.lbl_Name, 0, 0, 1, 1)
		self.lbl_Name = QtGui.QLabel(self.centralwidget)
		self.lbl_Name.setObjectName("lbl_Name")
		self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", '\t\t\t'+'Call/Event Name with URL'+"\t\t\t", None, QtGui.QApplication.UnicodeUTF8))
		self.lbl_Name.setStyleSheet("font-weight: bold")
		self.PropertyLayout.addWidget(self.lbl_Name, 0, 2, 1, 1)
		self.lbl_Name = QtGui.QLabel(self.centralwidget)
		self.lbl_Name.setObjectName("lbl_Name")
		self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Randomness", None, QtGui.QApplication.UnicodeUTF8))
		self.lbl_Name.setStyleSheet("font-weight: bold")
		self.PropertyLayout.addWidget(self.lbl_Name, 0, 3, 1, 1)
		self.lbl_Name = QtGui.QLabel(self.centralwidget)
		self.lbl_Name.setObjectName("lbl_Name")
		self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Sequence", None, QtGui.QApplication.UnicodeUTF8))
		self.lbl_Name.setStyleSheet("font-weight: bold")
		self.PropertyLayout.addWidget(self.lbl_Name, 0, 4, 1, 1)
		self.lbl_Name = QtGui.QLabel(self.centralwidget)
		self.lbl_Name.setObjectName("lbl_Name")
		self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Repeated", None, QtGui.QApplication.UnicodeUTF8))
		self.lbl_Name.setStyleSheet("font-weight: bold")
		self.PropertyLayout.addWidget(self.lbl_Name, 0, 5, 1, 1)
		############################BUTTONS###############################
		self.Dependencies = QtGui.QPushButton(self.centralwidget)
		self.Dependencies.clicked.connect(self.dependencyUi)
		self.Dependencies.setObjectName("Dependencies")
		self.Dependencies.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Dependencies", None, QtGui.QApplication.UnicodeUTF8))
		self.PropertyLayout.addWidget(self.Dependencies, 1, 7, 1, 1)

		self.pb_addRow_1 = QtGui.QPushButton(self.centralwidget)
		self.pb_addRow_1.setObjectName("pb_addRow_1")
		self.pb_addRow_1.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Add New Call", None, QtGui.QApplication.UnicodeUTF8))
		self.PropertyLayout.addWidget(self.pb_addRow_1, 1, 6, 1, 1)

		globals()['ui_Keys']['Dependencies'] = self.Dependencies

		############################WIDGETS###############################
		self.check = QtGui.QCheckBox("INSTALL")
		self.check.setChecked(1)
		self.check.setObjectName("I")
		self.checkO = QtGui.QCheckBox("OPEN")
		self.checkO.setObjectName("O")
		self.call = QtGui.QLineEdit(call+" URL["+url+"]")
		self.call.setMinimumWidth(800)
		# self.call.setEnabled(False)
		self.randomness = QtGui.QSpinBox()
		self.sequence = QtGui.QSpinBox()
		self.repeat = QtGui.QSpinBox()
		self.randomness.setRange(0,100)
		self.sequence.setRange(0,5000)
		self.repeat.setRange(0,1500)
		self.randomness.setMaximumWidth(50)
		self.sequence.setMaximumWidth(50)
		self.repeat.setMaximumWidth(50)
		self.randomness.setValue(100)
		self.sequence.setValue(1)
		self.repeat.setValue(1)
		self.PropertyLayout.addWidget(self.check, 1, 0, 1, 1)
		self.PropertyLayout.addWidget(self.checkO, 1, 1)
		self.PropertyLayout.addWidget(self.call, 1, 2, 1, 1)
		self.PropertyLayout.addWidget(self.randomness, 1, 3, 1, 1)
		self.PropertyLayout.addWidget(self.sequence, 1, 4, 1, 1)
		self.PropertyLayout.addWidget(self.repeat, 1, 5, 1, 1)
		self.randomness.setObjectName("randomness")
		self.sequence.setObjectName("sequence")
		self.repeat.setObjectName("repeat")
		globals()['ui_Keys']['check'] = self.check
		globals()['ui_Keys']['checkO'] = self.checkO
		globals()['ui_Keys']['call'] = self.call
		globals()['ui_Keys']['randomness'] = self.randomness
		globals()['ui_Keys']['sequence'] = self.sequence
		globals()['ui_Keys']['repeat'] = self.repeat
		globals()['ui_Data'].append(globals()['ui_Keys'])
		globals()['ui_Keys']={}

		self.MainLayout.addLayout(self.PropertyLayout, 0, 0, 1, 1)

		spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
		self.MainLayout.addItem(spacerItem2, 1, 0, 1, 1)
		self.horizontalLayout_2.addLayout(self.MainLayout)

		scroll = QtGui.QScrollArea()
		scroll.setWidgetResizable(True)
		scroll.setWidget(self.centralwidget)
		fiberModesMainWindow.setCentralWidget(scroll)
		self.retranslateUi(fiberModesMainWindow)
		QtCore.QMetaObject.connectSlotsByName(fiberModesMainWindow)
		fiberModesMainWindow.setWindowIcon(QtGui.QIcon("reference/logo.png"))

	def retranslateUi(self, fiberModesMainWindow):
		fiberModesMainWindow.setWindowTitle(QtGui.QApplication.translate("fiberModesMainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))

	#UI for the dependencies
	def dependencyUi(self):
		index = self.PropertyLayout.indexOf(self.sender())
		seq = self.PropertyLayout.getItemPosition(index)[0]
		filter(None,globals()['ui_Data'])
		tokenList=[]
		for d in globals()['ui_Data']:
			if shiboken.isValid(d.get('sequence')):
				if int(d.get('sequence').value())<seq:
					tokenList.append(d.get('call').text())
		dependency = subprocess.check_output(["python", "open_complete/ui/dependency_ui.py", '-tokens='+str(tokenList)])
		globals()['ui_Data'][seq-1]['DependentOnCalls']=[a.strip() for a in dependency.split("\n")]

class DesignerMainWindow(QtGui.QMainWindow, Ui_fiberModesMainWindow):
	def __init__(self, parent = None):
		super(DesignerMainWindow, self).__init__(parent)
		self.callPatternInfo,self.retentionInfo,self.purchaseInfo = None,None,None
		self.setupUi(self, globals()['totalCalls'][0], globals()['urls'][0])
		self.pb_addRow_1.clicked.connect(self.addRow)

		self.buttonGroup = QtGui.QGridLayout()
		self.buttonGroup.setObjectName("buttonGroup")

		#Button for call pattern
		rows = self.PropertyLayout.rowCount()
		self.call_pattern = QtGui.QPushButton(self.centralwidget)
		self.call_pattern.setObjectName("pb_addRow_1")
		self.call_pattern.clicked.connect(self.callPatternUi)
		self.call_pattern.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Call Pattern Configuration", None, QtGui.QApplication.UnicodeUTF8))
		self.buttonGroup.addWidget(self.call_pattern, 0, 0, 1, 1)

		#Button for Purchase Token
		self.purchaseUI = QtGui.QPushButton(self.centralwidget)
		self.purchaseUI.setObjectName("pb_addRow_1")
		self.purchaseUI.clicked.connect(self.purchaseUi)
		self.purchaseUI.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Purchase Configuration", None, QtGui.QApplication.UnicodeUTF8))
		self.buttonGroup.addWidget(self.purchaseUI, 0, 1, 1, 1)

		#Button for Retention Token
		self.retentionUI = QtGui.QPushButton(self.centralwidget)
		self.retentionUI.setObjectName("pb_addRow_1")
		self.retentionUI.clicked.connect(self.retentionUi)
		self.retentionUI.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Setup Retention", None, QtGui.QApplication.UnicodeUTF8))
		self.buttonGroup.addWidget(self.retentionUI, 0, 2, 1, 1)
		self.MainLayout.addLayout(self.buttonGroup, rows+1, 0, 1, 1)

		#Button for Finalise the Script
		self.finalizeUI = QtGui.QPushButton(self.centralwidget)
		self.finalizeUI.setObjectName("pb_addRow_1")
		self.finalizeUI.clicked.connect(self.final)
		self.finalizeUI.setText(QtGui.QApplication.translate("fiberModesMainWindow", "FINALIZE SCRIPT", None, QtGui.QApplication.UnicodeUTF8))
		self.MainLayout.addWidget(self.finalizeUI, rows+2, 0, 1, 1)

	#UI for adding calls to the Dashboard
	def addRow( self , call_name="Call Name here", r=100, s=0, INSTALL=1, repeat=1):
		rows = self.PropertyLayout.rowCount()
		columns = self.PropertyLayout.columnCount()
		for column in range(columns):
			layout = self.PropertyLayout.itemAtPosition(rows - 1, column)
			if layout is not None:
				widget = layout.widget()
				if isinstance(widget, QtGui.QPushButton) and widget.text()=="Add New Call":
					widget.setText('Delete %d' % (rows - 1))
					widget.clicked.disconnect(self.addRow)
					widget.clicked.connect(self.removeRow)
					widget.clicked.connect(self.sortSeq)

		globals()['ui_Keys']['check'] = QtGui.QCheckBox("INSTALL")
		globals()['ui_Keys']['checkO'] = QtGui.QCheckBox("OPEN")
		globals()['ui_Keys']['call'] = QtGui.QLineEdit(call_name)
		globals()['ui_Keys']['call'].setMinimumWidth(800)
		# call.setMaximumWidth(800)
		globals()['ui_Keys']['randomness'] = QtGui.QSpinBox()
		globals()['ui_Keys']['sequence'] = QtGui.QSpinBox()
		globals()['ui_Keys']['repeat'] = QtGui.QSpinBox()
		globals()['ui_Keys']['randomness'].setMaximumWidth(50)
		globals()['ui_Keys']['sequence'].setMaximumWidth(50)
		globals()['ui_Keys']['repeat'].setMaximumWidth(50)
		globals()['ui_Keys']['randomness'].setRange(0,100)
		globals()['ui_Keys']['sequence'].setRange(0,5000)
		globals()['ui_Keys']['repeat'].setRange(0,1500)
		globals()['ui_Keys']['randomness'].setValue(r)
		globals()['ui_Keys']['sequence'].setValue(s)
		globals()['ui_Keys']['repeat'].setValue(repeat)
		globals()['ui_Keys']['randomness'].setObjectName("randomness")
		globals()['ui_Keys']['sequence'].setObjectName("sequence")
		globals()['ui_Keys']['repeat'].setObjectName("repeat")
		globals()['ui_Keys']['call'].setModified(True)

		globals()['ui_Keys']['Dependencies'] = QtGui.QPushButton(self.centralwidget)
		globals()['ui_Keys']['Dependencies'].setText('Dependencies')
		globals()['ui_Keys']['Dependencies'].clicked.connect(super(DesignerMainWindow, self).dependencyUi)
		globals()['ui_Keys']['check'].setChecked(INSTALL)


		if call_name=="Call Name here":
			globals()['ui_Keys']['sequence'].setValue(rows)

		self.PropertyLayout.addWidget(globals()['ui_Keys']['check'], globals()['ui_Keys']['sequence'].value(), 0, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['checkO'], globals()['ui_Keys']['sequence'].value(), 1, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['call'], globals()['ui_Keys']['sequence'].value(), 2, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['randomness'], globals()['ui_Keys']['sequence'].value(), 3, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['sequence'], globals()['ui_Keys']['sequence'].value(), 4, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['repeat'], globals()['ui_Keys']['sequence'].value(), 5, 1, 1)
		self.PropertyLayout.addWidget(globals()['ui_Keys']['Dependencies'], globals()['ui_Keys']['sequence'].value(), 7, 1, 1)
		tokenList=[]
		filter(None,globals()['ui_Data'])
		for d in globals()['ui_Data']:
			if shiboken.isValid(d.get('sequence')) and d.get('sequence'):
				if int(d.get('sequence').value())<s:
					tokenList.append(d.get('call').text())
		globals()['ui_Keys']['DependentOnCalls']=tokenList


		widget = QtGui.QPushButton(self.centralwidget)
		widget.setText('Add New Call')
		widget.clicked.connect(self.addRow)
		self.PropertyLayout.addWidget(widget, int(globals()['ui_Keys']['sequence'].value()), columns - 2, 1, 1)

		
		globals()['ui_Data'].append(globals()['ui_Keys'])
		globals()['ui_Keys']={}

	#UI for removing call from the dashboard
	def removeRow(self):
		index = self.PropertyLayout.indexOf(self.sender())
		row = self.PropertyLayout.getItemPosition(index)[0]
		
		for column in range(self.PropertyLayout.columnCount()):
			layout = self.PropertyLayout.itemAtPosition(row, column)
			if layout is not None:
				layout.widget().deleteLater()
				self.PropertyLayout.removeItem(layout)

		cursor = int(self.sender().text().split("Delete")[1].strip())
		if cursor-1<len(globals()['ui_Data']):
			if not shiboken.isValid(globals()['ui_Data'][cursor-1].get("call")):
				del globals()['ui_Data'][cursor-1]
				globals()['ui_Data'][cursor-1]={}

	#UI for auto managing sequence of the calls on dashboard
	def sortSeq(self):
		counter=1
		columns = self.PropertyLayout.columnCount()
		filter(None,globals()['ui_Data'])
		for r in range(len(globals()['ui_Data'])):
			flag=False
			layout2 = self.PropertyLayout.itemAtPosition(r, columns-2)
			if layout2 is not None:
				widget2 = layout2.widget()
				if isinstance(widget2, QtGui.QPushButton) and 'Delete' in widget2.text() or widget.text()=="Add New Call":
					widget2.setText('Delete %d' % (counter))

			layout = self.PropertyLayout.itemAtPosition(r, columns-4)
			if layout is not None:
				widget = layout.widget()
				if isinstance(widget, QtGui.QSpinBox):
					widget.setValue(counter)
					counter+=1

			tokenList=[]
			filter(None,globals()['ui_Data'])
			for parsed in range(r):
				if shiboken.isValid(globals()['ui_Data'][parsed].get('sequence')) and globals()['ui_Data'][parsed].get('call'):
					tokenList.append(globals()['ui_Data'][parsed].get('call').text())

			globals()['ui_Data'][r]['DependentOnCalls']=tokenList	

		self.hide()
		self.show()
	
	#data processed for dependencies,callpattern,retention and purchase 
	def final(self):
		import engine
		temp=[]
		calls=[]
		for dic in globals()['ui_Data']:
			data={}
			for enc in dic.keys():
				enc.encode("utf-8")
			for widget in dic.keys():
				if dic[widget] is not None and shiboken.isValid(dic[widget]):
					if isinstance(dic[widget],QtGui.QCheckBox):
						data[dic[widget].text()] = dic[widget].isChecked()
					elif isinstance(dic[widget],QtGui.QLineEdit):
						data["call"] = str(dic[widget].text()).encode("utf-8")
					elif isinstance(dic[widget],QtGui.QSpinBox):
						data[widget] = str(dic[widget].value())
					elif widget=="DependentOnCalls":
						data[widget] = str(dic[widget])
			if data:
				temp.append(data)

		for trash in temp:
			if not trash.get('sequence'):
				temp.remove(trash)
			else:
				calls.append(trash)

		self.hide()
		engine.processCalls(globals()['script'], calls, self.callPatternInfo, self.purchaseInfo, self.retentionInfo)

	#clearing the layout from the dashboard
	def clearLayout(self, layout):
		while layout.count():
			child = layout.takeAt(0)
			if child.widget():
				child.widget().deleteLater()

	#Coloring For calls
	def callPatternUi(self):
		self.callPatternInfo={}
		callPattern = subprocess.check_output(["python", "open_complete/ui/call_pattern_ui.py"])
		cp = [a.strip() for a in callPattern.split("\n")]
		for data in range(len(cp)):
			if "==>" in cp[data]:
				self.callPatternInfo[cp[data].split("==>")[0]] = cp[data].split("==>")[1]
		print self.callPatternInfo
		self.call_pattern.setStyleSheet("background-color: green")
		if not self.callPatternInfo:
			self.call_pattern.setStyleSheet("background-color: red")
		for c in self.callPatternInfo.keys():
			if not self.callPatternInfo.get(c):
				self.call_pattern.setStyleSheet("background-color: red")

	#coloring for retention
	def retentionUi(self):
		self.retentionInfo={}
		retention = subprocess.check_output(["python", "open_complete/ui/retention_ui.py"])
		rt = [a.strip() for a in retention.split("\n")]
		for data in range(len(rt)):
			if "==>" in rt[data]:
				self.retentionInfo[rt[data].split("==>")[0]] = rt[data].split("==>")[1]
		self.retentionUI.setStyleSheet("background-color: green")
		if not self.retentionInfo:
			self.retentionUI.setStyleSheet("background-color: red")
		for c in self.retentionInfo.keys():
			if not self.retentionInfo.get(c):
				self.retentionUI.setStyleSheet("background-color: red")
		print self.retentionInfo

	#coloring for purchase
	def purchaseUi(self):
		self.purchaseInfo={}
		purchase = subprocess.check_output(["python", "open_complete/ui/purchase_ui.py"])
		prch = [a.strip() for a in purchase.split("\n")]
		for data in range(len(prch)):
			if "==>" in prch[data]:
				self.purchaseInfo[prch[data].split("==>")[0]] = prch[data].split("==>")[1]
		self.purchaseUI.setStyleSheet("background-color: green")
		if not self.purchaseInfo:
			self.purchaseUI.setStyleSheet("background-color: red")
		for c in self.purchaseInfo.keys():
			if not self.purchaseInfo.get(c):
				self.purchaseUI.setStyleSheet("background-color: red")
		print self.purchaseInfo


#Managing the calls that goes to install,open or call pattern 
def initialize():
	if not QtGui.QApplication:
		app = QtGui.QApplication(sys.argv)
	dmw = DesignerMainWindow()
	seq=1
	r=1
	for a in range(1,len(globals()['totalCalls'])):
		if a<len(globals()['totalCalls'])-1:
			if globals()['totalCalls'][a]==globals()['totalCalls'][a+1]:
				r+=1
			else:
				seq+=1
				dmw.addRow(globals()['totalCalls'][a]+" [URL="+globals()['urls'][a]+"]",s=seq,repeat=r)
				r=1
		else:
			seq+=1
			dmw.addRow(globals()['totalCalls'][a]+" [URL="+globals()['urls'][a]+"]",s=seq,repeat=r)
			r=1

	for b in range(1,len(globals()['calls_from_logs'])):
		if b<len(globals()['calls_from_logs'])-1:
			if globals()['calls_from_logs'][b]==globals()['calls_from_logs'][b+1]:
				r+=1
			else:
				seq+=1
				dmw.addRow(globals()['calls_from_logs'][b]+" [URL="+globals()['urls_from_logs'][b]+"]",s=seq,INSTALL=0,repeat=r)
				r=1
		else:
			seq+=1
			dmw.addRow(globals()['calls_from_logs'][b]+" [URL="+globals()['urls_from_logs'][b]+"]",s=seq,INSTALL=0,repeat=r)
			r=1

	dmw.addRow()
	dmw.showMaximized()
	sys.exit( app.exec_() )