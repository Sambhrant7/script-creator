# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
import os, importlib, shutil, subprocess, json

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

try:
	_encoding = QtGui.QApplication.UnicodeUTF8
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig)

try:
	shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
	os.remove(getPath + "/Input/res/source.zip")
except:
	pass

################ UTIL FUNCTIONS ################
def parse_logsData(urls,params,data):
	logs_url_list=[]
	logs_call_list=[]
	cursor=0
	for url in urls:
		url = url.replace("https://","http://")
		################## Adjust Calling Data Extraction ########################
		if "adjust" in url:
			logs_url_list.append(url)
			if "event" in url:
				if data[cursor].get("callback_params") and data[cursor].get("partner_params"):
					value="'cp':"+data[cursor].get("callback_params") + ", 'pp':"+data[cursor].get("partner_params")
				elif data[cursor].get("callback_params"):
					value="'cp':"+data[cursor].get("callback_params")
				elif data[cursor].get("partner_params"):
					value="'pp':"+data[cursor].get("partner_params")
				else:
					value=""

				logs_call_list.append("Adjust Event Name-> "+data[cursor].get("event_token")+", Event Value={"+value+"}")
			elif "sdk_click" in url:
				logs_call_list.append("Adjust sdk_click-> "+data[cursor].get("source"))
			else:
				logs_call_list.append("Adjust "+url.split("app.adjust.com/")[1])

		################## Appsflyer Calling Data Extraction ########################
		elif "appsflyer.com" in url:
			logs_url_list.append(url)
			if "events.appsflyer.com" in url:
				try:
		 			logs_call_list.append("Appsflyer Event Name-> "+data[cursor].get("eventName")+", Event Value="+data[cursor].get("eventValue"))
		 		except:
		 			print "exception with appsflyer event "
			elif "api.appsflyer.com" in url:
				logs_call_list.append("Appsflyer API call")
			elif "register.appsflyer.com" in url:
				logs_call_list.append("Appsflyer register call")
			elif "attr.appsflyer.com" in url:
				logs_call_list.append("Appsflyer attr call")
			elif "sdk-services.appsflyer" in url:
				logs_call_list.append("Appsflyer SDK services call")
			elif "validate.appsflyer.com" in url:
				logs_call_list.append("Appsflyer Validate call")		
			elif "t.appsflyer.com" in url:
				logs_call_list.append("Appsflyer track call")
			elif "stats.appsflyer.com" in url:
				logs_call_list.append("Appsflyer stats call")
			else:
				logs_call_list.append("Unknown Appsflyer call, URL:- "+url)

		################## Apsalar Calling Data Extraction ########################
		elif "apsalar.com" in url:
			logs_url_list.append(url)
			if "event" in url:
				ev = "Event Value="+params[cursor].get("e")
				if params[cursor].get("custom_user_id"):
					ev+=", Custom User ID="+str(params[cursor].get("custom_user_id"))
				logs_call_list.append("Apsalar Event Name-> "+params[cursor].get("n")+", "+ev)
			elif "resolve" in url:
				logs_call_list.append("Apsalar Resolve, IFU Value="+params[cursor].get("k"))
			else:
				logs_call_list.append("Apsalar "+url.rsplit("/",1)[1])

		################## Kochava Calling Data Extraction ########################
		elif "kochava.com" in url:
			if "control.kochava.com/track/kvTracker.php" in url:
				logs_url_list.append(url)
				if not isinstance(data[cursor],list):
					if data[cursor].get("action")=="event":
						logs_call_list.append("Kochava Event Name-> "+data[cursor].get("action")+", Event Value="+data[cursor].get("action"))
					elif data[cursor].get("action")=="session":
						logs_call_list.append("Kochava "+data[cursor].get("action")+", State="+data[cursor].get("data").get("state"))
					else:	
						logs_call_list.append("Kochava "+data[cursor].get("action"))
				else:
					for x in data[cursor]:
						logs_url_list.append(url)
						if x.get("action")=="event":
							logs_call_list.append("Kochava Event Name-> "+x.get("action"))
						elif data[cursor].get("action")=="session":
							logs_call_list.append("Kochava "+data[cursor].get("action")+", State="+data[cursor].get("data").get("state"))
						else:
							logs_call_list.append("Kochava "+x.get("action"))
			elif "control.kochava.com/track/json" in url:
				if not isinstance(data[cursor],list):
					logs_url_list.append(url)
					if data[cursor].get("action")=="event":
						logs_call_list.append("Kochava Event Name-> "+data[cursor].get('data').get('event_name')+", Kochava Event Value="+data[cursor].get('data').get('event_data'))
					elif data[cursor].get("action")=="session":
						logs_call_list.append("Kochava "+data[cursor].get("action")+", State="+data[cursor].get("data").get("state"))
					else:	
						logs_call_list.append("Kochava "+data[cursor].get("action"))
				else:
					for x in data[cursor]:
						logs_url_list.append(url)
						if x.get("action")=="event":
							logs_call_list.append("Kochava Event Name-> "+x.get("action")+", Kochava Event Value="+x.get("data").get("event_data"))
						elif data[cursor].get("action")=="session":
							logs_call_list.append("Kochava "+data[cursor].get("action")+", State="+data[cursor].get("data").get("state"))
						else:
							logs_call_list.append("Kochava "+x.get("action"))
			else:
				logs_url_list.append(url)
				logs_call_list.append("Kochava "+url.rsplit("/",1)[1])

		################## Adforce Calling Data Extraction ########################
		elif "app-adforce.jp" in url:
			logs_url_list.append(url)
			if "app-adforce.jp/ad/p/cv" in url:
				if params[cursor].get("_buid") or params[cursor].get("_price"):
					logs_call_list.append("Ad-force CV Short")
				else:
					logs_call_list.append("Ad-force CV Normal")		

			else:
				logs_call_list.append("Ad-force:- "+url.rsplit("/",1)[1])


		################## Tenjin Calling Data Extraction ########################
		elif "tenjin.io" in url:
			logs_url_list.append(url)
			if "user" in url:
				logs_call_list.append("Tenjin:- "+url.rsplit("/",1)[1]+" call")
			elif data[cursor].get("event"):
				logs_call_list.append("Tenjin Event Name-> "+data[cursor].get("event"))
			else:
				logs_call_list.append("Tenjin tracking unnamed event")


		################## MAT Calling Data Extraction ########################
		elif "mobileapptracking.com" in url:
			logs_url_list.append(url)
			if params[cursor].get('action')=='conversion':
				logs_call_list.append('MAT Event Name-> '+params[cursor].get('site_event_name'))
			elif params[cursor].get('action')=='install':
				logs_call_list.append('MAT:-install')
			elif params[cursor].get('action')=='deeplink':
				logs_call_list.append('MAT:-deeplink')
			else:
				logs_call_list.append('MAT:-session')

		cursor+=1
	if len(logs_call_list)==len(logs_url_list):
		return logs_call_list, logs_url_list
	else:
		raise Exception("SYNC ERROR WHILE PARSING LOGS")

#Multiple logs data Merged
def mergeLogs(callsFromLogs, urls_from_logs):
	if callsFromLogs.get('install1') and callsFromLogs.get('install2'):
		for k in range(len(callsFromLogs.get("install2"))):
			if not callsFromLogs.get("install2")[k] in callsFromLogs.get("install1"):
				if k==0:
					callsFromLogs.get("install1").append(callsFromLogs.get("install2")[k])
					urls_from_logs.get("install1").append(urls_from_logs.get("install2")[k])
				else:
					missing = callsFromLogs.get("install2")[k]
					missingURL = urls_from_logs.get("install2")[k]
					prev = callsFromLogs.get("install2")[callsFromLogs.get("install2").index(callsFromLogs.get("install2")[k])-1]
					callsFromLogs.get("install1").insert(callsFromLogs.get("install1").index(prev)+1, missing)
					urls_from_logs.get("install1").insert(callsFromLogs.get("install1").index(prev)+1, missingURL)

	if callsFromLogs.get('open1') and callsFromLogs.get('open2'):
		for k in range(len(callsFromLogs.get("open2"))):
			if not callsFromLogs.get("open2")[k] in callsFromLogs.get("open1"):
				if k==0:
					callsFromLogs.get("open1").append(callsFromLogs.get("open2")[k])
					urls_from_logs.get("open1").append(urls_from_logs.get("open2")[k])
				else:
					missing = callsFromLogs.get("open2")[k]
					missingURL = urls_from_logs.get("open2")[k]
					prev = callsFromLogs.get("open2")[callsFromLogs.get("open2").index(callsFromLogs.get("open2")[k])-1]
					callsFromLogs.get("open1").insert(callsFromLogs.get("open1").index(prev)+1, missing)
					urls_from_logs.get("open1").insert(callsFromLogs.get("open1").index(prev)+1, missingURL)


#Script and Logs data to be merged
def mergeLogswithScript(callsFromLogs,urls_from_logs,callsfromscript,urlsfromscript):
	for k in range(len(callsFromLogs)):
		if not urls_from_logs[k] in urlsfromscript:
			if k==0:
				callsfromscript.append(callsFromLogs[k])
				urlsfromscript.append(urls_from_logs[k])
			else:
				missing = callsFromLogs[k]
				missingURL = urls_from_logs[k]
				prev = callsFromLogs[callsFromLogs.index(callsFromLogs[k])-1]
				if "Event Name->" in prev:
					prevtoken=prev.split("Event Name->")[1]
					if "," in prevtoken:
						prevtoken = prevtoken.split(",")[0].strip()
				else:
					prevtoken=prev
				for seq in range(len(callsfromscript)):
					if prevtoken in callsfromscript[seq]:
						crsr=seq+1
						break	
				callsfromscript.insert(crsr, missing)
				urlsfromscript.insert(crsr, missingURL)\
				
	return callsfromscript, urlsfromscript

################################ UI CLASS ################################
#UI for the logs and Script to be load
class Ui_dialog(object):
	def __init__(self):
		self.logsList={}

	def setupUi(self, dialog):
		self.ui = Dialog
		self.logs_name = False
		self.temp_name = False
		self.script_name = False

		dialog.setObjectName(_fromUtf8("dialog"))
		dialog.resize(662, 265)
		font = QtGui.QFont()
		font.setPointSize(10)
		font = QtGui.QFont()
		font.setPointSize(10)
		self.label_2 = QtGui.QLabel(dialog)
		self.label_2.setGeometry(QtCore.QRect(10, 10, 2000, 30))
		self.label_2.setObjectName(_fromUtf8("label_2"))
		self.label_3 = QtGui.QLabel(dialog)
		self.label_3.setGeometry(QtCore.QRect(10, 40, 121, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.label_3.setFont(font)
		self.label_3.setObjectName(_fromUtf8("label_3"))
		self.label_2.setFont(font)
		self.label_2.setObjectName(_fromUtf8("label_3"))
		self.pushButton = QtGui.QPushButton(dialog)
		self.pushButton.clicked.connect(self.file_explorer)
		self.pushButton.clicked.connect(self.load_logs_but_ins1)
		self.pushButton.setGeometry(QtCore.QRect(149, 44, 251, 21))
		self.pushButton.setObjectName(_fromUtf8("pushButton"))

		self.pushButton_2 = QtGui.QPushButton(dialog)
		self.pushButton_2.setGeometry(QtCore.QRect(150, 72, 251, 21))
		self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
		self.pushButton_2.clicked.connect(self.file_explorer)
		self.pushButton_2.clicked.connect(self.load_logs_but_ins2)

		self.pushButton_3 = QtGui.QPushButton(dialog)
		self.pushButton_3.setGeometry(QtCore.QRect(150, 100, 251, 21))
		self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
		self.pushButton_3.clicked.connect(self.file_explorer)
		self.pushButton_3.clicked.connect(self.load_logs_but_open1)

		self.pushButton_4 = QtGui.QPushButton(dialog)
		self.pushButton_4.setGeometry(QtCore.QRect(150, 130, 251, 21))
		self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
		self.pushButton_4.clicked.connect(self.file_explorer)
		self.pushButton_4.clicked.connect(self.load_logs_but_open2)

		self.label_4 = QtGui.QLabel(dialog)
		self.label_4.setGeometry(QtCore.QRect(10, 165, 121, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.label_4.setFont(font)
		self.label_4.setObjectName(_fromUtf8("label_4"))
		self.loadScript = QtGui.QPushButton(dialog)
		self.loadScript.setGeometry(QtCore.QRect(150, 170, 251, 21))
		self.loadScript.setObjectName(_fromUtf8("loadScript"))
		self.loadScript.clicked.connect(self.file_explorer)
		self.loadScript.clicked.connect(self.load_script_but)
		self.confirmButton = QtGui.QPushButton(dialog)
		self.confirmButton.setGeometry(QtCore.QRect(24, 212, 621, 31))
		self.confirmButton.setObjectName(_fromUtf8("confirmButton"))
		self.confirmButton.clicked.connect(lambda: self.proceed(dialog))

		self.retranslateUi(dialog)
		dialog.setStyleSheet("background-color: #bcd1d0")
		QtCore.QMetaObject.connectSlotsByName(dialog)

	#Opens File Explorer
	def file_explorer(self):
		self.temp_name = QtGui.QFileDialog.getOpenFileName(self.ui)

	def load_script_but(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		self.script_name = self.temp_name
		
		if not str(self.script_name) or str(self.script_name)=="":
			self.loadScript.setStyleSheet("background-color: red")
			QtGui.QMessageBox.information(self.ui, "ERROR",
										  'Script required. Please load file before proceeding.')
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])

		elif str(self.script_name)[-3:] != ".py":
			self.loadScript.setStyleSheet("background-color: red")
			QtGui.QMessageBox.information(self.ui, "ERROR",
										  'Invalid python file. File extension must be ".py".')
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])

		self.loadScript.setStyleSheet("background-color: green")
		print "Location for your Script-->" + str(self.script_name)
		self.label_2.setText(_translate("dialog", "Status : Script Loaded", None))

	def load_logs_but_ins1(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		if self.temp_name:
			self.pushButton.setStyleSheet("background-color: green")
			self.logsList["install1"] = self.temp_name
			if str(self.logsList["install1"])[-3:] != "saz":
				QtGui.QMessageBox.information(self.ui, "ERROR",
											  'Invalid logs file. File extension must be ".saz". Only fiddler logs acceptable.')
				os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			print "Location for your install detailed(1) logs-->" + str(self.logsList["install1"])
			self.label_2.setText(_translate("dialog", "Status : install detailed (1) logs loaded.", None))

	def load_logs_but_ins2(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		if self.temp_name:
			self.pushButton_2.setStyleSheet("background-color: green")
			self.logsList["install2"] = self.temp_name
			if str(self.logsList["install2"])[-3:] != "saz":
				QtGui.QMessageBox.information(self.ui, "ERROR",
											  'Invalid logs file. File extension must be ".saz". Only fiddler logs acceptable.')
				os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			print "Location for your install detailed(2) logs-->" + str(self.logsList["install2"])
			self.label_2.setText(_translate("dialog", "Status : Install detailed second device (2) logs loaded.", None))

	def load_logs_but_open1(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		if self.temp_name:
			self.pushButton_3.setStyleSheet("background-color: green")
			self.logsList["open1"] = self.temp_name
			if str(self.logsList["open1"])[-3:] != "saz":
				QtGui.QMessageBox.information(self.ui, "ERROR",
											  'Invalid logs file. File extension must be ".saz". Only fiddler logs acceptable.')
				os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			print "Location for your open complete(1) logs-->" + str(self.logsList["open1"])
			self.label_2.setText(_translate("dialog", "Status : open complete logs loaded.", None))

	def load_logs_but_open2(self):
		"""
		Open a File dialog when the button is pressed
		:return:
		"""
		if self.temp_name:
			self.pushButton_4.setStyleSheet("background-color: green")
			self.logsList["open2"] = self.temp_name
			if str(self.logsList["open2"])[-3:] != "saz":
				QtGui.QMessageBox.information(self.ui, "ERROR",
											  'Invalid logs file. File extension must be ".saz". Only fiddler logs acceptable.')
				os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			print "Location for your open complete(2) logs-->" + str(self.logsList["open2"])
			self.label_2.setText(_translate("dialog", "Status : open complete other device(2) logs loaded.", None))

	def retranslateUi(self, dialog):
		dialog.setWindowTitle(_translate("dialog", "Script Creator", None))
		self.label_2.setText(_translate("dialog", "Status :", None))
		self.label_3.setText(_translate("dialog", "Select your Logs File-", None))
		self.pushButton.setText(_translate("dialog", "Load Install Logs 1", None))
		self.pushButton_2.setText(_translate("dialog", "Load Install Logs 2", None))
		self.pushButton_3.setText(_translate("dialog", "Load Open Logs 1", None))
		self.pushButton_4.setText(_translate("dialog", "Load Open Logs 2", None))
		self.label_4.setText(_translate("dialog", "Select your Script-", None))
		self.loadScript.setText(_translate("dialog", "Load Script", None))
		self.confirmButton.setText(_translate("dialog", "Next", None))

	def proceed(self, dialog):
		########################### VALIDATE ##########################
		if not self.script_name:
			QtGui.QMessageBox.information(self.ui, "ERROR",
										  'Select a script first.')
			os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
			self.label_2.setText(_translate("dialog", "Status : No script selected.", None))
		else:
			self.label_2.setText(_translate("dialog", "Status : PROCESSING...", None))
			fileName = str(self.script_name).rsplit("/",1)[1]
			getPath = os.getcwd()
			runner_files = os.listdir(getPath + "\\open_complete\\runner")
			try:
				shutil.rmtree(getPath + "/Input/res/install1/extracted", ignore_errors=True)#
				shutil.rmtree(getPath + "/Input/res/install2/extracted", ignore_errors=True)#
				shutil.rmtree(getPath + "/Input/res/open1/extracted", ignore_errors=True)#
				shutil.rmtree(getPath + "/Input/res/open2/extracted", ignore_errors=True)#
				shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
				os.remove(getPath + "/Input/res/install1/source.zip")
				os.remove(getPath + "/Input/res/install2/source.zip")
				os.remove(getPath + "/Input/res/open1/source.zip")
				os.remove(getPath + "/Input/res/open2/source.zip")
				os.remove(getPath + "/Input/res/source.zip")
			except:
				pass
			for file in runner_files:
				if not file=="__init__.py" and not file=="clicker.py" and not file=="Config.py" and not file=="sdk" and not "run.py":
					os.remove(getPath + "\\open_complete\\runner\\"+file)			
			shutil.copy2(str(self.script_name).replace("/","\\"), getPath + "\\open_complete\\runner")

		######################### RUN SCRIPT ##########################
		global call_list,url_list
		call_list,url_list = False, False
		from runner.run import main
		from runner.sdk.util import call_list,url_list
		script = importlib.import_module("."+fileName.split(".py")[0], "runner")
		if str(self.script_name)[-6:]=="ios.py":
			platform="ios"
		else:
			platform="Android"
		campaign_data = main(script,platform)


		######################## GET LOGS DATA ########################
		callsFromLogs={}
		urlsFromLogs={}
		
		import open_filter
		for log in self.logsList.keys():
			logsD = open_filter.filter_logs_data(self.logsList.get(log), platform=platform, logs_details=log)
			parsed_LogsD = parse_logsData(urls=logsD[0].get('url'),params=logsD[0].get('params') ,data=logsD[0].get('data'))
			if not callsFromLogs.get(log):
				callsFromLogs[log]=[]
			if not urlsFromLogs.get(log):
				urlsFromLogs[log]=[]
			for c in range(len(parsed_LogsD[0])):
				if not parsed_LogsD[0][c] in call_list:
					callsFromLogs[log].append(parsed_LogsD[0][c])
					urlsFromLogs[log].append(parsed_LogsD[1][c])

		mergeLogs(callsFromLogs, urlsFromLogs)
		m= mergeLogswithScript(callsFromLogs.get("install1"),urlsFromLogs.get("install1"),call_list,url_list)
		call_list=m[0]
		url_list=m[1]
		################ MERGE WITH INSTALL AND OPEN ###################
		if callsFromLogs.get("install1") and callsFromLogs.get("open1"):
			call_list2=callsFromLogs.get("install1")+callsFromLogs.get("open1")
			url_list2=urlsFromLogs.get("install1")+urlsFromLogs.get("open1")
		else:
			call_list2=callsFromLogs.get("install1")
			url_list2=urlsFromLogs.get("install1")

		################### PASS DATA TO DASHBOARD #####################
		dialog.close()
		import dashboard
		dashboard.main(call_list,url_list,call_list2,url_list2,fileName)


if __name__ == "__main__":
	import sys
	app = QtGui.QApplication(sys.argv)
	Dialog = QtGui.QDialog()
	ui = Ui_dialog()
	ui.setupUi(Dialog)
	Dialog.setWindowIcon(QtGui.QIcon("reference/logo.png"))
	Dialog.setWindowTitle("Script Creator Tool  -  AppAnalytics")
	Dialog.show()
	sys.exit(app.exec_())