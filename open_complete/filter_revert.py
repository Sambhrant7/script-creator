# -*- coding: utf-8 -*-
import re, json

#calling pattern for Event Definition
callPattern_sample_event_callingDictionary={
	'appsflyer': "	print 'Appsflyer : EVENT___________________________'+'" + "EVENTNAME" + "\n	eventName			= '" + "EVENTNAME" + """\n	eventValue= """ + "EVENTVALUE" + """\n	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)\n	util.execute_request(**request)\n\n\n""",

	'adjust': "	print 'Adjust : EVENT______________________________'+" + "EVENTNAME" + "\n	request=adjust_event(campaign_data, app_data, device_data,callback_params=" + "json.dumps(EVENTVALUE.get('cp'))" + ",partner_params=" + "json.dumps(EVENTVALUE.get('pp'))" + ",event_token=" + "EVENTNAME" + ",t1=t1,t2=t2)\n	util.execute_request(**request)\n\n",

	'apsalar': "	print 'Apsalr : EVENT______________________________'+" + "EVENTNAME" + "\n	request=apsalar_event(campaign_data, app_data, device_data,event_name='" + "EVENTNAME" + "',value=" + "EVENTVALUE" + ",custom_user_id=custom_user_id)\n	util.execute_request(**request)\n\n",

	'kochava': """	print """ + repr(
				"\n") + """+"-----------------------------KOCHAVA EVENT----+"""+"EVENTNAME"+"""+---------------------------------"\n	request=kochava_event(campaign_data, app_data, device_data, eventName='"""+"EVENTNAME"+"""', event_data="""+"EVENTVALUE"+""")\n	util.execute_request(**request)\n\n""",
	'tenjin': """	print """ + repr(
					"\n") + """+"------------------------ Tenjin Event -------------------------"\n	request=tenjin_event( campaign_data, device_data, app_data, eventName='"""+"EVENTNAME"+""" )\n	util.execute_request(**request)\n\n"""
}

calling_data_dictionary = {

		#####################			  APPSFLYER		 CALLINGS 			####################

		'http://attr.appsflyer.com/api/v4/androidevent': """	request = appsflyer_attr(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://t.appsflyer.com/api/v4/androidevent->first': """	print """ + repr(
			"\n") + """+'Appsflyer : Track____________________________________'\n	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)\n	util.execute_request(**request)\n\n""",

		'http://t.appsflyer.com/api/v4/androidevent': """	print """ + repr(
			"\n") + """+'Appsflyer : Track____________________________________'\n	request  = appsflyer_track(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://events.appsflyer.com/api/v4/androidevent': '',

		'http://stats.appsflyer.com/stats': """	print """ + repr(
			"\n") + """+'Appsflyer : Stats____________________________________'\n	request  = appsflyer_stats(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://api.appsflyer.com/install_data/': """	print """ + repr(
			"\n") + """+'Appsflyer : API____________________________________'\n	request = appsflyer_api(campaign_data, app_data, device_data)\n	response=util.execute_request(**request)\n	catch(response,app_data,"appsflyer")\n\n""",

		'http://register.appsflyer.com/api/v4/androidevent': """	print """ + repr(
			"\n") + """+'Appsflyer : Register____________________________________'\n	request = appsflyer_register(campaign_data,app_data,device_data)\n	util.execute_request(**request)\n\n""",

		#####################			  ADJUST		 CALLINGS 			####################

		'http://app.adjust.com/session': """	print """ + repr(
			"\n") + """+'Adjust : SESSION____________________________________'\n	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/sdk_info': """	print """ + repr(
			"\n") + """+'Adjust : SDK INFO____________________________________'\n	request=adjust_sdkinfo(campaign_data, app_data, device_data,t1=in1,t2=in2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/sdk_click->reftag': """	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/sdk_click->install_referrer': """	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/sdk_click->iad3': """	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_iad3,source='iad3',t1=id1,t2=id2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/sdk_click->deeplink': """	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_deeplink,source='deeplink',t1=dp1,t2=dp2)\n	util.execute_request(**request)\n\n""",

		'http://app.adjust.com/attribution': """	print """ + repr(
			"\n") + """+'Adjust : ATTRIBUTION____________________________________'\n	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)\n	util.execute_request(**request)\n\n""",

		#####################			  ADBRIX		 CALLINGS 			####################

		'http://tracking.ad-brix.com/v1/tracking->first': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking Start '\n	ad_brix_tracking_1 = ad_brix_tracking_start(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])\n	util.execute_request(**ad_brix_tracking_1)\n\n""",

		'http://cvr.ad-brix.com/v1/conversion/GetReferral': """		\n	print """ + repr(
			"\n") + """+'AD BRIX get Referral'\n	get_referral = ad_brix_get_referral(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])\n	get_referral_result =util.execute_request(**get_referral)\n	try:\n		get_referral_result_decode = json.loads(get_referral_result.get('data'))\n		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))\n		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))\n	except:\n		app_data['user_number'] = ""\n		app_data['shard_number'] = ""\n\n""",

		'http://as.ad-brix.com/v1/users/login': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Get login'\n	ad_brix_login= ad_brix_user_login(campaign_data, app_data, device_data)\n	response=util.execute_request(**ad_brix_login)\n	try:\n		get_data=json.loads(response.get('data'))\n		app_data['session_id']=get_data.get('user').get('sessionId')\n	except:\n		app_data['session_id']=	'4018cc614fc9c0f3'\n\n""",

		'http://as.ad-brix.com/v1/users/save': """		\n	print """ + repr(
			"\n") + """+'AD BRIX users_save'\n	users_save=ad_brix_users_save(campaign_data, app_data, device_data)\n	util.execute_request(**users_save)\n\n""",

		'http://as.ad-brix.com/v1/users/updateRegistration': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Update Registration'\n	ad_brix_conversion= ad_brix_update_registration(campaign_data, app_data, device_data)\n	util.execute_request(**ad_brix_conversion)\n\n""",

		'http://as.ad-brix.com/v1/users/updateConversion': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Update Conversion'\n	ad_brix_conversion= ad_brix_update_conversion(campaign_data, app_data, device_data)\n	util.execute_request(**ad_brix_conversion)\n\n""",

		'http://as.ad-brix.com/v1/users/getPopups': """		\n	print """ + repr(
			"\n") + """+'Ad Brix getPopups'\n	adpop=ad_brix_update_get_popups(campaign_data, app_data, device_data)\n	util.execute_request(**adpop) \n\n""",

		'http://as.ad-brix.com/v1/users/enablePushService': """		\n	print """ + repr(
			"\n") + """+'Adbrix--enable push services---'\n	req=enablepushservice(app_data,campaign_data,device_data)\n	util.execute_request(**req)\n\n""",

		'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Get Schedule'\n	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)\n	util.execute_request(**ad_brix_schedule)\n\n""",

		'http://tracking.ad-brix.com/v1/tracking/SetUserDemographic': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking'\n	adtg=ad_brix_tracking_SetUserDemographic(app_data, device_data)\n	util.execute_request(**adtg)\n\n""",

		'http://apiab4c.ad-brix.com/v1/event': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking'\n	adbrix_prchse=adbrix_purchase(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],choice)\n	util.execute_request(**adbrix_prchse)\n\n""",

		'http://event.adbrix.io/api/v1/deferred-deeplink/': """		\n	print """+repr("\n")+"""+'AD BRIX deferred_deeplink'\n	deferred_deeplink = adbrix_deferred_deeplink(campaign_data, app_data, device_data)\n	util.execute_request(**deferred_deeplink)\n\n""",

		#####################			  MAT		 CALLINGS		  ####################

		'engine.mobileapptracking.com/serve' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------MAT----session---------------------------------"\n	call.get('call') = mat_serve(campaign_data, app_data, device_data,action="session")\n	response=util.execute_request(**call.get('call'))\n	catch(response,app_data,"mat")\n	set_matOpenLogID(app_data)\n\n""",

		###############################      Apsalar Calling  ############################

		'http://e-ssl.apsalar.com/api/v1/resolve': """		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	call.get('call') = apsalarResolve(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://e.apsalar.com/api/v1/resolve':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	call.get('call') = apsalarResolve(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://e-ssl.apsalar.com/api/v1/start': """		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Start---------------------------------"\n	call.get('call') = apsalarStart(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://e.apsalar.com/api/v1/start':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Start---------------------------------"\n	call.get('call') = apsalarStart(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",


		###############################  KOCHAVA CALLINGS  ################################

		'http://control.kochava.com/track/kvinit' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvinit---------------------------------"\n	request=Kvinit(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://control.kochava.com/track/kvTracker.php->initial' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava initial---------------------------------"\n	request=android_KvTracker_initial(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://control.kochava.com/track/kvTracker.php->update' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava update---------------------------------"\n	request=kochava_tracker_update( campaign_data, device_data, app_data )\n	util.execute_request(**request)\n\n""",

		'http://control.kochava.com/track/kvquery' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvquery---------------------------------"\n	request=kv_Query(campaign_data,app_data,device_data)\n	util.execute_request(**request)\n\n""",

		'http://kvinit-prod.api.kochava.com/track/kvinit' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvinit---------------------------------"\n	Kv_init=kochavaKvinit(campaign_data, app_data, device_data)\n	util.execute_request(**Kv_init)\n\n""",		

		'http://control.kochava.com/track/json->initial' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava initial---------------------------------"\n	request=kochava_initial(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		'http://control.kochava.com/track/json->identityLink' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------kochava identityLink---------------------------------"\n	request=kochava_identityLink(campaign_data, app_data, device_data)\n	util.execute_request(**request)\n\n""",

		############################## Adforce Calling ######################################

		'http://app-adforce.jp/ad/p/tmck':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------Ad-Force tmck---------------------------------"\n	call.get('call') = adforce_tmck(campaign_data, app_data, device_data)\n	response=util.execute_request(**call.get('call'))\n	try:\n		app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]\n	except:\n		app_data['uid']='foxGC0lu1gEW6E'\n\n""",

		'http://app-adforce.jp/ad/view/collect.html':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- Ad-Force collect ------------------------------"\n	call.get('call') = adforce_collect(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://analytics.app-adforce.jp/fax/analytics':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce Analytics ------------------------------"\n	call.get('call') = adforce_analytics(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://app-adforce.jp/ad/i/fprc':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce fprc ------------------------------"\n	call.get('call') = adforce_fprc(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://app-adforce.jp/ad/p/fp':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce fp ------------------------------"\n	call.get('call') = adforce_fp(campaign_data, app_data, device_data)\n	response=util.execute_request(**call.get('call'))\n	global fp_ID, fp_TDL\n	try:\n		uid=json.loads(result.get('data'))\n		fp_ID = uid.get('id')\n		fp_TDL = uid.get('tdl')\n	except:\n		fp_ID = '6%2E8%2E2%3AA314A6E4FB54ADB3FCDB834CAE7B4E044CA858E7%7Ctid%3AtGH3CB1gClM5'\n		fp_TDL = '-1206245842'\n\n""",

		'http://app-adforce.jp/ad/p/cv->cv':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce cv ------------------------------"\n	call.get('call') = adforce_cv(campaign_data, app_data, device_data)\n	response=util.execute_request(**call.get('call'))\n	try:\n		app_data['cookie_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]\n	except:\n		app_data['cookie_uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'\n\n""",

		'http://app-adforce.jp/ad/p/cv->short':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce cv short ------------------------------"\n	call.get('call') = adforce_cv_short(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		'http://app-adforce.jp/ad/p/jump':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce p jump  ------------------------------"\n	call.get('call') = adforce_p_jump(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

		########################### tenjin calling ###################################

		'http://track.tenjin.io/v0/user':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- tenjin user  ------------------------------"\n	call.get('call') = tenjin_user(campaign_data, app_data, device_data)\n	util.execute_request(**call.get('call'))\n\n""",

	}

def	setupEvents(callList):
	event_counter=0
	event_list=[]
	eventData_List = []
	for call in callList:

		if not call.get("INSTALL") and not call.get("OPEN"):
			continue

		######################## APPSFLYER EVENT NAME DEFINITION #####################

		if "http://events.appsflyer.com/api" in call.get('call'):
			event_counter += 1
			e=call.get('call').split("Event Name->")[1]
			eventName=e.strip()
			if "," in e:
				eventName=e.split(",")[0].strip()
			eventValue=call.get('call').split("Event Value=")[1].rsplit("}",1)[0]+"}".strip()
			calling_data_dictionary['http://events.appsflyer.com/api/v4/androidevent->'+ eventName] = "	" + re.sub(r'\W+', '',
																						eventName).lower() + "(campaign_data, app_data, device_data)\n\n"
			event_list.append(eventName)
			eventData_List.append(eventValue)

		######################### ADJUST EVENT NAME DEFINITION #######################

		if 'http://app.adjust.com/event' in call.get('call'):
			event_counter += 1
			e=call.get('call').split("Event Name->")[1]
			eventToken=e.strip()
			if "," in e:
				eventToken=e.split(",")[0].strip()
			eventValue=eval(call.get('call').split("Event Value=")[1].rsplit("[",1)[0].strip())
			calling_data_dictionary[
				'http://app.adjust.com/event->' + eventToken] = "	adjust_token_" + eventToken + "(campaign_data, app_data, device_data,e1,e2)\n\n"
			event_list.append('adjust-->' + str(eventToken))
			if eventValue.get('cp') and eventValue.get('pp'):
				eventData_List.append([eventValue.get('cp'),
									   eventValue.get('pp')])
			elif eventValue.get('cp'):
				eventData_List.append([eventValue.get('cp'), None])
			elif eventValue.get('pp'):
				eventData_List.append([None, eventValue.get('pp')])
			else:
				eventData_List.append(None)

		######################### MAT EVENT NAME DEFINITION #######################
		# if "engine.mobileapptracking.com/serve" in call.get('call') and "Event Name->" in call.get('call'):
		# 	event_counter += 1
		# 	conversion_name = eventValue.get('conversion_name')
		# 	calling_data_dictionary[
		# 		'engine.mobileapptracking.com/serve'+'->' + conversion_name] = """		\n	print """ + repr(
		# "\n") + """+"-----------------------------MAT----"""+conversion_name+"""---------------------------------"\n	call.get('call') = mat_serve(campaign_data, app_data, device_data,action="conversion:"""+conversion_name+"""")\n	response = util.execute_request(**call.get('call'))\n"""

		######################## Apsalar Event Name Definition #####################

		if 'e-ssl.apsalar.com' in call.get('call'):
			if 'event' in call.get('call'):
				event_counter += 1
				if "Custom User ID=" in call.get('call'):
					custom_user_id =", custom_user_id=True)"
				else:
					custom_user_id =")"
				e=call.get('call').split("Event Name->")[1]
				eventName=e.strip()
				if "," in e:
					eventName=e.split(",")[0].strip()
				calling_data_dictionary[
					'e-ssl.apsalar.com/api/v1/event->' + eventName] = "	apsalar_event_" + eventName + "(campaign_data, app_data, device_data"+custom_user_id+"\n\n"
				event_list.append('apsalar-->' + str(eventName))
				if "Event Value=" in call.get('call'):
 					eventValue=call.get('call').split("Event Value=")[1].split("[")[0].strip()
					eventData_List.append(eventValue)

			elif "resolve" in call.get('call'):
				ifu=""
				if "IFU Value=" in call.get('call'):
					ifu=call.get('call').split("IFU Value=")[1].strip()
				calling_data_dictionary['e-ssl.apsalar.com/api/v1/resolve->'+ifu] = """	print """ + repr(
		"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	call.get('call') = apsalarResolve(campaign_data, app_data, device_data, ifu='"""+ifu+"""')\n	util.execute_request(**call.get('call'))\n\n""" 

		if 'e.apsalar.com' in call.get('call'):
			if 'event' in call.get('call'):
				event_counter += 1
				if "Custom User ID=" in call.get('call'):
					custom_user_id =", custom_user_id=True)"
				else:
					custom_user_id =")"
				e=call.get('call').split("Event Name->")[1]
				eventName=e.strip()
				if "," in e:
					eventName=e.split(",")[0].strip()
				calling_data_dictionary[
					'e.apsalar.com/api/v1/event->' + eventName] = "	apsalar_event_" + eventName + "(campaign_data, app_data, device_data"+custom_user_id+"\n\n"
				event_list.append('apsalar-->' + str(eventName))
				if "Event Value=" in call.get('call'):
 					eventValue=call.get('call').split("Event Value=")[1].split("[")[0].strip()
					eventData_List.append(eventValue)

			elif "resolve" in call.get('call'):
				ifu=""
				if "IFU Value=" in call.get('call'):
					ifu=call.get('call').split("IFU Value=")[1].strip()
				calling_data_dictionary['e.apsalar.com/api/v1/resolve->'+ifu] = """	print """ + repr(
		"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	call.get('call') = apsalarResolve(campaign_data, app_data, device_data, ifu='"""+ifu+"""')\n	util.execute_request(**call.get('call'))\n\n"""

		######################### KOCHAVA EVENT NAME DEFINITION #######################
		if "control.kochava.com" in call.get('call'):
			if "control.kochava.com/track/json" in call.get('call'):
				if "Event Name->" in call.get('call'):
					event_counter += 1
					e=call.get('call').split("Event Name->")[1]
					eventName=e.strip()
					if "," in e:
						eventName=e.split(",")[0].strip()
					eventValue=call.get('call').split("Event Value=")[1].split("[")[0].strip()
					calling_data_dictionary[
						'control.kochava.com/track/json->' + eventName] = """	print """ + repr(
				"\n") + """+"-----------------------------KOCHAVA EVENT----"""+eventName.strip()+"""---------------------------------"\n	request=kochava_event(campaign_data, app_data, device_data, eventName='"""+eventName+"""', event_data="""+str(eventValue)+""")\n	util.execute_request(**request)\n\n"""

				elif "State=" in call.get('call'):
					state = call.get('call').split("State=")[1].strip()
					calling_data_dictionary[
						'control.kochava.com/track/json->' + state] = """	print """ + repr(
				"\n") + """+"-----------------------------KOCHAVA session----"""+state+"""---------------------------------"\n	request=kochava_session(campaign_data, app_data, device_data, state='"""+state+"""')\n	util.execute_request(**request)\n\n"""

			elif "control.kochava.com/track/kvTracker.php" in call.get('call'):
				if "Event Name->" in call.get('call'):
					event_counter += 1
					e=call.get('call').split("Event Name->")[1]
					eventName=e.strip()
					if "," in e:
						eventName=e.split(",")[0].strip()
					eventValue=call.get('call').split("Event Value=")[1].split("[")[0].strip()
					calling_data_dictionary[
						'control.kochava.com/track/kvTracker.php->' + eventName] = """	print """ + repr(
				"\n") + """+"-----------------------------KOCHAVA EVENT----"""+eventName.strip()+"""---------------------------------"\n	request=kochava_tracker_event( campaign_data, device_data, app_data, eventName='"""+eventName+"""', eventValue="""+str(eventValue)+""" )\n	util.execute_request(**request)\n\n"""
					
				elif "State=" in call.get('call'):
					state = call.get('call').split("State=")[1].strip()
					calling_data_dictionary[
						'control.kochava.com/track/kvTracker.php->' + state] = """	request=kochava_tracker_session( campaign_data, device_data, app_data, state = '"""+state+"""' )\n	util.execute_request(**request)\n\n"""


		################### Tenjin Event Name Definition ################

		if 'track.tenjin.io' in call.get('call') and not "http://track.tenjin.io/v0/user" in call.get('call'):
			if "Event Name->" in call.get('call'):
				event_counter += 1
				e=call.get('call').split("Event Name->")[1]
				eventName=e.strip()
				if "," in e:
					eventName=e.split(",")[0].strip()	
				calling_data_dictionary['track.tenjin.io/v0/event->' + eventName] = """	print """ + repr(
					"\n") + """+"------------------------ Tenjin Event -------------------------"\n	request=tenjin_event( campaign_data, device_data, app_data, eventName='"""+eventName+""" )\n	util.execute_request(**request)\n\n"""
			else:
				calling_data_dictionary['track.tenjin.io/v0/event'] = """	print """ + repr(
					"\n") + """+"------------------------ Tenjin Event -------------------------"\n	request=tenjin_event( campaign_data, device_data, app_data)\n	util.execute_request(**request)\n\n"""


			filtered_logs_data.get('host').append('track.tenjin.io/v0/event')

		##################### MAT Event Name Definition ###################

		if 'mobileapptracking.com' in call.get('call'):
			if "Event Name->" in call.get('call'):
				event_counter += 1
				e=call.get('call').split("Event Name->")[1]
				eventName=e.strip()
				if "," in e:
					eventName=e.split(",")[0].strip()	
				calling_data_dictionary['engine.mobileapptracking.com/serve->' + eventName] = """	print """ + repr(
					"\n") + """+"------------------------ MAT Event -------------------------"\n	request=mat_serve( campaign_data, device_data, app_data, action=conversion:'"""+eventName+""" )\n	util.execute_request(**request)\n\n"""
			elif "MAT:- deeplink" in call.get('call'):
				calling_data_dictionary['deeplink.mobileapptracking.com/v1/link.txt'] = """	print """ + repr(
					"\n") + """+"------------------------ MAT deeplink -------------------------"\n	request=mat_serve( campaign_data, device_data, app_data, action="deeplink" )\n	util.execute_request(**request)\n\n"""

			elif "MAT:- install" in call.get('call'):
				calling_data_dictionary['engine.mobileapptracking.com/serve'] = """	print """ + repr(
					"\n") + """+"------------------------ MAT install -------------------------"\n	request=mat_serve( campaign_data, device_data, app_data, action="install" )\n	util.execute_request(**request)\n\n"""

			else:
				calling_data_dictionary['engine.mobileapptracking.com/serve'] = """	print """ + repr(
					"\n") + """+"------------------------ MAT install -------------------------"\n	request=mat_serve( campaign_data, device_data, app_data, action="session" )\n	util.execute_request(**request)\n	catch(response,app_data,"mat")\n	set_matOpenLogID(app_data)\n\n"""

	################ CREATING EVENT DEFINITIONS ##############
	events_definitions_list = []
	e = list(set(event_list))
	for event in range(len(event_list)):
		if event_list[event] in e:
			if 'adjust' in event_list[event]:
				cp = pp = 'None'
				if eventData_List[event]:
					if eventData_List[event][0]:
						cp = 'str(json.dumps(' + str(eventData_List[event][0]) + '))'
					if eventData_List[event][1]:
						pp = 'str(json.dumps(' + str(eventData_List[event][1]) + '))'

				token = event_list[event].split('adjust-->')[1]
				events_definitions_list.append(
					"def adjust_token_" + token + "(campaign_data, app_data, device_data,t1=0,t2=0):\n" + "	print 'Adjust : EVENT______________________________" + token + "'\n	request=adjust_event(campaign_data, app_data, device_data,callback_params=" + cp + ",partner_params=" + pp + ",event_token='" + token + "',t1=t1,t2=t2)\n	util.execute_request(**request)\n\n")
				e.remove(event_list[event])

			elif 'apsalar' in event_list[event]:
				if not eventData_List[event]:
					eventData_List[event]=json.dumps({})
				ev = 'urllib.quote_plus(json.dumps(' + eventData_List[event] + '))'

				eventName = event_list[event].split('apsalar-->')[1]
				events_definitions_list.append(
					"def apsalar_event_" + eventName.replace(" ","_") + "(campaign_data, app_data, device_data, custom_user_id=False):\n" + "	print 'Apsalr : EVENT______________________________" + eventName.replace(" ","_") + "'\n	request=apsalar_event(campaign_data, app_data, device_data,event_name='" + eventName + "',value=" + ev + ",custom_user_id=custom_user_id)\n	util.execute_request(**request)\n\n")	
				e.remove(event_list[event])

			else:
				events_definitions_list.append("def " + re.sub(r'\W+', '', event_list[
					event]).lower() + "(campaign_data, app_data, device_data):\n" + "	print 'Appsflyer : EVENT___________________________" +
												 event_list[event] + "'\n	eventName			= '" + event_list[
													 event] + """'\n	eventValue			= '""" + json.dumps(eventData_List[event]) + """'\n	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)\n	util.execute_request(**request)\n\n\n""")
				e.remove(event_list[event])
	
	return events_definitions_list 
