import sys
from PySide import QtCore, QtGui


dic = {}

class Ui_fiberModesMainWindow(object):
    def setupUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setObjectName("fiberModesMainWindow")
        fiberModesMainWindow.resize(520, 120)
        self.centralwidget = QtGui.QWidget(fiberModesMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.MainLayout = QtGui.QGridLayout()
        self.MainLayout.setObjectName("MainLayout")
        self.PropertyLayout = QtGui.QGridLayout()
        self.PropertyLayout.setObjectName("PropertyLayout")
        self.lbl_Name = QtGui.QLabel(self.centralwidget)
        self.lbl_Name.setObjectName("lbl_Name")
        self.PropertyLayout.addWidget(self.lbl_Name, 0, 0, 1, 1)

        self.lbl_Name2 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name2.setObjectName("lbl_Name2")
        self.PropertyLayout.addWidget(self.lbl_Name2, 0, 1, 1, 1)

        self.lbl_Name3 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name3.setObjectName("lbl_Name3")
        self.PropertyLayout.addWidget(self.lbl_Name3, 0, 2, 1, 1)

        self.pb_addRow_1 = QtGui.QPushButton(self.centralwidget)
        self.pb_addRow_1.setObjectName("pb_addRow_1")
        self.PropertyLayout.addWidget(self.pb_addRow_1, 1, 5, 1, 1)

        self.ledit_Name_1 = QtGui.QLineEdit(self.centralwidget)
        self.ledit_Name_1.setObjectName("ledit_Name_1")
        self.PropertyLayout.addWidget(self.ledit_Name_1, 1, 0, 1, 1)

        self.spin_Box_1 = QtGui.QSpinBox(self.centralwidget)
        self.spin_Box_1.setObjectName("spin_Box_1")
        self.spin_Box_1.setRange(0,100)
        self.PropertyLayout.addWidget(self.spin_Box_1, 1, 1, 1, 1)

        self.spin_Box_2 = QtGui.QSpinBox(self.centralwidget)
        self.spin_Box_2.setObjectName("spin_Box_2")
        self.spin_Box_2.setRange(0,100)
        self.PropertyLayout.addWidget(self.spin_Box_2, 1, 2, 1, 1)

        self.MainLayout.addLayout(self.PropertyLayout, 0, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.MainLayout.addItem(spacerItem2, 1, 0, 1, 1)
        self.horizontalLayout_2.addLayout(self.MainLayout)
        fiberModesMainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(fiberModesMainWindow)
        QtCore.QMetaObject.connectSlotsByName(fiberModesMainWindow)

    def retranslateUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setWindowTitle(QtGui.QApplication.translate("fiberModesMainWindow", "Retention", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Retention Token", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name2.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Randomness", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name3.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Day", None, QtGui.QApplication.UnicodeUTF8))
        self.pb_addRow_1.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Add Token", None, QtGui.QApplication.UnicodeUTF8))

    def getData(self):
        print "Randomness==>"+str(self.spin_Box_1.value())
        print "Day==>"+str(self.spin_Box_2.value())
        print "Token 1==>"+str(self.ledit_Name_1.text())
        tokenCount=2
        for token in dic.keys():
            print "Token "+str(tokenCount)+"==>"+str(dic[token].text())
            tokenCount+=1

class DesignerMainWindow(QtGui.QMainWindow, Ui_fiberModesMainWindow):
    def __init__(self, parent = None):
        super(DesignerMainWindow, self).__init__(parent)
        self.setupUi(self)
        self.okButton()
        self.pb_addRow_1.clicked.connect( self.addRow )

    def addRow( self ):
        rows = self.PropertyLayout.rowCount()
        columns = self.PropertyLayout.columnCount()
        for column in range(columns):
            layout = self.PropertyLayout.itemAtPosition(rows - 1, column)
            if layout is not None:
                widget = layout.widget()
                if isinstance(widget, QtGui.QPushButton):
                    widget.setText('Remove Token %d' % (rows - 1))
                    widget.clicked.disconnect(self.addRow)
                    widget.clicked.connect(self.removeRow)
                else:
                    widget.setEnabled(True)

        dic['top day '+str(rows-1)] = QtGui.QLineEdit(self.centralwidget)
        self.PropertyLayout.addWidget(dic['top day '+str(rows-1)], rows, 0, 1, 1)
        dic['top day '+str(rows)] = QtGui.QSpinBox(self.centralwidget)
        self.PropertyLayout.addWidget(dic['top day '+str(rows)], rows, 1, 1, 1)
        dic['top day '+str(rows+1)] = QtGui.QSpinBox(self.centralwidget)
        self.PropertyLayout.addWidget(dic['top day '+str(rows+1)], rows, 2, 1, 1)
        widget = QtGui.QPushButton(self.centralwidget)
        widget.setText('Add Token')
        widget.clicked.connect(self.addRow)
        self.PropertyLayout.addWidget(widget, rows, columns - 1, 1, 1)

    def removeRow(self):
        index = self.PropertyLayout.indexOf(self.sender())
        row = self.PropertyLayout.getItemPosition(index)[0]
        for column in range(self.PropertyLayout.columnCount()):
            layout = self.PropertyLayout.itemAtPosition(row, column)
            if layout is not None:
                layout.widget().deleteLater()
                self.PropertyLayout.removeItem(layout)

    def okButton(self):
        widget2 = QtGui.QPushButton(self.centralwidget)
        widget2.setText('OK')
        widget2.clicked.connect(super(DesignerMainWindow, self).getData)
        widget2.clicked.connect(QtCore.QCoreApplication.instance().quit)
        self.MainLayout.addWidget(widget2, self.MainLayout.rowCount(), 1, 2, 1)


if __name__ == '__main__':
    app = QtGui.QApplication( sys.argv )
    dmw = DesignerMainWindow()
    dmw.show()
    sys.exit( app.exec_() )
