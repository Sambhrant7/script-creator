import sys
from PySide import QtCore, QtGui

dic = {}


class Ui_fiberModesMainWindow(object):
    def setupUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setObjectName("fiberModesMainWindow")
        fiberModesMainWindow.resize(500, 120)
        self.centralwidget = QtGui.QWidget(fiberModesMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.MainLayout = QtGui.QGridLayout()
        self.MainLayout.setObjectName("MainLayout")
        self.PropertyLayout = QtGui.QGridLayout()
        self.PropertyLayout.setObjectName("PropertyLayout")
        self.lbl_Name = QtGui.QLabel(self.centralwidget)
        self.lbl_Name.setObjectName("lbl_Name")
        self.PropertyLayout.addWidget(self.lbl_Name, 0, 1, 1, 1)

        self.lbl_Name2 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name2.setObjectName("lbl_Name2")

        self.PropertyLayout.addWidget(self.lbl_Name2, 0, 0, 1, 1)
        self.pb_addRow_1 = QtGui.QPushButton(self.centralwidget)
        self.pb_addRow_1.setObjectName("pb_addRow_1")
        self.PropertyLayout.addWidget(self.pb_addRow_1, 1, 5, 1, 1)

        self.ledit_Name_1 = QtGui.QLineEdit(self.centralwidget)
        self.ledit_Name_1.setObjectName("ledit_Name_1")
        self.PropertyLayout.addWidget(self.ledit_Name_1, 1, 1, 1, 2)

        self.spin_Box_1 = QtGui.QSpinBox(self.centralwidget)
        self.spin_Box_1.setObjectName("spin_Box_1")
        self.spin_Box_1.setValue(10)
        self.PropertyLayout.addWidget(self.spin_Box_1, 1, 0, 1, 1)

        self.MainLayout.addLayout(self.PropertyLayout, 0, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.MainLayout.addItem(spacerItem2, 1, 0, 1, 1)
        self.horizontalLayout_2.addLayout(self.MainLayout)
        fiberModesMainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(fiberModesMainWindow)
        QtCore.QMetaObject.connectSlotsByName(fiberModesMainWindow)

    def retranslateUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setWindowTitle(QtGui.QApplication.translate("fiberModesMainWindow", "Purchase", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Purchase Token", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name2.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Advertiser Demand", None, QtGui.QApplication.UnicodeUTF8))
        self.pb_addRow_1.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Add Token", None, QtGui.QApplication.UnicodeUTF8))


    def getData(self):
        print "AdvertiserDemand==>"+str(self.spin_Box_1.value())
        print "PurchaseToken==>"+str(self.ledit_Name_1.text())
        tokenCount=2
        for day in dic.keys():
            print "PurchaseToken"+str(tokenCount)+"==>"+str(dic[day].text())
            tokenCount+=1

class DesignerMainWindow(QtGui.QMainWindow, Ui_fiberModesMainWindow):
    def __init__(self, parent = None):
        super(DesignerMainWindow, self).__init__(parent)
        self.setupUi(self)
        self.okButton()
        self.pb_addRow_1.clicked.connect( self.addRow )

    def addRow( self ):
        rows = self.PropertyLayout.rowCount()
        columns = self.PropertyLayout.columnCount()
        for column in range(columns):
            layout = self.PropertyLayout.itemAtPosition(rows - 1, column)
            if layout is not None:
                widget = layout.widget()
                if isinstance(widget, QtGui.QPushButton):
                    widget.setText('Remove Token %d' % (rows - 1))
                    widget.clicked.disconnect(self.addRow)
                    widget.clicked.connect(self.removeRow)
                else:
                    widget.setEnabled(True)

        dic['top day '+str(rows-1)] = QtGui.QLineEdit(self.centralwidget)
        self.PropertyLayout.addWidget(dic['top day '+str(rows-1)], rows, 1, 1, 2)
        widget = QtGui.QPushButton(self.centralwidget)
        widget.setText('Add Token')
        widget.clicked.connect(self.addRow)
        self.PropertyLayout.addWidget(widget, rows, columns - 1, 1, 1)

    def removeRow(self):
        index = self.PropertyLayout.indexOf(self.sender())
        row = self.PropertyLayout.getItemPosition(index)[0]
        for column in range(self.PropertyLayout.columnCount()):
            layout = self.PropertyLayout.itemAtPosition(row, column)
            if layout is not None:
                layout.widget().deleteLater()
                self.PropertyLayout.removeItem(layout)

    def okButton(self):
        widget2 = QtGui.QPushButton(self.centralwidget)
        widget2.setText('OK')
        widget2.clicked.connect(super(DesignerMainWindow, self).getData)
        widget2.clicked.connect(QtCore.QCoreApplication.instance().quit)
        self.MainLayout.addWidget(widget2, self.MainLayout.rowCount(), 1, 2, 1)


if __name__ == '__main__':
    app = QtGui.QApplication( sys.argv )
    dmw = DesignerMainWindow()
    dmw.show()
    sys.exit( app.exec_() )
