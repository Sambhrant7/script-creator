import sys
from PySide import QtCore, QtGui

dic={}


class Ui_fiberModesMainWindow(object):
    def setupUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setObjectName("fiberModesMainWindow")
        fiberModesMainWindow.resize(500, 210)
        self.centralwidget = QtGui.QWidget(fiberModesMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.MainLayout = QtGui.QGridLayout()
        self.MainLayout.setObjectName("MainLayout")
        self.PropertyLayout = QtGui.QGridLayout()
        self.PropertyLayout.setObjectName("PropertyLayout")
        self.lbl_Name = QtGui.QLabel(self.centralwidget)
        self.lbl_Name.setObjectName("lbl_Name")
        self.PropertyLayout.addWidget(self.lbl_Name, 1, 0, 1, 1)

        self.lbl_Name1 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name1.setObjectName("lbl_Name1")
        self.PropertyLayout.addWidget(self.lbl_Name1, 0, 1, 1, 1)

        self.lbl_Name2 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name2.setObjectName("lbl_Name2")
        self.PropertyLayout.addWidget(self.lbl_Name2, 0, 3, 1, 1)


        self.lbl_Name3 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name3.setObjectName("lbl_Name3")
        self.PropertyLayout.addWidget(self.lbl_Name3, 2, 0, 1, 1)

        self.lbl_Name4 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name4.setObjectName("lbl_Name4")
        self.PropertyLayout.addWidget(self.lbl_Name4, 3, 0, 1, 1)

        self.lbl_Name5 = QtGui.QLabel(self.centralwidget)
        self.lbl_Name5.setObjectName("lbl_Name5")
        self.PropertyLayout.addWidget(self.lbl_Name5, 4, 0, 1, 1)

        self.pb_addRow_1 = QtGui.QPushButton(self.centralwidget)
        self.pb_addRow_1.setObjectName("pb_addRow_1")
        self.PropertyLayout.addWidget(self.pb_addRow_1, 1, 5, 1, 1)

        self.spinbox_1 = QtGui.QSpinBox(self.centralwidget)
        self.spinbox_1.setObjectName("spinbox_1")
        self.PropertyLayout.addWidget(self.spinbox_1, 1, 1, 1, 2)

        self.spinbox_2 = QtGui.QSpinBox(self.centralwidget)
        self.spinbox_2.setObjectName("spinbox_2")
        self.spinbox_2.setMaximum(2000)
        self.PropertyLayout.addWidget(self.spinbox_2, 1, 3, 1, 2)

        self.lineedit_1 = QtGui.QLineEdit(self.centralwidget)
        self.lineedit_1.setObjectName("lineedit_1")
        self.PropertyLayout.addWidget(self.lineedit_1, 2, 1, 1, 4)

        self.lineedit_2 = QtGui.QLineEdit(self.centralwidget)
        self.lineedit_2.setObjectName("lineedit_2")
        self.PropertyLayout.addWidget(self.lineedit_2, 3, 1, 1, 4)

        self.lineedit_3 = QtGui.QLineEdit(self.centralwidget)
        self.lineedit_3.setObjectName("lineedit_3")
        self.PropertyLayout.addWidget(self.lineedit_3, 4, 1, 1, 4)

        self.MainLayout.addLayout(self.PropertyLayout, 0, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.MainLayout.addItem(spacerItem2, 1, 0, 1, 1)
        self.horizontalLayout_2.addLayout(self.MainLayout)
        fiberModesMainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(fiberModesMainWindow)
        QtCore.QMetaObject.connectSlotsByName(fiberModesMainWindow)

    def retranslateUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setWindowTitle(QtGui.QApplication.translate("fiberModesMainWindow", "Call Pattern", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name1.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Min", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name2.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Max", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name3.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Diff/Seq", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name4.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Install Top", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name5.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Open Top", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Name.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Level Range", None, QtGui.QApplication.UnicodeUTF8))
        self.pb_addRow_1.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Add Day", None, QtGui.QApplication.UnicodeUTF8))


    def getData(self):
        print "Minimum==>"+str(self.spinbox_1.value())
        print "Maximum==>"+str(self.spinbox_2.value())
        print "Difference==>"+str(self.lineedit_1.text())
        print "InstallTop==>"+str(self.lineedit_2.text())
        print "OpenTop==>"+str(self.lineedit_3.text())
        d=1
        for day in dic.keys():
            print "Day"+str(d+1)+"==>"+str(dic[day].text())
            d+=1
        sys.exit()




class DesignerMainWindow(QtGui.QMainWindow, Ui_fiberModesMainWindow):
    def __init__(self, parent = None):
        super(DesignerMainWindow, self).__init__(parent)
        self.setupUi(self)
        self.okButton()
        self.pb_addRow_1.clicked.connect( self.addRow )

    def addRow( self ):
        rows = self.PropertyLayout.rowCount()
        columns = self.PropertyLayout.columnCount()

        widget1 = QtGui.QLabel(self.centralwidget)
        widget1.setText("Day %d" %(rows-3))
        self.PropertyLayout.addWidget(widget1, rows, 0, 1, 2)

        dic['top day '+str(rows-4)] = QtGui.QLineEdit(self.centralwidget)
        self.PropertyLayout.addWidget(dic['top day '+str(rows-4)], rows, 1, 1, 4)
        widget = QtGui.QPushButton(self.centralwidget)
        widget.setText('Remove Day')
        widget.clicked.connect(self.removeRow)
        self.PropertyLayout.addWidget(widget, rows, columns-1, 1, 1)

    def removeRow(self):
        index = self.PropertyLayout.indexOf(self.sender())
        row = self.PropertyLayout.getItemPosition(index)[0]
        for column in range(self.PropertyLayout.columnCount()):
            layout = self.PropertyLayout.itemAtPosition(row, column)
            if layout is not None:
                layout.widget().deleteLater()
                self.PropertyLayout.removeItem(layout)

    def okButton(self):
        widget2 = QtGui.QPushButton(self.centralwidget)
        widget2.setText('OK')
        widget2.clicked.connect(super(DesignerMainWindow, self).getData)
        widget2.clicked.connect(QtCore.QCoreApplication.instance().quit)
        self.MainLayout.addWidget(widget2, self.MainLayout.rowCount(), 1, 2, 1)


if __name__ == '__main__':
    app = QtGui.QApplication( sys.argv )
    dmw = DesignerMainWindow()
    dmw.show()
    sys.exit( app.exec_() )
