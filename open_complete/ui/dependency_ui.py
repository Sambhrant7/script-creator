# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import sys, argparse
from PySide import QtCore, QtGui


parser = argparse.ArgumentParser()
parser.add_argument("-tokens")
args = parser.parse_args()
globals()["tokenList"]= eval(args.tokens)
globals()["CheckList"]=[]

class Ui_fiberModesMainWindow(object):
    def setupUi(self, fiberModesMainWindow):
        self.swap = 0
        fiberModesMainWindow.setObjectName("fiberModesMainWindow")
        fiberModesMainWindow.resize(400, 200)
        self.centralwidget = QtGui.QWidget(fiberModesMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.MainLayout = QtGui.QGridLayout()
        self.MainLayout.setObjectName("MainLayout")
        self.PropertyLayout = QtGui.QGridLayout()
        self.PropertyLayout.setObjectName("PropertyLayout")

        i=0
        if not globals()["tokenList"]:
            blank = QtGui.QLabel(self.centralwidget)
            blank.setObjectName("Checkbox_1")
            blank.setText("INDEPENDENT TOKEN")
            # d[token].clicked.connect(switch)
            self.PropertyLayout.addWidget(blank, i, 1, 1, 1)
            globals()["CheckList"].append(blank.text())
        else:
            for token in globals()["tokenList"]:
                d={}
                i+=1
                check = QtGui.QCheckBox(self.centralwidget)
                check.setObjectName("Checkbox_1")
                check.setText(str(token))
                check.setChecked(1)
                # d[token].clicked.connect(switch)
                self.PropertyLayout.addWidget(check, i, 1, 1, 1)
                globals()["CheckList"].append(check)


        self.pushButton1 = QtGui.QPushButton(self.centralwidget)
        self.pushButton1.clicked.connect(self.setAllButtonsChecked)
        self.pushButton1.setText('Check/Uncheck')
        self.PropertyLayout.addWidget(self.pushButton1, 1, 2, 1, 4)

        self.MainLayout.addLayout(self.PropertyLayout, 0, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.MainLayout.addItem(spacerItem2, 1, 0, 1, 1)
        self.horizontalLayout_2.addLayout(self.MainLayout)
        fiberModesMainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(fiberModesMainWindow)
        QtCore.QMetaObject.connectSlotsByName(fiberModesMainWindow)

    def retranslateUi(self, fiberModesMainWindow):
        fiberModesMainWindow.setWindowTitle(QtGui.QApplication.translate("fiberModesMainWindow", "Dependency", None, QtGui.QApplication.UnicodeUTF8))
        # self.lbl_Name2.setText(QtGui.QApplication.translate("fiberModesMainWindow", "Randomness", None, QtGui.QApplication.UnicodeUTF8))

    def setAllButtonsChecked(self):
        for token in globals()["CheckList"]:
            token.setChecked(self.swap)
        if self.swap == 1:
            self.swap = 0
        elif self.swap == 0:
            self.swap = 1


class DesignerMainWindow(QtGui.QMainWindow, Ui_fiberModesMainWindow):
    def __init__(self, parent = None):
        super(DesignerMainWindow, self).__init__(parent)
        self.setupUi(self)
        self.okButton()
        # self.pb_addRow_1.clicked.connect( self.addRow )

    def addRow( self ):
        rows = self.PropertyLayout.rowCount()
        columns = self.PropertyLayout.columnCount()
        for column in range(columns):
            layout = self.PropertyLayout.itemAtPosition(rows - 1, column)
            if layout is not None:
                widget = layout.widget()
                if isinstance(widget, QtGui.QPushButton):
                    widget.setText('Remove Token %d' % (rows - 1))
                    widget.clicked.disconnect(self.addRow)
                    widget.clicked.connect(self.removeRow)
                else:
                    widget.setEnabled(True)
        
    def removeRow(self):
        index = self.PropertyLayout.indexOf(self.sender())
        row = self.PropertyLayout.getItemPosition(index)[0]
        for column in range(self.PropertyLayout.columnCount()):
            layout = self.PropertyLayout.itemAtPosition(row, column)
            if layout is not None:
                layout.widget().deleteLater()
                self.PropertyLayout.removeItem(layout)

    def okButton(self):
        widget2 = QtGui.QPushButton(self.centralwidget)
        widget2.setText('OK')
        widget2.clicked.connect(self.submit)
        self.MainLayout.addWidget(widget2, self.MainLayout.rowCount(), 1, 2, 1)

    def submit(self):
        if globals()["CheckList"][0] =="INDEPENDENT TOKEN":
            sys.exit()    
        else:
            for check in range(len(globals()["CheckList"])):
                if globals()["CheckList"][check].isChecked()==True:
                    print globals()["tokenList"][check]
        sys.exit()

if __name__ == '__main__':
    app = QtGui.QApplication( sys.argv )
    dmw = DesignerMainWindow()
    dmw.show()
    sys.exit( app.exec_() )
