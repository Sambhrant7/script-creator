def remove_sleep(func):
   def wrapper(a):
       if isinstance(a, (int, float)) and a >= 0:
           pass
       else:
           raise Exception('Must be >= 0')

   return wrapper