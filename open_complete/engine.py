import inspect,os, shutil
from filter_revert import calling_data_dictionary, setupEvents, callPattern_sample_event_callingDictionary
import webbrowser
globals()['callPatternType']=""
getPath = os.getcwd()

#Defining randomness for the calls
def calculateRandomness(currentRandomness,requiredRandomness):
	return int((float(requiredRandomness)/currentRandomness)*100)

#Call pattern to get call in the install as well as in open
def generateCallPattern(callPatternList, callPatternData, flag):
	callPattern1 = """\n\ndef call_pattern(app_data,device_data,type1='install',day=False):\n	if not app_data.get('levels_achieved'):\n		app_data['levels_achieved']="""+str(callPatternData.get("Minimum"))+"""\n\n	if type1=='install':\n		play=random.choice(["""+str(callPatternData.get("InstallTop"))+"""])\n	if type1=='open':\n		if day==1:\n			print "day 1"\n			play=random.choice(["""+str(callPatternData.get("OpenTop"))+"""])	\n"""
	callPattern2 = ""
	tt=""
	if flag:
		callPattern2 = "\tif app_data.get('registration')==True:\n\t"
		tt="\t\t"	
	callPattern2 += """\tplayUpto= app_data.get('levels_achieved')+play\n	t1=0\n	t2=0\n\tfor i in range(app_data.get('levels_achieved'),playUpto):\n		print 'Playing game... '+str(i)\n""".replace("\n","\n"+tt)

	callPattern4 = """app_data['levels_achieved']=playUpto\n\n"""
	callPattern3 = "\t"
	if not callPatternData.get('Day2'):
		callPattern1.strip('if day==1:\n			print "day 1"\n			')
	elif not callPatternData.get('Day3'):
		callPattern1+="""		else:\n			play=random.choice(["""+str(callPatternData.get("Day2"))+"""])\n"""
	else:
		for days in sorted(callPatternData.keys()):
			if "Day" in days:
				d = days.split("Day")[1].strip()
				callPattern1+="""\t\tif day=="""+d+""":\n			print "day """+d+""""\n			play=random.choice(["""+str(callPatternData.get(days))+"""])		\n"""
		callPattern1 = callPattern1.rsplit("if day==",1)[0]+"else:"+callPattern1.rsplit(":",1)[1]
	
	if "[i]" in callPatternList[0] and callPatternData.get("Difference"):
		globals()['callPatternType']="Incremental"
		tab="\t"		
		if "," in callPatternData.get("Difference"):
			callPattern3="""\t\tif any(str(app_data.get('levels_achieved')) in l for l in """+str(callPatternData.get("Difference").split(","))+")"
			tab+="\t"
			callPattern3+=":\n"+tab

		for i in callPatternList:
			if "[i]" in i:
				for track in callPattern_sample_event_callingDictionary.keys():
					if track in i:
						e=i.split("Event Name->")[1]
						eventName=e.strip()
						if "," in e:
							eventName=e.split(",")[0].strip()
						callPattern3+=callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME",eventName).replace("EVENTVALUE",i.split("Event Value=")[1].split("[")[0].strip()).replace("[i]","'+app_data.get('levels_achieved')").replace("levels_achieved')'","levels_achieved')").replace("\n","\n"+tab)

			elif "[j]" in i:
				for track in callPattern_sample_event_callingDictionary.keys():
					if track in i:
						e=i.split("Event Name->")[1]
						eventName=e.strip()
						if "," in e:
							eventName=e.split(",")[0].strip()
						callPattern3+=callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME",eventName).replace("EVENTVALUE",i.split("Event Value=")[1].split("[")[0].strip()).replace("[j]","+app_data.get('levels_achieved')").replace("levels_achieved')'","levels_achieved')").replace("\n","\n"+tab)

			elif "<<<" in i:
				e=i.split("Event Name->")[1]
				eventName=e.strip()
				if "," in e:
					eventName=e.split(",")[0].strip()
				callPattern3+="\t\t"+"if app_data.get('levels_achieved')=="+i.split("<<<")[1].split(">>>")[0].strip()+":\n\t\t\t"
				callPattern3+=callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME",eventName).replace("EVENTVALUE",i.split("Event Value=")[1].split("[")[0].strip()).replace("\n","\n"+tab)
		if len(tab)>1:
			callPattern3=callPattern3[:-1]


	else:
		t=""
		tab="\t"
		globals()['callPatternType']="List"
		if "," in callPatternData.get("Difference"):
			# raise Exception("Cannot use sequence with list type call pattern.")
			callPattern3="""\t\tif any(str(app_data.get('levels_achieved')) in l for l in """+str(callPatternData.get("Difference").split(","))+")"
			tab+="\t"
			callPattern3+=":\n"+tab	
		elif int(callPatternData.get("Difference"))>1:
			callPattern3+="""\tif app_data.get('levels_achieved')%"""+str(int(callPatternData.get("Difference")))+"""==0:\n\t\t"""
			t+="\t"
		eList=[]
		vList=[]
		for i in callPatternList:
			if not "Event Name->" in i:
				print "This is not an event:-\n",i
				continue
			e=i.split("Event Name->")[1]
			eventName=e.strip()
			if "," in e:
				eventName=e.split(",")[0].strip()
			eventValue = eval(i.split("Event Value=")[1].split("[")[0].strip())
			eList.append(eventName)
			vList.append(eventValue)


			for track in callPattern_sample_event_callingDictionary.keys():
				if track in i and not callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME","EVENTNAMELIST[app_data.get('levels_achieved')]").replace("EVENTVALUE","EVENTVALUELIST[app_data.get('levels_achieved')]").replace("\n","\n\t"+t+tab[:-1]).replace("eventName			= '","eventName			= ").replace("EVENT___________________________'+'","EVENT___________________________'+") in callPattern3:
					print track
					callPattern3+=callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME","EVENTNAMELIST[app_data.get('levels_achieved')]").replace("EVENTVALUE","EVENTVALUELIST[app_data.get('levels_achieved')]").replace("\n","\n\t"+t+tab[:-1]).replace("eventName			= '","eventName			= ").replace("EVENT___________________________'+'","EVENT___________________________'+")
		if t:
			callPattern3 = callPattern3[:-1]
		if len(tab)>1:
			callPattern3=callPattern3[:-1]
		global EVENTVALUELIST,EVENTNAMELIST
		globals()["EVENTNAMELIST"]=eList
		globals()["EVENTVALUELIST"]=vList

	if globals().get("EVENTVALUELIST"):
		callPattern2 = "\n	EVENTVALUELIST="+str(globals().get("EVENTVALUELIST"))+"\n"+callPattern2
	if globals().get("EVENTNAMELIST"):
		callPattern2 = "\n	EVENTNAMELIST="+str(globals().get("EVENTNAMELIST"))+"\n"+callPattern2

	return 	callPattern1+tt+callPattern2+callPattern3.replace("\n","\n"+tt)+callPattern4.replace("\n","\n"+tt)	

#Writting Open function in the Script 
def writeOpenScript(script, installCalls, openCalls, eventDefinitions=None, callPattern=""):
	newAppFile = open(getPath+"\\open_complete\\runner\\"+script, "r")
	checks=""
	lines=[]
	for i in newAppFile.readlines():
		lines.append(i)
	upperHalf=[]
	lowerHalf=[]
	for line in lines:
		if "###########		CALLS		############" in line:
			break
		elif "#	 CALLS		 #" in line:
			break
		elif "util.execute_request" in line:
			upperHalf=upperHalf[:-3]
			break
		else:
			upperHalf.append(line)

	flag=False
	for line in upperHalf:
		if flag:
			checks+=line
		if "installtimenew.main(app_data" in line:
			flag=True

	midPart="""	return {'status':True}
	
def open(app_data, device_data, day=1):	\n\tif not app_data.get('times'):\n		print "Please wait installing..."\n		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')\n"""+checks
	returnCount = 0
	for line in lines:
		if any(key in line for key in ["return {",'return{',"return  {"]) and returnCount<2:
			returnCount+=1
		elif returnCount>=2:
			lowerHalf.append(line)

	lh="".join(lowerHalf)
	lh.replace("\n################################################################\n# EVENT DEFINITION\n#\n# Define all the event's call below\n################################################################\n","\n")
	kill=[]
	for duplicate in eventDefinitions:
		if "def " in duplicate:
			if duplicate.split("def ")[1].split(":")[0] in lh:
				kill.append(duplicate)
	for d in kill:
		eventDefinitions.remove(d)

	eventDefinitions.insert(0,"\n################################################################\n# EVENT DEFINITION\n#\n# Define all the event's call below\n################################################################\n")

	# print "".join(upperHalf),"\n","".join(installCalls),midPart,"".join(openCalls),callPattern,"\n\n","".join(eventDefinitions),lh

	################### WRITTING SCRIPT ######################
															 #
	f = open(getPath + "\\Output\\" + script, 'a')  		 #
	for i in upperHalf:										 #
		f.write(i)											 #
	for j in installCalls:									 #
		f.write(j)											 #
	for k in midPart:									 	 #
		f.write(k)											 #
	for l in openCalls:									 	 #
		f.write(l)											 #
	for m in callPattern:									 #
		f.write(m)											 #
	f.write("\n")											 #
	for n in eventDefinitions:								 #
		f.write(n)											 #
	f.write(lh)											 	 #
	f.close()												 #
	##########################################################
	try:
		shutil.rmtree(getPath + "/Input/res/install1/extracted", ignore_errors=True)#
		shutil.rmtree(getPath + "/Input/res/install2/extracted", ignore_errors=True)#
		shutil.rmtree(getPath + "/Input/res/open1/extracted", ignore_errors=True)#
		shutil.rmtree(getPath + "/Input/res/open2/extracted", ignore_errors=True)#
		shutil.rmtree(getPath + "/Input/res/extracted", ignore_errors=True)#
		os.remove(getPath + "/Input/res/install1/source.zip")
		os.remove(getPath + "/Input/res/install2/source.zip")
		os.remove(getPath + "/Input/res/open1/source.zip")
		os.remove(getPath + "/Input/res/open2/source.zip")
		os.remove(getPath + "/Input/res/source.zip")
	except:
		pass
	webbrowser.open(getPath + "\\Output\\" + script)

#Defining Mendatory things while processing calls
def processCalls(script,callList,callPatternData,purchaseData,retentionData):
	sorted(callList, key = lambda i: i['sequence'])
	calls=[]
	for li in callList:
		calls.append(u''+li.get("call"))		
	installCalls=[]
	openCalls=[]
	callPatternList=[]
	firstTrack=False
	adbFirstTracking=False
	eventDefinitions = setupEvents(callList)
	presentInstallRandomness =  100
	installIndent="\t"
	presentOpenRandomness =  100
	OpenIndent="\t"
	purchase= None
	retention= None
	flag=False
	index= 0
	randCheck = ""
	eventCheck = ""
	for call in callList:
		############################# ELIMINATING LAST CALL ##############################
		if call.get('call')=="Call Name here":
			continue
		if call.get("DependentOnCalls") and "" in eval(call.get("DependentOnCalls")):
			if "Event Name->" in callList[int(call.get('sequence'))-2].get("call"):
				e=callList[int(call.get('sequence'))-2].get("call").split("Event Name->")[1]
				if "," in e:
					eventCheck=e.split(",")[0].strip()
				else:
					eventCheck=e.strip()
				if call.get("INSTALL"):
					installCalls[len(installCalls)-1] = installCalls[len(installCalls)-1]+installIndent+"app_data['"+eventCheck+"']=True\n\n"
				if call.get("OPEN"):
					openCalls[len(openCalls)-1] = openCalls[len(openCalls)-1]+OpenIndent+"app_data['"+eventCheck+"']=True\n\n"
				installIndent="\t"
				OpenIndent="\t"

		####################### RANDOMNESS AND INDENT MANAGEMENT ########################
		elif not int(call.get("randomness"))==100:
			if call.get("INSTALL"):
				if not presentInstallRandomness==100:
					r = calculateRandomness(presentInstallRandomness,int(call.get("randomness")))
				else:
					r = int(call.get("randomness"))
				presentInstallRandomness=int(call.get("randomness"))
				if not r==100:
					if eventCheck:
						installCalls.append("\n"+installIndent+"if random.randint(1,100)<="+str(r)+" and app_data.get('"+eventCheck+"'):\n")
						eventCheck=""
					else:
						installCalls.append("\n"+installIndent+"if random.randint(1,100)<="+str(r)+":\n")
					installCalls.append(installIndent)
					installIndent+="\t"
				else:
					if eventCheck:
						installCalls.append(installIndent[:-1]+"if app_data.get('"+eventCheck+"'):\n")
						eventCheck=""
					else:
						installCalls.append(installIndent[:-1])
			if call.get("OPEN"):
				if not presentOpenRandomness==100:
					r2 = calculateRandomness(presentOpenRandomness,int(call.get("randomness")))
				else:
					randCheck = "100"
					r2 = int(call.get("randomness"))
				if not r2==100:
					if eventCheck:
						openCalls.append(OpenIndent+"if random.randint(1,100)<="+str(r2)+" and app_data.get('"+eventCheck+"'):\n")
						eventCheck=""	
					else:
						openCalls.append(OpenIndent+"if random.randint(1,100)<="+str(r2)+":\n")
					presentOpenRandomness=int(call.get("randomness"))
					openCalls.append(OpenIndent)
					OpenIndent+="\t"
				else:
					if eventCheck:
						openCalls.append(OpenIndent[:-1]+"if app_data.get('"+eventCheck+"'):\n")
						eventCheck=""
					else:	
						openCalls.append(OpenIndent[:-1])

		######################## ADDING LOOP TO REPEATED CALLS ###########################
		if int(call.get("repeat"))>1:
			if call.get("INSTALL"):
				installCalls.append("\tfor i in range("+str(call.get("repeat"))+"):\n"+installIndent)
			if call.get("OPEN"):
				openCalls.append("\tfor i in range("+str(call.get("repeat"))+"):\n"+OpenIndent)

		# try:
		try:
			url = call.get("call").split("[URL=")[1].split("]")[0]
		except:
			url = call.get("call").split("URL[")[1].split("]")[0]

		########################### ADDING CALL PATTERN CALLS ############################
		if not call.get("INSTALL") and not call.get("OPEN"):
			callPatternList.append(call.get("call"))
		###################### ADDING CALLS TO INSTALL AND OPEN ##########################
		else:				
			for k in calling_data_dictionary.keys():
				if "->" in k and "Event Name" in call.get("call") and not "kochava" in call.get("call"):
					e=call.get("call").split("Event Name->")[1]
					if "," in e:
						eventName=e.split(",")[0].strip()
					else:
						eventName=e.strip()
					if call.get("INSTALL"):
						installCalls.append(calling_data_dictionary[url+"->"+eventName])
					if call.get("OPEN"):
						openCalls.append(calling_data_dictionary[url+"->"+eventName])
					break

				elif "->" in k:
					if k.split("->")[0] in url:
						if url=="http://t.appsflyer.com/api/v4/androidevent" and firstTrack==False:
							if call.get("INSTALL"):
								installCalls.append(calling_data_dictionary[url+"->first"])
								break
								firstTrack=True
						elif url=="http://tracking.ad-brix.com/v1/tracking" and adbFirstTracking==False:
							if call.get("INSTALL"):
								installCalls.append(calling_data_dictionary[url+"->first"])
								break
								adbFirstTracking=True
						elif url=="http://app.adjust.com/sdk_click":
							if call.get("INSTALL"):
								source=str(call.get("call").split("Adjust sdk_click->")[1]).split("[")[0].strip()
								installCalls.append(calling_data_dictionary[url+"->"+source])
							if call.get("OPEN"):
								source=str(call.get("call").split("Adjust sdk_click->")[1]).split("[")[0].strip()
								openCalls.append(calling_data_dictionary[url+"->"+source])
							break
						elif url=="http://control.kochava.com/track/json" or url=="http://control.kochava.com/track/kvTracker.php":
							a=call.get("call").split("Kochava Event name->")[1]
							if "," in a:
								action=a.split(",")[0].strip()
							else:
								action=a.strip()
							if call.get("INSTALL"):
								installCalls.append(calling_data_dictionary[url+"->"+action])
							if call.get("OPEN"):
								openCalls.append(calling_data_dictionary[url+"->"+action])
							break

						elif "engine.mobileapptracking.com" in url:
							url.split(".",1)[1]
							break

						else:
							if call.get("INSTALL"):
								installCalls.append(calling_data_dictionary[url])
							if call.get("OPEN"):
								openCalls.append(calling_data_dictionary[url])
							break
				else:
					if url in k:
						if call.get("INSTALL"):
							installCalls.append(calling_data_dictionary[url])
						if call.get("OPEN"):
							openCalls.append(calling_data_dictionary[url])
						break

		if "<<<REGISTRATION>>>" in call.get("call"):
			if call.get("INSTALL"):
				installCalls.append(installIndent[:-1]+"\tapp_data['registration'] = True\n\n")
				flag=True
			if call.get("OPEN"):
				if randCheck=="100":
					openCalls[int(call.get('sequence'))-1] = openCalls[int(call.get('sequence'))-1].replace("if random.randint(","if not app_data.get('registration') and random.randint(")
				else:
					openCalls[int(call.get('sequence'))-1] = OpenIndent+"if not app_data.get('registration'):\n"+OpenIndent+openCalls[int(call.get('sequence'))-1]
				flag=True

		index+=1					
	# except:
	# 	print "EXCEPTION IN CALL", call.get("call")

	############################# ADDING CALL PATTERN CALLING WITH INDENT ##############################
	installCalls.append(installIndent)	
	installCalls.append("""call_pattern(app_data,device_data,type1='install')\n\n""")

	openCalls.append(OpenIndent)
	openCalls.append("""call_pattern(app_data,device_data,type1='open')\n\n""")
	
	############################# GENERATE CALL PATTERN ##############################
	callPattern = generateCallPattern(callPatternList, callPatternData, flag)
	
	########################### ADD PURCHASE CALL/CALLS ##############################
	if purchaseData is not None and purchaseData.get('PurchaseToken'):
		purchase=OpenIndent+"""if  purchase.isPurchase(app_data,day,advertiser_demand="""+purchaseData.get("AdvertiserDemand")+"""):\n	time.sleep(random.randint(10,15))\n	pur_var=random.randint(1,100)\n""".replace("\n","\n"+OpenIndent)
		purchaseTokenList=[purchaseData.get('PurchaseToken')]
		for i in range(2,10):
			if purchaseData.get('PurchaseToken'+str(i)):
				purchaseTokenList.append(purchaseData.get('PurchaseToken'+str(i)))

		for token in callList:
			for ptoken in purchaseTokenList:
				if ptoken in token.get("call"):
					i = token.get("call") 
					e=token.get("call").split("Event Name->")[1]
					purchaseEventName=e.strip()
					if "," in e:
						purchaseEventName=e.split(",")[0].strip()
					if globals()['callPatternType']=="List":
						purchaseEventName = '"'+purchaseEventName+'"'
					print globals()['callPatternType']
					purchaseEventValue=token.get("call").split("Event Value=")[1].split("[")[0].strip()

					for track in callPattern_sample_event_callingDictionary.keys():
						if track in i:
							purchase+=callPattern_sample_event_callingDictionary.get(track).replace("EVENTNAME",purchaseEventName).replace("EVENTVALUE",purchaseEventValue).replace("\n","\n"+OpenIndent)

	if purchase:
		openCalls.append(purchase[:-len(OpenIndent)])

	########################### ADD RETENTION CALL/CALLS ##############################
	if retentionData is not None and retentionData.get('Token 1'):
		rtCall=None
		for openCall in openCalls:
			if retentionData.get('Token 1') in openCall:
				pos=openCalls.index(openCall)
				tab=""
				if int(openCall.count("\t"))>1:
					tab="\t"
				rtCall = "\n"+"\t"*openCall.count("\t")+tab+tab+tab+"if day=="+str(retentionData.get("Day"))+":\n\t"+tab*2+tab+openCall
		if rtCall:
			openCalls[pos] = rtCall

	openCalls.append("\treturn {'status':True}\n\n")			
	########################## ADDING API HIT TIME TO FIRST CALL IN INSTALL ############################
	if "response=util.execute_request" in installCalls[0]:
		installCalls[0]=installCalls[0].replace("response=util.execute_request","app_data['api_hit_time']=time.time()\n	util.execute_request")
	else:
		installCalls[0]=installCalls[0].replace("util.execute_request","app_data['api_hit_time']=time.time()\n	util.execute_request")

	############################## WRITE FINAL SCRIPT  ###############################
	writeOpenScript(script, installCalls, openCalls, eventDefinitions, callPattern)