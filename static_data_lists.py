#################### DATA WRITTING LISTS ###################

##################################
# ##########

global campaign_data, imports, install_function
imports=["# -*- coding: utf-8 -*-\n","from sdk import installtimenew\n","from sdk import getsleep\n","import requests\n"]
campaign_data=["""\n\n#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {\n"""]

campaign_ending="""	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}\n\n"""

install_function=['''#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################
AF_SENSOR = AFSensorManger()
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."\n''']

open_function="""\n\n#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	# def_appsflyerUID(app_data)
	# def_eventsRecords(app_data)
	# generate_realtime_differences(device_data,app_data)

	return {'status':'true'}\n\n"""

event_definition_start="""################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################\n\n"""

android_required_functions=['''#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def saveClicks(data):
    url = "http://v1-appapi.appsuccessor.com/v4/clicks/save"
    proxies = {"http": None, "https": None}
    resp = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
    print(json.loads(resp.text))


def	saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp,event_token=""):
	try:
		count = 1
		if resp.get('res').status_code == 500:
			call = str(request.get('url'))
			if call == "https://android.clients.google.com/c2dm/register3":
				call = request.get('url')
				while count < 4:
					count += 1		
					if resp.get('res').status_code in [500, 502, 429]:	
						call += ","+str(resp.get('res').status_code)	
						time.sleep(3)
						request=android_clients_google( campaign_data, device_data, app_data )
						resp = util.execute_request(**request)			
					else:			
						break	

			elif call in ["https://app.adjust.com/attribution", "https://app.adjust.com/sdk_info", "https://app.adjust.com/event"]:
				call = request.get('url')
				if not app_data.get('session_check'):
					app_data['session_check']=False

				while count < 4:
					count += 1		
					if resp.get('res').status_code in [500, 502, 429]:	
						if call == "https://app.adjust.com/event":
							call += ","+str(event_token)
							call += ","+str(resp.get('res').status_code)
							break

						call += ","+str(resp.get('res').status_code)
						time.sleep(3)
						if app_data.get('session_check') == False:
							session_createdAt=get_date(app_data,device_data)
							time.sleep(2)
							session_sentAt=get_date(app_data,device_data)

							session_request = adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt)
							session_response = util.execute_request(**session_request)	
							if session_response.get('res').status_code == 200:
								app_data['session_check'] = True
						if app_data.get('session_check') == True:
							resp = util.execute_request(**request)	
					else:			
						break
			else:
				pass

		elif resp.get('res').status_code in [502, 429]:
			call = request.get('url')
			while count < 4:
				count += 1		
				if resp.get('res').status_code in [502, 429]:	
					call += ","+str(resp.get('res').status_code)
					time.sleep(3)
					resp = util.execute_request(**request)
				else:				
					break
		elif not resp.get('res').status_code == 200:
			call = request.get('url')
			count += 1

		if count >1:
			data = {'appname': os.path.basename(__file__).split(".")[0],'agentId': Config.AGENTID,'tracker': campaign_data.get('tracker'),'call': call,'resCode': resp.get('res').status_code,'id':device_data.get('android_id')}
			url = "http://v1-appapi.appsuccessor.com/rd/attribution-call-status/save"
			proxies = {"http": None, "https": None}
			respData = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
			print(json.loads(respData.text))

		return resp
	except:
		print 'Could not save attributionCallStatus data'
		return resp

def saveBatterydata(data):
    url = "http://v1-appapi.appsuccessor.com/temp/module/battery"
    proxies = {"http": None, "https": None}
    resp = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
    print(json.loads(resp.text))
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"''']

ios_required_functions=['''# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

def saveClicks(data):
    url = "http://v1-appapi.appsuccessor.com/v4/clicks/save"
    proxies = {"http": None, "https": None}
    resp = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
    print(json.loads(resp.text))


def	saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp,event_token=""):
	try:
		count = 1
		if resp.get('res').status_code == 500:
			call = str(request.get('url'))
			if call == "https://android.clients.google.com/c2dm/register3":
				call = request.get('url')
				while count < 4:
					count += 1		
					if resp.get('res').status_code in [500, 502, 429]:	
						call += ","+str(resp.get('res').status_code)	
						time.sleep(3)
						request=android_clients_google( campaign_data, device_data, app_data )
						resp = util.execute_request(**request)			
					else:			
						break	

			elif call in ["https://app.adjust.com/attribution", "https://app.adjust.com/sdk_info", "https://app.adjust.com/event"]:
				call = request.get('url')
				if not app_data.get('session_check'):
					app_data['session_check']=False

				while count < 4:
					count += 1		
					if resp.get('res').status_code in [500, 502, 429]:	
						if call == "https://app.adjust.com/event":
							call += ","+str(event_token)
							call += ","+str(resp.get('res').status_code)
							break

						call += ","+str(resp.get('res').status_code)
						time.sleep(3)
						if app_data.get('session_check') == False:
							session_createdAt=get_date(app_data,device_data)
							time.sleep(2)
							session_sentAt=get_date(app_data,device_data)

							session_request = adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt)
							session_response = util.execute_request(**session_request)	
							if session_response.get('res').status_code == 200:
								app_data['session_check'] = True
						if app_data.get('session_check') == True:
							resp = util.execute_request(**request)	
					else:			
						break
			else:
				pass

		elif resp.get('res').status_code in [502, 429]:
			call = request.get('url')
			while count < 4:
				count += 1		
				if resp.get('res').status_code in [502, 429]:	
					call += ","+str(resp.get('res').status_code)
					time.sleep(3)
					resp = util.execute_request(**request)
				else:				
					break
		elif not resp.get('res').status_code == 200:
			call = request.get('url')
			count += 1

		if count >1:
			data = {'appname': os.path.basename(__file__).split(".")[0],'agentId': Config.AGENTID,'tracker': campaign_data.get('tracker'),'call': call,'resCode': resp.get('res').status_code,'id':device_data.get('mac')}
			url = "http://v1-appapi.appsuccessor.com/rd/attribution-call-status/save"
			proxies = {"http": None, "https": None}
			respData = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
			print(json.loads(respData.text))

		return resp
	except:
		print 'Could not save attributionCallStatus data'
		return resp

def saveBatterydata(data):
    url = "http://v1-appapi.appsuccessor.com/temp/module/battery"
    proxies = {"http": None, "https": None}
    resp = requests.post(url, data=json.dumps(data), proxies=proxies, timeout=100)
    print(json.loads(resp.text))

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"
''']