from adbrix_aes256 import decrypt_data
import json, time,random
import subprocess
from filter import adDoubleFlag

newKeysFound=[]
report = []

"""* 
	>>>>CLASS TO ACCESS ALL REQUEST DATA FROM LOGS TO BE USED IN SCRIPT FOR DEFINITIONS<<<<
*"""
class access_required_data:
	def __init__(self, calls_data_dict):
		self.data = calls_data_dict
		global platform
		platform = self.data.get('platform')
		"""* 
			>>>>PLATFORM DEPENDENT IMPORTS<<<<
		*"""
		if platform == "Android":
				global Adjust, Appsflyer, Adbrix, mat, Apsalar, Kochava, Adforce, Tenjin, branch
				from reference import Adjust, Appsflyer, Adbrix, mat, Apsalar, Kochava ,Adforce , Tenjin, branch
				from mat_aes import decrypt_mat_data
		elif platform == "ios":
				global Adjust, mat, decrypt_mat_data
				from reference import Adjustios as Adjust
				from reference import mat_ios as mat
				from reference import Apsalarios as Apsalar
				from reference import Kochavaios as Kochava
				from reference import Adforceios as Adforce
				from mat_IOS_AES import decrypt_mat_data

	def get_hosts(self):
		"""* 
			>>>>GET ALL TRACKINGS PRESENT IN APP<<<<
		*"""
		global hosts_present
		hosts_present = []
		for host in self.data.get('host'):
				if 'appsflyer' in host:
						 hosts_present.append('Appsflyer')
				if 'adjust' in host:
						 hosts_present.append('Adjust')
				if 'ad-brix' in host or 'adbrix' in host:
						 hosts_present.append('Adbrix')
				if 'kochava' in host:
						 hosts_present.append('Kochava')
				if 'engine.mobileapptracking' in host:
						 hosts_present.append('MAT')
				if 'apsalar' in host:
						 hosts_present.append('Apsalar')
				if 'adforce' in host:
						 hosts_present.append('Adforce')
				if 'tenjin' in host:
						 hosts_present.append('Tenjin')
				if 'branch' in host:
						 hosts_present.append('branch')
				if 'invenio' in host:
						 hosts_present.append('Invenio')

		return list(set(hosts_present))

	##################################################################################################
	"""* 
		>>>>APPSFLYER RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	@property
	def appsflyer_definition_data(self):
		report.append("Please match data for all tracks, data may vary for diffrent track calls.")
		self.main_campaign_data = {}
		all_appsflyer_call_function_defs = []
		flyer_campaign_data = {}
		timmings = {}
		total = []
		kef, cksm = False, False

		"""* 
			>>>>REMOVING EVENT NAMES FROM HOSTS TO GET DEFINITION FROM REFERENCE FILE<<<<
		*"""
		for x in self.data.get('host'):
				if 'appsflyer' in x:
						 if 'event' in x:
								total.append(x.split('/')[0])
						 else:
								total.append(x)
		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		total = list(set(total))
		"""* 
			>>>>MANAGING FIRST TRACK CALL TO FETCH ONLY ONE DEFINITION FOR APPSFLYER TRACK<<<<
		*"""
		total.remove("first->t.appsflyer.com")
		if not "t.appsflyer.com" in total:
				total.append("t.appsflyer.com")

		"""* 
			>>>>ITERATING THROUGH ALL THE HOSTS FILTERED<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "appsflyer" in self.data.get('host')[c]:
						 """* 
							 >>>>ELIMINATING EXTRA ADD ONS TO HOST<<<<
						 *"""
						 if self.data.get('host')[c] == "first->t.appsflyer.com":
								cursor = "t.appsflyer.com"
						 elif 'event' in self.data.get('host')[c]:
								cursor = "events.appsflyer.com"
						 else:
								cursor = self.data.get('host')[c]
						 """* 
							>>>>CHECK FOR UNIQUENESS OF DEFINITIONS<<<<
						 *"""
						 if cursor in total and "appsflyer" in self.data.get('host')[c]:
								all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('start'))
								all_appsflyer_call_function_defs.append('	method = "')
								all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('method'))
								all_appsflyer_call_function_defs.append('"\n	url = ')
								all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('url'))

								for dic in ["headers", "params", "data"]:
								 all_appsflyer_call_function_defs.append('\n	' + dic + ' = {\n')
								 if self.data.get(dic)[c]:
										for k in sorted(self.data.get(dic)[c].keys()):
												if k == "deviceData":
														 all_appsflyer_call_function_defs.append('		"' + k + '" : {\n')
														 for devicedata in sorted(self.data.get(dic)[c].get('deviceData').keys()):
																if devicedata == "btch" or devicedata == "arch":
																 all_appsflyer_call_function_defs.append(
																		'						"' + devicedata + '" : ' + str(
																				Appsflyer.definitions.get(cursor).get(dic).get(k).get(
																						 devicedata)) + ',\n')
																elif devicedata == "sensors":
																 all_appsflyer_call_function_defs.append(
																		'						"' + devicedata + '" : AF_SENSOR.sessionValues,\n')
																else:
																 all_appsflyer_call_function_defs.append(
																		'						"' + devicedata + '" : ' + str(
																				Appsflyer.definitions.get(cursor).get(dic).get(k).get(
																						 devicedata)) + ',\n')
														 all_appsflyer_call_function_defs.append('		},\n')

												elif k[:3] == "kef":
														 from reference import appsflyer_kef_verify
														 kefKey = appsflyer_kef_verify.get_key_half(str(self.data.get(dic)[c].get("brand")), str(self.data.get(dic)[c].get("sdk")), str(self.data.get(dic)[c].get("lang")), str(self.data.get(dic)[c].get("af_timestamp")), flyer_campaign_data.get('buildnumber'))
														 if not "kef"+kefKey == str(k):
																report.append("kef key does not match please check key manually and retry.")
																print "kef key does not match please check key manually and retry."
																# raise Exception("KEF key does not match!!! Please re-check appsflyer kef key then retry.")
														 kefVal = appsflyer_kef_verify.generateValue1get(str(self.data.get(dic)[c].get("af_timestamp")), str(self.data.get(dic)[c].get("firstLaunchDate")), flyer_campaign_data['buildnumber'])
														 if not kefVal == str(self.data.get(dic)[c].get(k))[:16]:
																report.append("First half for KEF value does not match please check key manually and retry.")
																print "kef key does not match please check key manually and retry."
																# raise Exception("First half for KEF value does not match!!! Please re-check appsflyer kef key then retry.")
														 kef = True

												elif k == "lang":
														 if self.data.get(dic)[c].get(k).isalnum():
																all_appsflyer_call_function_defs.append(
																 '		"' + k + '" : ' + Appsflyer.definitions.get(cursor).get(dic).get(
																		k) + ',\n')
														 else:
																all_appsflyer_call_function_defs.append(
																 '		"' + k + '" : "' + self.data.get(dic)[c].get(k) + '",\n')

												elif Appsflyer.definitions.get(cursor).get(dic).get(k):
														 if Appsflyer.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
																all_appsflyer_call_function_defs.append(
																 '		"' + k + '" : "' + self.data.get(dic)[c].get(k) + '",\n')

														 elif Appsflyer.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
																all_appsflyer_call_function_defs.append(
																 '		"' + k + '" : ' + str(self.data.get(dic)[c].get(k)) + ',\n')

														 else:
																all_appsflyer_call_function_defs.append(
																 '		"' + k + '" : ' + Appsflyer.definitions.get(cursor).get(dic).get(
																		k) + ',\n')
												else:
														 if not k in ["Host", "Content-Length", "Connection", "af_v", "af_v2", "eventName",
																		 "eventValue", "appUserId", "rfr", "prev_event"]:
																if not k+" in "+cursor in newKeysFound:
																 newKeysFound.append(k+" in "+cursor)
																 report.append(
																		"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(cursor))
																 outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(k))])
																 confirmed_value=""
																 confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																 confirmed_value=confirmed_value.join(confirmed_lines)
																 valueType = str(outP).rsplit("\n",2)[1].strip()
																 if valueType=="Dynamic":
																		all_appsflyer_call_function_defs.append(
																				'		"' + k + '" : '+confirmed_value+',\n')
																 else:
																		all_appsflyer_call_function_defs.append(
																				'		"' + k + '" : "'+confirmed_value+'",\n')
																 print "NEW KEY FOUND---> "+k
																 print "Confirmed_value-> "+confirmed_value

												if k == "buildnumber" or k == "appsflyerKey":
														 flyer_campaign_data[k] = self.data.get(dic)[c].get(k)

												elif k == "firstLaunchDate":
														 timmings[k] = self.data.get(dic)[c].get(k)
														 if not "0000" in str(self.data.get(dic)[c].get(k)):
																report.append("Local Time Zone not used in appsflyer...sdk seems to be old. Please add sec in all timmings according to timezone in logs.")
																print "Local Time Zone not used in appsflyer...sdk seems to be old. Please add sec in all timmings according to timezone in logs."

												elif k == "cksm_v1":
														 cksm = True
														 cksm_len = len(self.data.get(dic)[c].get(k))
														 cksm_val = ""
														 from reference import appsflyer_cksm_verify
														 if cksm_len==32:
																cksm_val = appsflyer_cksm_verify.cksm_v1_32(str(self.data.get(dic)[c].get("date1")), str(self.data.get(dic)[c].get("af_timestamp")), self.main_campaign_data["app_id"])
														 elif cksm_len==34:
																cksm_val = appsflyer_cksm_verify.cksm_v1_34(str(self.data.get(dic)[c].get("date1")),
																												str(self.data.get(dic)[c].get("af_timestamp")),
																												self.main_campaign_data["app_id"])
														 else:
																report.append("New length found for cksm_v1 value, i.e NOT 32 or 34, BUT "+ str(cksm_len))
																print "New Length for cksm_v1 found. Value is not 32/34..It is"+str(cksm_len)
																raise Exception("New Length for cksm_v1 found. Value is not 32/34..It is"+str(cksm_len))
														 if not cksm_val == str(self.data.get(dic)[c].get(k)):
																report.append(
																 "Could not verify cksm_v1 value, Please manually check cksm value, as original cksm value is:" + str(self.data.get(dic)[c].get(k))+" BUT generated one is:-"+str(cksm_val))
																print "Could not verify cksm_v1 value, Please manually check cksm value, as original cksm value is:" + str(self.data.get(dic)[c].get(k))+" BUT generated one is:-"+str(cksm_val)
																raise Exception(
																 "Could not verify cksm_v1 value, Please manually check cksm value, "
																 "as original cksm value is:" + str(self.data.get(dic)[c].get(k))+" BUT "
																																										 "generated one is:-"+str(cksm_val))
												elif k == "rfr":
														 all_appsflyer_call_function_defs.append('		"' + k + '" : {\n')
														 for r in sorted(self.data.get(dic)[c].get(k).keys()):
																if str(self.data.get(dic)[c].get(k).get(r)) == '0':
																 all_appsflyer_call_function_defs.append(
																		'								"' + r + '" : "0",\n')
																elif str(r) == "val":
																 all_appsflyer_call_function_defs.append(
																		'								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",\n')
																elif str(r) == "code":
																 all_appsflyer_call_function_defs.append(
																		'								"code": "' + self.data.get(dic)[c].get(
																				k).get(r) + '",\n')
																else:
																 if r == "clk":
																		all_appsflyer_call_function_defs.append(
																				'								"' + r + '" : str(int(app_data.get("times").get("click_time"))),\n')
																 elif r == "ins" or r == "install":
																		all_appsflyer_call_function_defs.append(
																				'								"' + r + '" : str(int(app_data.get("times").get("download_begin_time"))),\n')
														 all_appsflyer_call_function_defs.append('		},\n')

												elif k == "app_id" or k == "app_version_name" or k == "app_version_code" or k == "app_name":
														 if not self.data.get(dic)[c].get(k) == "com.hidemyass.hidemyassprovpn" and not \
																 self.data.get(dic)[c].get(k) == "HMA! Pro VPN":
																self.main_campaign_data[k] = self.data.get(dic)[c].get(k)

												# elif k == "p_receipt":
												# 	rec_len=len(self.data.get(dic)[c].get(k))
												# 	alpha_numeric_elements = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
												# 	random_string = ''.join(random.choice(alpha_numeric_elements) for i in range(rec_len-1))

												# 	receipt=random_string.replace('-','+').replace('_','/')+'='
												# 	all_appsflyer_call_function_defs.append(
												# 				 '		"' + k + '" : "' + receipt + '",\n')

								 all_appsflyer_call_function_defs.append('\n		}')

								if "com/api/" in self.data.get('url')[c]:
								 flyer_campaign_data['version'] = self.data.get('url')[c].split('com/api/')[1].split('/')[0]

								if "extraReferrers" in self.data.get(dic)[c].keys():
								 all_appsflyer_call_function_defs.append("\n")
								 all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('extraReferrers'))

								if Appsflyer.definitions.get(cursor).get('conditional_data'):
								 all_appsflyer_call_function_defs.append(
										Appsflyer.definitions.get(cursor).get('conditional_data'))
								if Appsflyer.definitions.get(cursor).get('cksm_v1') and cksm:
								 all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('cksm_v1'))
								if Appsflyer.definitions.get(cursor).get('kef') and kef:
								 all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('kef'))
								all_appsflyer_call_function_defs.append("\n\n	")
								all_appsflyer_call_function_defs.append(Appsflyer.definitions.get(cursor).get('return'))

								all_appsflyer_call_function_defs.append("\n\n")
								total.remove(cursor)
		"""* 
			>>>>ADDING CKSM AND KEF CALLING IN APPSFLYER DEFINITION IF PRESENT IN APP<<<<
		*"""
		if cksm:
				all_appsflyer_call_function_defs.append(Appsflyer.definitions.get('cksm_34') if cksm_len==34 else Appsflyer.definitions.get('cksm_32'))
		if kef:
				all_appsflyer_call_function_defs.append(Appsflyer.definitions.get('kef_gen'))

		return all_appsflyer_call_function_defs, flyer_campaign_data, self.main_campaign_data, timmings


	##################################################################################################
	"""* 
		>>>>ADJUST RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def adjust_definition_data(self):
		repeat_sdk_click = False
		self.main_campaign_data = {}
		all_adjust_call_function_defs = []
		adjust_campaign_data = {}
		timmings = {}
		total2 = []
		receipt = None
		AUTH = False
		valid = True

		"""* 
			>>>>ELIMINATING EXTRA ADD ONS TO HOSTS TO GET DEFINITIONS FROM ADJUST REFERENCE FILES<<<<
		*"""
		for y in self.data.get('host'):
				if 'adjust' in y:
						 if '->' in y:
								total2.append(y.split('->')[0])
						 else:
								total2.append(y)
		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		total2 = list(set(total2))

		"""* 
			>>>>MANAGING HOST NAME FOR SDK_CLICK OF VARIOUS TYPES<<<<
		*"""
		if any('install_referrer' in h for h in self.data.get('host')) and any(
						 'reftag' in g for g in self.data.get('host')):
				total2.append("app.adjust.com/sdk_click")
		if any('deeplink' in h for h in self.data.get('host')) and any('iad3' in g for g in self.data.get('host')):
				total2.append("app.adjust.com/sdk_click")

		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				call_type = self.data.get('url')[c].split('//')[1].split('/')[1]
				if "adjust" in self.data.get('host')[c]:
						 """* 
							>>>>SET CURSOR IN LOOP AFTER REMOVING EXTRA ADD ON STRINGS IN HOST<<<<
						 *"""
						 if '->' in self.data.get('host')[c]:
								cursor = self.data.get('host')[c].split('->')[0]
						 else:
								cursor = self.data.get('host')[c]
						 """* 
							>>>>CHECK FOR UNIQUE DEFINITIONS IN SCRIPT<<<<
						 *"""
						 if cursor in total2:
								print cursor
								"""* 
									>>>>CHECK FOR PREVENTING MULTIPLE DEFINITIONS OF DIFFERENT TYPES OF SDK_CLICK CALLS<<<<
							 	*"""
								if repeat_sdk_click and "sdk_click" in cursor:
								 print "repeat_sdk_click---------"
								 for z in self.data.get(dic)[c].keys():
										k = z.replace('\n', '')
										if k == "click_time":
												timmings[self.data.get('data')[c].get('source') + "_" + k] = self.data.get(dic)[c].get(
														 z)

										elif k == "created_at":
												if call_type == "sdk_click":

														 timmings[self.data.get('data')[c].get("source") + "_" + call_type + "_" + k] = \
																self.data.get(dic)[c].get(z)
												else:
														 timmings[call_type + "_" + k] = self.data.get(dic)[c].get(z)

										elif k == "sent_at":
												if call_type == "sdk_click":
														 timmings[self.data.get('data')[c].get("source") + "_" + call_type + "_" + k] = \
																self.data.get(dic)[c].get(z)
												else:
														 timmings[call_type + "_" + k] = self.data.get(dic)[c].get(z)


								else:
								 all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('start'))
								 all_adjust_call_function_defs.append('	method = "')
								 all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('method'))
								 all_adjust_call_function_defs.append('"\n	url = ')
								 all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('url'))

								 for dic in ["headers", "params", "data"]:
										all_adjust_call_function_defs.append('\n	' + dic + ' = {\n')
										if self.data.get(dic)[c]:
												dictionary = []
												for z in self.data.get(dic)[c].keys():
														 k = z.replace('\n', '')

														 if k == "app_token" or k == "app_updated_at":
																adjust_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k.lower().strip() == "client-sdk":
														 		adjust_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k == "click_time":
																timmings[self.data.get('data')[c].get('source') + "_" + k] = self.data.get(dic)[
																 c].get(z)

														 elif k == "created_at":
																if call_type == "sdk_click":
																 repeat_sdk_click = True
																 timmings[
																		self.data.get('data')[c].get("source") + "_" + call_type + "_" + k] = \
																		self.data.get(dic)[c].get(z)
																else:
																 timmings[call_type + "_" + k] = self.data.get(dic)[c].get(z)

														 elif k == "sent_at":
																if call_type == "sdk_click":
																 repeat_sdk_click = True
																 timmings[
																		self.data.get('data')[c].get("source") + "_" + call_type + "_" + k] = \
																		self.data.get(dic)[c].get(z)
																else:
																 timmings[call_type + "_" + k] = self.data.get(dic)[c].get(z)

														 elif k == "Authorization" and AUTH == False:
																report.append(
																 "AUTHORIZATION FOUND IN APP---- PLEASE VERIFY SECRET KEY AND id.")
																getAuth = subprocess.check_output(['python' ,'reference/get_adjust_auth.py'])
																adjust_campaign_data["secret_id"] = getAuth.split("\n")[0].strip()
																adjust_campaign_data["secret_key"] = getAuth.split("\n")[1].strip()
																AUTH = True

														 elif k == "package_name" or k == "app_version" or k == "app_version_short" or k == "bundle_id":
																self.main_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k == "install_receipt":
																receipt = self.data.get(dic)[c].get(z)
																from iap import validate_receipt

														 elif platform == "ios" and k == "User-Agent":
																self.main_campaign_data["app_name"] = \
																 self.data.get(dic)[c].get(z).split("/", 1)[0]

														 if Adjust.definitions.get(cursor).get(dic).get(k):
																if k == "details":
																 dictionary.append(
																		'		"' + k + '" : json.dumps(' + self.data.get(dic)[c].get(
																				z) + '),\n')
																elif Adjust.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
																 dictionary.append(
																		'		"' + k + '" : "' + self.data.get(dic)[c].get(z) + '",\n')

																elif Adjust.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
																 dictionary.append(
																		'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

																else:
																 dictionary.append(
																		'		"' + k + '" : ' + Adjust.definitions.get(cursor).get(dic).get(
																				k) + ',\n')

														 else:
																if not k in ["Host", "Content-Length", "Connection", "click_time", "Authorization"]:
																 if not k+" in "+cursor in newKeysFound:
																		newKeysFound.append(k+" in "+cursor)
																		report.append(
																				"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																						 cursor))
																		outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																		confirmed_value=""
																		confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																		confirmed_value=confirmed_value.join(confirmed_lines)
																		valueType = str(outP).rsplit("\n",2)[1].strip()
																		if valueType=="Dynamic":
																				dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																		else:
																				dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

												dictionary.sort()
												all_adjust_call_function_defs.extend(dictionary)

										all_adjust_call_function_defs.append('\n		}')

								 if Adjust.definitions.get(cursor).get('conditional_statements'):
										all_adjust_call_function_defs.append(
												Adjust.definitions.get(cursor).get('conditional_statements'))
								 all_adjust_call_function_defs.append("\n\n	")
								 """* 
									>>>>ADD AUTHORIZATION IN SCRIPT IF PRESENT IN APP<<<<
								 *"""
								 if AUTH:
										all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('auth'))
								 else:
										all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('auth').replace("headers[","# headers["))
								 all_adjust_call_function_defs.append(Adjust.definitions.get(cursor).get('return'))
								 all_adjust_call_function_defs.append("\n\n")
								 total2.remove(cursor)

		"""* 
			>>>>VALIDATING INSTALL RECEIPT IF PRESENT IN APP<<<<
		*"""
		if receipt:
				try:
						 valid = validate_receipt(receipt)
				except:
						 valid = False

				if valid:
						 ver = valid[0].get("application_version")
						 ver2 = valid[0].get("original_application_version")
						 if self.main_campaign_data.get('app_version_short'):
								if not ver == self.main_campaign_data.get(
										'app_version_short') and not ver2 == self.main_campaign_data.get(
								 'app_version_short') and not ver == self.main_campaign_data.get(
								 'app_version') and not ver2 == self.main_campaign_data.get('app_version'):
								 print "Install Recipt outdated, please check versions"
								 report.append("------>>>>>>>Install Recipt outdated, please check versions<<<<<<<--------")
						 report.append(
								"Install receipt validated successfully.\n Please check data and match version in the receipt data with logs.")
						 report.append("Receipt_data=" + str(valid[0]))
				else:
						 report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		"""* 
			>>>>WARNINGS AND ERROR LOGGING IN ADJUST<<<<
		*"""
		if any("reftag" in ref for ref in timmings.keys()) and any("install_referrer" in ir for ir in timmings.keys()):
				report.append(
						 "Script contains both install referrer and reftag sdk_clicks, kimdly match data for both, as their might be ambiguity while matching data.")
		if not valid:
				print "------>>>>>>>>INVALID INSTALL RECEIPT PLEASE CHECK.<<<<<<<<<----------"
				report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		print valid
		return all_adjust_call_function_defs, adjust_campaign_data, self.main_campaign_data, timmings, valid

	
	##################################################################################################
	"""* 
		>>>>MAT RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def mat_definition_data(self,key):
		"""* 
			>>>>GET MAT KEY FROM USER THROUGH POP UP<<<<
		*"""
		matKey = subprocess.check_output(['python' ,'reference/get_mat_decryption_key.py']).strip()
		self.main_campaign_data = {}
		all_mat_call_function_defs = []
		mat_campaign_data = {}
		timmings = {}
		total3 = []
		receipt = None
		valid = True

		mat_campaign_data['key'] = matKey
		print "Your MAT decryption key is:- "+matKey

		"""* 
			>>>>ITERATE AND PROCESS THROUGH DECRYPTED DATA FROM MAT CALL<<<<
		*"""
		def process_mat_decrypted_data(data_value, cursor):
			decrypted_data_val = []
			decrypted_data_val.append('\n	data_value = {\n')
			for dKeys in sorted(data_value.keys()):
				if dKeys == "app_name" or dKeys == "app_version_name" or dKeys == "app_version":
					self.main_campaign_data[dKeys] = data_value.get(dKeys)
				elif dKeys=="bundle_id":
					self.main_campaign_data["package_name"] = data_value.get(dKeys)
				
				if mat.definitions.get(cursor).get("data_value").get(dKeys):
					if mat.definitions.get(cursor).get("data_value").get(dKeys) == "CONSTANT":
						decrypted_data_val.append(
							'		"' + dKeys + '" : "' + data_value.get(dKeys) + '",\n')

					elif mat.definitions.get(cursor).get("data_value").get(dKeys) == "BOOLEAN":
						decrypted_data_val.append(
							'		"' + dKeys + '" : ' + str(data_value.get(dKeys)) + ',\n')

					else:
						decrypted_data_val.append(
							'		"' + dKeys + '" : ' + mat.definitions.get(cursor).get("data_value").get(
									dKeys) + ',\n')
				else:
					if not dKeys+" in "+cursor in newKeysFound:
						try:
							newKeysFound.append(dKeys+" in "+cursor)
							report.append("NEW KEY FOUND---> " + str(dKeys) + " in encrypted data of url:-" + str(cursor))
							outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(dKeys), '-value='+str(data_value.get(dKeys)).strip()])
							confirmed_value=""
							confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
							confirmed_value=confirmed_value.join(confirmed_lines)
							valueType = str(outP).rsplit("\n",2)[1].strip()
							if valueType=="Dynamic":
								decrypted_data_val.append('		"' + dKeys + '" : ' +confirmed_value+ ',\n')
							else:
								decrypted_data_val.append('		"' + dKeys + '" : "' +confirmed_value+ '",\n')
						except:
							decrypted_data_val.append('		"' + dKeys + '" : Could not get unknown_key,\n')

			decrypted_data_val.append('	}\n')
			return decrypted_data_val 				

		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		for y in self.data.get('host'):
				if 'engine.mobileapptracking' in y:
						 total3.append(y)
		total3 = list(set(total3))

		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "engine.mobileapptracking" in self.data.get('host')[c]:
						 DD=False
						 if "->" in self.data.get('host')[c]:
							 cursor = self.data.get('host')[c].split("->")[0]
						 else:
						 	 cursor = self.data.get('host')[c]

						 if cursor in total3:
								 print cursor
								 all_mat_call_function_defs.append(mat.definitions.get(cursor).get('start'))
								 all_mat_call_function_defs.append('	method = "')
								 all_mat_call_function_defs.append(mat.definitions.get(cursor).get('method'))
								 all_mat_call_function_defs.append('"\n	url = ')
								 all_mat_call_function_defs.append(mat.definitions.get(cursor).get('url'))

								 for dic in ["headers", "params", "data"]:
										all_mat_call_function_defs.append('\n	' + dic + ' = {\n')
										if self.data.get(dic)[c]:
												dictionary = []
												for z in self.data.get(dic)[c].keys():
														 k = z.replace('\n', '')

														 if k == "advertiser_id" or k == "sdk" or k == "ver":
																mat_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k == "package_name":
																self.main_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k == "apple_receipt":
																receipt = self.data.get(dic)[c].get(z)
																from iap import validate_receipt

														 if k=="data" and dic=="params":
														 	encryp_data = self.data.get(dic)[c].get("data").strip() 
														 	data_value = decrypt_mat_data(key=mat_campaign_data.get('key'), data=encryp_data)
														 	DD = process_mat_decrypted_data(data_value, cursor)

														 elif mat.definitions.get(cursor).get(dic).get(k):
																if mat.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
																 dictionary.append(
																		'		"' + k + '" : "' + self.data.get(dic)[c].get(z) + '",\n')

																elif mat.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
																 dictionary.append(
																		'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

																else:
																 dictionary.append(
																		'		"' + k + '" : ' + mat.definitions.get(cursor).get(dic).get(
																				k) + ',\n')

														 else:
																if not k in ["Host", "Content-Length", "Connection", "click_time", "Authorization", "open_log_id", "last_open_log_id"]:
																 if not k+" in "+cursor in newKeysFound:
																		newKeysFound.append(k+" in "+cursor)
																		report.append(
																				"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																						 cursor))
																		outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																		confirmed_value=""
																		confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																		confirmed_value=confirmed_value.join(confirmed_lines)
																		valueType = str(outP).rsplit("\n",2)[1].strip()
																		if valueType=="Dynamic":
																				dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																		else:
																				dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

												dictionary.sort()
												all_mat_call_function_defs.extend(dictionary)

										all_mat_call_function_defs.append('\n		}')

								 if DD:
								 	all_mat_call_function_defs.extend(DD)

								 if mat.definitions.get(cursor).get('conditional_statements'):
										all_mat_call_function_defs.append(
												mat.definitions.get(cursor).get('conditional_statements'))
								 all_mat_call_function_defs.append("\n\n	")
								 all_mat_call_function_defs.append(mat.definitions.get(cursor).get('return'))
								 all_mat_call_function_defs.append("\n\n")
								 total3.remove(cursor)
		"""* 
			>>>>VALIDATING INSTALL RECEIPT IF PRESENT IN APP<<<<
		*"""
		if receipt:
				try:
						 valid = validate_receipt(receipt)
				except:
						 valid = False

				if valid:
						 ver = valid[0].get("application_version")
						 ver2 = valid[0].get("original_application_version")
						 if self.main_campaign_data.get('app_version_short'):
								if not ver == self.main_campaign_data.get(
										'app_version_short') and not ver2 == self.main_campaign_data.get(
								 'app_version_short') and not ver == self.main_campaign_data.get(
								 'app_version') and not ver2 == self.main_campaign_data.get('app_version'):
								 print "Install Recipt outdated, please check versions"
								 report.append("------>>>>>>>Install Recipt outdated, please check versions<<<<<<<--------")
						 report.append(
								"Install receipt validated successfully.\n Please check data and match version in the receipt data with logs.")
						 report.append("Receipt_data=" + str(valid[0]))
				else:
						 report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		if not valid:
				print "------>>>>>>>>INVALID INSTALL RECEIPT PLEASE CHECK.<<<<<<<<<----------"
				report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		return all_mat_call_function_defs, mat_campaign_data, self.main_campaign_data, timmings, valid


	##################################################################################################
	"""* 
		>>>>APSALAR RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def apsalar_definition_data(self):
		"""* 
			>>>>GET APSALAR KEY FROM USER THROUGH POP UP<<<<
		*"""
		apsalarkey = subprocess.check_output(['python' ,'reference/apsalar_secret.py']).strip()
		self.main_campaign_data = {}
		all_apsalar_call_function_defs = []
		apsalar_campaign_data = {}
		timmings = {}
		total2 = []
		receipt = None
		valid = True
		"""* 
			>>>>ELIMINATING DOMAIN CONFLICTS AND EXTRA ADD ON STRINGS IN HOST TO GET DEFINITION FROM REFERENCE FILE<<<<
		*"""
		for y in self.data.get('host'):
			if 'apsalar' in y:
				 if str(y)[:6]=="e-ssl.":
				 	y=y[6:]
				 if str(y)[:2]=="e.":
				 	y=y[2:]

				 if '->' in y:
						total2.append(y.split('->')[0])
				 else:
						total2.append(y)

		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		total2 = list(set(total2))
		apsalar_campaign_data['key'] = apsalarkey
		print "Your apsalar secret key is:- "+apsalarkey
		print total2

		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "apsalar" in self.data.get('host')[c]:
						 if '->' in self.data.get('host')[c]:
								cursor = self.data.get('host')[c].split('->')[0]
						 else:
								cursor = self.data.get('host')[c]
						 if str(cursor)[:6]=="e-ssl.":
						 	cursor=cursor[6:]
						 if str(cursor)[:2]=="e.":
						 	cursor=cursor[2:]
						 if cursor in total2:
								 print cursor
								 all_apsalar_call_function_defs.append(Apsalar.definitions.get(cursor).get('start'))
								 all_apsalar_call_function_defs.append('	method = "')
								 all_apsalar_call_function_defs.append(self.data.get("method")[c])
								 all_apsalar_call_function_defs.append('"\n	url = ')
								 all_apsalar_call_function_defs.append('"'+self.data.get("url")[c].replace("https","http")+'"')

								 for dic in ["headers", "params", "data"]:
										all_apsalar_call_function_defs.append('\n	' + dic + ' = {\n')
										if self.data.get(dic)[c]:
												dictionary = []
												for z in self.data.get(dic)[c].keys():
														 k = z.replace('\n', '')

														 if k == "sdk" or k == "a":
																apsalar_campaign_data[k] = self.data.get(dic)[c].get(z)


														 elif k == "i" or k == "av":
																self.main_campaign_data[k] = self.data.get(dic)[c].get(z)

														 elif k == "install_receipt":
																receipt = self.data.get(dic)[c].get(z)
																from iap import validate_receipt

														 elif platform == "ios" and k == "User-Agent":
																self.main_campaign_data["app_name"] = \
																self.data.get(dic)[c].get(z).split("/", 1)[0]


														 if k=="h":
														 	pass

														 elif Apsalar.definitions.get(cursor).get(dic).get(k):
																
																if Apsalar.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
																 dictionary.append(
																		'		"' + k + '" : "' + self.data.get(dic)[c].get(z) + '",\n')

																elif Apsalar.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
																 dictionary.append(
																		'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

																else:
																 dictionary.append(
																		'		"' + k + '" : ' + Apsalar.definitions.get(cursor).get(dic).get(
																				k) + ',\n')

														 else:
																if not k in ["Host", "Content-Length", "Connection", "Accept", "custom_user_id"]:
																 if not k+" in "+cursor in newKeysFound:
																		newKeysFound.append(k+" in "+cursor)
																		report.append(
																				"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																						 cursor))
																		outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																		confirmed_value=""
																		confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																		confirmed_value=confirmed_value.join(confirmed_lines)
																		valueType = str(outP).rsplit("\n",2)[1].strip()
																		if valueType=="Dynamic":
																				dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																		else:
																				dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

												dictionary.sort()
												all_apsalar_call_function_defs.extend(dictionary)

										all_apsalar_call_function_defs.append('\n		}')

								 if Apsalar.definitions.get(cursor).get('conditional_statements'):
										all_apsalar_call_function_defs.append(
												Apsalar.definitions.get(cursor).get('conditional_statements'))
								 all_apsalar_call_function_defs.append("\n\n	")
								 all_apsalar_call_function_defs.append(Apsalar.definitions.get(cursor).get('return'))
								 all_apsalar_call_function_defs.append("\n\n")
								 total2.remove(cursor)

		"""* 
			>>>>VALIDATING INSTALL RECEIPT IF PRESENT IN APP<<<<
		*"""
		if receipt:
			try:
				valid = validate_receipt(receipt)
			except:
				valid = False

			print valid
			if valid:
				ver = valid[0].get("application_version")
				ver2 = valid[0].get("original_application_version")
				if self.main_campaign_data.get('app_version_short'):
					if not ver == self.main_campaign_data.get(
							'app_version_short') and not ver2 == self.main_campaign_data.get(
					 'app_version_short') and not ver == self.main_campaign_data.get(
					 'app_version') and not ver2 == self.main_campaign_data.get('app_version'):
					 print "Install Recipt outdated, please check versions"
					 report.append("------>>>>>>>Install Recipt outdated, please check versions<<<<<<<--------")
				report.append(
					"Install receipt validated successfully.\n Please check data and match version in the receipt data with logs.")
				report.append("Receipt_data=" + str(valid[0]))
			else:
					report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		if not valid:
				print "------>>>>>>>>INVALID INSTALL RECEIPT PLEASE CHECK.<<<<<<<<<----------"
				report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		return all_apsalar_call_function_defs, apsalar_campaign_data, self.main_campaign_data, timmings, valid

	
	##################################################################################################
	"""* 
		>>>>ADFORCE RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def adforce_definition_data(self):
		self.main_campaign_data = {}
		all_adforce_call_function_defs = []
		adforce_campaign_data = {}
		timmings = {}
		total2 = []
		receipt = None
		valid = True
		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		total2 = list(set(self.data.get('host')))
		print total2

		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "adforce" in self.data.get('host')[c]:
					 cursor = self.data.get('host')[c]
					 print cursor
					 if cursor in total2:
						 all_adforce_call_function_defs.append(Adforce.definitions.get(cursor).get('start'))
						 all_adforce_call_function_defs.append('	method = "')
						 all_adforce_call_function_defs.append(self.data.get("method")[c])
						 all_adforce_call_function_defs.append('"\n	url = ')
						 for checkD in adDoubleFlag:
						 	if checkD in self.data.get("url")[c]:
						 		self.data.get("url")[c] = self.data.get("url")[c].replace("/ad/","/ad//")
						 
						 all_adforce_call_function_defs.append('"'+self.data.get("url")[c].replace("https","http")+'"')

						 for dic in ["headers", "params", "data"]:
								all_adforce_call_function_defs.append('\n	' + dic + ' = {\n')
								if self.data.get(dic)[c]:
										dictionary = []
										for z in self.data.get(dic)[c].keys():
												 k = z.replace('\n', '')

												 if k == "_app" or k == "_sdk_ver" or k=="_gms_version":
														adforce_campaign_data[k] = self.data.get(dic)[c].get(z)


												 elif k == "_bundle_id" or k == "_bv":
														self.main_campaign_data[k] = self.data.get(dic)[c].get(z)

												 elif k == "install_receipt":
														receipt = self.data.get(dic)[c].get(z)
														from iap import validate_receipt

												 elif platform == "ios" and k == "User-Agent":
														self.main_campaign_data["app_name"] = \
														self.data.get(dic)[c].get(z).split("/", 1)[0]

												 if Adforce.definitions.get(cursor).get(dic).get(k):
														if Adforce.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
														 dictionary.append(
																'		"' + k + '" : "' + self.data.get(dic)[c].get(z) + '",\n')

														elif Adforce.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
														 dictionary.append(
																'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

														else:
														 dictionary.append(
																'		"' + k + '" : ' + Adforce.definitions.get(cursor).get(dic).get(
																		k) + ',\n')

												 else:
														if not k in ["Host", "Content-Length", "Connection"]:
														 if not k+" in "+cursor in newKeysFound:
																newKeysFound.append(k+" in "+cursor)
																report.append(
																		"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																				 cursor))
																outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																confirmed_value=""
																confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																confirmed_value=confirmed_value.join(confirmed_lines)
																valueType = str(outP).rsplit("\n",2)[1].strip()
																if valueType=="Dynamic":
																		dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																else:
																		dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

										dictionary.sort()
										all_adforce_call_function_defs.extend(dictionary)

								all_adforce_call_function_defs.append('\n		}')

						 if Adforce.definitions.get(cursor).get('conditional_statements'):
								all_adforce_call_function_defs.append(
										Adforce.definitions.get(cursor).get('conditional_statements'))
						 all_adforce_call_function_defs.append("\n\n	")
						 all_adforce_call_function_defs.append(Adforce.definitions.get(cursor).get('return'))
						 all_adforce_call_function_defs.append("\n\n")
						 total2.remove(cursor)

		"""* 
			>>>>VALIDATING INSTALL RECEIPT IF PRESENT IN APP<<<<
		*"""
		if receipt:
			try:
				valid = validate_receipt(receipt)
			except:
				valid = False

			print valid
			if valid:
				ver = valid[0].get("application_version")
				ver2 = valid[0].get("original_application_version")
				if self.main_campaign_data.get('app_version_short'):
					if not ver == self.main_campaign_data.get(
							'app_version_short') and not ver2 == self.main_campaign_data.get(
					 'app_version_short') and not ver == self.main_campaign_data.get(
					 'app_version') and not ver2 == self.main_campaign_data.get('app_version'):
					 print "Install Recipt outdated, please check versions"
					 report.append("------>>>>>>>Install Recipt outdated, please check versions<<<<<<<--------")
				report.append(
					"Install receipt validated successfully.\n Please check data and match version in the receipt data with logs.")
				report.append("Receipt_data=" + str(valid[0]))
			else:
					report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		if not valid:
				print "------>>>>>>>>INVALID INSTALL RECEIPT PLEASE CHECK.<<<<<<<<<----------"
				report.append("INVALID INSTALL RECEIPT PLEASE CHECK.")

		return all_adforce_call_function_defs, adforce_campaign_data, self.main_campaign_data, timmings, valid

	
	##################################################################################################
	"""* 
		>>>>TENJIN RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def tenjin_definition_data(self):
		self.main_campaign_data = {}
		all_tenjin_call_function_defs = []
		tenjin_campaign_data = {}
		timmings = {}
		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""
		total2 = []
		total2 = list(set(self.data.get('host')))
		print total2
		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "tenjin" in self.data.get('host')[c]:
					 cursor = self.data.get('host')[c]
					 print cursor
					 if cursor in total2:
						 all_tenjin_call_function_defs.append(Tenjin.definitions.get(cursor).get('start'))
						 all_tenjin_call_function_defs.append('	method = "')
						 all_tenjin_call_function_defs.append(self.data.get("method")[c])
						 all_tenjin_call_function_defs.append('"\n	url = ')
						 all_tenjin_call_function_defs.append('"'+self.data.get("url")[c].replace("https","http")+'"')

						 for dic in ["headers", "params", "data"]:
								all_tenjin_call_function_defs.append('\n	' + dic + ' = {\n')
								if self.data.get(dic)[c]:
										dictionary = []
										for z in self.data.get(dic)[c].keys():
												 k = z.replace('\n', '')

												 if k == "Authorization" or k == "sdk_version" or k=="api_key":
														tenjin_campaign_data[k] = self.data.get(dic)[c].get(z)


												 elif k == "bundle_id" or k == "app_version":
														self.main_campaign_data[k] = self.data.get(dic)[c].get(z)


												 elif Tenjin.definitions.get(cursor).get(dic).get(k):
														
														if Tenjin.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
														 dictionary.append(
																'		"' + k + '" : "' + self.data.get(dic)[c].get(z) + '",\n')

														elif Tenjin.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
														 dictionary.append(
																'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

														else:
														 dictionary.append(
																'		"' + k + '" : ' + Tenjin.definitions.get(cursor).get(dic).get(
																		k) + ',\n')

												 else:
														if not k in ["Host", "Content-Length", "Connection"]:
														 if not k+" in "+cursor in newKeysFound:
																newKeysFound.append(k+" in "+cursor)
																report.append(
																		"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																				 cursor))
																outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																confirmed_value=""
																confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																confirmed_value=confirmed_value.join(confirmed_lines)
																valueType = str(outP).rsplit("\n",2)[1].strip()
																if valueType=="Dynamic":
																		dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																else:
																		dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

										dictionary.sort()
										all_tenjin_call_function_defs.extend(dictionary)

								all_tenjin_call_function_defs.append('\n		}')

						 if Tenjin.definitions.get(cursor).get('conditional_statements'):
								all_tenjin_call_function_defs.append(
										Tenjin.definitions.get(cursor).get('conditional_statements'))
						 all_tenjin_call_function_defs.append("\n\n	")
						 all_tenjin_call_function_defs.append(Tenjin.definitions.get(cursor).get('return'))
						 all_tenjin_call_function_defs.append("\n\n")
						 total2.remove(cursor)

		return all_tenjin_call_function_defs, tenjin_campaign_data, self.main_campaign_data, timmings


	##################################################################################################
	"""* 
		>>>>BRANCH RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def branch_definition_data(self):
		self.main_campaign_data = {}
		all_branch_call_function_defs = []
		branch_campaign_data = {}
		timmings = {}
		total5 = []
		"""* 
			>>>>RESTORING ONLY UNIQUE HOSTS TO TOTAL FOR UNIQUE DIFINITIONS IN SCRIPT<<<<
		*"""

		for z in self.data.get('host'):
			if 'branch' in z:
 					 if 'api.' in z:
							z=z.replace('api.','api2.')
					 if '/standard/' in z:
							total5.append("api2.branch.io/v2/event/standard")
					 elif '/custom/' in z:
					 		total5.append("api2.branch.io/v2/event/custom")
					 else:
							total5.append(z)
		# total2 = []
		total5 = list(set(total5))
		# print total5
		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "branch" in self.data.get('host')[c]:
					 cursor = self.data.get('host')[c]
					 if 'standard' in self.data.get('host')[c]:
					 	cursor='api2.branch.io/v2/event/standard'
					 if 'custom' in self.data.get('host')[c]:
					 	cursor='api2.branch.io/v2/event/custom'

					 finalUrl=self.data.get("url")[c].replace("https","http")
					 if 'branch' in cursor:
						if 'api.' in cursor:
							cursor=cursor.replace('api.','api2.')
							finalUrl=finalUrl.replace('api2.','api.')

					 # print cursor
					 # print total5
					 if cursor in total5:
					 	 # print "-------definitions---------------"
					 	 # print branch.definitions.get(cursor)
						 all_branch_call_function_defs.append(branch.definitions.get(cursor).get('start'))
						 all_branch_call_function_defs.append('	method = "')
						 all_branch_call_function_defs.append(self.data.get("method")[c])
						 all_branch_call_function_defs.append('"\n	url = ')
						 all_branch_call_function_defs.append('"'+finalUrl+'"')
						 # print all_branch_call_function_defs
						 
						 for dic in ["headers", "params", "data"]:
								all_branch_call_function_defs.append('\n	' + dic + ' = {\n')
								if self.data.get(dic)[c]:
										dictionary = []
										for z in self.data.get(dic)[c].keys():
												 k = z.replace('\n', '')
												 # print "--------------key--------------"
												 # print k
												 if k == "branch_key" or k == "sdk":
														branch_campaign_data[k] = self.data.get(dic)[c].get(z)


												 elif k == "cd":
														self.main_campaign_data['package_name'] = self.data.get(dic)[c].get(z).get('pn')
														# print self.main_campaign_data

												 elif k == "app_version":
														self.main_campaign_data[k] = self.data.get(dic)[c].get(z)



												 if branch.definitions.get(cursor).get(dic).get(k):
														# print branch.definitions.get(cursor).get(dic).get(k)
														# print self.data.get(dic)[c].get(z)

														if k == "instrumentation":
															all_branch_call_function_defs.append('		"' + k + '" : {\n')
															for r in sorted(self.data.get(dic)[c].get(k).keys()):
																
																 all_branch_call_function_defs.append(
																		'		"' + r + '" : "' + self.data.get(dic)[c].get(k).get(r) + '",\n')

															all_branch_call_function_defs.append('		},\n')

														elif k == "metadata":
															all_branch_call_function_defs.append('		"' + k + '" : {\n')
															for r in sorted(self.data.get(dic)[c].get(k).keys()):
																
																 all_branch_call_function_defs.append(
																		'		"' + r + '" : "' + self.data.get(dic)[c].get(k).get(r) + '",\n')

															all_branch_call_function_defs.append('		},\n')
												 		
														
														elif k == "cd":
														 all_branch_call_function_defs.append('		"' + k + '" : {\n')
														 for r in sorted(self.data.get(dic)[c].get(k).keys()):
																if str(self.data.get(dic)[c].get(k).get(r)) == '-1':
																 all_branch_call_function_defs.append(
																		'								"' + r + '" : "-1",\n')
																elif str(r) == "pn":
																 all_branch_call_function_defs.append(
																		'								"pn": campaign_data.get("package_name"),\n')
																
														 all_branch_call_function_defs.append('		},\n')

														elif k == "user_data":
														 all_branch_call_function_defs.append('		"' + k + '" : {\n')
														 for r in sorted(self.data.get(dic)[c].get(k).keys()):
																print r
																if str(branch.definitions.get(cursor).get(dic).get(k).get(r)) == "CONSTANT":
																 all_branch_call_function_defs.append(
																		'		"' + r + '" : "' + str(self.data.get(dic)[c].get(k).get(r)) + '",\n')

																elif str(branch.definitions.get(cursor).get(dic).get(k).get(r)) == "BOOLEAN":
																 all_branch_call_function_defs.append(
																		'		"' + r + '" : ' + str(self.data.get(dic)[c].get(k).get(r)) + ',\n')

																else:
																	if branch.definitions.get(cursor).get(dic).get(k).get(r):
																		all_branch_call_function_defs.append(
																		'		"' + r + '" : ' + branch.definitions.get(cursor).get(dic).get(
																				k).get(r) + ',\n')
																	else:
																 		all_branch_call_function_defs.append(
																		'		"' + r + '" : ' + str(self.data.get(dic)[c].get(k).get(r)) + ',\n')
																
														 all_branch_call_function_defs.append('		},\n')
														elif k == "advertising_ids":
														 all_branch_call_function_defs.append('		"' + k + '" : {\n')
														 all_branch_call_function_defs.append(
																		'								"aaid": device_data.get("adid"),\n')
																
														 all_branch_call_function_defs.append('		},\n')
														 
														elif branch.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
														 dictionary.append(
																'		"' + k + '" : "' + str(self.data.get(dic)[c].get(z)) + '",\n')

														elif branch.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
														 dictionary.append(
																'		"' + k + '" : ' + str(self.data.get(dic)[c].get(z)) + ',\n')

														else:
														 dictionary.append(
																'		"' + k + '" : ' + branch.definitions.get(cursor).get(dic).get(
																		k) + ',\n')

												 else:
														if not k in ["Host", "Content-Length", "Connection"]:
														 if not k+" in "+cursor in newKeysFound:
																newKeysFound.append(k+" in "+cursor)
																report.append(
																		"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(
																				 cursor))
																outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(z))])
																confirmed_value=""
																confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																confirmed_value=confirmed_value.join(confirmed_lines)
																valueType = str(outP).rsplit("\n",2)[1].strip()
																if valueType=="Dynamic":
																		dictionary.append('		"' + k + '" : '+confirmed_value+',\n')
																else:
																		dictionary.append('		"' + k + '" : "'+confirmed_value+'",\n')

										dictionary.sort()
										all_branch_call_function_defs.extend(dictionary)

								all_branch_call_function_defs.append('\n		}')

						 if branch.definitions.get(cursor).get('conditional_statements'):
								all_branch_call_function_defs.append(
										branch.definitions.get(cursor).get('conditional_statements'))
						 all_branch_call_function_defs.append("\n\n	")
						 all_branch_call_function_defs.append(branch.definitions.get(cursor).get('return'))
						 all_branch_call_function_defs.append("\n\n")
						 total5.remove(cursor)
		# print self.main_campaign_data
		return all_branch_call_function_defs, branch_campaign_data, self.main_campaign_data, timmings

	"""* 
		>>>>AD-BRIX RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def adbrix_definition_data(self, key=None):
		self.main_campaign_data = {}
		self.ad_event_delta = {}
		adbrix_campaign_data = {}
		timmings = {}
		all_adbrix_call_function_defs = []
		total3 = []
		"""* 
			>>>>GET AD-BRIX KEYS AND DETERMINE TYPE OF KEY IN AD-BRIX[MULTIPART/REFERRAL]<<<<
		*"""
		if "\n" in key:
			adbrix_campaign_data['key_multipart'] = str(key).split("\n")[0].strip()
			adbrix_campaign_data['iv_multipart'] = str(key).split("\n")[1].strip()
			print "Key and IV for multipart:-"
			print adbrix_campaign_data['key_multipart']
			print adbrix_campaign_data['iv_multipart']
		else:
			adbrix_campaign_data['key_getreferral'] = str(key) + str(key)
			adbrix_campaign_data['iv_getreferral'] = str(key)
			print "Key for referral:-"
			print adbrix_campaign_data['key_getreferral']

		"""* 
			>>>>ITERATING THROUGH ALL THE DECRYPTED DATA KEYS FROM AD-BRIX ENCRYPTED DATA<<<<
		*"""
		def process_decrypted_data(data_value, cursor):
				if Adbrix.definitions.get(cursor).get("data_value"):
						 decrypted_data_val = []
						 decrypted_data_val.append('	data_value = {\n')
						 for eKeys in sorted(data_value.keys()):
								if Adbrix.definitions.get(cursor).get("data_value").get(eKeys):
								 if isinstance(data_value.get(eKeys), dict):
								 		
										decrypted_data_val.append('		"' + eKeys + '" : {')
										for subdict in sorted(data_value.get(eKeys).keys()):
												

												if eKeys == "cohort_info":					
														
													decrypted_data_val.append('	"' + subdict + '" : "' + str(
																						 data_value.get(eKeys).get(subdict)) + '",\n	')

													
												elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(subdict):
														 if subdict == "install_datetime":
																self.ad_event_delta["event_date_1"] = data_value.get('adbrix_user_info').get(
																 "install_datetime")
														 if Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(
																 subdict) == "CONSTANT":
																decrypted_data_val.append(
																 '	"' + subdict + '" : "' + str(data_value.get(eKeys).get(subdict)) + '",\n		')

														 elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(
																 subdict) == "BOOLEAN":
																decrypted_data_val.append(
																 '	"' + subdict + '" : ' + str(data_value.get(eKeys).get(subdict)) + ',\n		')
														 else:
																if isinstance(Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(
																		subdict), dict):
																 decrypted_data_val.append('	"' + subdict + '" : {')
																 for sub_sub_dict in sorted(data_value.get(eKeys).get(subdict).keys()):
																		if Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(
																						 subdict).get(sub_sub_dict) == "CONSTANT":
																				decrypted_data_val.append('	"' + sub_sub_dict + '" : "' + str(
																						 data_value.get(eKeys).get(subdict).get(sub_sub_dict)) + '",\n	')

																		elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys).get(
																						 subdict).get(sub_sub_dict) == "BOOLEAN":
																				decrypted_data_val.append('	"' + sub_sub_dict + '" : ' + str(
																						 data_value.get(eKeys).get(subdict).get(sub_sub_dict)) + ',\n	')

																		else:
																				if not Adbrix.definitions.get(
																									cursor).get("data_value").get(eKeys).get(subdict).get(
																									sub_sub_dict):
																					 newKeysFound.append(subdict+" in "+sub_sub_dict+" in "+cursor)
																					 report.append("NEW KEY FOUND---> " + str(subdict) + "in dictionary" + str(
																							eKeys) + " in "+sub_sub_dict+" in encrypted data of url:-" + str(cursor))
																					 outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(data_value.get(eKeys).get(subdict))])
																					 confirmed_value=""
																					 confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																					 confirmed_value=confirmed_value.join(confirmed_lines)
																					 valueType = str(outP).rsplit("\n",2)[1].strip()
																					 if valueType=="Dynamic":
																							decrypted_data_val.append('"' + sub_sub_dict + '" : ' +confirmed_value+ ',\n')
																					 else:
																							decrypted_data_val.append('"' + sub_sub_dict + '" : "' +confirmed_value+ '",\n')
																				else:
																					 decrypted_data_val.append(
																							 '	"' + sub_sub_dict + '" : ' + Adbrix.definitions.get(
																									cursor).get("data_value").get(eKeys).get(subdict).get(
																									sub_sub_dict) + ',')
																 decrypted_data_val.append('},\n	')

																else:
																 decrypted_data_val.append(
																		'"' + subdict + '" : ' + Adbrix.definitions.get(cursor).get(
																				"data_value").get(eKeys).get(subdict) + ',\n	')
												else:
														 if subdict == "install_datetime" or subdict == "install_mdatetime":
																decrypted_data_val.append('"' + subdict + '" : "",')
														 else:
																if not subdict+" in "+cursor in newKeysFound:
																 newKeysFound.append(subdict+" in "+cursor)
																 report.append("NEW KEY FOUND---> " + str(subdict) + "in dictionary" + str(
																		eKeys) + " in encrypted data of url:-" + str(cursor))
																 outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(data_value.get(eKeys).get(subdict))])
																 confirmed_value=""
																 confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																 confirmed_value=confirmed_value.join(confirmed_lines)
																 valueType = str(outP).rsplit("\n",2)[1].strip()
																 if valueType=="Dynamic":
																		decrypted_data_val.append('"' + subdict + '" : ' +confirmed_value+ ',\n')
																 else:
																		decrypted_data_val.append('"' + subdict + '" : "' +confirmed_value+ '",\n')

										decrypted_data_val.append('},\n')

								 elif isinstance(data_value.get(eKeys), list):
										if len(data_value.get(eKeys)) >= 1:
												decrypted_data_val.append('		"' + eKeys + '" : [')
												for act in range(len(data_value.get(eKeys))):
														 if isinstance(act, dict):
																self.ad_event_delta['event_date'] = data_value.get(eKeys)[0].get('created_at')
																decrypted_data_val.append('{')
																for act_keys in sorted(data_value.get(eKeys)[act].keys()):
																 if Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0].get(
																				act_keys):
																		if Adbrix.definitions.get(cursor).get('data_value').get(eKeys)[0].get(
																						 act_keys) == "CONSTANT":
																				decrypted_data_val.append(
																						 '"' + act_keys + '":"' + data_value.get(eKeys)[act].get(
																								act_keys) + '",')

																		elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0].get(
																						 act_keys) == "BOOLEAN":
																				decrypted_data_val.append('"' + act_keys + '" : ' + str(
																						 data_value.get(eKeys)[act].get(act_keys)) + ',')

																		else:
																				decrypted_data_val.append('"' + act_keys + '" : ' +
																														Adbrix.definitions.get(cursor).get(
																																"data_value").get(eKeys)[0].get(
																																act_keys) + ',')
																decrypted_data_val.append('},')
														 else:
																if eKeys == "conversion_cache":
																 decrypted_data_val.append(
																		Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0])
																else:
																 self.ad_event_delta['event_date'] = data_value.get(eKeys)[0].get(
																		'created_at')
																 decrypted_data_val.append('{')
																 for act_keys in sorted(data_value.get(eKeys)[act].keys()):
																		if len(Adbrix.definitions.get(cursor).get("data_value").get(
																						 eKeys)) != 0:
																				if Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[
																						 0].get(act_keys):
																						 if Adbrix.definitions.get(cursor).get('data_value').get(eKeys)[
																								0].get(act_keys) == "CONSTANT":
																								decrypted_data_val.append(
																								 '"' + act_keys + '":"' + data_value.get(eKeys)[act].get(
																										act_keys) + '",')

																						 elif \
																								 Adbrix.definitions.get(cursor).get("data_value").get(
																										eKeys)[
																										0].get(act_keys) == "BOOLEAN":
																								decrypted_data_val.append('"' + act_keys + '" : ' + str(
																								 data_value.get(eKeys)[act].get(act_keys)) + ',')

																						 else:
																								decrypted_data_val.append('"' + act_keys + '" : ' +
																																		 Adbrix.definitions.get(
																																				cursor).get("data_value").get(
																																				eKeys)[0].get(act_keys) + ',')
																		else:
																				decrypted_data_val.append('"' + act_keys + '":[],')
																 decrypted_data_val.append('},')

												decrypted_data_val.append('],\n')
										# else:
										# 	decrypted_data_val.append('		"'+eKeys+'" : '+str(data_value.get(eKeys))+',\n')
										else:
												decrypted_data_val.append('		"' + eKeys + '" : [],\n')

								 else:
										if Adbrix.definitions.get(cursor).get("data_value").get(eKeys) == "CONSTANT":
												decrypted_data_val.append(
														 '		"' + eKeys + '" : "' + str(data_value.get(eKeys)) + '",\n')

										elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys) == "BOOLEAN":
												decrypted_data_val.append(
														 '		"' + eKeys + '" : ' + str(data_value.get(eKeys)) + ',\n')

										else:
												decrypted_data_val.append(
														 '		"' + eKeys + '" : ' + Adbrix.definitions.get(cursor).get("data_value").get(
																eKeys) + ',\n')

								elif isinstance(data_value.get(eKeys), list):
								 if len(data_value.get(eKeys)) >= 1:
										decrypted_data_val.append('		"' + eKeys + '" : [')
										for act in range(len(data_value.get(eKeys))):
												if isinstance(act, dict):
														 self.ad_event_delta['event_date'] = data_value.get(eKeys)[0].get('created_at')
														 decrypted_data_val.append('{')
														 for act_keys in sorted(data_value.get(eKeys)[act].keys()):
																if Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0].get(act_keys):
																 if Adbrix.definitions.get(cursor).get('data_value').get(eKeys)[0].get(
																				act_keys) == "CONSTANT":
																		decrypted_data_val.append(
																				'"' + act_keys + '":"' + data_value.get(eKeys)[act].get(
																						 act_keys) + '",')

																 elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0].get(
																				act_keys) == "BOOLEAN":
																		decrypted_data_val.append('"' + act_keys + '" : ' + str(
																				data_value.get(eKeys)[act].get(act_keys)) + ',')

																 else:
																		decrypted_data_val.append('"' + act_keys + '" : ' +
																												 Adbrix.definitions.get(cursor).get(
																														"data_value").get(eKeys)[0].get(
																														act_keys) + ',')
														 decrypted_data_val.append('},')
												else:
														 if eKeys == "conversion_cache":
																decrypted_data_val.append(
																 Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0])
														 else:
																self.ad_event_delta['event_date'] = data_value.get(eKeys)[0].get('created_at')
																decrypted_data_val.append('{')
																for act_keys in sorted(data_value.get(eKeys)[act].keys()):
																 if len(Adbrix.definitions.get(cursor).get("data_value").get(eKeys)) != 0:
																		if Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[0].get(
																						 act_keys):
																				if Adbrix.definitions.get(cursor).get('data_value').get(eKeys)[
																						 0].get(act_keys) == "CONSTANT":
																						 decrypted_data_val.append(
																								'"' + act_keys + '":"' + data_value.get(eKeys)[act].get(
																								 act_keys) + '",')

																				elif Adbrix.definitions.get(cursor).get("data_value").get(eKeys)[
																						 0].get(act_keys) == "BOOLEAN":
																						 decrypted_data_val.append('"' + act_keys + '" : ' + str(
																								data_value.get(eKeys)[act].get(act_keys)) + ',')

																				else:
																						 decrypted_data_val.append('"' + act_keys + '" : ' +
																																Adbrix.definitions.get(cursor).get(
																																		 "data_value").get(eKeys)[0].get(
																																		 act_keys) + ',')
																 else:
																		decrypted_data_val.append('"' + act_keys + '":[],')
																decrypted_data_val.append('},')
								 else:
										decrypted_data_val.append('		"' + eKeys + '" : [],\n')

								else:
								 if not eKeys+" in "+cursor in newKeysFound:
										newKeysFound.append(eKeys+" in "+cursor)
										report.append("NEW KEY FOUND---> " + str(eKeys) + " in encrypted data of url:-" + str(cursor))
										outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(data_value.get(eKeys))])
										confirmed_value=""
										confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
										confirmed_value=confirmed_value.join(confirmed_lines)
										valueType = str(outP).rsplit("\n",2)[1].strip()
										if valueType=="Dynamic":
												decrypted_data_val.append('		"' + eKeys + '" : ' +confirmed_value+ ',\n')
										else:
												decrypted_data_val.append('		"' + eKeys + '" : "' +confirmed_value+ '",\n')

						 decrypted_data_val.append('	\n		}\n')
						 return decrypted_data_val

		"""* 
			>>>>ELIMINATING EXTRA ADD ON STRINGS FROM HOSTS TO GET DEFINITIONS FROM REFERENCE FILE<<<<
		*"""
		for z in self.data.get('host'):
				if 'ad-brix' in z or 'adbrix' in z:
						 if '/tracking/' in z and not 'SetUserDemographic' in z:
								total3.append("tracking.ad-brix.com/v1/tracking")
						 elif 'adbrix' in z and "->" in z:
						 		total3.append(z.split("->")[0])
						 else:
								total3.append(z)
		"""* 
			>>>>EXTRACT UNIQUE CALLS FROM LIST OF HOST TO MAKE UNIQUE CALL DIFINITIONS IN SCRIPT<<<<
		*"""
		total3 = list(set(total3))
		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				D = False
				if "ad-brix" in self.data.get('host')[c] or "adbrix" in self.data.get('host')[c]:
						 if '/tracking/' in self.data.get('host')[c] and not 'SetUserDemographic' in self.data.get('host')[c]:
								cursor = self.data.get('host')[c].rsplit('/', 1)[0]
						 elif 'adbrix' in self.data.get('host')[c] and '->' in self.data.get('host')[c]:
						 		cursor = self.data.get('host')[c].split("->")[0] 
						 else:
								cursor = self.data.get('host')[c]
						 if cursor in total3:
						 		print cursor
						 		if "/api/v2/" in cursor:
							 		apiInUrl = Adbrix.definitions.get(cursor).get('start').replace("/api/v2/","/api/v1/")
							 	elif "/api/v3/" in cursor:
							 		apiInUrl = Adbrix.definitions.get(cursor).get('start').replace("/api/v3/","/api/v1/")
							 	else:
							 		apiInUrl = Adbrix.definitions.get(cursor).get('start')
							 		
								all_adbrix_call_function_defs.append(apiInUrl)
								all_adbrix_call_function_defs.append('method =')
								all_adbrix_call_function_defs.append('"' + self.data.get('method')[c] + '"\n')
								if Adbrix.definitions.get(cursor).get('referrer_data'):
								 all_adbrix_call_function_defs.append(Adbrix.definitions.get(cursor).get('referrer_data'))

								for dic in ["headers", "data"]:
								 all_adbrix_call_function_defs.append('\n	' + dic + ' = {\n')
								 if self.data.get(dic)[c]:
										for keyy in sorted(self.data.get(dic)[c].keys()):
												k = keyy.replace('\n', '')
												if keyy == "j":
														 encrypted_data = str(self.data.get(dic)[c].get(keyy))
														 # try:
														 if "GetReferral" in cursor:
														 		if '{' in encrypted_data:
														 			dd = encrypted_data
														 			
														 		else:
																	dd = decrypt_data(encrypted_data, adbrix_campaign_data.get('key_getreferral'),
																						adbrix_campaign_data.get('iv_getreferral'), "referral",
																						self.data.get('call_sequence')[c])

																	
																data_value = eval(
																 (dd.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"',
																																										"sfalse").replace(
																		"true", "True").replace("false", "False").replace("strue",
																																						'"true"').replace(
																		"sfalse", '"false"'))
																adbrix_campaign_data['app_key'] = str(data_value.get('appkey'))
																adbrix_campaign_data['ver'] = str(data_value.get('version'))
																self.main_campaign_data['package_name'] = data_value.get('package_name')
																self.main_campaign_data['app_version_name'] = data_value.get('app_version_name')
																self.main_campaign_data['app_version_code'] = data_value.get('app_version_code')

																if data_value.get('installation_info'):
																 if data_value.get('installation_info').get('install_actions_timestamp'):
																		iat = data_value.get('installation_info').get(
																				'install_actions_timestamp')
																		if iat.get('app_first_open'):
																				timmings['adbrix_app_first_open_time'] = iat.get('app_first_open')
																		elif iat.get('app_first_open') == 0:
																				timmings['adbrix_app_first_open_time'] = int(time.time())

																 else:
																		timmings['adbrix_app_first_open_time'] = int(time.time())
																else:
																 timmings['adbrix_app_first_open_time'] = int(time.time())

														 else:
																try:
																 call_seq = self.data.get('call_sequence')[c]
																except:
																 call_seq = "N.A"
																 report.append(
																		"Call sequence may vary for ad-brix tracking calls...please do not rely on them.")
																if '{' in encrypted_data:
																	dd = encrypted_data
																else:
																	dd = decrypt_data(encrypted_data, call_seq=call_seq)
																data_value = json.loads(dd.rsplit('}', 1)[0] + '}')

														 D = process_decrypted_data(data_value, cursor)
												# except:
												# 	print "INVALID REFERRAL KEY OR PADDING USED."

												elif Adbrix.definitions.get(cursor).get(dic).get(k):
														 if Adbrix.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
																all_adbrix_call_function_defs.append(
																 '		"' + k + '" : "' + self.data.get(dic)[c].get(k) + '",\n')

														 elif Adbrix.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
																all_adbrix_call_function_defs.append(
																 '		"' + k + '" : ' + str(self.data.get(dic)[c].get(k)) + ',\n')

														 else:
																if isinstance(self.data.get(dic)[c].get(k), dict):
																 all_adbrix_call_function_defs.append('		"' + k + '":{\n')
																 for subd in sorted(self.data.get(dic)[c].get(k).keys()):
																		if Adbrix.definitions.get(cursor).get(dic).get(k).get(
																						 subd) == "CONSTANT":
																				all_adbrix_call_function_defs.append(
																						 '		"' + subd + '" : "' + self.data.get(dic)[c].get(k).get(
																								subd) + '",\n')
																		elif Adbrix.definitions.get(cursor).get(dic).get(k).get(
																						 subd) == "BOOLEAN":
																				all_adbrix_call_function_defs.append(
																						 '		"' + subd + '" : ' + str(
																								self.data.get(dic)[c].get(k).get(subd)) + ',\n')
																		else:
																				if isinstance(self.data.get(dic)[c].get(k).get(subd), dict):
																						 all_adbrix_call_function_defs.append(
																								'		"' + subd + '":{\n')
																						 for subsubd in sorted(
																								 self.data.get(dic)[c].get(k).get(subd).keys()):
																								if Adbrix.definitions.get(cursor).get(dic).get(k).get(
																										subd).get(subsubd) == "CONSTANT":
																								 all_adbrix_call_function_defs.append(
																										'		"' + subsubd + '" : "' +
																										self.data.get(dic)[c].get(k).get(subd).get(
																												subsubd) + '",\n')
																								elif Adbrix.definitions.get(cursor).get(dic).get(k).get(
																										subd).get(subsubd) == "BOOLEAN":
																								 all_adbrix_call_function_defs.append(
																										'		"' + subsubd + '" : ' + str(
																												self.data.get(dic)[c].get(k).get(subd).get(
																														 subsubd)) + ',\n')
																								else:
																								 all_adbrix_call_function_defs.append(
																										'		"' + subsubd + '" : ' + Adbrix.definitions.get(
																												cursor).get(dic).get(k).get(subd).get(
																												subsubd) + ',\n')
																						 all_adbrix_call_function_defs.append('		},\n')
																				else:
																						 all_adbrix_call_function_defs.append(
																								'		"' + subd + '" : ' + Adbrix.definitions.get(
																								 cursor).get(dic).get(k).get(subd) + ',\n')

																 all_adbrix_call_function_defs.append('		},\n')

																elif isinstance(self.data.get(dic)[c].get(k), list):
																 if len(self.data.get(dic)[c].get(k)) > 1:
																		all_adbrix_call_function_defs.append('		"' + k + '" :[')
																		for li in range(len(self.data.get(dic)[c].get(k))):
																				if isinstance(self.data.get(dic)[c].get(k)[li], dict):
																						 all_adbrix_call_function_defs.append('{')
																						 for sd in sorted(self.data.get(dic)[c].get(k)[li].keys()):
																								if Adbrix.definitions.get(cursor).get(dic).get(k)[li].get(
																										sd) == "CONSTANT":
																								 all_adbrix_call_function_defs.append(
																										'		"' + sd + '" : "' +
																										self.data.get(dic)[c].get(k)[li].get(sd) + '",\n')
																								elif Adbrix.definitions.get(cursor).get(dic).get(k)[li].get(
																										sd) == "BOOLEAN":
																								 all_adbrix_call_function_defs.append(
																										'		"' + sd + '" : ' + str(
																												self.data.get(dic)[c].get(k)[li].get(
																														 sd)) + ',\n')
																								else:
																								 if isinstance(self.data.get(dic)[c].get(k)[li].get(sd),
																														dict):
																										all_adbrix_call_function_defs.append('{')
																										for subsd in self.data.get(dic)[c].get(k)[li].get(
																														 sd):
																												if self.data.get(dic)[c].get(k)[li].get(sd).get(
																																subsd) == "CONSTANT":
																														 all_adbrix_call_function_defs.append(
																																'		"' + subsd + ' : "' +
																																self.data.get(dic)[c].get(k)[li].get(
																																 sd).get(subsd) + '",\n')
																												elif self.data.get(dic)[c].get(k)[li].get(
																																sd).get(subsd) == "BOOLEAN":
																														 all_adbrix_call_function_defs.append(
																																'		"' + subsd + ' : ' + str(
																																 self.data.get(dic)[c].get(k)[
																																		li].get(sd).get(subsd)) + ',\n')
																												else:
																														 all_adbrix_call_function_defs.append(
																																'		"' + subsd + '" : ' +
																																Adbrix.definitions.get(cursor).get(
																																 dic).get(k)[li].get(sd).get(
																																 subsd) + ',\n')

																										all_adbrix_call_function_defs.append('},\n')
																								 elif isinstance(
																												self.data.get(dic)[c].get(k)[li].get(sd), list):
																										all_adbrix_call_function_defs.append('[')
																										for li2 in range(len(
																														 self.data.get(dic)[c].get(k)[li].get(sd))):
																												if isinstance(
																																self.data.get(dic)[c].get(k)[li].get(
																																 sd)[li2], dict):
																														 all_adbrix_call_function_defs.append('{')
																														 for ky in sorted(
																																 self.data.get(dic)[c].get(k)[
																																		li].get(sd)[li2].keys()):
																																if self.data.get(dic)[c].get(k)[li].get(
																																		sd)[li2].get(ky) == "CONSTANT":
																																 all_adbrix_call_function_defs.append(
																																		'		"' + ky + ' : "' +
																																		self.data.get(dic)[c].get(k)[
																																				li].get(sd)[li2].get(
																																				ky) + '",\n')
																																elif \
																																		self.data.get(dic)[c].get(k)[
																																				li].get(
																																				sd)[li2].get(
																																				ky) == "BOOLEAN":
																																 all_adbrix_call_function_defs.append(
																																		'		"' + ky + ' : ' + str(
																																				self.data.get(dic)[c].get(
																																						 k)[li].get(sd)[li2].get(
																																						 ky)) + ',\n')
																																else:
																																 if isinstance(
																																				self.data.get(dic)[c].get(
																																						 k)[li].get(sd)[li2],
																																				dict):
																																		all_adbrix_call_function_defs.append(
																																				'{')
																																		for nested in sorted(
																																						 self.data.get(dic)[
																																								c].get(k)[li].get(
																																								sd)[
																																								li2].keys()):
																																				if Adbrix.definitions.get(
																																								cursor).get(
																																						 dic).get(k)[li].get(
																																						 sd)[li2].get(
																																						 nested) == "CONSTANT":
																																						 all_adbrix_call_function_defs.append(
																																								nested + ':"' + str(
																																								 self.data.get(
																																										dic)[c].get(
																																										k)[li].get(
																																										sd)[
																																										li2].get(
																																										nested)) + '",')
																																				elif Adbrix.definitions.get(
																																								cursor).get(
																																						 dic).get(k)[li].get(
																																						 sd)[li2].get(
																																						 nested) == "BOOLEAN":
																																						 all_adbrix_call_function_defs.append(
																																								nested + ':' + str(
																																								 self.data.get(
																																										dic)[c].get(
																																										k)[li].get(
																																										sd)[
																																										li2].get(
																																										nested)) + ',')
																																				else:
																																						 if isinstance(
																																								 Adbrix.definitions.get(
																																										cursor).get(
																																										dic).get(
																																										k)[
																																										li].get(sd)[
																																										li2].get(
																																										nested),
																																								 dict):
																																								all_adbrix_call_function_defs.append(
																																								 nested + ':{')
																																								for suprnested in sorted(
																																										Adbrix.definitions.get(
																																												cursor).get(
																																												dic).get(
																																												k)[
																																												li].get(
																																												sd)[
																																												li2].get(
																																												nested).keys()):
																																								 if \
																																												Adbrix.definitions.get(
																																														 cursor).get(
																																														 dic).get(
																																														 k)[
																																														 li].get(
																																														 sd)[
																																														 li2].get(
																																														 nested).get(
																																														 suprnested) == "CONSTANT":
																																										all_adbrix_call_function_defs.append(
																																												suprnested + ':"' + str(
																																														 self.data.get(
																																																dic)[
																																																c].get(
																																																k)[
																																																li].get(
																																																sd)[
																																																li2].get(
																																																nested).get(
																																																suprnested)) + '",')
																																								 elif \
																																												Adbrix.definitions.get(
																																														 cursor).get(
																																														 dic).get(
																																														 k)[
																																														 li].get(
																																														 sd)[
																																														 li2].get(
																																														 nested).get(
																																														 suprnested) == "BOOLEAN":
																																										all_adbrix_call_function_defs.append(
																																												suprnested + ':' + str(
																																														 self.data.get(
																																																dic)[
																																																c].get(
																																																k)[
																																																li].get(
																																																sd)[
																																																li2].get(
																																																nested).get(
																																																suprnested)) + ',')
																																								 else:
																																										all_adbrix_call_function_defs.append(
																																												suprnested + ':' + str(
																																														 Adbrix.definitions.get(
																																																cursor).get(
																																																dic).get(
																																																k)[
																																																li].get(
																																																sd)[
																																																li2].get(
																																																nested).get(
																																																suprnested)) + ',')
																																								all_adbrix_call_function_defs.append(
																																								 '},')

																																						 else:
																																								all_adbrix_call_function_defs.append(
																																								 nested + ':"' +
																																								 Adbrix.definitions.get(
																																										cursor).get(
																																										dic).get(k)[
																																										li].get(sd)[
																																										li2].get(
																																										nested) + '",')
																																		all_adbrix_call_function_defs.append(
																																				'},')

																																 else:
																																		all_adbrix_call_function_defs.append(
																																				'		"' + ky + '" : ' +
																																				Adbrix.definitions.get(
																																						 cursor).get(dic).get(k)[
																																						 li].get(sd)[
																																						 li2] + ',\n')
																														 all_adbrix_call_function_defs.append('},')
																										all_adbrix_call_function_defs.append('],')

																								 else:
																										all_adbrix_call_function_defs.append(
																												'		"' + sd + '" : ' +
																												Adbrix.definitions.get(cursor).get(dic).get(k)[
																														 li].get(sd) + ',\n')

																						 all_adbrix_call_function_defs.append('	},')
																				else:
																						 if Adbrix.definitions.get(cursor).get(dic).get(k)[
																								li] == "CONSTANT":
																								all_adbrix_call_function_defs.append(
																								 '"' + self.data.get(dic)[c].get(k)[li] + '",\n')
																						 elif Adbrix.definitions.get(cursor).get(dic).get(k)[
																								li] == "BOOLEAN":
																								all_adbrix_call_function_defs.append(
																								 str(self.data.get(dic)[c].get(k)[li]) + ',\n')
																						 else:
																								all_adbrix_call_function_defs.append(
																								 Adbrix.definitions.get(cursor).get(dic).get(k)[
																										li] + ',\n')

																		all_adbrix_call_function_defs.append('	],\n')
																 else:
																		all_adbrix_call_function_defs.append('		"' + k + '" :[],\n')

																else:
																 all_adbrix_call_function_defs.append(
																		'		"' + k + '" : ' + Adbrix.definitions.get(cursor).get(dic).get(
																				k) + ',\n')

												elif dic=="data" and "adbrix" in self.data.get("host")[c]:
													aesType = ""
													if not "bulk" in self.data.get("url")[c]:
														aesType = "referral"

													dd = decrypt_data(keyy.strip(), adbrix_campaign_data.get('key_multipart'),
																						adbrix_campaign_data.get('iv_multipart'), aesType,
																						self.data.get('call_sequence')[c])
													data_value = eval(
													 (dd.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"',
																																							"sfalse").replace(
															"true", "True").replace(
															"null", "None").replace("false", "False").replace("strue",
																																			'"true"').replace(
															"sfalse", '"false"'))
													adbrix_campaign_data['app_key'] = str(data_value.get('common').get('appkey'))
													adbrix_campaign_data['ver'] = str(data_value.get('common').get('sdk_version'))
													self.main_campaign_data['package_name'] = data_value.get('common').get('package_name')
													self.main_campaign_data['app_version_name'] = data_value.get('common').get('app_version')
													D = process_decrypted_data(data_value, cursor)													
												else:
														 if not k in ["Host", "Content-Length", "Connection", "j"]:
																if not k+" in "+cursor in newKeysFound:
																 newKeysFound.append(k+" in "+cursor)
																 report.append(
																		"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(cursor))
																 outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(k), '-value='+str(self.data.get(dic)[c].get(k))])
																 confirmed_value=""
																 confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
																 confirmed_value=confirmed_value.join(confirmed_lines)
																 valueType = str(outP).rsplit("\n",2)[1].strip()
																 if valueType=="Dynamic":
																		all_adbrix_call_function_defs.append(
																				'		"' + k + '" : ' +confirmed_value+ ',\n')
																 else:
																		all_adbrix_call_function_defs.append(
																				'		"' + k + '" : "' +confirmed_value+ '",\n')

								 all_adbrix_call_function_defs.append('\n		}\n')

								if self.ad_event_delta.get('event_date') and self.ad_event_delta.get('event_date_1'):
								 delta = 0
								 if self.ad_event_delta.get('event_date') > self.ad_event_delta.get('event_date_1'):
										delta = int(self.ad_event_delta.get('event_date')) - int(
												self.ad_event_delta.get('event_date_1'))
										if delta <= 3:
												pl = [1, delta + 1]
										else:
												if delta < 60:
														 pl = [delta - 2, delta + 3]
												else:
														 pl = [60, 100]
										all_adbrix_call_function_defs.append(
												"""\n	event_date = get_date(app_data,device_data,para1=""" + str(
														 pl[0]) + """,para2=""" + str(pl[1]) + """)\n""")
								 elif self.ad_event_delta.get('event_date') < self.ad_event_delta.get('event_date_1'):
										delta = int(self.ad_event_delta.get('event_date_1')) - int(
												self.ad_event_delta.get('event_date'))
										if delta <= 3:
												pl = [1, delta + 1]
										else:
												if delta < 60:
														 pl = [delta - 1, delta + 2]
												else:
														 pl = [60, 100]
										all_adbrix_call_function_defs.append(
												"""\n	event_date = get_date(app_data,device_data,para1=0,para2=0)\n	""")
								 else:
										all_adbrix_call_function_defs.append(
												"""\n	event_date = get_date(app_data,device_data,para1=0,para2=0)\n""")
								elif self.ad_event_delta.get('event_date') or self.ad_event_delta.get('event_date_1'):
								 all_adbrix_call_function_defs.append(
										"""\n	event_date = get_date(app_data,device_data,para1=0,para2=0)\n""")
								self.ad_event_delta = {}

								if D:
								 for d in D:
										all_adbrix_call_function_defs.append(d)
								all_adbrix_call_function_defs.append(Adbrix.definitions.get(cursor).get('return'))
								all_adbrix_call_function_defs.append("\n")
								total3.remove(cursor)


		


		return all_adbrix_call_function_defs, adbrix_campaign_data, self.main_campaign_data, timmings

	
	##################################################################################################
	"""* 
		>>>>KOCHAVA RELATED DATA PARSING AND CREATION OF CALL DEFINITIONS IN SCRIPT<<<<
	*"""
	def kochava_definition_data(self):
		self.main_campaign_data = {}
		all_kochava_call_function_defs = []
		kochava_campaign_data = {}
		timmings = {}
		total=[]
		"""* 
			>>>>MANIPULATING HOSTS AS PER THEIR ACTION TYPE TO GET CORRESPONDING DEFINITION FROM REFERENCE FILE<<<<
		*"""
		for x in range(len(self.data.get('host'))):
			if "->" in self.data.get('host')[x]:
				callAction = self.data.get('host')[x].split("->")[1].strip()
				if not callAction in ["update", "initial", "identityLink"]:
					if callAction.strip().lower() in ["launch", "pause", "resume", "exit"]:
						total.append(self.data.get('host')[x].split("->")[0]+"->session")
					else:
						total.append(self.data.get('host')[x].split("->")[0]+"->event")
				else:
					total.append(self.data.get('host')[x])
			else:
				total.append(self.data.get('host')[x])
		"""* 
			>>>>ELIMINATING REPEATED HOSTS FROM LIST OF LOGS TO GET UNIQUE DEFINITIONS<<<<
		*"""
		total = list(set(total))
		"""* 
			>>>>ITERATING THROUGH ALL THE FILTERED HOSTS IN LOGS<<<<
		*"""
		for c in range(len(self.data.get('host'))):
				if "kochava" in self.data.get('host')[c]:
						if "->" in self.data.get('host')[c]:
							callAction = self.data.get('host')[c].split("->")[1].strip()
							if not callAction in ["update", "initial", "identityLink"]:
								if callAction.strip().lower() in ["launch", "pause", "resume", "exit"]:
									cursor = self.data.get('host')[c].split("->")[0]+"->session"
								else:
									cursor = self.data.get('host')[c].split("->")[0]+"->event"
							else:
								cursor = self.data.get('host')[c]
						else:
							cursor = self.data.get('host')[c]

						if cursor in total and "kochava" in self.data.get('host')[c]:
							print cursor
							all_kochava_call_function_defs.append(Kochava.definitions.get(cursor).get('start'))
							all_kochava_call_function_defs.append('	method = "')
							all_kochava_call_function_defs.append(Kochava.definitions.get(cursor).get('method'))
							all_kochava_call_function_defs.append('"\n	url = ')
							all_kochava_call_function_defs.append(Kochava.definitions.get(cursor).get('url'))

							for dic in ["headers", "params", "data"]:
								all_kochava_call_function_defs.append('\n	' + dic + ' = {\n')
								if self.data.get(dic)[c]:
									if not isinstance(self.data.get(dic)[c], list):
										self.data.get(dic)[c] = [self.data.get(dic)[c]]
									for li in self.data.get(dic)[c]:
										for k in sorted(li.keys()):
											if k == "data" or k == "data_orig":
												all_kochava_call_function_defs.append('		"' + k + '" : {\n')
												for sub_data in sorted(li.get(k).keys()):
													if Kochava.definitions.get(cursor).get(dic).get(k).get(sub_data):
														if sub_data == "install_referrer":
															all_kochava_call_function_defs.append('						"' + sub_data + '" : {\n')
															for refer_data in sorted(li.get(k).get(sub_data).keys()):
																if Kochava.definitions.get(cursor).get(dic).get(k).get(sub_data).get(refer_data)=="CONSTANT":
																	all_kochava_call_function_defs.append(
																		'						"' + refer_data + '" : "' + str(li.get(k).get(sub_data).get(refer_data)) + '",\n')
																elif Kochava.definitions.get(cursor).get(dic).get(k).get(sub_data).get(refer_data)=="BOOLEAN":
																	all_kochava_call_function_defs.append(
																		'						"' + refer_data + '" : ' + str(li.get(k).get(sub_data).get(refer_data)) + ',\n')
																else:
																	all_kochava_call_function_defs.append(
																		'						"' + refer_data + '" : ' + str(Kochava.definitions.get(cursor).get(dic).get(
																	k).get(sub_data).get(refer_data)) + ',\n')

															all_kochava_call_function_defs.append('			},\n')

														elif Kochava.definitions.get(cursor).get(dic).get(k).get(sub_data)=="CONSTANT":
															all_kochava_call_function_defs.append(
																'						"' + sub_data + '" : "' + str(li.get(k).get(sub_data)) + '",\n')
														elif Kochava.definitions.get(cursor).get(dic).get(k).get(sub_data)=="BOOLEAN":
															all_kochava_call_function_defs.append(
																'						"' + sub_data + '" : ' + str(li.get(k).get(sub_data)) + ',\n')
														else:
															all_kochava_call_function_defs.append(
																'						"' + sub_data + '" : ' + str(Kochava.definitions.get(cursor).get(dic).get(
																	k).get(sub_data)) + ',\n')

														if sub_data == "package":
															self.main_campaign_data["package_name"] = str(li.get(k).get(sub_data))

														elif sub_data == "kochava_app_id":
															self.main_campaign_data[sub_data] = str(li.get(k).get(sub_data))

														elif sub_data == "app_short_string":
															self.main_campaign_data[sub_data] = str(li.get(k).get(sub_data))

														elif sub_data == "app_version":
															try:
																verz = str(li.get(k).get(sub_data)).rsplit(" ",1)
																self.main_campaign_data[sub_data] = verz[1]
																self.main_campaign_data["app_name"] = verz[0]
															except:
																# if only version is present
																self.main_campaign_data[sub_data] = verz[0]

														elif sub_data == "app_name":
															self.main_campaign_data[sub_data] = str(li.get(k).get(sub_data))

														elif sub_data == "package" or sub_data == "package_name":
															self.main_campaign_data["package_name"] = str(li.get(k).get(sub_data))

													else:
														if not sub_data in ["Host", "Content-Length", "Connection"]:
															if not sub_data+" in "+cursor in newKeysFound:
																newKeysFound.append(sub_data+" in "+cursor)
																report.append(
																	"NEW KEY FOUND---> " + str(sub_data) + " in "+k+" under "+ str(dic) + " of " + str(cursor))
																dialog = self.newKeyDialogue(str(k)+'->'+sub_data,li.get(k).get(sub_data))
																confirmed_value = dialog[0]
																valueType = dialog[1]
																if valueType=="Dynamic":
																	all_kochava_call_function_defs.append(
																'						"' + sub_data + '" : ' + confirmed_value + ',\n')
																else:
																	all_kochava_call_function_defs.append(
																'						"' + sub_data + '" : "' + confirmed_value + '",\n')

												all_kochava_call_function_defs.append('		},\n')

											elif Kochava.definitions.get(cursor).get(dic).get(k):
												if Kochava.definitions.get(cursor).get(dic).get(k) == "CONSTANT":
													all_kochava_call_function_defs.append(
														 '		"' + k + '" : "' + str(li.get(k)) + '",\n')

												elif Kochava.definitions.get(cursor).get(dic).get(k) == "BOOLEAN":
													all_kochava_call_function_defs.append(
														 '		"' + k + '" : ' + str(li.get(k)) + ',\n')

												else:
													all_kochava_call_function_defs.append(
														 '		"' + k + '" : ' + Kochava.definitions.get(cursor).get(dic).get(
																k) + ',\n')
											else:
												if not k in ["Host", "Content-Length", "Connection"]:
													if not k+" in "+cursor in newKeysFound:
														newKeysFound.append(k+" in "+cursor)
														report.append(
															"NEW KEY FOUND---> " + str(k) + " in " + str(dic) + " of " + str(cursor))
														dialog = self.newKeyDialogue(dic+'->'+k,li.get(k))
														confirmed_value = dialog[0]
														valueType = dialog[1]
														if valueType=="Dynamic":
															all_kochava_call_function_defs.append(
																	'		"' + k + '" : '+confirmed_value+',\n')
														else:
															all_kochava_call_function_defs.append(
																	'		"' + k + '" : "'+confirmed_value+'",\n')

											if k == "sdk_version" or k == "kochava_app_id" or k == "sdk_build_date":
												kochava_campaign_data[k] = str(li.get(k))

											elif k == "sdk_protocol" or k == "sdk_procotol":
												kochava_campaign_data["sdk_protocol"] = str(li.get(k))								

								all_kochava_call_function_defs.append('\n		}')

							if Kochava.definitions.get(cursor).get('conditional_data'):
								all_kochava_call_function_defs.append(
									Kochava.definitions.get(cursor).get('conditional_data'))

							all_kochava_call_function_defs.append("\n\n	")
							all_kochava_call_function_defs.append(Kochava.definitions.get(cursor).get('return'))

							all_kochava_call_function_defs.append("\n\n")
							total.remove(cursor)

		return all_kochava_call_function_defs, kochava_campaign_data, self.main_campaign_data, timmings

	def newKeyDialogue(self,key,value):
		outP = subprocess.check_output(['python' ,'reference/unknown_key.py', '-key='+str(key), '-value='+str(value)])
		confirmed_value=""
		confirmed_lines = str(outP).rsplit("\n",2)[0].splitlines()
		confirmed_value=confirmed_value.join(confirmed_lines)
		valueType = str(outP).rsplit("\n",2)[1].strip()
		return confirmed_value, valueType