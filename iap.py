import subprocess
import sys

"""* 
	>>>>IMPORT PYTHON LIBRARY TO VALIDATE INAPP RECEIPT IN IOS<<<<
*"""
try:
	import itunesiap
except ImportError:
	print "itunesiap not installed "
	print "Please wait installing itunesiap..."
	subprocess.call([sys.executable, "-m", "pip", "install", "itunes-iap"])


"""* 
	>>>>FUNCTION TO VALIDATE IOS APPLE RECEIPT<<<<
*"""
def validate_receipt(r):
	try:
		response = itunesiap.verify(r)  # base64-encoded data
		data = eval(str(response.receipt).split("<Receipt(")[1][:-2])  # other values are also available as property!
		print "Validating install receipt...\nApp_version in receipt:-"
		print data.get('application_version')
		print "Complete receipt_data:-\n"
		print data
		return data, r
	except itunesiap.exc.InvalidReceipt as e:
		print('invalid receipt')
		return False
