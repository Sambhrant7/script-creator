# -*- coding: utf-8 -*-
import multiprocessing
import re, json

"""* 
	>>>>IMPORT FILES<<<<
*"""
from reference import branch
from adbrix_aes256 import decrypt_data
import read_logs

globals()['adDoubleFlag'] = []
globals()['brDoubleFlag'] = []

"""* 
	>>>>FUNCTION TO FILTER CALLS FROM LOGS TO USE ONLY TRACKING RELATED URLS<<<<
*"""
def filter_logs_data(logs, platform, key=None):
	"""* 
		>>>>DICTIONARY TO STORE SCRIPT CALLINGS W.R.T A PART OF THEIR URL<<<<
	*"""
	calling_data_dictionary = {

		#####################			  APPSFLYER		 CALLINGS 			####################

		'attr.appsflyer.com': """	request = appsflyer_attr(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'""",

		'first->t.appsflyer.com': """	\n	print """ + repr(
			"\n") + """+'Appsflyer : Track____________________________________'\n	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		't.appsflyer.com': """	\n	print """ + repr(
			"\n") + """+'Appsflyer : Track____________________________________'\n	request  = appsflyer_track(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'events.appsflyer.com': '',

		'stats.appsflyer.com': """	\n	print """ + repr(
			"\n") + """+'Appsflyer : Stats____________________________________'\n	request  = appsflyer_stats(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'api.appsflyer.com': """		\n	print """ + repr(
			"\n") + """+'Appsflyer : API____________________________________'\n	request = appsflyer_api(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	catch(resp,app_data,"appsflyer")\n""",

		'register.appsflyer.com': """		\n	print """ + repr(
			"\n") + """+'Appsflyer : Register____________________________________'\n	request = appsflyer_register(campaign_data,app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		#####################			  ADJUST		 CALLINGS 			####################

		'app.adjust.com/session': """		\n	print """ + repr(
			"\n") + """+'Adjust : SESSION____________________________________'\n	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n		else:\n			app_data['session_check'] = True\n\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/sdk_info': """		\n	print """ + repr(
			"\n") + """+'Adjust : SDK INFO____________________________________'\n	request=adjust_sdkinfo(campaign_data, app_data, device_data,t1=in1,t2=in2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/sdk_click->reftag': """		\n	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/sdk_click->install_referrer': """		\n	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/sdk_click->iad3': """		\n	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_iad3,source='iad3',t1=id1,t2=id2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/sdk_click->deeplink': """		\n	print """ + repr(
			"\n") + """+'Adjust : SDK CLICK____________________________________'\n	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_deeplink,source='deeplink',t1=dp1,t2=dp2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app.adjust.com/attribution': """		\n	print """ + repr(
			"\n") + """+'Adjust : ATTRIBUTION____________________________________'\n	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		#####################			  ADBRIX		 CALLINGS 			####################

		'first->tracking.ad-brix.com': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking Start '\n	request = ad_brix_tracking_start(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'cvr.ad-brix.com/v1/conversion/GetReferral':"""		\n	print """ + repr(
			"\n") + """+'AD BRIX get Referral'\n	request = ad_brix_get_referral(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		get_referral_result_decode = json.loads(resp.get('data'))\n		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))\n		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))\n		app_data["install_datetime"] = str(get_referral_result_decode.get('Data').get('install_datetime'))\n		print app_data["install_datetime"]\n		var=time.mktime(datetime.datetime.strptime(app_data.get('install_datetime'), "%Y%m%d%H%M%S").timetuple())\n		app_data['install_mdatetime']=int(var*1000)\n		print app_data['install_mdatetime']		\n	except:\n		app_data['user_number'] = ""\n		app_data['shard_number'] = ""\n		app_data['install_datetime'] = get_date(app_data,device_data,para1=0,para2=0)\n		var=time.mktime(datetime.datetime.strptime(app_data.get('install_datetime'), "%Y%m%d%H%M%S").timetuple())\n		app_data['install_mdatetime']=int(var*1000)\n""",

		'as.ad-brix.com/v1/users/login': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Get login'\n	request= ad_brix_user_login(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		get_data=json.loads(resp.get('data'))\n		app_data['session_id']=get_data.get('user').get('sessionId')\n	except:\n		app_data['session_id']=	'4018cc614fc9c0f3'\n""",

		'as.ad-brix.com/v1/users/save': """		\n	print """ + repr(
			"\n") + """+'AD BRIX users_save'\n	request=ad_brix_users_save(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'as.ad-brix.com/v1/users/updateRegistration': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Update Registration'\n	request= ad_brix_update_registration(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'as.ad-brix.com/v1/users/updateConversion': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Update Conversion'\n	request= ad_brix_update_conversion(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'as.ad-brix.com/v1/users/getPopups': """		\n	print """ + repr(
			"\n") + """+'Ad Brix getPopups'\n	request=ad_brix_update_get_popups(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data' """,

		'as.ad-brix.com/v1/users/enablePushService': """		\n	print """ + repr(
			"\n") + """+'Adbrix--enable push services---'\n	request=enablepushservice(app_data,campaign_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'""",

		'campaign.ad-brix.com/v1/CampaignVer2/GetSchedule': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Get Schedule'\n	request = ad_brix_get_schedule(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'tracking.ad-brix.com/v1/tracking/SetUserDemographic': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking'\n	request=ad_brix_tracking_SetUserDemographic(app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'apiab4c.ad-brix.com/v1/event': """		\n	print """ + repr(
			"\n") + """+'Ad Brix Tracking'\n	request=adbrix_purchase(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'])\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'event.adbrix.io/api/v1/deferred-deeplink': """		\n	print """+repr("\n")+"""+'AD BRIX deferred_deeplink'\n	request = adbrix_deferred_deeplink(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		#####################			  MAT		 CALLINGS		  ####################

		'engine.mobileapptracking.com/serve' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------MAT----session---------------------------------"\n	request = mat_serve(campaign_data, app_data, device_data,action="session")\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	catch(resp,app_data,"mat")\n	set_matOpenLogID(app_data)\n""",

		###############################      Apsalar Calling  ############################

		'e-ssl.apsalar.com/api/v1/resolve': """		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'e.apsalar.com/api/v1/resolve':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'e-ssl.apsalar.com/api/v1/start': """		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Start---------------------------------"\n	request = apsalarStart(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'e.apsalar.com/api/v1/start':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Start---------------------------------"\n	request = apsalarStart(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'e-ssl.apsalar.com/api/v1/event':'',

		'e.apsalar.com/api/v1/event':'',

		###############################  KOCHAVA CALLINGS  ################################

		'control.kochava.com/track/kvinit' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvinit---------------------------------"\n	request=Kvinit(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'control.kochava.com/track/kvTracker.php->initial' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava initial---------------------------------"\n	request=android_KvTracker_initial(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'control.kochava.com/track/kvTracker.php->update' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava update---------------------------------"\n	request=kochava_tracker_update( campaign_data, device_data, app_data )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'control.kochava.com/track/kvquery' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvquery---------------------------------"\n	request=kv_Query(campaign_data,app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'kvinit-prod.api.kochava.com/track/kvinit' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava kvinit---------------------------------"\n	request=kochavaKvinit(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",		

		'control.kochava.com/track/json->initial' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------Kochava initial---------------------------------"\n	request=kochava_initial(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'control.kochava.com/track/json->identityLink' : """		\n	print """ + repr(
			"\n") + """+"-----------------------------kochava identityLink---------------------------------"\n	request=kochava_identityLink(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		############################## Adforce Calling ######################################

		'app-adforce.jp/ad/p/tmck':"""		\n	print """ + repr(
			"\n") + """+"-----------------------------Ad-Force tmck---------------------------------"\n	request = adforce_tmck(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		app_data['uid']=resp.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]\n	except:\n		app_data['uid']='foxGC0lu1gEW6E'\n""",

		'app-adforce.jp/ad/view/collect.html':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- Ad-Force collect ------------------------------"\n	request = adforce_collect(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app-adforce.jp/ad/view/collect_ios.html':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- Ad-Force collect ------------------------------"\n	request = adforce_collect(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'analytics.app-adforce.jp/fax/analytics':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce Analytics ------------------------------"\n	request = adforce_analytics(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app-adforce.jp/ad/i/fprc':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce fprc ------------------------------"\n	request = adforce_fprc(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app-adforce.jp/ad/p/fp':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce fp ------------------------------"\n	request = adforce_fp(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	global fp_ID, fp_TDL\n	try:\n		uid=json.loads(resp.get('data'))\n		fp_ID = uid.get('id')\n		fp_TDL = uid.get('tdl')\n	except:\n		fp_ID = '6%2E8%2E2%3AA314A6E4FB54ADB3FCDB834CAE7B4E044CA858E7%7Ctid%3AtGH3CB1gClM5'\n		fp_TDL = '-1206245842'\n""",

		'app-adforce.jp/ad/p/cv->cv':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce cv ------------------------------"\n	request = adforce_cv(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		app_data['cookie_uid']=resp.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]\n	except:\n		app_data['cookie_uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'\n""",

		'app-adforce.jp/ad/p/cv->short':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce cv short ------------------------------"\n	request = adforce_cv_short(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'app-adforce.jp/ad/p/jump':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- adforce p jump  ------------------------------"\n	request = adforce_p_jump(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		########################### tenjin calling ###################################

		'track.tenjin.io/v0/user':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- tenjin user  ------------------------------"\n	request = tenjin_user(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'track.tenjin.io/v0/event':"",


		############################## Branch Calling ######################################

		'api2.branch.io/v1/install':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- branch install------------------------------"\n	request = api_branch_install(campaign_data,app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		json_result=json.loads(resp.get('data'))\n		app_data['device_fingerprint_id']=json_result.get('device_fingerprint_id')\n		app_data['identity_id']=json_result.get('identity_id')\n		app_data['session_id']=json_result.get('session_id')\n	except:\n		app_data['device_fingerprint_id']='613284344550945604'\n		app_data['identity_id']='641581897171978611'\n		app_data['session_id']='641581897189090110'\n""",


		'api2.branch.io/v1/close': """		\n	print """ + repr(
			"\n") + """+'________________branch_close____________________'\n	request=api_branch_close(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api2.branch.io/v1/open': """		\n	print """ + repr(
			"\n") + """+'________________branch_open____________________'\n	request=api_branch_open(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'api2.branch.io/v1/profile': """		\n	print """ + repr(
			"\n") + """+'________________branch_profile____________________'\n	request=api_branch_profile(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api2.branch.io/v1/cpid/latd': """		\n	print """ + repr(
			"\n") + """+'________________branch_latd____________________'\n	request=api2_branch_latd(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api2.branch.io/v2/event/standard': '',

		'api2.branch.io/v2/event/custom': '',

		'cdn.branch.io/sdk/uriskiplist_v1.json': """		\n	print """ + repr(
			"\n") + """+'________________branch_uriskiplist____________________'\n	request=branch_call(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'cdn.branch.io/sdk/uriskiplist_v2.json': """		\n	print """ + repr(
			"\n") + """+'________________branch_uriskiplist____________________'\n	request=branch_call2(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'api.branch.io/v1/install':"""		\n	print """ + repr(
			"\n") + """+"----------------------------- branch install------------------------------"\n	req = api_branch_install(campaign_data,app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		json_result=json.loads(resp.get('data'))\n		app_data['device_fingerprint_id']=json_result.get('device_fingerprint_id')\n		app_data['identity_id']=json_result.get('identity_id')\n		app_data['session_id']=json_result.get('session_id')\n	except:\n		app_data['device_fingerprint_id']='613284344550945604'\n		app_data['identity_id']='641581897171978611'\n		app_data['session_id']='641581897189090110'\n""",


		'api.branch.io/v1/close': """		\n	print """ + repr(
			"\n") + """+'________________branch_close____________________'\n	request=api_branch_close(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api.branch.io/v1/open': """		\n	print """ + repr(
			"\n") + """+'________________branch_open____________________'\n	request=api_branch_open(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",

		'api.branch.io/v1/profile': """		\n	print """ + repr(
			"\n") + """+'________________branch_profile____________________'\n	request=api_branch_profile(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api.branch.io/v1/cpid/latd': """		\n	print """ + repr(
			"\n") + """+'________________branch_latd____________________'\n	request=api2_branch_latd(campaign_data, app_data,device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""",


		'api.branch.io/v2/event/standard': '',

		'api.branch.io/v2/event/custom': '',

	}

	# p = multiprocessing.Process(target=read_logs.get_logs_data, args=(logs,queue))
	manager = multiprocessing.Manager()
	queue = manager.dict()
	p = multiprocessing.Process(target=read_logs.get_logs_data, args=(logs, queue))
	p.start()
	p.join()
	logs_Data = queue.get('logs')
	# logs_Data=read_logs.get_logs_data(logs)

	"""* 
		>>>>DETERMINE VALID CALLS TO CONSIDER FROM LOGS WITH RESPECT TO PLATFORM<<<<
	*"""
	if platform == "Android":
		calling_data_dictionary_list = ['attr.appsflyer.com', 't.appsflyer.com', 'events.appsflyer.com',
								   'stats.appsflyer.com', 'api.appsflyer.com', 'register.appsflyer.com',
								   'app.adjust.com', 'cvr.ad-brix.com', 'campaign.ad-brix.com', 'as.ad-brix.com',
								   'tracking.ad-brix.com', 'apiab4c.ad-brix.com', 'engine.mobileapptracking.com','e-ssl.apsalar.com','e.apsalar.com', 'kvinit-prod.api.kochava.com', 'control.kochava.com','app-adforce.jp','analytics.app-adforce.jp','track.tenjin.io', 'event.adbrix.io','api2.branch.io','api.branch.io','cdn.branch.io']
	elif platform == "ios":
		calling_data_dictionary_list = ['app.adjust.com','e-ssl.apsalar.com','e.apsalar.com','kvinit-prod.api.kochava.com', 'control.kochava.com', 'engine.mobileapptracking.com','app-adforce.jp','analytics.app-adforce.jp']

	"""* 
		>>>>ITERATING THROUGH ALL CALLS IN LOGS AND FILTERING ONLY TRACKING RELATED CALLS<<<<
	*"""
	filtered_logs_data = {'host': [], 'call_sequence': [], 'method': [], 'url': [], 'headers': [], 'params': [],
						  'data': []}
	sflag = True
	isFirstCall = True
	isfirsttrackingevent = True
	event_list = []
	eventData_List = []
	event_counter = 0
	for cursor in range(len(logs_Data.get('hosts'))):
		if logs_Data.get('hosts')[cursor] in calling_data_dictionary_list or 'engine.mobileapptracking.com' in logs_Data.get('hosts')[cursor]:
			if "adforce" in logs_Data.get('hosts')[cursor]:
				if '/ad//' in logs_Data.get('urls')[cursor]:
					globals()['adDoubleFlag'].append(logs_Data.get('urls')[cursor].split('/ad/')[1])
				logs_Data.get('urls')[cursor] = logs_Data.get('urls')[cursor].replace('/ad//','/ad/')
				logs_Data.get('hosts')[cursor] = logs_Data.get('hosts')[cursor].replace('/ad//','/ad/')
				if logs_Data.get('urls')[cursor][-3:]==".js":
					continue
			flag = True
			if "http://" in logs_Data.get('urls')[cursor]:
				logs_Data.get('urls')[cursor] = (logs_Data.get('urls')[cursor]).replace("http://", "https://")
			if "adbrix" or "ad-brix" in url:
				logs_Data.get('urls')[cursor] = (logs_Data.get('urls')[cursor]).replace("/api/v2/","/api/v1/").replace("/api/v3/","/api/v1/")

			######################## APPSFLYER CALLING DATA FORMATTING ########################

			if logs_Data.get('hosts')[cursor] == 't.appsflyer.com' and isFirstCall == True:
				filtered_logs_data.get('host').append('first->t.appsflyer.com')
				isFirstCall = False

			elif 'appsflyer' in logs_Data.get('hosts')[cursor]:
				if logs_Data.get('hosts')[cursor] == 'events.appsflyer.com':
					pass
				else:
					filtered_logs_data.get('host').append(logs_Data.get('hosts')[cursor])

			######################## ADJUST CALLING DATA FORMATTING ########################

			elif logs_Data.get('hosts')[cursor] == "app.adjust.com":
				if isinstance(logs_Data.get('data')[cursor], dict):
					if logs_Data.get('data')[cursor].get('source'):
						if logs_Data.get('data')[cursor].get('source') == "reftag" or logs_Data.get('data')[cursor].get(
								'source') == "install_referrer" or logs_Data.get('data')[cursor].get(
							'source') == "iad3" or logs_Data.get('data')[cursor].get('source') == "deeplink":
							host = logs_Data.get('hosts')[cursor] + '/' + \
								   (logs_Data.get('urls')[cursor]).rsplit('/', 1)[1] + '->' + logs_Data.get('data')[
									   cursor].get('source')
							if logs_Data.get('data')[cursor].get('callback_params') and logs_Data.get('data')[
								cursor].get('partner_params'):
								one = calling_data_dictionary[host].split(")\n", 1)[0]
								two = calling_data_dictionary[host].split(")\n", 1)[1]
								calling_data_dictionary[host] = one + ",callback_params=json.dumps(" + logs_Data.get('data')[
									cursor].get('callback_params').replace('@appanalytics.in','@gmail.com') + "),partner_params=json.dumps(" + \
														   logs_Data.get('data')[cursor].get(
															   'partner_params').replace('@appanalytics.in','@gmail.com') + "))\n" + two
							elif logs_Data.get('data')[cursor].get('callback_params'):
								# print calling_data_dictionary[host]
								one = calling_data_dictionary[host].split(")\n", 1)[0]
								# print one
								two = calling_data_dictionary[host].split(")\n", 1)[1]
								calling_data_dictionary[host] = one + ",callback_params=json.dumps(" + logs_Data.get('data')[
									cursor].get('callback_params').replace('@appanalytics.in','@gmail.com') + "),partner_params=None)\n" + two
							elif logs_Data.get('data')[cursor].get('partner_params'):
								one = calling_data_dictionary[host].split(")\n", 1)[0]
								two = calling_data_dictionary[host].split(")\n", 1)[1]
								calling_data_dictionary[host] = one + ",callback_params=None,partner_params=json.dumps(" + \
														   logs_Data.get('data')[cursor].get(
															   'partner_params').replace('@appanalytics.in','@gmail.com') + "))\n" + two

						else:
							host = logs_Data.get('hosts')[cursor] + '/' + \
								   (logs_Data.get('urls')[cursor]).rsplit('/', 1)[1]

					elif logs_Data.get('data')[cursor].get('event_token'):
						host = logs_Data.get('hosts')[cursor] + '/' + (logs_Data.get('urls')[cursor]).rsplit('/', 1)[
							1] + '->' + logs_Data.get('data')[cursor].get('event_token')

					else:
						if 'app.adjust.com' in logs_Data.get('urls')[cursor]:
							host = logs_Data.get('urls')[cursor].split('//')[1]
							if "session" in host and sflag:
								if logs_Data.get('data')[cursor].get('callback_params') and logs_Data.get('data')[
									cursor].get('partner_params'):
									one = calling_data_dictionary[host].split(")\n	")[0]
									two = calling_data_dictionary[host].split(")\n	")[1]
									calling_data_dictionary[host] = one + ",callback_params=json.dumps(" + \
															   logs_Data.get('data')[cursor].get(
																   'callback_params').replace('@appanalytics.in','@gmail.com') + "),partner_params=json.dumps(" + \
															   logs_Data.get('data')[cursor].get(
																   'partner_params').replace('@appanalytics.in','@gmail.com') + "))\n	" + two
								elif logs_Data.get('data')[cursor].get('callback_params'):
									one = calling_data_dictionary[host].split(")\n	")[0]
									two = calling_data_dictionary[host].split(")\n	")[1]
									calling_data_dictionary[host] = one + ",callback_params=json.dumps(" + \
															   logs_Data.get('data')[cursor].get(
																   'callback_params').replace('@appanalytics.in','@gmail.com') + "),partner_params=None)\n	" + two
								elif logs_Data.get('data')[cursor].get('partner_params'):
									one = calling_data_dictionary[host].split(")\n	")[0]
									two = calling_data_dictionary[host].split(")\n	")[1]
									calling_data_dictionary[
										host] = one + ",callback_params=None,partner_params=json.dumps(" + \
												logs_Data.get('data')[cursor].get('partner_params').replace('@appanalytics.in','@gmail.com') + "))\n	" + two
								sflag = False

					filtered_logs_data.get('host').append(host)

			######################## AD-BRIX CALLING DATA FORMATTING ########################

			elif logs_Data.get('urls')[
				cursor] == "https://tracking.ad-brix.com/v1/tracking" and isfirsttrackingevent == True:
				filtered_logs_data.get('host').append('first->tracking.ad-brix.com')
				isfirsttrackingevent = False

			elif logs_Data.get('urls')[cursor] == "https://cvr.ad-brix.com/v1/conversion/GetReferral":
				filtered_logs_data.get('host').append('cvr.ad-brix.com/v1/conversion/GetReferral')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/login":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/login')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/save":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/save')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/updateRegistration":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/updateRegistration')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/updateConversion":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/updateConversion')
			elif logs_Data.get('urls')[cursor] == "https://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule":
				filtered_logs_data.get('host').append('campaign.ad-brix.com/v1/CampaignVer2/GetSchedule')
			elif logs_Data.get('urls')[cursor] == "https://tracking.ad-brix.com/v1/tracking/SetUserDemographic":
				filtered_logs_data.get('host').append('tracking.ad-brix.com/v1/tracking/SetUserDemographic')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/getPopups":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/getPopups')
			elif logs_Data.get('urls')[cursor] == "https://as.ad-brix.com/v1/users/enablePushService":
				filtered_logs_data.get('host').append('as.ad-brix.com/v1/users/enablePushService')
			elif logs_Data.get('urls')[cursor] == "https://apiab4c.ad-brix.com/v1/event":
				filtered_logs_data.get('host').append('apiab4c.ad-brix.com/v1/event')

			elif "https://event.adbrix.io/api/v1/deferred-deeplink/" in logs_Data.get('urls')[cursor]:
				filtered_logs_data.get('host').append('event.adbrix.io/api/v1/deferred-deeplink')

			######################## MAT CALLING DATA FORMATTING ########################
			elif "engine.mobileapptracking.com/serve" in logs_Data.get('urls')[cursor] and logs_Data.get('params')[cursor].get('action')=="session":
				filtered_logs_data.get('host').append('engine.mobileapptracking.com/serve')

			######################## Apsalar Calling Data Formatting ####################

			elif logs_Data.get('urls')[cursor] == "https://e-ssl.apsalar.com/api/v1/resolve" and logs_Data.get('params')[cursor].get('k') == 'Android':
				filtered_logs_data.get('host').append('e-ssl.apsalar.com/api/v1/resolve')
			elif logs_Data.get('urls')[cursor] == "https://e.apsalar.com/api/v1/resolve" and logs_Data.get('params')[cursor].get('k') == 'Android':
				filtered_logs_data.get('host').append('e.apsalar.com/api/v1/resolve')
			elif logs_Data.get('urls')[cursor] == "https://e-ssl.apsalar.com/api/v1/start":
				filtered_logs_data.get('host').append('e-ssl.apsalar.com/api/v1/start')
			elif logs_Data.get('urls')[cursor] == "https://e.apsalar.com/api/v1/start":
				filtered_logs_data.get('host').append('e.apsalar.com/api/v1/start')

			######################## KOCHAVA Calling Data Formatting ####################

			elif logs_Data.get('urls')[cursor] == "https://kvinit-prod.api.kochava.com/track/kvinit":
				filtered_logs_data.get('host').append('kvinit-prod.api.kochava.com/track/kvinit')
			elif logs_Data.get('urls')[cursor] == "https://control.kochava.com/track/kvquery":
				filtered_logs_data.get('host').append('control.kochava.com/track/kvquery')
			elif logs_Data.get('urls')[cursor] == "https://control.kochava.com/track/kvinit":
				filtered_logs_data.get('host').append('control.kochava.com/track/kvinit')

			elif logs_Data.get('urls')[cursor] == "https://control.kochava.com/track/kvTracker.php" and not type(logs_Data.get('data')[cursor])==list:
				if logs_Data.get('data')[cursor].get('action')=="identityLink":
					filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->identityLink')
				elif logs_Data.get('data')[cursor].get('action')=="update":
					filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->update')
				elif logs_Data.get('data')[cursor].get('action')=="initial":
					filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->initial')

			elif logs_Data.get('urls')[cursor] == "https://control.kochava.com/track/json" and not type(logs_Data.get('data')[cursor])==list:
				if logs_Data.get('data')[cursor].get('action')=="identityLink":
					filtered_logs_data.get('host').append('control.kochava.com/track/json->identityLink')
				elif logs_Data.get('data')[cursor].get('action')=="update":
					filtered_logs_data.get('host').append('control.kochava.com/track/json->update')
				elif logs_Data.get('data')[cursor].get('action')=="initial":
					filtered_logs_data.get('host').append('control.kochava.com/track/json->initial')

			################################ Adforce calling Data Formatting #################

			elif logs_Data.get('urls')[cursor]=='https://app-adforce.jp/ad/p/cv':
				if logs_Data.get('params')[cursor].get('_price') or logs_Data.get('params')[cursor].get('_buid'):
					filtered_logs_data.get('host').append('app-adforce.jp/ad/p/cv->short')
				else:
					filtered_logs_data.get('host').append('app-adforce.jp/ad/p/cv->cv')

			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/p/tmck":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/p/tmck')
			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/view/collect.html":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/view/collect.html')
			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/view/collect_ios.html":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/view/collect_ios.html')
			elif logs_Data.get('urls')[cursor] == "https://analytics.app-adforce.jp/fax/analytics":
				filtered_logs_data.get('host').append('analytics.app-adforce.jp/fax/analytics')
			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/i/fprc":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/i/fprc')
			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/p/fp":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/p/fp')
			elif logs_Data.get('urls')[cursor] == "https://app-adforce.jp/ad/p/jump":
				filtered_logs_data.get('host').append('app-adforce.jp/ad/p/jump')


			######################## Tenjin Calling Data Formatting ###########################

			elif logs_Data.get('urls')[cursor] == "https://track.tenjin.io/v0/user":
				filtered_logs_data.get('host').append('track.tenjin.io/v0/user')


			######################## BRANCH Calling Data Formatting ###########################

			elif logs_Data.get('urls')[cursor] == "https://api2.branch.io/v1/install":
				filtered_logs_data.get('host').append('api2.branch.io/v1/install')
			elif logs_Data.get('urls')[cursor] == "https://api2.branch.io/v1/close":
				filtered_logs_data.get('host').append('api2.branch.io/v1/close')
			elif logs_Data.get('urls')[cursor] == "https://api2.branch.io/v1/open":
				filtered_logs_data.get('host').append('api2.branch.io/v1/open')
			elif logs_Data.get('urls')[cursor] == "https://api2.branch.io/v1/profile":
				filtered_logs_data.get('host').append('api2.branch.io/v1/profile')
			elif logs_Data.get('urls')[cursor] == "https://api2.branch.io/v1/cpid/latd":
				filtered_logs_data.get('host').append('api2.branch.io/v1/cpid/latd')
			elif logs_Data.get('urls')[cursor] == "https://api.branch.io/v1/install":
				filtered_logs_data.get('host').append('api.branch.io/v1/install')
			elif logs_Data.get('urls')[cursor] == "https://api.branch.io/v1/close":
				filtered_logs_data.get('host').append('api.branch.io/v1/close')
			elif logs_Data.get('urls')[cursor] == "https://api.branch.io/v1/open":
				filtered_logs_data.get('host').append('api.branch.io/v1/open')
			elif logs_Data.get('urls')[cursor] == "https://api.branch.io/v1/profile":
				filtered_logs_data.get('host').append('api.branch.io/v1/profile')
			elif logs_Data.get('urls')[cursor] == "https://api.branch.io/v1/cpid/latd":
				filtered_logs_data.get('host').append('api.branch.io/v1/cpid/latd')
			elif logs_Data.get('urls')[cursor] == "https://cdn.branch.io/sdk/uriskiplist_v1.json":
				filtered_logs_data.get('host').append('cdn.branch.io/sdk/uriskiplist_v1.json')
			elif logs_Data.get('urls')[cursor] == "https://cdn.branch.io/sdk/uriskiplist_v2.json":
				filtered_logs_data.get('host').append('cdn.branch.io/sdk/uriskiplist_v2.json')

			######################## AD-BRIX EVENT NAME DEFINITION #######################

			elif "https://event.adbrix.io/api/v1/event/single/" in logs_Data.get('urls')[cursor]:
				event_counter += 1
				if key:
					K = key.split("\n")[0].strip()
					IV = key.split("\n")[1].strip()
					ddata = decrypt_data(logs_Data.get('data')[cursor].keys()[0].strip(), K, IV,"referral",log=False)
					ddata = eval(
													 (ddata.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"',
																																							"sfalse").replace(
															"true", "True").replace(
															"null", "None").replace("false", "False").replace("strue",
																																			'"true"').replace(
															"sfalse", '"false"'))

					filtered_logs_data.get('host').append('event.adbrix.io/api/v1/event/single/')
					calling_data_dictionary['event.adbrix.io/api/v1/event/single/'] = """		\n	print """+repr("\n")+"""+'AD BRIX event single'\n	request = adbrix_event_single(campaign_data, app_data, device_data, event_name='"""+ddata.get('evt').get('event_name')+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n	try:\n		app_data['abxusr']=resp.get('res').headers.get('Set-Cookie').split('abxusr=')[1].split(';')[0]\n		print app_data['abxusr']\n	except:\n		app_data['abxusr']='c%3A5f188e9e-38b8-412d-9ffd-2e0b83a154aa'\n"""
				else:
					filtered_logs_data.get('host').append('event.adbrix.io/api/v1/event/single/')
					calling_data_dictionary['event.adbrix.io/api/v1/event/single/'] = ""

			elif "https://event.adbrix.io/api/v1/event/bulk/" in logs_Data.get('urls')[cursor]:
				if key:
					K = key.split("\n")[0].strip()
					IV = key.split("\n")[1].strip()
					ddata = decrypt_data(logs_Data.get('data')[cursor].keys()[0].strip(), K, IV, log=False)
					ddata = eval(
													 (ddata.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"',
																																							"sfalse").replace(
															"true", "True").replace(
															"null", "None").replace("false", "False").replace("strue",
																																			'"true"').replace(
															"sfalse", '"false"'))
					if len(ddata.get('evts'))>1:
						flag = False
						for evt in ddata.get('evts'):
							event_counter += 1
							calling_data_dictionary['event.adbrix.io/api/v1/event/bulk/->'+evt.get('event_name')+'->'+evt.get('group')] = """		\n	print """+repr("\n")+"""+'AD BRIX event bulk Login_facebook'\n	request = adbrix_event_bulk(campaign_data, app_data, device_data,eventName='"""+evt.get('event_name')+"""',eventValue="""+str(evt.get('param'))+""",group='"""+evt.get('group')+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
							filtered_logs_data.get('host').append('event.adbrix.io/api/v1/event/bulk/->'+evt.get('event_name')+'->'+evt.get('group'))
							filtered_logs_data.get('method').append(logs_Data.get('methods')[cursor])
							filtered_logs_data.get('url').append(logs_Data.get('urls')[cursor])
							filtered_logs_data.get('headers').append(logs_Data.get('headers')[cursor])
							filtered_logs_data.get('params').append(logs_Data.get('params')[cursor])
							filtered_logs_data.get('data').append(logs_Data.get('data')[cursor])
					else:
						calling_data_dictionary['event.adbrix.io/api/v1/event/bulk/->'+ddata.get('evts')[0].get('event_name')+'->'+ddata.get('evts')[0].get('group')] = """		\n	print """+repr("\n")+"""+'AD BRIX event bulk Login_facebook'\n	request = adbrix_event_bulk(campaign_data, app_data, device_data,eventName='"""+ddata.get('evts')[0].get('event_name')+"""',eventValue="""+str(ddata.get('evts')[0].get('param'))+""",group='"""+ddata.get('evts')[0].get('group')+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
						filtered_logs_data.get('host').append('event.adbrix.io/api/v1/event/bulk/->'+ddata.get('evts')[0].get('event_name')+'->'+evt.get('group'))

				else:
					filtered_logs_data.get('host').append('event.adbrix.io/api/v1/event/bulk/')
					calling_data_dictionary['event.adbrix.io/api/v1/event/bulk/'] = ""

			elif logs_Data.get('urls')[cursor] == "https://tracking.ad-brix.com/v1/tracking" and isfirsttrackingevent == False:
				event_counter += 1
				if '{' in logs_Data.get('data')[cursor].get('j'):
					tracking_data = logs_Data.get('data')[cursor].get('j')
				else:
					tracking_data = decrypt_data(logs_Data.get('data')[cursor].get('j'), log=False)
				tracking_group = ""
				tracking_param = ""
				tracking_event = ""
				if '"activity":' in tracking_data:
					act_info_list = eval(
						(tracking_data.rsplit('}', 1)[0] + '}').replace('"true"', "strue").replace('"false"',
																								   "sfalse").replace(
							"true", "True").replace("false", "False").replace("strue", '"true"').replace("sfalse",
																										 '"false"')).get(
						'activity_info')
					# print act_info_list
					if len(act_info_list) > 1:
						for act in act_info_list:
							flag = False
							tracking_event = act.get('activity')
							tracking_group = act.get('group')
							tracking_param = act.get('param')
							calling_data_dictionary[
								'tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group] = """		\n	print """ + repr(
								"\n") + """+"ad-brix event """ + tracking_event + """"\n	tracking_event=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='""" + tracking_group + """',param='""" + tracking_param + """',activity='""" + tracking_event + """')\n	resp = util.execute_request(**tracking_event)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, tracking_event, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
							filtered_logs_data.get('host').append('tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group)
							filtered_logs_data.get('method').append(logs_Data.get('methods')[cursor])
							filtered_logs_data.get('url').append(logs_Data.get('urls')[cursor])
							filtered_logs_data.get('headers').append(logs_Data.get('headers')[cursor])
							filtered_logs_data.get('params').append(logs_Data.get('params')[cursor])
							filtered_logs_data.get('data').append(logs_Data.get('data')[cursor])

					else:
						tracking_event = act_info_list[0].get('activity')
						tracking_group = act_info_list[0].get('group')
						tracking_param = act_info_list[0].get('param')
						calling_data_dictionary[
							'tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group] = """		\n	print """ + repr(
							"\n") + """+"ad-brix event """ + tracking_event + """"\n	tracking_event=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='""" + tracking_group + """',param='""" + tracking_param + """',activity='""" + tracking_event + """')\n	resp = util.execute_request(**tracking_event)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, tracking_event, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
						filtered_logs_data.get('host').append('tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group)
				else:
					tracking_event = 'ad_brix_tracking_event_' + str(event_counter)  ############RED
					calling_data_dictionary[
						'tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group] = """		\n	print """ + repr(
						"\n") + """+"ad-brix event """ + tracking_event + """"\n	tracking_event=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='""" + tracking_group + """',param='""" + tracking_param + """',activity='""" + tracking_event + """')\n	resp = util.execute_request(**tracking_event)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, tracking_event, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
					filtered_logs_data.get('host').append('tracking.ad-brix.com/v1/tracking/' + tracking_event+'->'+tracking_group)

			######################## APPSFLYER EVENT NAME DEFINITION #####################

			if logs_Data.get('hosts')[cursor] == 'events.appsflyer.com':
				event_counter += 1
				try:
					eventName = logs_Data.get('data')[cursor].get('eventName').strip(' ')
				except:
					try:
						eventName = logs_Data.get('data')[cursor].get('eventName').strip(' ').encode('utf-8')
					except:
						eventName = 'demo_event_' + str(event_counter)
				try:
					eventValue = json.dumps(logs_Data.get('data')[cursor].get('eventValue'), ensure_ascii=False)
				except:
					eventValue = logs_Data.get('data')[cursor].get('eventValue')
				filtered_logs_data.get('host').append('events.appsflyer.com/' + eventName)
				calling_data_dictionary['events.appsflyer.com/' + eventName] = "\n	" + re.sub(r'\W+', '',
																							eventName).lower() + "(campaign_data, app_data, device_data)\n"
				event_list.append(eventName)
				eventData_List.append(eventValue.replace('@appanalytics.in','@gmail.com'))

			######################### ADJUST EVENT NAME DEFINITION #######################

			if logs_Data.get('hosts')[cursor] == 'app.adjust.com' and 'event' in logs_Data.get('urls')[cursor]:
				event_counter += 1
				eventToken = logs_Data.get('data')[cursor].get('event_token')
				calling_data_dictionary[
					'app.adjust.com/event->' + eventToken] = "\n	adjust_token_" + eventToken + "(campaign_data, app_data, device_data,e1,e2)\n"
				event_list.append('adjust-->' + str(eventToken))
				if logs_Data.get('data')[cursor].get('callback_params') and logs_Data.get('data')[cursor].get(
						'partner_params'):
					eventData_List.append([logs_Data.get('data')[cursor].get('callback_params').replace('@appanalytics.in','@gmail.com'),
										   logs_Data.get('data')[cursor].get('partner_params')])
				elif logs_Data.get('data')[cursor].get('callback_params'):
					eventData_List.append([logs_Data.get('data')[cursor].get('callback_params').replace('@appanalytics.in','@gmail.com'), None])
				elif logs_Data.get('data')[cursor].get('partner_params'):
					eventData_List.append([None, logs_Data.get('data')[cursor].get('partner_params').replace('@appanalytics.in','@gmail.com')])
				else:
					eventData_List.append(None)

			######################### MAT EVENT NAME DEFINITION #######################
			if "engine.mobileapptracking.com" in logs_Data.get('hosts')[cursor] and logs_Data.get('params')[cursor].get('action')=="conversion":
				event_counter += 1
				conversion_name = logs_Data.get('params')[cursor].get('site_event_name')
				calling_data_dictionary[
					'engine.mobileapptracking.com/serve->' + conversion_name] = """		\n	print """ + repr(
			"\n") + """+"-----------------------------MAT----"""+conversion_name+"""---------------------------------"\n	request = mat_serve(campaign_data, app_data, device_data,action="conversion:"""+conversion_name+"""")\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
				filtered_logs_data.get('host').append('engine.mobileapptracking.com/serve->' + conversion_name)

			if "engine.mobileapptracking.com" in logs_Data.get('hosts')[cursor] and logs_Data.get('params')[cursor].get('action')=="install":
				calling_data_dictionary[
					'engine.mobileapptracking.com/serve->install'] = """		\n	print """ + repr(
			"\n") + """+"-----------------------------MAT_INSTALL-------------------------------------"\n	request = mat_serve(campaign_data, app_data, device_data,action="install")\n	response = resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
				filtered_logs_data.get('host').append('engine.mobileapptracking.com/serve->install')

			######################## Apsalar Event Name Definition #####################

			if logs_Data.get('hosts')[cursor] == 'e-ssl.apsalar.com':
				if 'event' in logs_Data.get('urls')[cursor]:
					event_counter += 1
					if logs_Data.get('params')[cursor].get('custom_user_id'):
						custom_user_id =", custom_user_id=True)"
					else:
						custom_user_id =")"
					eventName = logs_Data.get('params')[cursor].get('n').replace(" ","_")
					calling_data_dictionary[
						'e-ssl.apsalar.com/api/v1/event->' + eventName] = "\n	apsalar_event_" + eventName + "(campaign_data, app_data, device_data"+custom_user_id+"\n"
					event_list.append('apsalar-->' + str(eventName))
					if logs_Data.get('params')[cursor].get('e'):
						eventData_List.append(logs_Data.get('params')[cursor].get('e').replace('@appanalytics.in','@gmail.com'))
					else:
						eventData_List.append("") 
					filtered_logs_data.get('host').append('e-ssl.apsalar.com/api/v1/event->' + eventName)

				elif "resolve" in logs_Data.get('urls')[cursor]:
					if platform=="ios":
						calling_data_dictionary['e-ssl.apsalar.com/api/v1/resolve->'+logs_Data.get('params')[cursor].get('k')] = """		\n	print """ + repr(
				"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data, ifu='"""+logs_Data.get('params')[cursor].get('k')+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""" 

						filtered_logs_data.get('host').append('e-ssl.apsalar.com/api/v1/resolve->'+logs_Data.get('params')[cursor].get('k'))
					else:
						calling_data_dictionary['e-ssl.apsalar.com/api/v1/resolve'] = """		\n	print """ + repr(
				"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""" 

						filtered_logs_data.get('host').append('e-ssl.apsalar.com/api/v1/resolve')


			if logs_Data.get('hosts')[cursor] == 'e.apsalar.com':
				if 'event' in logs_Data.get('urls')[cursor]:
					event_counter += 1
					if logs_Data.get('params')[cursor].get('custom_user_id'):
						custom_user_id =", custom_user_id=True)"
					else:
						custom_user_id =")"
					eventName = logs_Data.get('params')[cursor].get('n')
					calling_data_dictionary[
						'e.apsalar.com/api/v1/event->' + eventName] = "\n	apsalar_event_" + eventName.replace(" ","_") + "(campaign_data, app_data, device_data"+custom_user_id+"\n"
					event_list.append('apsalar-->' + str(eventName))
					if logs_Data.get('params')[cursor].get('e'):
						eventData_List.append(logs_Data.get('params')[cursor].get('e').replace('@appanalytics.in','@gmail.com'))
					else:
						eventData_List.append("")

					filtered_logs_data.get('host').append('e.apsalar.com/api/v1/event->' + eventName)

				elif "resolve" in logs_Data.get('urls')[cursor]:
					if platform=="ios":
						calling_data_dictionary['e.apsalar.com/api/v1/resolve->'+logs_Data.get('params')[cursor].get('k')] = """		\n	print """ + repr(
				"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data, ifu='"""+logs_Data.get('params')[cursor].get('k')+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""" 
						
						filtered_logs_data.get('host').append('e.apsalar.com/api/v1/resolve->'+logs_Data.get('params')[cursor].get('k'))
					else:
						calling_data_dictionary['e.apsalar.com/api/v1/resolve'] = """		\n	print """ + repr(
			"\n") + """+"-----------------------------APSALAR Resolve---------------------------------"\n	request = apsalarResolve(campaign_data, app_data, device_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n""" 
					
					filtered_logs_data.get('host').append('e.apsalar.com/api/v1/resolve')

			######################### KOCHAVA EVENT NAME DEFINITION #######################
			if "control.kochava.com" in logs_Data.get('hosts')[cursor]:
				if "control.kochava.com/track/json" in logs_Data.get('urls')[cursor]:
					#print type(logs_Data.get('data')[cursor])
					#print logs_Data.get('urls')[cursor]
					#print logs_Data.get('data')[cursor]
					#print logs_Data.get('data')[cursor].get('action')
					#print logs_Data.get('data')[cursor].get('data').get('action')
					if type(logs_Data.get('data')[cursor])==list:
						#print "if condition"
						for actions in logs_Data.get('data')[cursor]:
							action = actions.get("action")
							#print action
							if action=="event":
								event_counter += 1
								event_name = actions.get('data').get('event_name')
								event_value = actions.get('data').get('event_data')
								calling_data_dictionary[
									'control.kochava.com/track/json->' + event_name] = """		\n	print """ + repr(
							"\n") + """+"-----------------------------KOCHAVA EVENT----"""+event_name.strip()+"""---------------------------------"\n	request=kochava_event(campaign_data, app_data, device_data, event_name='"""+event_name+"""', event_data="""+str(event_value).replace('@appanalytics.in','@gmail.com')+""")\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
								flag = False
								filtered_logs_data.get('host').append('control.kochava.com/track/json->' + event_name)

							elif action=="session":
								flag = False
								state = actions.get('data').get('state')
								calling_data_dictionary[
									'control.kochava.com/track/json->' + state] = """		\n	print """ + repr(
							"\n") + """+"-----------------------------KOCHAVA session----"""+state+"""---------------------------------"\n	request=kochava_session(campaign_data, app_data, device_data, state='"""+state+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

								filtered_logs_data.get('host').append('control.kochava.com/track/json->' + state)

							elif action=="install":
								flag = False
								calling_data_dictionary[
									'control.kochava.com/track/json->' + action] = """		\n	print """ + repr(
							"\n") + """+"-----------------------------KOCHAVA session----"""+action+"""---------------------------------"\n	request=kochava_session(campaign_data, app_data, device_data, state='"""+action+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

								filtered_logs_data.get('host').append('control.kochava.com/track/json->' + action)

							if flag==False:
								filtered_logs_data.get('method').append(logs_Data.get('methods')[cursor])
								filtered_logs_data.get('url').append(logs_Data.get('urls')[cursor])
								filtered_logs_data.get('headers').append(logs_Data.get('headers')[cursor])
								filtered_logs_data.get('params').append(logs_Data.get('params')[cursor])
								filtered_logs_data.get('data').append(logs_Data.get('data')[cursor])

					elif logs_Data.get('data')[cursor].get('action')=="session":
						state = logs_Data.get('data')[cursor].get('data').get('state')
						calling_data_dictionary[
							'control.kochava.com/track/json->' + state] = """		\n	print """ + repr(
					"\n") + """+"-----------------------------KOCHAVA session----"""+state+"""---------------------------------"\n	request=kochava_session(campaign_data, app_data, device_data, state='"""+state+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

						filtered_logs_data.get('host').append('control.kochava.com/track/json->' + state)

						# print "elif 1 condition"

					elif logs_Data.get('data')[cursor].get('action')=="event":
						print logs_Data.get('data')[cursor].get('data').get('action')
						event_counter += 1
						event_name = logs_Data.get('data')[cursor].get('data').get('event_name')
						event_value = logs_Data.get('data')[cursor].get('data').get('event_data')
						calling_data_dictionary[
							'control.kochava.com/track/json->' + event_name] = """		\n	print """ + repr(
					"\n") + """+"-----------------------------KOCHAVA EVENT----"""+event_name.strip()+"""---------------------------------"\n	request=kochava_event(campaign_data, app_data, device_data, event_name='"""+event_name+"""', event_data="""+str(event_value).replace('@appanalytics.in','@gmail.com')+""")\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
						filtered_logs_data.get('host').append('control.kochava.com/track/json->' + event_name)
						# print "elif 2 condition"

					elif logs_Data.get('data')[cursor].get('action')=="install":
						state = logs_Data.get('data')[cursor].get('action')
						calling_data_dictionary[
							'control.kochava.com/track/json->' + state] = """		\n	print """ + repr(
					"\n") + """+"-----------------------------KOCHAVA ----"""+state+"""---------------------------------"\n	request=kochava_session(campaign_data, app_data, device_data, state='"""+state+"""')\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

						filtered_logs_data.get('host').append('control.kochava.com/track/json->' + state)
						# print "elif 3 condition"

				elif "control.kochava.com/track/kvTracker.php" in logs_Data.get('urls')[cursor]:

					if type(logs_Data.get('data')[cursor])==list:
						for actions in logs_Data.get('data')[cursor]:
							action = actions.get("action")

							if action=="event":
								event_counter += 1
								event_name = actions.get('data').get('event_name')
								event_value = actions.get('data').get('event_data')
								calling_data_dictionary[
									'control.kochava.com/track/kvTracker.php->' + event_name] = """		\n	print """ + repr(
							"\n") + """+"-----------------------------KOCHAVA EVENT----"""+event_name.strip()+"""---------------------------------"\n	request=kochava_tracker_event( campaign_data, device_data, app_data, event_name='"""+event_name+"""', event_value="""+str(event_value).replace('@appanalytics.in','@gmail.com')+""" )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
								flag = False
								filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->' + event_name)

							elif action=="session":
								flag = False
								state = actions.get('data').get('state')
								calling_data_dictionary[
									'control.kochava.com/track/kvTracker->' + state] = """		\n	request=kochava_tracker_session( campaign_data, device_data, app_data, state = '"""+state+"""' )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

								filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker->' + state)

							elif action=="initial":
								flag = False
								calling_data_dictionary[
									'control.kochava.com/track/kvTracker.php->' + action] = """		\n	request=kochava_tracker_session( campaign_data, device_data, app_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

								filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->' + action)

							if flag==False:
								filtered_logs_data.get('method').append(logs_Data.get('methods')[cursor])
								filtered_logs_data.get('url').append(logs_Data.get('urls')[cursor])
								filtered_logs_data.get('headers').append(logs_Data.get('headers')[cursor])
								filtered_logs_data.get('params').append(logs_Data.get('params')[cursor])
								filtered_logs_data.get('data').append(logs_Data.get('data')[cursor])

					elif logs_Data.get('data')[cursor].get('action')=="session":
						state = logs_Data.get('data')[cursor].get('data').get('state')
						calling_data_dictionary['control.kochava.com/track/kvTracker.php->' + state] = """		\n	request=kochava_tracker_session( campaign_data, device_data, app_data, state = '"""+state+"""' )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""

						filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->' + state)

					elif logs_Data.get('data')[cursor].get('action')=="event":
						event_counter += 1
						event_name = logs_Data.get('data')[cursor].get('data').get('event_name')
						event_value = logs_Data.get('data')[cursor].get('data').get('event_data')
						calling_data_dictionary['control.kochava.com/track/kvTracker.php->' + event_name] = """		\n	print """ + repr(
					"\n") + """+"-----------------------------KOCHAVA EVENT----"""+event_name.strip()+"""---------------------------------"\n	request=kochava_tracker_event( campaign_data, device_data, app_data, event_name='"""+event_name+"""', event_value="""+str(event_value).replace('@appanalytics.in','@gmail.com')+""" )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
						filtered_logs_data.get('host').append('control.kochava.com/track/kvTracker.php->' + event_name)


			################### Tenjin Event Name Definition ################

			if logs_Data.get('hosts')[cursor] == 'track.tenjin.io' and not logs_Data.get('urls')[cursor] == "https://track.tenjin.io/v0/user":
				event_counter += 1
				if logs_Data.get('data')[cursor].get('event'):
					eventName = logs_Data.get('data')[cursor].get('event')
							
					calling_data_dictionary['track.tenjin.io/v0/event' + eventName] = """		\n	print """ + repr(
						"\n") + """+"------------------------ Tenjin Event -------------------------"\n	request=tenjin_event( campaign_data, device_data, app_data, event_name='"""+eventName+""" )\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""
				else:
					calling_data_dictionary['track.tenjin.io/v0/event'] = """		\n	print """ + repr(
						"\n") + """+"------------------------ Tenjin Event -------------------------"\n	request=tenjin_event( campaign_data, device_data, app_data)\n	resp = util.execute_request(**request)\n	try:\n		if not resp.get('res').status_code in [200, 201, 204]:\n			resp = saveAttributionCallStatus(campaign_data, device_data, app_data, request, resp)\n	except:\n		print 'Could not save attributionCallStatus data'\n"""


				filtered_logs_data.get('host').append('track.tenjin.io/v0/event')


			######################## BRANCH EVENT NAME DEFINITION #####################

			if logs_Data.get('hosts')[cursor] == 'api2.branch.io' and 'standard' in logs_Data.get('urls')[cursor]:
				event_counter += 1
				try:
					eventName = logs_Data.get('data')[cursor].get('name').strip(' ')
				except:
					try:
						eventName = logs_Data.get('data')[cursor].get('name').strip(' ').encode('utf-8')
					except:
						eventName = 'demo_event_' + str(event_counter)

				if logs_Data.get('data')[cursor].get('event_data'):
					
					eventValue = logs_Data.get('data')[cursor].get('event_data')
					
				else:
					eventValue=None
				filtered_logs_data.get('host').append('api2.branch.io/v2/event/standard/' + eventName)
				calling_data_dictionary['api2.branch.io/v2/event/standard/' + eventName] = "\n	" + re.sub(r'\W+', '',
																							eventName).lower() + "(campaign_data, app_data, device_data)\n"
				event_list.append('branch_standard-->' + str(eventName))
				if logs_Data.get('data')[cursor].get('event_data'):
					eventData_List.append(str(eventValue).replace('@appanalytics.in','@gmail.com'))
				else:
					eventData_List.append(None)

			if logs_Data.get('hosts')[cursor] == 'api2.branch.io' and 'custom' in logs_Data.get('urls')[cursor]:
				event_counter += 1
				try:
					eventName = logs_Data.get('data')[cursor].get('name').strip(' ')
				except:
					try:
						eventName = logs_Data.get('data')[cursor].get('name').strip(' ').encode('utf-8')
					except:
						eventName = 'demo_event_' + str(event_counter)

				if logs_Data.get('data')[cursor].get('custom_data'):			
					eventValue = logs_Data.get('data')[cursor].get('custom_data')
				else:
					eventValue = None
				filtered_logs_data.get('host').append('api2.branch.io/v2/event/custom/' + eventName)
				calling_data_dictionary['api2.branch.io/v2/event/custom/' + eventName] = "\n	" + re.sub(r'\W+', '',
																							eventName).lower() + "(campaign_data, app_data, device_data)\n"
				event_list.append('branch_custom-->' + str(eventName))
				if logs_Data.get('data')[cursor].get('custom_data'):
					eventData_List.append(str(eventValue).replace('@appanalytics.in','@gmail.com'))
				else:
					eventData_List.append(None
						)
				

			######################## 		END 		#####################

			if flag == True:
				filtered_logs_data.get('call_sequence').append(logs_Data.get('seq')[cursor])
				filtered_logs_data.get('method').append(logs_Data.get('methods')[cursor])
				filtered_logs_data.get('url').append(logs_Data.get('urls')[cursor])
				filtered_logs_data.get('headers').append(logs_Data.get('headers')[cursor])
				filtered_logs_data.get('params').append(logs_Data.get('params')[cursor])
				filtered_logs_data.get('data').append(logs_Data.get('data')[cursor])
			# print filtered_logs_data.get('url')
			# print filtered_logs_data.get('host')

	"""* 
		>>>>CHECK TO CONFIRM THAT DATA IS SYNCHRONISED<<<<
	*"""

	
	if len(filtered_logs_data.get('host')) == len(filtered_logs_data.get('data')):
		filtered_logs_data['platform'] = platform
		# print filtered_logs_data.get('data')
		# exit()
		return filtered_logs_data, calling_data_dictionary, event_list, eventData_List
	else:
		"""* 
			>>>>IF IN CASE THEIR IS SOME MIS-MATCH IN SEQUENCE OF DATA/HOST/PARAMS/URL WHILE APPENDING CALL DATA, THE FOLLOWING ERROR WILL RAISE<<<<
		*"""
		# print filtered_logs_data.get('url')
		# print len(filtered_logs_data.get('url'))
		# print filtered_logs_data.get('data')
		# print len(filtered_logs_data.get('data'))
		# print filtered_logs_data.get('host')
		# print len(filtered_logs_data.get('host'))
		raise Exception("ERROR FILTER ASYNCHRONISED")
		print "ERROR FILTER ASYNCHRONISED"
		return False
